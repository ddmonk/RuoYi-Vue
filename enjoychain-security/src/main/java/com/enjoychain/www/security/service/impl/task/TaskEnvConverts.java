package com.enjoychain.www.security.service.impl.task;

import com.enjoychain.www.security.entity.*;

import java.util.ArrayList;
import java.util.List;

public class TaskEnvConverts {


    /**
     * 将现有的 {@link CorpEnvBasicInfo} 转化为单独服务该任务的 {@link TaskCorpEnvBasicInfo}
     * @param {@link CorpEnvBasicInfo} 公司基础信息内容
     * @param taskId
     * @return {@link TaskCorpEnvBasicInfo }
     */
    public static TaskCorpEnvBasicInfo convertEnvBasicInfoToTaskBasicInfo(CorpEnvBasicInfo corpEnvBasicInfo, String taskId) {
        TaskCorpEnvBasicInfo taskCorpEnvBasicInfo = new TaskCorpEnvBasicInfo();
        taskCorpEnvBasicInfo.setCorpAddress(corpEnvBasicInfo.getCorpAddress());
        taskCorpEnvBasicInfo.setCorpName(corpEnvBasicInfo.getCorpName());
        taskCorpEnvBasicInfo.setCertNo(corpEnvBasicInfo.getCertNo());
        taskCorpEnvBasicInfo.setCorpId(corpEnvBasicInfo.getId());
        taskCorpEnvBasicInfo.setBusiType(corpEnvBasicInfo.getBusiType());
        taskCorpEnvBasicInfo.setTaskId(taskId);
        taskCorpEnvBasicInfo.setLinkName(corpEnvBasicInfo.getLinkName());
        taskCorpEnvBasicInfo.setLinkTel(corpEnvBasicInfo.getLinkTel());
        taskCorpEnvBasicInfo.setProductProcess(corpEnvBasicInfo.getProductProcess());
        taskCorpEnvBasicInfo.setReportStatus(corpEnvBasicInfo.getReportStatus());
        taskCorpEnvBasicInfo.setUnloadingType(corpEnvBasicInfo.getUnloadingType());
        taskCorpEnvBasicInfo.setUnloadingDate(corpEnvBasicInfo.getUnloadingDate());
        taskCorpEnvBasicInfo.setUnloadingValidityBegin(corpEnvBasicInfo.getUnloadingValidityBegin());
        taskCorpEnvBasicInfo.setUnloadingValidityEnd(corpEnvBasicInfo.getUnloadingValidityEnd());
        taskCorpEnvBasicInfo.setMergencyStatus(corpEnvBasicInfo.getMergencyStatus());
        taskCorpEnvBasicInfo.setMergencyRecordStatus(corpEnvBasicInfo.getMergencyRecordStatus());
        taskCorpEnvBasicInfo.setMergencyRecordTime(corpEnvBasicInfo.getMergencyRecordTime());
        taskCorpEnvBasicInfo.setMergencyLagoonStatus(corpEnvBasicInfo.getMergencyLagoonStatus());
        taskCorpEnvBasicInfo.setWaterMonitorStatus(corpEnvBasicInfo.getWaterMonitorStatus());
        taskCorpEnvBasicInfo.setWaterMonitorTime(corpEnvBasicInfo.getWaterMonitorTime());
        taskCorpEnvBasicInfo.setCleanProductStatus(corpEnvBasicInfo.getCleanProductStatus());
        taskCorpEnvBasicInfo.setSewageTraceStatus(corpEnvBasicInfo.getSewageTraceStatus());
        taskCorpEnvBasicInfo.setSewageTraceRemark(corpEnvBasicInfo.getSewageTraceRemark());
        taskCorpEnvBasicInfo.setDrainageSystem(corpEnvBasicInfo.getDrainageSystem());
        taskCorpEnvBasicInfo.setSewageType(corpEnvBasicInfo.getSewageType());
        taskCorpEnvBasicInfo.setSewageEquipStatus(corpEnvBasicInfo.getSewageEquipStatus());
        taskCorpEnvBasicInfo.setSewageEquipRemark(corpEnvBasicInfo.getSewageEquipRemark());
        taskCorpEnvBasicInfo.setSewageSolidStatus(corpEnvBasicInfo.getSewageSolidStatus());
        taskCorpEnvBasicInfo.setSewageSolidRemark(corpEnvBasicInfo.getSewageSolidRemark());
        taskCorpEnvBasicInfo.setWasteGasStatus(corpEnvBasicInfo.getWasteGasStatus());
        taskCorpEnvBasicInfo.setNoiseMethodStatus(corpEnvBasicInfo.getNoiseMethodStatus());
        taskCorpEnvBasicInfo.setNoiseMethodRemark(corpEnvBasicInfo.getNoiseMethodRemark());
        taskCorpEnvBasicInfo.setSolidWasteStatus(corpEnvBasicInfo.getSolidWasteStatus());
        taskCorpEnvBasicInfo.setSolidWasteArea(corpEnvBasicInfo.getSolidWasteArea());
        taskCorpEnvBasicInfo.setHwStatus(corpEnvBasicInfo.getHwStatus());
        taskCorpEnvBasicInfo.setHwArea(corpEnvBasicInfo.getHwArea());
        taskCorpEnvBasicInfo.setRemark(corpEnvBasicInfo.getRemark());

        taskCorpEnvBasicInfo.setJuridicalName(corpEnvBasicInfo.getJuridicalName());
        taskCorpEnvBasicInfo.setJuridicalTel(corpEnvBasicInfo.getJuridicalTel());


        taskCorpEnvBasicInfo.setHwItems(convertHwItemToTaskHwItems(corpEnvBasicInfo.getHwItems(), taskId));
        taskCorpEnvBasicInfo.setSolidWasteItems(convertSolidWasteToTaskSolidWastes(corpEnvBasicInfo.getSolidWasteItems(), taskId));
        taskCorpEnvBasicInfo.setEquipItems(convertEquipmentToTaskEquipments(corpEnvBasicInfo.getEquipItems(), taskId));
        taskCorpEnvBasicInfo.setReportItems(convertReportItemsToTaskReportItems(corpEnvBasicInfo.getReportItems(),taskId));
        taskCorpEnvBasicInfo.setGasEquipmentItems(convertGasEquipmentItemsToTaskGasEquipments(corpEnvBasicInfo.getGasEquipmentItems(),taskId));
        return taskCorpEnvBasicInfo;
    }

    private static List<TaskEpGasEquipment> convertGasEquipmentItemsToTaskGasEquipments(List<CorpEpGasEquipment> gasEquipmentItems, String taskId) {
        List<TaskEpGasEquipment> aRes = new ArrayList<>();
        for (CorpEpGasEquipment item : gasEquipmentItems){
            if (item != null){
                aRes.add(convertGasEquipmentItemToTaskGasEquipment(item,taskId));
            }
        }
        return aRes;

    }

    private static TaskEpGasEquipment convertGasEquipmentItemToTaskGasEquipment(CorpEpGasEquipment item, String taskId) {
        TaskEpGasEquipment gasEquipment = new TaskEpGasEquipment();
        gasEquipment.setCorpId(item.getCorpId());
        gasEquipment.setProductSection(item.getProductSection());
        gasEquipment.setHandleEquipName(item.getHandleEquipName());
        gasEquipment.setExhaustFunnelNo(item.getExhaustFunnelNo());
        gasEquipment.setPollutionFactor(item.getPollutionFactor());
        gasEquipment.setType(item.getType());
        gasEquipment.setDischargeStatus(item.getDischargeStatus());
        gasEquipment.setStatus(item.getStatus());
        gasEquipment.setRemark(item.getRemark());
        return gasEquipment;
    }

    private static List<TaskEpEia> convertReportItemsToTaskReportItems(List<CorpEpEia> reportItems, String taskId) {
        List<TaskEpEia> aRes = new ArrayList<>();
        for (CorpEpEia item : reportItems){
            if (item != null){
                aRes.add(convetReportItemToTaskReportItem(item,taskId));
            }
        }
        return aRes;
    }

    private static TaskEpEia convetReportItemToTaskReportItem(CorpEpEia item, String taskId) {
        TaskEpEia taskReport = new TaskEpEia();
        taskReport.setCorpId(item.getCorpId());
        taskReport.setProjectName(item.getProjectName());
        taskReport.setReportType(item.getReportType());
        taskReport.setOfficialTime(item.getOfficialTime());
        taskReport.setOfficialNo(item.getOfficialNo());
        taskReport.setStatus(item.getStatus());
        taskReport.setCheckTime(item.getCheckTime());
        taskReport.setRemark(item.getRemark());
        return taskReport;
    }

    private static List<TaskEpEquipInfo> convertEquipmentToTaskEquipments(List<CorpEpEquipInfo> equipItems, String taskId) {
        List<TaskEpEquipInfo> aRes = new ArrayList<>();
        for (CorpEpEquipInfo item : equipItems){
            if (item != null){
                aRes.add(convertEquipmentToTaskEquipment(item,taskId));
            }
        }
        return aRes;
    }

    private static TaskEpEquipInfo convertEquipmentToTaskEquipment(CorpEpEquipInfo item, String taskId) {
        TaskEpEquipInfo taskEpEquipItem = new TaskEpEquipInfo();
        taskEpEquipItem.setCorpId(item.getCorpId());
        taskEpEquipItem.setEquipName(item.getEquipName());
        taskEpEquipItem.setEquipNum(item.getEquipNum());
        taskEpEquipItem.setEquipSource(item.getEquipSource());
        taskEpEquipItem.setTaskId(taskId);
        taskEpEquipItem.setRemark(item.getRemark());
        taskEpEquipItem.setStatus(item.getStatus());
        return taskEpEquipItem;
    }

    private static List<TaskEpSolidWasteItem> convertSolidWasteToTaskSolidWastes(List<SolidWasteItem> solidWasteItems, String taskId) {
        List<TaskEpSolidWasteItem> aRes = new ArrayList<>();
        for (SolidWasteItem item : solidWasteItems){
            if (item != null){
                aRes.add(convertSolidWasteToTaskSolidWaste(item,taskId));
            }
        }
        return aRes;
    }

    private static TaskEpSolidWasteItem convertSolidWasteToTaskSolidWaste(SolidWasteItem item, String taskId) {
        TaskEpSolidWasteItem solidWaste = new TaskEpSolidWasteItem();
        solidWaste.setTaskId(taskId);
        solidWaste.setSolidNum(item.getSolidNum());
        solidWaste.setDeadline(item.getDeadline());
        solidWaste.setCorpId(item.getCorpId());
        solidWaste.setRemark(item.getRemark());
        solidWaste.setSolidName(item.getSolidName());
        solidWaste.setStatus(item.getStatus());
        return solidWaste;
    }

    /**
     * 将危废信息数组转化为任务对象。
     * @param hwItems
     * @return
     */
    private static List<TaskEpHwItem> convertHwItemToTaskHwItems(List<HwItem> hwItems, String taskId) {
        List<TaskEpHwItem> aRes = new ArrayList<>();
        for (HwItem item : hwItems){
            if (item != null){
                aRes.add(convertHwItemToTaskHwItem(item,taskId));
            }
        }
        return aRes;
    }

    /**
     * 将基础信息的危废信息转换为task中的内容。
     * @param hwItem
     * @param taskId
     * @return
     */
    private static TaskEpHwItem convertHwItemToTaskHwItem(HwItem hwItem, String taskId){
        if (hwItem != null) {
            TaskEpHwItem item = new TaskEpHwItem();
            item.setTaskId(taskId);
            item.setRemark(hwItem.getRemark());
            item.setCorpId(hwItem.getCorpId());
            item.setStatus(hwItem.getStatus());
            item.setDeadline(hwItem.getDeadline());
            item.setHwCode(hwItem.getHwCode());
            item.setHwName(hwItem.getHwName());
            item.setHwNum(hwItem.getHwNum());
            return item;
        }
        return null;
    }

    public static CorpEnvBasicInfo convertTaskEnvBasicInfoToCorpEnvBasicInfo(TaskCorpEnvBasicInfo taskCorpEnvBasicInfo){
        CorpEnvBasicInfo corpEnvBasicInfo = new CorpEnvBasicInfo();
        corpEnvBasicInfo.setCorpAddress(taskCorpEnvBasicInfo.getCorpAddress());
        corpEnvBasicInfo.setCorpName(taskCorpEnvBasicInfo.getCorpName());
        corpEnvBasicInfo.setCertNo(taskCorpEnvBasicInfo.getCertNo());
        corpEnvBasicInfo.setBusiType(taskCorpEnvBasicInfo.getBusiType());
        corpEnvBasicInfo.setId(taskCorpEnvBasicInfo.getCorpId());
        corpEnvBasicInfo.setLinkName(taskCorpEnvBasicInfo.getLinkName());
        corpEnvBasicInfo.setLinkTel(taskCorpEnvBasicInfo.getLinkTel());
        corpEnvBasicInfo.setProductProcess(taskCorpEnvBasicInfo.getProductProcess());
        corpEnvBasicInfo.setReportStatus(taskCorpEnvBasicInfo.getReportStatus());
        corpEnvBasicInfo.setUnloadingType(taskCorpEnvBasicInfo.getUnloadingType());
        corpEnvBasicInfo.setUnloadingDate(taskCorpEnvBasicInfo.getUnloadingDate());
        corpEnvBasicInfo.setUnloadingValidityBegin(taskCorpEnvBasicInfo.getUnloadingValidityBegin());
        corpEnvBasicInfo.setUnloadingValidityEnd(taskCorpEnvBasicInfo.getUnloadingValidityEnd());
        corpEnvBasicInfo.setMergencyStatus(taskCorpEnvBasicInfo.getMergencyStatus());
        corpEnvBasicInfo.setMergencyRecordStatus(taskCorpEnvBasicInfo.getMergencyRecordStatus());
        corpEnvBasicInfo.setMergencyRecordTime(taskCorpEnvBasicInfo.getMergencyRecordTime());
        corpEnvBasicInfo.setMergencyLagoonStatus(taskCorpEnvBasicInfo.getMergencyLagoonStatus());
        corpEnvBasicInfo.setWaterMonitorStatus(taskCorpEnvBasicInfo.getWaterMonitorStatus());
        corpEnvBasicInfo.setWaterMonitorTime(taskCorpEnvBasicInfo.getWaterMonitorTime());
        corpEnvBasicInfo.setCleanProductStatus(taskCorpEnvBasicInfo.getCleanProductStatus());
        corpEnvBasicInfo.setSewageTraceStatus(taskCorpEnvBasicInfo.getSewageTraceStatus());
        corpEnvBasicInfo.setSewageTraceRemark(taskCorpEnvBasicInfo.getSewageTraceRemark());
        corpEnvBasicInfo.setDrainageSystem(taskCorpEnvBasicInfo.getDrainageSystem());
        corpEnvBasicInfo.setSewageType(taskCorpEnvBasicInfo.getSewageType());
        corpEnvBasicInfo.setSewageEquipStatus(taskCorpEnvBasicInfo.getSewageEquipStatus());
        corpEnvBasicInfo.setSewageEquipRemark(taskCorpEnvBasicInfo.getSewageEquipRemark());
        corpEnvBasicInfo.setSewageSolidStatus(taskCorpEnvBasicInfo.getSewageSolidStatus());
        corpEnvBasicInfo.setSewageSolidRemark(taskCorpEnvBasicInfo.getSewageSolidRemark());
        corpEnvBasicInfo.setWasteGasStatus(taskCorpEnvBasicInfo.getWasteGasStatus());
        corpEnvBasicInfo.setNoiseMethodStatus(taskCorpEnvBasicInfo.getNoiseMethodStatus());
        corpEnvBasicInfo.setNoiseMethodRemark(taskCorpEnvBasicInfo.getNoiseMethodRemark());
        corpEnvBasicInfo.setSolidWasteStatus(taskCorpEnvBasicInfo.getSolidWasteStatus());
        corpEnvBasicInfo.setSolidWasteArea(taskCorpEnvBasicInfo.getSolidWasteArea());
        corpEnvBasicInfo.setHwStatus(taskCorpEnvBasicInfo.getHwStatus());
        corpEnvBasicInfo.setHwArea(taskCorpEnvBasicInfo.getHwArea());
        corpEnvBasicInfo.setRemark(taskCorpEnvBasicInfo.getRemark());

        corpEnvBasicInfo.setJuridicalName(taskCorpEnvBasicInfo.getJuridicalName());
        corpEnvBasicInfo.setJuridicalTel(taskCorpEnvBasicInfo.getJuridicalTel());

        corpEnvBasicInfo.setHwItems(convertTaskEpHwItemToHwItems(taskCorpEnvBasicInfo.getHwItems()));
        corpEnvBasicInfo.setSolidWasteItems(convertTaskEpSolidWasteToSolidWastes(taskCorpEnvBasicInfo.getSolidWasteItems()));
        corpEnvBasicInfo.setEquipItems(convertTaskEpEquipmentToEpEquipments(taskCorpEnvBasicInfo.getEquipItems()));
        corpEnvBasicInfo.setReportItems(convertTaskEpReportItemsToEpReportItems(taskCorpEnvBasicInfo.getReportItems()));
        corpEnvBasicInfo.setGasEquipmentItems(convertTaskEpGasEquipmentItemsToEpGasEquipments(taskCorpEnvBasicInfo.getGasEquipmentItems()));
        return corpEnvBasicInfo;
    }

    private static List<CorpEpGasEquipment> convertTaskEpGasEquipmentItemsToEpGasEquipments(List<TaskEpGasEquipment> gasEquipmentItems) {
        List<CorpEpGasEquipment> aRes = new ArrayList<>();
        if (gasEquipmentItems != null && gasEquipmentItems.size() > 0){
            for (TaskEpGasEquipment item : gasEquipmentItems){
                aRes.add(convertTaskEpGasEquipmentItemsToEpGasEquipment(item));
            }
        }
        return aRes;
    }

    private static CorpEpGasEquipment convertTaskEpGasEquipmentItemsToEpGasEquipment(TaskEpGasEquipment item) {
        CorpEpGasEquipment gasEquipment = new CorpEpGasEquipment();
        gasEquipment.setCorpId(item.getCorpId());
        gasEquipment.setProductSection(item.getProductSection());
        gasEquipment.setHandleEquipName(item.getHandleEquipName());
        gasEquipment.setExhaustFunnelNo(item.getExhaustFunnelNo());
        gasEquipment.setPollutionFactor(item.getPollutionFactor());
        gasEquipment.setType(item.getType());
        gasEquipment.setDischargeStatus(item.getDischargeStatus());
        gasEquipment.setStatus(item.getStatus());
        gasEquipment.setRemark(item.getRemark());
        return gasEquipment;
    }

    private static List<CorpEpEia> convertTaskEpReportItemsToEpReportItems(List<TaskEpEia> reportItems) {
        List<CorpEpEia> aRes = new ArrayList<>();
        if (reportItems != null && reportItems.size() > 0){
            for (TaskEpEia item : reportItems){
                aRes.add(convertTaskEpReportItemsToEpReportItem(item));
            }
        }
        return aRes;
    }

    private static CorpEpEia convertTaskEpReportItemsToEpReportItem(TaskEpEia item) {
        CorpEpEia report = new CorpEpEia();
        report.setCorpId(item.getCorpId());
        report.setProjectName(item.getProjectName());
        report.setReportType(item.getReportType());
        report.setOfficialTime(item.getOfficialTime());
        report.setOfficialNo(item.getOfficialNo());
        report.setStatus(item.getStatus());
        report.setCheckTime(item.getCheckTime());
        report.setRemark(item.getRemark());
        return report;
    }

    private static List<CorpEpEquipInfo> convertTaskEpEquipmentToEpEquipments(List<TaskEpEquipInfo> equipItems) {
        List<CorpEpEquipInfo> aRes = new ArrayList<>();
        if (equipItems != null && equipItems.size() > 0){
            for (TaskEpEquipInfo item : equipItems){
                aRes.add(convertTaskEpEquipmentToEpEquipment(item));
            }
        }
        return aRes;
    }

    private static CorpEpEquipInfo convertTaskEpEquipmentToEpEquipment(TaskEpEquipInfo item) {
        CorpEpEquipInfo epEquipItem = new CorpEpEquipInfo();
        epEquipItem.setCorpId(item.getCorpId());
        epEquipItem.setEquipName(item.getEquipName());
        epEquipItem.setEquipNum(item.getEquipNum());
        epEquipItem.setEquipSource(item.getEquipSource());
        epEquipItem.setRemark(item.getRemark());
        epEquipItem.setStatus(item.getStatus());
        return epEquipItem;
    }

    private static List<SolidWasteItem> convertTaskEpSolidWasteToSolidWastes(List<TaskEpSolidWasteItem> solidWasteItems) {
        List<SolidWasteItem> aRes = new ArrayList<>();
        if (solidWasteItems != null && solidWasteItems.size() > 0){
            for (TaskEpSolidWasteItem item : solidWasteItems){
                aRes.add(convertTaskEpSolidWasteToSolidWaste(item));
            }
        }
        return aRes;
    }

    private static SolidWasteItem convertTaskEpSolidWasteToSolidWaste(TaskEpSolidWasteItem item) {
        SolidWasteItem solidWaste = new SolidWasteItem();
        solidWaste.setSolidNum(item.getSolidNum());
        solidWaste.setDeadline(item.getDeadline());
        solidWaste.setCorpId(item.getCorpId());
        solidWaste.setRemark(item.getRemark());
        solidWaste.setSolidName(item.getSolidName());
        solidWaste.setStatus(item.getStatus());
        return solidWaste;
    }

    private static List<HwItem> convertTaskEpHwItemToHwItems(List<TaskEpHwItem> hwItems) {
        List<HwItem> aRes = new ArrayList<>();
        if (hwItems != null && hwItems.size() > 0){
            for (TaskEpHwItem item : hwItems){
                aRes.add(convertTaskEpHwItemToHwItem(item));
            }
        }
        return aRes;
    }

    private static HwItem convertTaskEpHwItemToHwItem(TaskEpHwItem taskItem) {
        if (taskItem != null) {
            HwItem item = new HwItem();
            item.setRemark(taskItem.getRemark());
            item.setCorpId(taskItem.getCorpId());
            item.setStatus(taskItem.getStatus());
            item.setDeadline(taskItem.getDeadline());
            item.setHwCode(taskItem.getHwCode());
            item.setHwName(taskItem.getHwName());
            item.setHwNum(taskItem.getHwNum());
            return item;
        }
        return null;
    }


}
