package com.enjoychain.www.security.mapper;

import com.enjoychain.www.security.entity.TaskEpEia;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author ddmonk
 * @since 2023-03-07
 */
public interface TaskEpEiaMapper extends BaseMapper<TaskEpEia> {

}
