package com.enjoychain.www.security.service.impl;

import com.enjoychain.www.security.constant.BusinessTaskType;
import com.enjoychain.www.security.entity.*;
import com.enjoychain.www.security.mapper.TaskSecPatrolItemMapper;
import com.enjoychain.www.security.service.IBasicFileService;
import com.enjoychain.www.security.service.ITaskSecCheckDictService;
import com.enjoychain.www.security.service.ITaskSecPatrolItemService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.enjoychain.www.security.service.impl.task.TaskSecConverts;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.bouncycastle.cms.PasswordRecipientId;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.io.Serializable;
import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.function.Function;
import java.util.stream.Collectors;

/**
 * <p>
 * 安全核查基础信息表 服务实现类
 * </p>
 *
 * @author ddmonk
 * @since 2023-04-18
 */
@Service
@Slf4j
public class TaskSecPatrolItemServiceImpl extends ServiceImpl<TaskSecPatrolItemMapper, TaskSecPatrolItem> implements ITaskSecPatrolItemService {


    @Autowired
    IBasicFileService fileService;

    @Autowired
    ITaskSecCheckDictService dictService;

    @Override
    public boolean removeByTaskId(Serializable id) {
        List<TaskSecPatrolItem> itemList = ITaskSecPatrolItemService.super.listByTaskId(id);
        for (TaskSecPatrolItem item : itemList){
            fileService.removeFileByConnectIdAndBusiType(BusinessTaskType.SECURITY,item.getId());
        }
        return ITaskSecPatrolItemService.super.removeByTaskId(id);
    }

    @Override
    public List<TaskSecPatrolItem> listByTaskId(Serializable id) {
        List<TaskSecPatrolItem> itemList = ITaskSecPatrolItemService.super.listByTaskId(id);
        for (TaskSecPatrolItem item : itemList){
            item.setFileList(fileService.getListByTypeAndId(BusinessTaskType.SECURITY,item.getId()));
        }
        return itemList;
    }

    /**
     * 初始化，按照企业基础信息进行初始化
     * @param basicInfo
     * @param taskId
     */
    @Override
    public void inital(TaskCorpSecBasicInfo basicInfo, String taskId) {
        List<TaskSecPatrolItem> items = initialSecCorpBasicInfo(basicInfo,taskId);
        this.saveOrUpdateBatch(items);
    }

    @Override
    public void updateByCorpSecBasicInfo(TaskCorpSecBasicInfo secBasicInfo) {
        List<TaskSecPatrolItem> itemList = this.getBatchTaskId(secBasicInfo.getTaskId());
        for (TaskSecPatrolItem item : itemList){
            if (StringUtils.isNotEmpty(item.getFieldName())){
                item.setDefaultValue(getValue(secBasicInfo, item.getFieldName()));
            }
        }
        this.updateBatchById(itemList);
    }

    @Override
    public List<TaskSecPatrolItem> listDetailByTaskId(String taskId) {
        List<TaskSecPatrolItem> itemList = this.listByTaskId(taskId);
        for (TaskSecPatrolItem item : itemList){
            item.setFileList(fileService.getListByTypeAndId(BusinessTaskType.SECURITY,item.getId()));
        }
        return itemList;
    }

    @Override
    public void updatePatrolItems(TaskCorpSecBasicInfo basicInfo, List<TaskSecPatrolItem> items) {
        Map<Integer,TaskSecCheckDict> dicts = dictService.list().stream().collect(Collectors.toMap(TaskSecCheckDict::getId, Function.identity()));
        for (TaskSecPatrolItem item : items){
            TaskSecCheckDict dict = dicts.get(item.getParentId());
            item.setDefaultValue(getValue(basicInfo, dict.getKeyWord()));
            item.setDescription(String.format(dict.getDescription(),item.getDefaultValue()));
        }
        this.saveOrUpdateBatch(items);
    }


    List<TaskSecPatrolItem> initialSecCorpBasicInfo(TaskCorpSecBasicInfo basicInfo, String taskId){
        List<TaskSecCheckDict> dicts = dictService.list();
        List<TaskSecPatrolItem> patrolItems = new ArrayList<>();
        for (TaskSecCheckDict dict : dicts){
            TaskSecPatrolItem item = new TaskSecPatrolItem();
            item.setPoint(dict.getCheckPoint());
            item.setClassType(dict.getClassType());
            item.setOrderNum(dict.getId());
            item.setTaskId(taskId);
            item.setFieldName(dict.getKeyWord());
            item.setDefaultValue(getValue(basicInfo, dict.getKeyWord()));
            item.setContent(dict.getContent());
            item.setParentId(dict.getId());
            item.setDescription(String.format(dict.getDescription(),item.getDefaultValue()));
            patrolItems.add(item);
        }
        return patrolItems;
    }

    private String getValue(TaskCorpSecBasicInfo basicInfo, String keyWord){
        String aRes = null;
        try {
            Field[] fields = basicInfo.getClass().getDeclaredFields();
            for (Field field : fields){
                if (field.getName().equals(keyWord)) {
                    field.setAccessible(true);
                    if (field.get(basicInfo) != null) {
                        aRes = field.get(basicInfo).toString();
                    }
                }
            }
        }catch (IllegalAccessException ex){
            log.error("GET VALUE ERROR:",ex);
        }
        return aRes;
    }






}
