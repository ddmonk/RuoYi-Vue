package com.enjoychain.www.security.service;

import com.enjoychain.www.security.entity.CorpEpHw;
import com.baomidou.mybatisplus.extension.service.IService;

import java.util.List;

/**
 * <p>
 * 危废生产情况 服务类
 * </p>
 *
 * @author ddmonk
 * @since 2023-01-13
 */
public interface ICorpEpHwService extends IService<CorpEpHw> {

    List<CorpEpHw> listWithStatus(int i, String corpId);
}
