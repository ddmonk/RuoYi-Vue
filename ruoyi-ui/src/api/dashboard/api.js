import request from '@/utils/request'

export function getPanel() {
  return request({
    url: '/security/dashboard/panel',
    method: 'get'
  })
}

export function delTask(id) {
  return request({
    url: '/security/taskInfo/' + id,
    method: 'delete'
  })
}

export function getTask(id) {
  return request({
    url: '/security/taskInfo/' + id,
    method: 'get'
  })
}

export function listTaskDetailItem(query) {
  return request({
    url: '/security/taskDetailItem/list',
    method: 'get',
    params: query
  })
}

export function addTaskDetailItem(data) {
  return request({
    url: '/security/taskDetailItem/list',
    method: 'post',
    data: data
  })
}

export function updateTaskDetailItem(data) {
  return request({
    url: '/security/taskDetailItem/list',
    method: 'put',
    data: data
  })
}

export function delTaskDetailItem(id) {
  return request({
    url: '/security/taskDetailItem/' + id,
    method: 'delete'
  })
}
