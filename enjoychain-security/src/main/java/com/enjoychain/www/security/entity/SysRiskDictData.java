package com.enjoychain.www.security.entity;

import com.baomidou.mybatisplus.annotation.FieldFill;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import java.io.Serializable;
import java.time.LocalDateTime;
import com.ruoyi.common.annotation.Excel;
import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;
import lombok.experimental.Accessors;

/**
 * <p>
 * 较大风险目录字典
 * </p>
 *
 * @author ddmonk
 * @since 2023-04-11
 */
@Getter
@Setter
@Accessors(chain = true)
@TableName("sys_risk_dict_data")
@ApiModel(value = "SysRiskDictData对象", description = "较大风险目录字典")
public class SysRiskDictData implements Serializable {

    private static final long serialVersionUID = 1L;

    @ApiModelProperty("主键")
    @Excel(name = "主键")
    @TableId(value = "id", type = IdType.AUTO)
    private Integer id;

    @ApiModelProperty("管理类别")
    @Excel(name = "管理类别")
    private String managerType;

    @ApiModelProperty("风险代码")
    @Excel(name = "风险代码")
    private String riskCode;

    @ApiModelProperty("风险名称")
    @Excel(name = "风险名称")
    private String riskName;

    @ApiModelProperty("主要事故类型")
    @Excel(name = "主要事故类型")
    private String accType;

    @ApiModelProperty("风险点")
    @Excel(name = "风险点")
    private String riskPoint;

    @ApiModelProperty("备注")
    @Excel(name = "备注")
    private String remark;

    @ApiModelProperty("状态（0正常 1停用）")
    @Excel(name = "状态（0正常 1停用）")
    private String status;

    @ApiModelProperty(value = "创建者", hidden = true)
    @TableField(fill = FieldFill.INSERT)
    private String createBy;

    @ApiModelProperty(value = "创建时间", hidden = true)
    @TableField(fill = FieldFill.INSERT)
    private LocalDateTime createTime;

    @ApiModelProperty("更新者")
    @Excel(name = "更新者")
    private String updateBy;

    @ApiModelProperty(value = "更新时间", hidden = true)
    @TableField(fill = FieldFill.INSERT_UPDATE)
    private LocalDateTime updateTime;

    @ApiModelProperty("字典排序")
    @Excel(name = "字典排序")
    private Integer dictSort;


}
