package com.enjoychain.www.security.service;

import com.enjoychain.www.security.entity.TaskProductProcess;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 * 生产工艺 服务类
 * </p>
 *
 * @author ddmonk
 * @since 2023-02-24
 */
public interface ITaskProductProcessService extends ITaskService<TaskProductProcess> {

}
