package com.enjoychain.www.security.mapper;

import com.enjoychain.www.security.entity.TaskWasteWater;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * 废水产生 Mapper 接口
 * </p>
 *
 * @author ddmonk
 * @since 2023-02-24
 */
public interface TaskWasteWaterMapper extends BaseMapper<TaskWasteWater> {

}
