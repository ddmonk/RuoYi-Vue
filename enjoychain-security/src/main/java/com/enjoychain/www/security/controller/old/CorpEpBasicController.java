package com.enjoychain.www.security.controller.old;

import org.springframework.web.bind.annotation.RequestMapping;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.common.utils.StringUtils;
import com.ruoyi.common.utils.poi.ExcelUtil;
import io.swagger.annotations.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import javax.servlet.http.HttpServletResponse;
import java.util.List;
import java.util.UUID;
import java.util.Arrays;
import com.enjoychain.www.security.entity.CorpEpBasic;
import com.enjoychain.www.security.service.ICorpEpBasicService;
import org.springframework.web.bind.annotation.RestController;

/**
 * <p>
 * 环保业务基础信息 前端控制器
 * </p>
 *
 * @author ddmonk
 * @since 2023-01-20
 */
@Api(tags = "环保业务基础信息")
@RestController
@RequestMapping("/security/corpEpBasic")
public class CorpEpBasicController {

    @Autowired
    ICorpEpBasicService service;

    @PreAuthorize("@ss.hasPermi('client:Epservice:list')")
    @GetMapping(path = "/list", produces = {"application/json"})
    @ApiOperation(value = "获取环保业务基础信息列表")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "corpId", value = "企业Id",paramType = "query", dataType = "String"),
            @ApiImplicitParam(name = "pageNo", value = "页码", paramType = "query", dataType = "long"),
            @ApiImplicitParam(name = "pageSize", value = "页面记录条数", paramType = "query", dataType = "long")
    })
    @ApiResponses({
        @ApiResponse(code = 200, message = "ok", response= CorpEpBasic.class),
    })
    public AjaxResult list(
                            @RequestParam(name = "corpId", required = false) String corpId,
                            @RequestParam(name = "pageNum", defaultValue = "1") long pageNum,
                            @RequestParam(name = "pageSize",defaultValue = "10") long pageSize){
        IPage<CorpEpBasic> entityIPage = new Page<>(pageNum, pageSize);
        QueryWrapper<CorpEpBasic> queryWrapper = new QueryWrapper<>();
        if (StringUtils.isNotEmpty(corpId)) {
            queryWrapper.eq("corp_id",corpId);
        }
        return AjaxResult.success("list-corp_ep_basic",service.page(entityIPage,queryWrapper));
    }

    @PreAuthorize("@ss.hasPermi('client:Epservice:query')")
    @GetMapping(value = "{id}")
    @ApiOperation("获取环保业务基础信息详情")
    @ApiImplicitParams({
        @ApiImplicitParam(name = "id", value = "id",paramType = "path", dataType = "String")
    })
    @ApiResponses({
        @ApiResponse(code = 200, message = "ok", response = CorpEpBasic.class)
    })
    public AjaxResult getCorpEpBasicInfo(@PathVariable(value = "id", required = false) String id)
    {
        return AjaxResult.success("get-corp_ep_basic",service.getById(id));
    }

    @PreAuthorize("@ss.hasPermi('client:Epservice:import')")
    @ApiOperation("导入环保业务基础信息")
    @Log(title = "环保业务基础信息", businessType = BusinessType.IMPORT)
    @PostMapping("/importData")
    public AjaxResult importData(MultipartFile file) throws Exception{
        ExcelUtil<CorpEpBasic> util = new ExcelUtil<CorpEpBasic>(CorpEpBasic.class);
        List<CorpEpBasic> infos = util.importExcel(file.getInputStream());
        return service.saveOrUpdateBatch(infos)?AjaxResult.success("批量插入成功"):AjaxResult.error("批量插入失败");
    }


    @ApiOperation("环保业务基础信息导出模板下载")
    @PostMapping("/importTemplate")
    public void importTemplate(HttpServletResponse response){
        ExcelUtil<CorpEpBasic> util = new ExcelUtil<CorpEpBasic>(CorpEpBasic.class);
        util.importTemplateExcel(response,"环保业务基础信息");
    }

    @PreAuthorize("@ss.hasPermi('client:Epservice:add')")
    @PostMapping(consumes = {"application/json"}, produces = {"application/json"})
    @ApiOperation("新增环保业务基础信息")
    @ApiResponses({
        @ApiResponse(code = 200, message = "ok", response = CorpEpBasic.class),
    })
    @Log(title = "环保业务基础信息", businessType = BusinessType.INSERT)
    public AjaxResult add(@RequestBody CorpEpBasic entity){
        return service.save(entity)? AjaxResult.success("add-corp_ep_basic",entity): AjaxResult.error("新增环保业务基础信息失败");
    }


    @PreAuthorize("@ss.hasPermi('client:Epservice:update')")
    @PutMapping(consumes = {"application/json"}, produces = {"application/json"})
    @ApiOperation("修改环保业务基础信息")
    @ApiResponses({
        @ApiResponse(code = 200, message = "ok", response = CorpEpBasic.class)
    })
    @Log(title = "环保业务基础信息", businessType = BusinessType.UPDATE)
    public AjaxResult update(@RequestBody CorpEpBasic corpBasicInfo){
        String operate = "update-corp_ep_basic";
        return service.updateById(corpBasicInfo)?AjaxResult.success(operate,service.getById(corpBasicInfo.getId())) : AjaxResult.error("修改客户基础信息失败");
    }

    @PreAuthorize("@ss.hasPermi('client:Epservice:delete')")
    @DeleteMapping(value = "{id}", produces = {"application/json"})
    @ApiOperation("删除环保业务基础信息")
    @ApiResponses({
        @ApiResponse(code = 200, message = "ok", response = AjaxResult.class),
    })
    @Log(title = "环保业务基础信息", businessType = BusinessType.DELETE)
    public AjaxResult delete(@PathVariable(value = "id", required = false) String id){
        String operate = "delete-corp_ep_basic";
        String[] ids = id.split(",");
        return service.removeBatchByIds(Arrays.asList(ids))?AjaxResult.success(operate) : AjaxResult.error("修改环保业务基础信息失败");
    }


    @Log(title = "环保业务基础信息", businessType = BusinessType.EXPORT)
    @PreAuthorize("@ss.hasPermi('client:Epservice:export')")
    @PostMapping("/export")
    @ApiOperation("导出环保业务基础信息")
    @ApiResponses({
        @ApiResponse(code = 200, message = "ok", response = AjaxResult.class),
    })
    public void export(HttpServletResponse response,@RequestParam(name = "corpId", required = false) String corpId){
        QueryWrapper<CorpEpBasic> queryWrapper = new QueryWrapper<>();
        if (StringUtils.isNotEmpty(corpId)) {
            queryWrapper.eq("corp_id",corpId);
        }
        List<CorpEpBasic> list = service.list(queryWrapper);
        ExcelUtil<CorpEpBasic> util = new ExcelUtil<CorpEpBasic>(CorpEpBasic.class);
        util.exportExcel(response,list, "导出环保业务基础信息");
    }
}
