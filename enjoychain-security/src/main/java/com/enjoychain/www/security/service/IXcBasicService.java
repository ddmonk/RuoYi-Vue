package com.enjoychain.www.security.service;


import com.baomidou.mybatisplus.extension.service.IService;
import com.enjoychain.www.security.entity.XcBasic;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author ddmonk
 * @since 2020-12-03
 */
public interface IXcBasicService extends IService<XcBasic> {

    int getValue(String key);

    boolean save(String key, String value);

    String getTaskNo();


}
