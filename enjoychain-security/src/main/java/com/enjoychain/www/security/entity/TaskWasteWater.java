package com.enjoychain.www.security.entity;

import com.baomidou.mybatisplus.annotation.FieldFill;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableName;
import java.io.Serializable;
import java.time.LocalDateTime;
import com.ruoyi.common.annotation.Excel;
import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;
import lombok.experimental.Accessors;

/**
 * <p>
 * 废水产生
 * </p>
 *
 * @author ddmonk
 * @since 2023-02-24
 */
@Getter
@Setter
@Accessors(chain = true)
@TableName("task_waste_water")
@ApiModel(value = "TaskWasteWater对象", description = "废水产生")
public class TaskWasteWater implements Serializable {

    private static final long serialVersionUID = 1L;

    private String id;

    @ApiModelProperty("任务id")
    @Excel(name = "任务id")
    private String taskId;

    @ApiModelProperty("生产废水产生情况")
    @Excel(name = "生产废水产生情况")
    private String wasteWaterDesc;

    @ApiModelProperty("设备运行情况")
    @Excel(name = "设备运行情况")
    private String wasteWaterEquipStatus;

    @ApiModelProperty("设备运行备注")
    @Excel(name = "设备运行备注")
    private String wasteWaterEquipRemark;

    @ApiModelProperty("有无废水排放口标识")
    @Excel(name = "有无废水排放口标识")
    private String drainageSignStatus;

    @ApiModelProperty(value = "创建时间", hidden = true)
    @TableField(fill = FieldFill.INSERT)
    private LocalDateTime createTime;

    @ApiModelProperty(value = "修改时间", hidden = true)
    @TableField(fill = FieldFill.INSERT_UPDATE)
    private LocalDateTime updateTime;

    @ApiModelProperty(value = "创建人员", hidden = true)
    @TableField(fill = FieldFill.INSERT)
    private String createBy;

    @ApiModelProperty(value = "修改人员", hidden = true)
    @TableField(fill = FieldFill.INSERT_UPDATE)
    private String modifyBy;


}
