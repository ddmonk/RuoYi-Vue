package com.enjoychain.www.security.service.impl;

import com.enjoychain.www.security.constant.BusinessTaskType;
import com.enjoychain.www.security.entity.TaskEpGasEquipment;
import com.enjoychain.www.security.entity.TaskGasPollutionItem;
import com.enjoychain.www.security.mapper.TaskGasPollutionItemMapper;
import com.enjoychain.www.security.service.IBasicFileService;
import com.enjoychain.www.security.service.ITaskGasPollutionItemService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author ddmonk
 * @since 2023-02-24
 */
@Service
public class TaskGasPollutionItemServiceImpl extends ServiceImpl<TaskGasPollutionItemMapper, TaskGasPollutionItem> implements ITaskGasPollutionItemService {



    List<String> targets = new ArrayList<String>(){{
        add("排气筒管道是否完好");
        add("风机是否开启");
        add("排气筒高度是否满足要求");
        add("检测口是否满足要求");
        add("处理设施是否开启");
        add("是否按时维护");
        add("是否按时更换处理材料");
        add("是否无其他异常");
        add("是否有标识牌");
    }};
    @Autowired
    IBasicFileService fileService;

    @Override
    public List<TaskGasPollutionItem> listByTaskId(String id) {
        List<TaskGasPollutionItem> items = list(buildQueryWithTaskId(id));
        for (TaskGasPollutionItem item : items){
            item.setFiles(fileService.getListByTypeAndId(BusinessTaskType.ENVIRONMENT,item.getId()));
        }
        return items;
    }

    @Override
    public void initial(TaskEpGasEquipment epGasEquipment, String id) {
        TaskGasPollutionItem item = new TaskGasPollutionItem();
        item.setTaskId(id);
        item.setPollutionFactor(epGasEquipment.getPollutionFactor());
        item.setExhaustFunnelNo(epGasEquipment.getExhaustFunnelNo());
        item.setProductSection(epGasEquipment.getProductSection());
        this.save(item);
//        int flag = 1;
//        for (String target: targets){
//            TaskGasPollutionItem item = new TaskGasPollutionItem();
//            item.setTaskId(id);
//            item.setPollutionFactor(epGasEquipment.getPollutionFactor());
//            item.setExhaustFunnelNo(epGasEquipment.getExhaustFunnelNo());
//            item.setProductSection(epGasEquipment.getProductSection());
//            flag++;
//            this.save(item);
//        }
    }

    @Override
    public void removeDetailByTaskId(String taskId) {
        List<TaskGasPollutionItem> items = list(buildQueryWithTaskId(taskId));
        for (TaskGasPollutionItem item : items){
            fileService.removeFileByConnectIdAndBusiType(BusinessTaskType.ENVIRONMENT,item.getId());
            removeById(item);
        }
    }
}
