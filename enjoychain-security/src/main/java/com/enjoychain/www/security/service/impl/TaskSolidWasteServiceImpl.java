package com.enjoychain.www.security.service.impl;

import com.enjoychain.www.security.constant.BusinessTaskType;
import com.enjoychain.www.security.entity.TaskSolidWaste;
import com.enjoychain.www.security.mapper.TaskSolidWasteMapper;
import com.enjoychain.www.security.service.IBasicFileService;
import com.enjoychain.www.security.service.ITaskSolidWasteService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 一般固废仓库 服务实现类
 * </p>
 *
 * @author ddmonk
 * @since 2023-02-24
 */
@Service
public class TaskSolidWasteServiceImpl extends ServiceImpl<TaskSolidWasteMapper, TaskSolidWaste> implements ITaskSolidWasteService {

    @Autowired
    IBasicFileService fileService;


    @Override
    public TaskSolidWaste getDetailByTaskId(String taskId) {
        TaskSolidWaste solidWaste = getByTaskId(taskId);
        if (solidWaste != null){
            solidWaste.setFiles(fileService.getListByTypeAndId(BusinessTaskType.ENVIRONMENT,solidWaste.getId()));
        }
        return solidWaste;
    }

    @Override
    public void removeDetailByTaskId(String taskId) {
        TaskSolidWaste solidWaste = getByTaskId(taskId);
        if (solidWaste != null){
            fileService.removeFileByConnectIdAndBusiType(BusinessTaskType.ENVIRONMENT,solidWaste.getId());
        }
        removeById(solidWaste);
    }
}
