package com.enjoychain.www.security.mapper;

import com.enjoychain.www.security.entity.job.JobDetailItem;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * 作业考核详情内容 Mapper 接口
 * </p>
 *
 * @author ddmonk
 * @since 2023-01-29
 */
public interface JobDetailItemMapper extends BaseMapper<JobDetailItem> {

}
