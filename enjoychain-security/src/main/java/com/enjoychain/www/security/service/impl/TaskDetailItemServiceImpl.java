package com.enjoychain.www.security.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.enjoychain.www.security.constant.BusinessTaskType;
import com.enjoychain.www.security.entity.accident.AccidentInfo;
import com.enjoychain.www.security.entity.task.TaskDetailItem;
import com.enjoychain.www.security.mapper.TaskDetailItemMapper;
import com.enjoychain.www.security.service.IAccidentInfoService;
import com.enjoychain.www.security.service.IBasicFileService;
import com.enjoychain.www.security.service.ITaskDetailItemService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.ruoyi.common.utils.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.util.List;

/**
 * <p>
 * 巡查任务详情信息 服务实现类
 * </p>
 *
 * @author ddmonk
 * @since 2023-01-20
 */
@Service
public class TaskDetailItemServiceImpl extends ServiceImpl<TaskDetailItemMapper, TaskDetailItem> implements ITaskDetailItemService {

    @Autowired
    IBasicFileService fileService;


    @Autowired
    IAccidentInfoService accidentInfoService;

    @Override
    public List<TaskDetailItem> getItems(String taskId) {
        int sort = 1;
        List<TaskDetailItem> items = baseMapper.selectList(queryWrapperWithTaskId(taskId));
        for (TaskDetailItem item : items){
            item.setShowSort(sort);
            item.setFileList(fileService.getListByTypeAndId(BusinessTaskType.ENVIRONMENT,item.getId()));
            sort++;
        }
        return items;
    }

    @Override
    public void removeByTaskId(String taskId) {
        baseMapper.delete(queryWrapperWithTaskId(taskId));
    }

    @Override
    public boolean removeBatchByTaskIds(List<String> asList) {
        QueryWrapper<TaskDetailItem> queryWrapper = new QueryWrapper<>();
        queryWrapper.in("task_id",asList);
        List<TaskDetailItem> items = list(queryWrapper);
        for (TaskDetailItem item : items){
            fileService.removeFileByConnectIdAndBusiType(BusinessTaskType.ENVIRONMENT,item.getId());
        }
        return remove(queryWrapper);
    }

    private boolean removeItemById(String id){
        fileService.removeFileByConnectIdAndBusiType(BusinessTaskType.ENVIRONMENT,id);
        return removeById(id);
    }


    @Override
    public TaskDetailItem findItemWithTaskIdAndAccidentInfo(String taskId, AccidentInfo info) {
        QueryWrapper<TaskDetailItem> queryWrapper = new QueryWrapper<>();
        queryWrapper.eq("task_id",taskId);
        if (StringUtils.isNotEmpty(info.getBasicInfo())){
            queryWrapper.eq("basic_info",info.getBasicInfo());
        }
        if (StringUtils.isNotEmpty(info.getTargetId())) {
            queryWrapper.eq("target_id", info.getTargetId());
        }
        if (StringUtils.isNotEmpty(info.getKeyId())){
            queryWrapper.eq("key_id",info.getKeyId());
        }
        if (StringUtils.isNotEmpty(info.getContent())){
            queryWrapper.eq("content",info.getContent());
        }
        TaskDetailItem item = baseMapper.selectOne(queryWrapper);
        item.setFileList(fileService.getListByTypeAndId(BusinessTaskType.ENVIRONMENT,item.getId()));
        return item;
    }

    @Override
    public boolean setAccidentByTaskItemId(String id) throws IOException {
        TaskDetailItem item = getById(id);
        // 构建AccidentInfo
        return accidentInfoService.saveByTaskDetailItem(item);
    }

    @Override
    public TaskDetailItem getItem(String id) {
        TaskDetailItem item = getById(id);
        item.setFileList(fileService.getListByTypeAndId(BusinessTaskType.ENVIRONMENT,item.getId()));
        return item;
    }

    private QueryWrapper<TaskDetailItem> queryWrapperWithTaskId(String taskId){
        QueryWrapper<TaskDetailItem> queryWrapper = new QueryWrapper<>();
        queryWrapper.eq("task_id",taskId);
        queryWrapper.orderByAsc("item_sort","basic_info");
        return queryWrapper;
    }


}
