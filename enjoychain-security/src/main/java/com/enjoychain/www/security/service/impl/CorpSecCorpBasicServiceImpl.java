package com.enjoychain.www.security.service.impl;

import com.enjoychain.www.security.entity.*;
import com.enjoychain.www.security.mapper.CorpSecCorpBasicMapper;
import com.enjoychain.www.security.service.ICorpSecBigRiskInfoService;
import com.enjoychain.www.security.service.ICorpSecCorpBasicService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.enjoychain.www.security.service.ICorpSecEquipInfoService;
import com.enjoychain.www.security.service.impl.task.TaskSecConverts;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

import static com.enjoychain.www.security.service.impl.task.TaskSecConverts.convertTaskSecBigRiskToCorpSecBigRiskInfoWithList;
import static com.enjoychain.www.security.service.impl.task.TaskSecConverts.convertTaskSecEquipInfoToCorpSecEquipInfoWithList;

/**
 * <p>
 * 安全生产基础信息 服务实现类
 * </p>
 *
 * @author ddmonk
 * @since 2023-04-14
 */
@Service
public class CorpSecCorpBasicServiceImpl extends ServiceImpl<CorpSecCorpBasicMapper, CorpSecCorpBasic> implements ICorpSecCorpBasicService {

    @Autowired
    ICorpSecBigRiskInfoService corpSecBigRiskInfoService;

    @Autowired
    ICorpSecEquipInfoService corpSecEquipInfoService;

    @Override
    @Transactional
    public void saveOrUpdateDetail(TaskCorpSecBasicInfo secBasicInfo) {
        CorpSecCorpBasic secCorpBasic = TaskSecConverts.convertTaskSecCorpBasicToSecBasicInfo(secBasicInfo);
        this.saveOrUpdate(secCorpBasic);
        corpSecBigRiskInfoService.removeByCorpId(secCorpBasic.getId());
        List<CorpSecBigRiskInfo> corpSecBigRiskInfoList = convertTaskSecBigRiskToCorpSecBigRiskInfoWithList(secBasicInfo.getBigRiskInfoList());
        corpSecBigRiskInfoService.saveOrUpdateBatch(corpSecBigRiskInfoList);

        corpSecEquipInfoService.removeByCorpId(secCorpBasic.getId());
        List<CorpSecEquipInfo> corpSecEquipInfos = convertTaskSecEquipInfoToCorpSecEquipInfoWithList(secBasicInfo.getEquipInfos());
        corpSecEquipInfoService.saveOrUpdateBatch(corpSecEquipInfos);


    }

    @Override
    @Transactional
    public boolean saveOrUpdateDetail(CorpSecCorpBasic secBasicInfo) {
        //重大事故
        corpSecBigRiskInfoService.removeByCorpId(secBasicInfo.getId());
        for (CorpSecBigRiskInfo bigRiskInfo : secBasicInfo.getBigRisks()){
            bigRiskInfo.setCorpId(secBasicInfo.getId());
        }
        corpSecBigRiskInfoService.saveOrUpdateBatch(secBasicInfo.getBigRisks());

        //设备
        corpSecEquipInfoService.removeByCorpId(secBasicInfo.getId());
        for (CorpSecEquipInfo equipInfo: secBasicInfo.getEquipInfos()){
            equipInfo.setCorpId(secBasicInfo.getId());
        }
        corpSecEquipInfoService.saveOrUpdateBatch(secBasicInfo.getEquipInfos());

        return this.saveOrUpdate(secBasicInfo);
    }

    @Override
    public void inital(CorpBasicInfo basicInfo) {
        CorpSecCorpBasic secCorpBasic = new CorpSecCorpBasic();
        secCorpBasic.setId(basicInfo.getId());
        secCorpBasic.setCorpAddress(basicInfo.getAddress());
        secCorpBasic.setCorpName(basicInfo.getName());
        secCorpBasic.setCertNo(basicInfo.getCertNo());
        secCorpBasic.setMajorName(basicInfo.getJuridical());
        this.save(secCorpBasic);
    }

    @Override
    public CorpSecCorpBasic getDetailById(String id) {
        CorpSecCorpBasic secCorpBasic = this.getById(id);
        secCorpBasic.setBigRisks(corpSecBigRiskInfoService.listByCorpId(id));
        secCorpBasic.setEquipInfos(corpSecEquipInfoService.listByCorpId(id));
        return secCorpBasic;
    }
}
