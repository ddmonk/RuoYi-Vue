// 环保业务基础信息
import request from '@/utils/request'

export function listBasic(query) {
  return request({
    url: '/security/corpEpBasic/list',
    method: 'get',
    params: query
  })
}

export function getBasic(id) {
  return request({
    url: '/security/corpEpBasic/' + id,
    method: 'get'
  })
}

export function addBasic(data) {
  return request({
    url: '/security/corpEpBasic',
    method: 'post',
    data: data
  })
}

export function updateBasic(data) {
  return request({
    url: '/security/corpEpBasic',
    method: 'put',
    data: data
  })
}

export function delBasic(id) {
  return request({
    url: '/security/corpEpBasic/' + id,
    method: 'delete'
  })
}

export function importBasic(data) {
  return request({
    url: '/security/corpEpBasic/importData',
    method: 'post',
    data: data
  })
}
