package com.enjoychain.www.security.service;

import com.enjoychain.www.security.entity.CorpBasicInfo;
import com.enjoychain.www.security.entity.TaskCorpEnvBasicInfo;
import com.enjoychain.www.security.pojo.EnvTaskDetail;
import com.enjoychain.www.security.pojo.TaskPojo;

public interface ITaskEnvService {

    void buildEnvTask(CorpBasicInfo basicInfo, String taskId);


    TaskCorpEnvBasicInfo getCorpEnvBasicInfoByTaskId(String taskId);

    EnvTaskDetail getEnvTaskDetailByTaskId(String taskId);

    void removeByTaskId(String taskId);

    void saveEnvCorpBasicInfo(TaskPojo taskPojo);

    void saveEnvTaskDetail(TaskPojo taskPojo);



}
