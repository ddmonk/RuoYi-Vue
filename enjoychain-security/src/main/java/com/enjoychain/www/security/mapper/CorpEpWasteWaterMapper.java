package com.enjoychain.www.security.mapper;

import com.enjoychain.www.security.entity.CorpEpWasteWater;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * 企业废水情况 Mapper 接口
 * </p>
 *
 * @author ddmonk
 * @since 2023-01-13
 */
public interface CorpEpWasteWaterMapper extends BaseMapper<CorpEpWasteWater> {

}
