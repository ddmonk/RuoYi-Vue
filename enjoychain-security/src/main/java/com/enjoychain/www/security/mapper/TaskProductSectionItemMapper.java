package com.enjoychain.www.security.mapper;

import com.enjoychain.www.security.entity.TaskProductSectionItem;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * 任务_工段明细 Mapper 接口
 * </p>
 *
 * @author ddmonk
 * @since 2023-02-24
 */
public interface TaskProductSectionItemMapper extends BaseMapper<TaskProductSectionItem> {

}
