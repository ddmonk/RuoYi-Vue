package com.enjoychain.www.security.service;

import com.enjoychain.www.security.entity.CorpEpEia;
import com.baomidou.mybatisplus.extension.service.IService;

import java.util.List;

/**
 * <p>
 * 环评情况表 服务类
 * </p>
 *
 * @author ddmonk
 * @since 2023-01-13
 */
public interface ICorpEpEiaService extends IService<CorpEpEia> {

    List<CorpEpEia> listByCorpId(String corpId);

    void saveOrUpdateBatch(List<CorpEpEia> reportItems, String id);

    void deleteByCorpId(String id);
}
