package com.enjoychain.www.security.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.enjoychain.www.security.entity.task.TaskBasic;
import com.enjoychain.www.security.mapper.TaskBasicMapper;
import com.enjoychain.www.security.service.ITaskBasicService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 巡查任务基础信息 服务实现类
 * </p>
 *
 * @author ddmonk
 * @since 2023-01-20
 */
@Service
public class TaskBasicServiceImpl extends ServiceImpl<TaskBasicMapper, TaskBasic> implements ITaskBasicService {


    @Override
    public TaskBasic getBasicByTaskId(String taskId) {
        return baseMapper.selectOne(queryWrapperWithTaskId(taskId));
    }

    @Override
    public void removeByTaskId(String taskId) {
        baseMapper.delete(queryWrapperWithTaskId(taskId));
    }


    /**
     * 构建基于taskId的QueryWrapper
     * @param taskId taskId(String)
     * @return
     */
    private QueryWrapper<TaskBasic> queryWrapperWithTaskId(String taskId){
        QueryWrapper<TaskBasic> queryWrapper = new QueryWrapper<>();
        queryWrapper.eq("task_id",taskId);
        return queryWrapper;
    }


}
