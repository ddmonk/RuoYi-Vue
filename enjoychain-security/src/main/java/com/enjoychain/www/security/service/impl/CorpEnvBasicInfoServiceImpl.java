package com.enjoychain.www.security.service.impl;

import com.enjoychain.www.security.constant.Constant;
import com.enjoychain.www.security.entity.CorpBasicInfo;
import com.enjoychain.www.security.entity.CorpEnvBasicInfo;
import com.enjoychain.www.security.entity.TaskCorpEnvBasicInfo;
import com.enjoychain.www.security.mapper.CorpEnvBasicInfoMapper;
import com.enjoychain.www.security.service.*;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.enjoychain.www.security.service.impl.task.TaskEnvConverts;
import com.ruoyi.common.constant.UserConstants;
import com.ruoyi.common.core.domain.entity.SysUser;
import com.ruoyi.common.utils.SecurityUtils;
import com.ruoyi.system.service.ISysUserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * <p>
 * 环保企业信息 服务实现类
 * </p>
 *
 * @author ddmonk
 * @since 2023-02-21
 */
@Service
public class CorpEnvBasicInfoServiceImpl extends ServiceImpl<CorpEnvBasicInfoMapper, CorpEnvBasicInfo> implements ICorpEnvBasicInfoService {

    @Autowired
    private IHwItemService hwItemService;

    @Autowired
    private ISolidWasteItemService solidWasteItemService;

    @Autowired
    private ICorpEpEquipInfoService epEquipInfoService;

    @Autowired
    private ICorpEpEiaService epEiaService;

    @Autowired
    private ICorpEpGasEquipmentService gasEquipmentService;

    @Autowired
    private ISysUserService userService;



    @Override
    public CorpEnvBasicInfo getDetailById(String id) {
        CorpEnvBasicInfo info = getById(id);
        info.setHwItems(hwItemService.listByCorpId(id));
        info.setSolidWasteItems(solidWasteItemService.listByCorpId(id));
        info.setEquipItems(epEquipInfoService.listWithStatus(null,id));
        info.setReportItems(epEiaService.listByCorpId(id));
        info.setGasEquipmentItems(gasEquipmentService.listWithStatus(null,id));
        return info;
    }

    @Override
    @Transactional
    public boolean removeBasicInfoByIds(List<String> asList) {
        boolean aRes = true;
        for (String id : asList){
            aRes = aRes && removeBasicInfoById(id);
        }
        return aRes;
    }


    public boolean removeBasicInfoById(String id){
        hwItemService.deleteByCorpId(id);
        solidWasteItemService.deleteByCorpId(id);
        epEiaService.deleteByCorpId(id);
        epEquipInfoService.deleteByCorpId(id);
        gasEquipmentService.deleteByCorpId(id);
        return this.removeById(id);
    }

    @Override
    public void saveOrUpdateDetail(TaskCorpEnvBasicInfo taskCorpEnvBasicInfo) {
        CorpEnvBasicInfo corpInfo = TaskEnvConverts.convertTaskEnvBasicInfoToCorpEnvBasicInfo(taskCorpEnvBasicInfo);
        //1、保存现有数据
        saveOrUpdateDetail(corpInfo);
    }

    @Override
    @Transactional
    public boolean saveOrUpdateDetail(CorpEnvBasicInfo corpInfo) {
        hwItemService.deleteByCorpId(corpInfo.getId());
        hwItemService.saveOrUpdateBatch(corpInfo.getHwItems(),corpInfo.getId());

        solidWasteItemService.deleteByCorpId(corpInfo.getId());
        solidWasteItemService.saveOrUpdateBatch(corpInfo.getSolidWasteItems(),corpInfo.getId());

        epEquipInfoService.deleteByCorpId(corpInfo.getId());
        epEquipInfoService.saveOrUpdateBatch(corpInfo.getEquipItems(),corpInfo.getId());

        epEiaService.deleteByCorpId(corpInfo.getId());
        epEiaService.saveOrUpdateBatch(corpInfo.getReportItems(),corpInfo.getId());

        gasEquipmentService.deleteByCorpId(corpInfo.getId());
        gasEquipmentService.saveOrUpdateBatch(corpInfo.getGasEquipmentItems(),corpInfo.getId());
        return this.saveOrUpdate(corpInfo);
    }

    @Override
    public CorpEnvBasicInfo inital(CorpBasicInfo basicInfo) {
        CorpEnvBasicInfo corpEnvBasicInfo = new CorpEnvBasicInfo();
        corpEnvBasicInfo.setCertNo(basicInfo.getCertNo());
        corpEnvBasicInfo.setId(basicInfo.getId());
        corpEnvBasicInfo.setCorpAddress(basicInfo.getAddress());
        corpEnvBasicInfo.setCorpName(basicInfo.getName());
        corpEnvBasicInfo.setJuridicalName(basicInfo.getJuridical());
        this.save(corpEnvBasicInfo);
        corpEnvInitialHook(basicInfo);
        return corpEnvBasicInfo;
    }

    private void corpEnvInitialHook(CorpBasicInfo basicInfo) {
        SysUser user = new SysUser();
        user.setUserName(basicInfo.getName());
        user.setNickName(basicInfo.getName());
        user.setRoleIds(new Long[]{4L});
        user.setDeptId(102L);
        user.setPassword(Constant.DEFAULT_PASSWORD);
        user.setCompanyIds(basicInfo.getId());
        if (UserConstants.NOT_UNIQUE.equals(userService.checkUserNameUnique(user)))
        {
            return ;
        }
        user.setPassword(SecurityUtils.encryptPassword(user.getPassword()));
        userService.insertUser(user);
    }
}
