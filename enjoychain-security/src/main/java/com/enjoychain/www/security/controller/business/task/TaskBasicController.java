package com.enjoychain.www.security.controller.business.task;

import org.springframework.web.bind.annotation.RequestMapping;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.common.utils.StringUtils;
import com.ruoyi.common.utils.poi.ExcelUtil;
import io.swagger.annotations.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import javax.servlet.http.HttpServletResponse;
import java.util.List;
import java.util.Arrays;
import com.enjoychain.www.security.entity.task.TaskBasic;
import com.enjoychain.www.security.service.ITaskBasicService;
import org.springframework.web.bind.annotation.RestController;

/**
 * <p>
 * 巡查任务基础信息 前端控制器
 * </p>
 *
 * @author ddmonk
 * @since 2023-01-20
 */
@Api(tags = "巡查任务基础信息")
@RestController
@RequestMapping("/security/taskBasic")
public class TaskBasicController {

    @Autowired
    ITaskBasicService service;

//    @PreAuthorize("@ss.hasPermi('security:task-basic:list')")
//    @GetMapping(path = "/list", produces = {"application/json"})
//    @ApiOperation(value = "获取巡查任务基础信息列表")
//    @ApiImplicitParams({
//            @ApiImplicitParam(name = "corpId", value = "企业Id",paramType = "query", required = false, dataType = "String"),
//            @ApiImplicitParam(name = "pageNo", value = "页码", paramType = "query", dataType = "long"),
//            @ApiImplicitParam(name = "pageSize", value = "页面记录条数", paramType = "query", dataType = "long")
//    })
//    @ApiResponses({
//        @ApiResponse(code = 200, message = "ok", response= TaskBasic.class),
//    })
//    public AjaxResult list(
//                            @RequestParam(name = "corpId", required = false) String corpId,
//                            @RequestParam(name = "pageNo", defaultValue = "1") long pageNo,
//                            @RequestParam(name = "pageSize",defaultValue = "10") long pageSize){
//        IPage<TaskBasic> entityIPage = new Page<>(pageNo, pageSize);
//        QueryWrapper<TaskBasic> queryWrapper = new QueryWrapper<>();
//        if (StringUtils.isNotEmpty(corpId)) {
//            queryWrapper.eq("corp_id",corpId);
//        }
//        return AjaxResult.success("list-task_basic",service.page(entityIPage,queryWrapper));
//    }

    @PreAuthorize("@ss.hasPermi('security:task-basic:query')")
    @GetMapping(value = "{id}")
    @ApiOperation("获取巡查任务基础信息详情")
    @ApiImplicitParams({
        @ApiImplicitParam(name = "id", value = "id",paramType = "path", dataType = "String")
    })
    @ApiResponses({
        @ApiResponse(code = 200, message = "ok", response = TaskBasic.class)
    })
    public AjaxResult getTaskBasicInfo(@PathVariable(value = "id", required = false) String id)
    {
        return AjaxResult.success("get-task_basic",service.getById(id));
    }

//    @PreAuthorize("@ss.hasPermi('security:task-basic:import')")
//    @ApiOperation("导入巡查任务基础信息")
//    @Log(title = "巡查任务基础信息", businessType = BusinessType.IMPORT)
//    @PostMapping("/importData")
//    public AjaxResult importData(MultipartFile file) throws Exception{
//        ExcelUtil<TaskBasic> util = new ExcelUtil<TaskBasic>(TaskBasic.class);
//        List<TaskBasic> infos = util.importExcel(file.getInputStream());
//        return service.saveOrUpdateBatch(infos)?AjaxResult.success("批量插入成功"):AjaxResult.error("批量插入失败");
//    }


//    @ApiOperation("巡查任务基础信息导出模板下载")
//    @PostMapping("/importTemplate")
//    public void importTemplate(HttpServletResponse response){
//        ExcelUtil<TaskBasic> util = new ExcelUtil<TaskBasic>(TaskBasic.class);
//        util.importTemplateExcel(response,"巡查任务基础信息");
//    }
//
//    @PreAuthorize("@ss.hasPermi('security:task-basic:add')")
//    @PostMapping(consumes = {"application/json"}, produces = {"application/json"})
//    @ApiOperation("新增巡查任务基础信息")
//    @ApiResponses({
//        @ApiResponse(code = 200, message = "ok", response = TaskBasic.class),
//    })
//    @Log(title = "巡查任务基础信息", businessType = BusinessType.INSERT)
//    public AjaxResult add(@RequestBody TaskBasic entity){
//        return service.save(entity)? AjaxResult.success("add-task_basic",entity): AjaxResult.error("新增巡查任务基础信息失败");
//    }


    @PreAuthorize("@ss.hasPermi('security:task-basic:update')")
    @PutMapping(consumes = {"application/json"}, produces = {"application/json"})
    @ApiOperation("修改巡查任务基础信息")
    @ApiResponses({
        @ApiResponse(code = 200, message = "ok", response = TaskBasic.class)
    })
    @Log(title = "巡查任务基础信息", businessType = BusinessType.UPDATE)
    public AjaxResult update(@RequestBody TaskBasic corpBasicInfo){
        String operate = "update-task_basic";
        return service.updateById(corpBasicInfo)?AjaxResult.success(operate,service.getById(corpBasicInfo.getId())) : AjaxResult.error("修改客户基础信息失败");
    }

    @PreAuthorize("@ss.hasPermi('security:task-basic:delete')")
    @DeleteMapping(value = "{id}", produces = {"application/json"})
    @ApiOperation("删除巡查任务基础信息")
    @ApiResponses({
        @ApiResponse(code = 200, message = "ok", response = AjaxResult.class),
    })
    @Log(title = "巡查任务基础信息", businessType = BusinessType.DELETE)
    public AjaxResult delete(@PathVariable(value = "id", required = false) String id){
        String operate = "delete-task_basic";
        String[] ids = id.split(",");
        return service.removeBatchByIds(Arrays.asList(ids))?AjaxResult.success(operate) : AjaxResult.error("修改巡查任务基础信息失败");
    }


//    @Log(title = "巡查任务基础信息", businessType = BusinessType.EXPORT)
//    @PreAuthorize("@ss.hasPermi('security:task-basic:export')")
//    @PostMapping("/export")
//    @ApiOperation("导出巡查任务基础信息")
//    @ApiResponses({
//        @ApiResponse(code = 200, message = "ok", response = AjaxResult.class),
//    })
//    public void export(HttpServletResponse response,@RequestParam(name = "corpId", required = false) String corpId){
//        QueryWrapper<TaskBasic> queryWrapper = new QueryWrapper<>();
//        if (StringUtils.isNotEmpty(corpId)) {
//            queryWrapper.eq("corp_id",corpId);
//        }
//        List<TaskBasic> list = service.list(queryWrapper);
//        ExcelUtil<TaskBasic> util = new ExcelUtil<TaskBasic>(TaskBasic.class);
//        util.exportExcel(response,list, "导出巡查任务基础信息");
//    }
}
