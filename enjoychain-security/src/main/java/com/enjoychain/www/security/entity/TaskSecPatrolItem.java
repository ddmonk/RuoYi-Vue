package com.enjoychain.www.security.entity;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableName;
import java.io.Serializable;
import java.util.List;

import com.ruoyi.common.annotation.Excel;
import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;
import lombok.experimental.Accessors;

/**
 * <p>
 * 安全核查基础信息表
 * </p>
 *
 * @author ddmonk
 * @since 2023-04-18
 */
@Getter
@Setter
@Accessors(chain = true)
@TableName("task_sec_patrol_item")
@ApiModel(value = "TaskSecPatrolItem对象", description = "安全核查基础信息表")
public class TaskSecPatrolItem implements Serializable {

    private static final long serialVersionUID = 1L;

    @ApiModelProperty("主键id")
    @Excel(name = "主键id")
    private String id;

    @ApiModelProperty("任务id")
    @Excel(name = "任务id")
    private String taskId;

    @ApiModelProperty("业务大类")
    @Excel(name = "业务大类")
    private String classType;

    @ApiModelProperty("核查要点")
    @Excel(name = "核查要点")
    private String point;

    @ApiModelProperty("核查内容")
    @Excel(name = "核查内容")
    private String content;

    @ApiModelProperty("核查结果")
    @Excel(name = "核查结果")
    private String result;

    @ApiModelProperty("默认值")
    @Excel(name = "默认值")
    private String defaultValue;

    @ApiModelProperty("核查记录")
    @Excel(name = "核查记录")
    private String advice;

    @ApiModelProperty("备注")
    @Excel(name = "备注")
    private String remark;

    @ApiModelProperty("序号")
    @Excel(name = "序号")
    private Integer orderNum;

    private Integer parentId;


    private String fieldName;

    private String description;

    @TableField(exist = false)
    private List<BasicFile> fileList;


}
