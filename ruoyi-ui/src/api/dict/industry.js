import request from '@/utils/request'

// 查询国民经济行业分类列表
export function listIndustry(query) {
  return request({
    url: '/security/industry/list',
    method: 'get',
    params: query
  })
}

// 查询国民经济行业分类详细
export function getIndustry(id) {
  return request({
    url: '/security/industry/' + id,
    method: 'get'
  })
}

// 新增国民经济行业分类
export function addIndustry(data) {
  return request({
    url: '/security/industry',
    method: 'post',
    data: data
  })
}

// 修改国民经济行业分类
export function updateIndustry(data) {
  return request({
    url: '/security/industry',
    method: 'put',
    data: data
  })
}

// 删除国民经济行业分类
export function delIndustry(id) {
  return request({
    url: '/security/industry/' + id,
    method: 'delete'
  })
}
