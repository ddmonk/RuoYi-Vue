package com.enjoychain.www.security.entity;

import com.baomidou.mybatisplus.annotation.TableName;
import java.io.Serializable;
import java.time.LocalDateTime;
import com.ruoyi.common.annotation.Excel;
import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;
import lombok.experimental.Accessors;

/**
 * <p>
 * 
 * </p>
 *
 * @author ddmonk
 * @since 2023-03-08
 */
@Getter
@Setter
@Accessors(chain = true)
@TableName("task_ep_solid_waste_item")
@ApiModel(value = "TaskEpSolidWasteItem对象", description = "")
public class TaskEpSolidWasteItem implements Serializable {

    private static final long serialVersionUID = 1L;

    private String id;

    @ApiModelProperty("任务ID")
    @Excel(name = "任务ID")
    private String taskId;

    @ApiModelProperty("企业id")
    @Excel(name = "企业id")
    private String corpId;

    @ApiModelProperty("一般固废名称")
    @Excel(name = "一般固废名称")
    private String solidName;

    @ApiModelProperty("数量")
    @Excel(name = "数量")
    private String solidNum;

    @ApiModelProperty("状态")
    @Excel(name = "状态")
    private String status;

    @ApiModelProperty("到期时间")
    @Excel(name = "到期时间", width = 30, dateFormat = "yyyy-MM-dd")
    private LocalDateTime deadline;

    @ApiModelProperty("备注")
    @Excel(name = "备注")
    private String remark;


}
