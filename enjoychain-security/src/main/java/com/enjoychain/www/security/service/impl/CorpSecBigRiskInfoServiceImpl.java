package com.enjoychain.www.security.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.enjoychain.www.security.entity.CorpSecBigRiskInfo;
import com.enjoychain.www.security.mapper.CorpSecBigRiskInfoMapper;
import com.enjoychain.www.security.service.ICorpSecBigRiskInfoService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * <p>
 * 较大以上安全风险信息 服务实现类
 * </p>
 *
 * @author ddmonk
 * @since 2023-01-13
 */
@Service
public class CorpSecBigRiskInfoServiceImpl extends ServiceImpl<CorpSecBigRiskInfoMapper, CorpSecBigRiskInfo> implements ICorpSecBigRiskInfoService {


    private QueryWrapper<CorpSecBigRiskInfo> buildQueryByCorpId(String corpId){
        QueryWrapper<CorpSecBigRiskInfo> queryWrapper = new QueryWrapper<>();
        queryWrapper.eq("corp_id",corpId);
        return queryWrapper;
    };
    @Override
    public boolean removeByCorpId(String corpId) {
        return remove(buildQueryByCorpId(corpId));
    }

    @Override
    public List<CorpSecBigRiskInfo> listByCorpId(String corpId) {
        return list(buildQueryByCorpId(corpId));
    }
}
