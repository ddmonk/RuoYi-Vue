package com.enjoychain.www.security.mapper;

import com.enjoychain.www.security.entity.CorpBasicInfo;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * 客户基础信息表 Mapper 接口
 * </p>
 *
 * @author baomidou
 * @since 2022-12-30
 */
public interface CorpBasicInfoMapper extends BaseMapper<CorpBasicInfo> {

}
