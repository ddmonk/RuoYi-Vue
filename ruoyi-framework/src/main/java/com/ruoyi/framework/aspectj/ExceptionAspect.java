//package com.ruoyi.framework.aspectj;
//
//import com.ruoyi.common.exception.ServiceException;
//import lombok.extern.slf4j.Slf4j;
//import org.springframework.http.HttpStatus;
//import org.springframework.http.ResponseEntity;
//import org.springframework.web.bind.annotation.ExceptionHandler;
//import org.springframework.web.bind.annotation.RestControllerAdvice;
//
//import static org.springframework.http.HttpStatus.BAD_REQUEST;
//
//@RestControllerAdvice
//@Slf4j
//public class ExceptionAspect {
//
//    @ExceptionHandler()
//    public ResponseEntity<String> exceptionHandler(ServiceException exception){
//        if (null != exception.getCode()){
//            return new ResponseEntity<>(exception.getMessage(), HttpStatus.resolve(exception.getCode()));
//        } else {
//            return new ResponseEntity<>(exception.getMessage(),BAD_REQUEST);
//        }
//    }
//
//}
