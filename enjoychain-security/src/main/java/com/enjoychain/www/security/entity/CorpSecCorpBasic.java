package com.enjoychain.www.security.entity;

import com.baomidou.mybatisplus.annotation.*;

import java.io.Serializable;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.List;

import com.ruoyi.common.annotation.Excel;
import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;
import lombok.experimental.Accessors;
import nonapi.io.github.classgraph.json.Id;

import static com.enjoychain.www.security.constant.Constant.DATE_FORMATTER;
import static com.enjoychain.www.security.constant.Constant.REPORT_DEFAULT_VALUE;

/**
 * <p>
 * 安全生产基础信息
 * </p>
 *
 * @author ddmonk
 * @since 2023-04-14
 */
@Getter
@Setter
@Accessors(chain = true)
@TableName("corp_sec_corp_basic")
@ApiModel(value = "CorpSecCorpBasic对象", description = "安全生产基础信息")
public class CorpSecCorpBasic implements Serializable {

    private static final long serialVersionUID = 1L;

    @ApiModelProperty("公司id")
    @Excel(name = "公司id")
    @TableId(value = "id", type = IdType.AUTO)
    private String id;

    @ApiModelProperty("公司名称")
    @Excel(name = "公司名称")
    private String corpName;

    @ApiModelProperty("统一社会信用代码")
    @Excel(name = "统一社会信用代码")
    private String certNo;

    @ApiModelProperty("注册地址")
    @Excel(name = "注册地址")
    private String corpAddress;

    @ApiModelProperty("行政区域")
    @Excel(name = "行政区域")
    private String adminArea;

    @ApiModelProperty("企业规模")
    @Excel(name = "企业规模")
    private String corpScale;

    @ApiModelProperty("行业分类")
    @Excel(name = "行业分类")
    private String businessType;

    @ApiModelProperty("主要负责人")
    @Excel(name = "主要负责人")
    private String majorName;

    @ApiModelProperty("安全负责人")
    @Excel(name = "安全负责人")
    private String safeName;

    @ApiModelProperty("职工人数")
    @Excel(name = "职工人数")
    private Integer staffNum;

    @ApiModelProperty("安全管理机构")
    @Excel(name = "安全管理机构")
    private String safetyManagementOrg;

    @ApiModelProperty("安全生产标准化")
    @Excel(name = "安全生产标准化")
    private String safetyStandard;

    @ApiModelProperty("重点行业领域")
    @Excel(name = "重点行业领域")
    private String vipIndustryArea;

    @ApiModelProperty("备注")
    @Excel(name = "备注")
    private String remark;

    @ApiModelProperty("主要生产工艺")
    @Excel(name = "主要生产工艺")
    private String productProcess;

    @ApiModelProperty("应急预案状态")
    @Excel(name = "应急预案状态")
    private String mergencyStatus;

    @ApiModelProperty("应急预案备案状态")
    @Excel(name = "应急预案备案状态")
    private String mergencyRecordStatus;

    @ApiModelProperty("应急预案备案时间")
    @Excel(name = "应急预案备案时间", width = 30, dateFormat = "yyyy-MM-dd")
    private LocalDateTime mergencyRecordTime;

    @TableField(exist = false)
    private String mergencyRecordTimeString ;

    public void setMergencyRecordTime(LocalDateTime mergencyRecordTime) {
        this.mergencyRecordTime = mergencyRecordTime;
        this.mergencyRecordTimeString = this.mergencyRecordTime == null ? REPORT_DEFAULT_VALUE : mergencyRecordTime.format(DateTimeFormatter.ofPattern(DATE_FORMATTER));
    }

    @ApiModelProperty("是否建立双重预防机制")
    @Excel(name = "是否建立双重预防机制")
    private String dualPreventionMechanismStatus;


    @ApiModelProperty("应急预案定期演习情况")
    @Excel(name = "应急预案定期演习情况")
    private String mergencyRegularExercise;

    @ApiModelProperty(value = "创建时间", hidden = true)
    @TableField(fill = FieldFill.INSERT)
    private LocalDateTime createTime;

    @ApiModelProperty(value = "修改时间", hidden = true)
    @TableField(fill = FieldFill.INSERT_UPDATE)
    private LocalDateTime updateTime;

    @ApiModelProperty(value = "创建人员", hidden = true)
    @TableField(fill = FieldFill.INSERT)
    private String createBy;

    @ApiModelProperty(value = "修改人员", hidden = true)
    @TableField(fill = FieldFill.INSERT_UPDATE)
    private String modifyBy;
    @TableField(exist = false)
    private List<CorpSecBigRiskInfo> bigRisks;

    @TableField(exist = false)
    private List<CorpSecEquipInfo> equipInfos;


}
