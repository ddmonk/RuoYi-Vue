package com.enjoychain.www.security.mapper;

import com.enjoychain.www.security.entity.CorpEpEia;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * 环评情况表 Mapper 接口
 * </p>
 *
 * @author ddmonk
 * @since 2023-01-13
 */
public interface CorpEpEiaMapper extends BaseMapper<CorpEpEia> {

}
