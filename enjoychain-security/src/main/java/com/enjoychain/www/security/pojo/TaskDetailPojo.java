package com.enjoychain.www.security.pojo;

import com.enjoychain.www.security.entity.task.TaskBasic;
import com.enjoychain.www.security.entity.task.TaskDetailItem;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import java.util.List;

@Data
public class TaskDetailPojo {

    @ApiModelProperty("基础信息")
    private TaskBasic basic;

    @ApiModelProperty("巡查详情信息")
    private List<TaskDetailItem> details;

}
