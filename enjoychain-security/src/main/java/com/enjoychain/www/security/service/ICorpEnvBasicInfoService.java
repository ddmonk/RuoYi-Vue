package com.enjoychain.www.security.service;

import com.enjoychain.www.security.entity.CorpBasicInfo;
import com.enjoychain.www.security.entity.CorpEnvBasicInfo;
import com.baomidou.mybatisplus.extension.service.IService;
import com.enjoychain.www.security.entity.TaskCorpEnvBasicInfo;

import java.util.List;

/**
 * <p>
 * 环保企业信息 服务类
 * </p>
 *
 * @author ddmonk
 * @since 2023-02-21
 */
public interface ICorpEnvBasicInfoService extends IService<CorpEnvBasicInfo> {

    CorpEnvBasicInfo getDetailById(String id);

    boolean removeBasicInfoByIds(List<String> asList);

    void saveOrUpdateDetail(TaskCorpEnvBasicInfo corpInfo);

    boolean saveOrUpdateDetail(CorpEnvBasicInfo corpBasicInfo);

    CorpEnvBasicInfo inital(CorpBasicInfo basicInfo);
}
