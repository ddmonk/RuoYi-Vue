package com.enjoychain.www.security.pojo;

import lombok.Data;

@Data
public class TaskBuilderPojo {

    private String jobId;

    private String corpId;

}
