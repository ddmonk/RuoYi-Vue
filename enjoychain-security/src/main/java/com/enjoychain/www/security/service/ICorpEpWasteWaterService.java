package com.enjoychain.www.security.service;

import com.enjoychain.www.security.entity.CorpEpWasteWater;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 * 企业废水情况 服务类
 * </p>
 *
 * @author ddmonk
 * @since 2023-01-13
 */
public interface ICorpEpWasteWaterService extends IService<CorpEpWasteWater> {

}
