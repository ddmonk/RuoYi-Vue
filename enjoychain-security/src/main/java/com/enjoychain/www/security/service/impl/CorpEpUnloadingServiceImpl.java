package com.enjoychain.www.security.service.impl;

import com.enjoychain.www.security.entity.CorpEpUnloading;
import com.enjoychain.www.security.mapper.CorpEpUnloadingMapper;
import com.enjoychain.www.security.service.ICorpEpUnloadingService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 排污许可证申领情况 服务实现类
 * </p>
 *
 * @author ddmonk
 * @since 2023-01-13
 */
@Service
public class CorpEpUnloadingServiceImpl extends ServiceImpl<CorpEpUnloadingMapper, CorpEpUnloading> implements ICorpEpUnloadingService {

}
