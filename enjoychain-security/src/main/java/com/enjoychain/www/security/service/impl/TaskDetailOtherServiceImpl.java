package com.enjoychain.www.security.service.impl;

import com.enjoychain.www.security.entity.TaskDetailOther;
import com.enjoychain.www.security.mapper.TaskDetailOtherMapper;
import com.enjoychain.www.security.service.ITaskDetailOtherService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author ddmonk
 * @since 2023-02-24
 */
@Service
public class TaskDetailOtherServiceImpl extends ServiceImpl<TaskDetailOtherMapper, TaskDetailOther> implements ITaskDetailOtherService {

}
