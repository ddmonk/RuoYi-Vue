package com.enjoychain.www.security.service;

import com.enjoychain.www.security.entity.SolidWasteItem;
import com.baomidou.mybatisplus.extension.service.IService;

import java.util.List;

/**
 * <p>
 * 一般固废详情 服务类
 * </p>
 *
 * @author ddmonk
 * @since 2023-02-21
 */
public interface ISolidWasteItemService extends IService<SolidWasteItem> {

    List<SolidWasteItem> listByCorpId(String id);

    void saveOrUpdateBatch(List<SolidWasteItem> solidWasteItems, String id);

    void deleteByCorpId(String id);
}
