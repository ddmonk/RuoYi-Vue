package com.enjoychain.www.security.service.impl;

import com.enjoychain.www.security.entity.TaskNoise;
import com.enjoychain.www.security.mapper.TaskNoiseMapper;
import com.enjoychain.www.security.service.ITaskNoiseService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 主要噪声源 服务实现类
 * </p>
 *
 * @author ddmonk
 * @since 2023-02-24
 */
@Service
public class TaskNoiseServiceImpl extends ServiceImpl<TaskNoiseMapper, TaskNoise> implements ITaskNoiseService {

}
