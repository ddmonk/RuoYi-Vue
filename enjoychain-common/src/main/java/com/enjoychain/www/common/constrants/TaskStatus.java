package com.enjoychain.www.common.constrants;

import com.baomidou.mybatisplus.annotation.EnumValue;
import com.fasterxml.jackson.annotation.JsonValue;

public enum TaskStatus {


    PLAN(0,"计划中"),

    DOING(1,"正在进行"),

    FINISH(2, "完成"),

    STOP(3, "终止");

    @EnumValue
    @JsonValue
    private int id;

    private String description;

    TaskStatus(int id, String description) {
        this.id = id;
        this.description = description;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }
}
