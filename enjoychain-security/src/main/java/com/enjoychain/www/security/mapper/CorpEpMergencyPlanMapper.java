package com.enjoychain.www.security.mapper;

import com.enjoychain.www.security.entity.CorpEpMergencyPlan;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * 应急预案情况 Mapper 接口
 * </p>
 *
 * @author ddmonk
 * @since 2023-01-13
 */
public interface CorpEpMergencyPlanMapper extends BaseMapper<CorpEpMergencyPlan> {

}
