import request from '@/utils/request'

export function uploadFile(data) {
  return request({
    url: '/security/basicFile/fileUpload',
    method: 'post',
    data: data
  })
}

export function uploadFiles(data) {
  return request({
    url: '/security/basicFile/batch/fileUpload',
    method: 'post',
    data: data
  })
}

export function deleteFiles(params) {
  return request({
    url: '/security/basicFile/batch/delete',
    method: 'delete',
    params: params
  })
}

export function deleteFile(id) {
  return request({
    url: '/security/basicFile/' + id,
    method: 'delete'
  })
}

