package com.enjoychain.www.security.controller.business.job;

import org.springframework.web.bind.annotation.RequestMapping;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.common.utils.StringUtils;
import com.ruoyi.common.utils.poi.ExcelUtil;
import io.swagger.annotations.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import javax.servlet.http.HttpServletResponse;
import java.util.List;
import java.util.UUID;
import java.util.Arrays;
import com.enjoychain.www.security.entity.JobBasicElement;
import com.enjoychain.www.security.service.IJobBasicElementService;
import org.springframework.web.bind.annotation.RestController;

/**
 * <p>
 * 核查任务要素信息 前端控制器
 * </p>
 *
 * @author ddmonk
 * @since 2023-01-30
 */
@Api(tags = "核查任务要素信息")
@RestController
@RequestMapping("/security/jobBasicElement")
public class JobBasicElementController {

    @Autowired
    IJobBasicElementService service;

    @PreAuthorize("@ss.hasPermi('job:basic-element:list')")
    @GetMapping(path = "/list", produces = {"application/json"})
    @ApiOperation(value = "获取核查任务要素信息列表")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "name", value = "名称",paramType = "query", required = false, dataType = "String"),
            @ApiImplicitParam(name = "type", value = "类型",paramType = "query", required = false, dataType = "int"),
            @ApiImplicitParam(name = "pageNum", value = "页码", paramType = "query", dataType = "long"),
            @ApiImplicitParam(name = "pageSize", value = "页面记录条数", paramType = "query", dataType = "long")
    })
    @ApiResponses({
        @ApiResponse(code = 200, message = "ok", response= JobBasicElement.class),
    })
    public AjaxResult list(
                            @RequestParam(name = "name", required = false) String name,
                            @RequestParam(name = "type", required = false) String type,
                            @RequestParam(name = "busiType", required = false) String busiType,
                            @RequestParam(name = "pageNum", defaultValue = "1") long pageNo,
                            @RequestParam(name = "pageSize",defaultValue = "10") long pageSize){
        IPage<JobBasicElement> entityIPage = new Page<>(pageNo, pageSize);
        QueryWrapper<JobBasicElement> queryWrapper = new QueryWrapper<>();
        if (StringUtils.isNotEmpty(name)) {
            queryWrapper.like("name",name);
        }
        if (StringUtils.isNotEmpty(type)){
            queryWrapper.eq("type",type);
        }
        if (StringUtils.isNotEmpty(busiType)){
            queryWrapper.eq("busi_type",busiType);
        }
        return AjaxResult.success("list-job_basic_element",service.page(entityIPage,queryWrapper));
    }

    @PreAuthorize("@ss.hasPermi('job:basic-element:query')")
    @GetMapping(value = "{id}")
    @ApiOperation("获取核查任务要素信息详情")
    @ApiImplicitParams({
        @ApiImplicitParam(name = "id", value = "id",paramType = "path", dataType = "String")
    })
    @ApiResponses({
        @ApiResponse(code = 200, message = "ok", response = JobBasicElement.class)
    })
    public AjaxResult getJobBasicElementInfo(@PathVariable(value = "id", required = false) String id)
    {
        return AjaxResult.success("get-job_basic_element",service.getById(id));
    }

    @PreAuthorize("@ss.hasPermi('job:basic-element:import')")
    @ApiOperation("导入核查任务要素信息")
    @Log(title = "核查任务要素信息", businessType = BusinessType.IMPORT)
    @PostMapping("/importData")
    public AjaxResult importData(MultipartFile file) throws Exception{
        ExcelUtil<JobBasicElement> util = new ExcelUtil<JobBasicElement>(JobBasicElement.class);
        List<JobBasicElement> infos = util.importExcel(file.getInputStream());
        return service.saveOrUpdateBatch(infos)?AjaxResult.success("批量插入成功"):AjaxResult.error("批量插入失败");
    }


    @ApiOperation("核查任务要素信息导出模板下载")
    @PostMapping("/importTemplate")
    public void importTemplate(HttpServletResponse response){
        ExcelUtil<JobBasicElement> util = new ExcelUtil<JobBasicElement>(JobBasicElement.class);
        util.importTemplateExcel(response,"核查任务要素信息");
    }

    @PreAuthorize("@ss.hasPermi('job:basic-element:add')")
    @PostMapping(consumes = {"application/json"}, produces = {"application/json"})
    @ApiOperation("新增核查任务要素信息")
    @ApiResponses({
        @ApiResponse(code = 200, message = "ok", response = JobBasicElement.class),
    })
    @Log(title = "核查任务要素信息", businessType = BusinessType.INSERT)
    public AjaxResult add(@RequestBody JobBasicElement entity){
        return service.save(entity)? AjaxResult.success("add-job_basic_element",entity): AjaxResult.error("新增核查任务要素信息失败");
    }


    @PreAuthorize("@ss.hasPermi('job:basic-element:update')")
    @PutMapping(consumes = {"application/json"}, produces = {"application/json"})
    @ApiOperation("修改核查任务要素信息")
    @ApiResponses({
        @ApiResponse(code = 200, message = "ok", response = JobBasicElement.class)
    })
    @Log(title = "核查任务要素信息", businessType = BusinessType.UPDATE)
    public AjaxResult update(@RequestBody JobBasicElement corpBasicInfo){
        String operate = "update-job_basic_element";
        return service.updateById(corpBasicInfo)?AjaxResult.success(operate,service.getById(corpBasicInfo.getId())) : AjaxResult.error("修改客户基础信息失败");
    }

    @PreAuthorize("@ss.hasPermi('job:basic-element:delete')")
    @DeleteMapping(value = "{id}", produces = {"application/json"})
    @ApiOperation("删除核查任务要素信息")
    @ApiResponses({
        @ApiResponse(code = 200, message = "ok", response = AjaxResult.class),
    })
    @Log(title = "核查任务要素信息", businessType = BusinessType.DELETE)
    public AjaxResult delete(@PathVariable(value = "id", required = false) String id){
        String operate = "delete-job_basic_element";
        String[] ids = id.split(",");
        return service.removeBatchByIds(Arrays.asList(ids))?AjaxResult.success(operate) : AjaxResult.error("修改核查任务要素信息失败");
    }


    @Log(title = "核查任务要素信息", businessType = BusinessType.EXPORT)
    @PreAuthorize("@ss.hasPermi('job:basic-element:export')")
    @PostMapping("/export")
    @ApiOperation("导出核查任务要素信息")
    @ApiResponses({
        @ApiResponse(code = 200, message = "ok", response = AjaxResult.class),
    })
    public void export(HttpServletResponse response,@RequestParam(name = "corpId", required = false) String corpId){
        QueryWrapper<JobBasicElement> queryWrapper = new QueryWrapper<>();
        if (StringUtils.isNotEmpty(corpId)) {
            queryWrapper.eq("corp_id",corpId);
        }
        List<JobBasicElement> list = service.list(queryWrapper);
        ExcelUtil<JobBasicElement> util = new ExcelUtil<JobBasicElement>(JobBasicElement.class);
        util.exportExcel(response,list, "导出核查任务要素信息");
    }


    @GetMapping("/list/all")
    public AjaxResult listAll(
            @RequestParam(name = "type", required = false) String type){
        QueryWrapper<JobBasicElement> queryWrapper = new QueryWrapper<>();
        if (StringUtils.isNotEmpty(type)) {
            queryWrapper.like("busi_type",type);
        }
        return AjaxResult.success("list-job_basic_element",service.list(queryWrapper));
    }

}
