package com.enjoychain.www.security.service;

import com.enjoychain.www.security.entity.TaskNoise;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 * 主要噪声源 服务类
 * </p>
 *
 * @author ddmonk
 * @since 2023-02-24
 */
public interface ITaskNoiseService extends ITaskService<TaskNoise> {

}
