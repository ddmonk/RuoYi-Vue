import request from '@/utils/request'

export function listWater(query) {
  return request({
    url: '/security/corpEpWasteWater/list',
    method: 'get',
    params: query
  })
}

export function getWater(id) {
  return request({
    url: '/security/corpEpWasteWater/' + id,
    method: 'get'
  })
}

export function addWater(data) {
  return request({
    url: '/security/corpEpWasteWater',
    method: 'post',
    data: data
  })
}

export function updateWater(data) {
  return request({
    url: '/security/corpEpWasteWater',
    method: 'put',
    data: data
  })
}

export function delWater(id) {
  return request({
    url: '/security/corpEpWasteWater/' + id,
    method: 'delete'
  })
}

export function importWater(data) {
  return request({
    url: '/security/corpEpWasteWater/importData',
    method: 'post',
    data: data
  })
}
