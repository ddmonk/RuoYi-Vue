package com.enjoychain.www.security.entity;

import com.baomidou.mybatisplus.annotation.FieldFill;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableName;
import java.io.Serializable;
import java.time.LocalDateTime;
import com.ruoyi.common.annotation.Excel;
import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;
import lombok.experimental.Accessors;

/**
 * <p>
 * 安全生产基础信息
 * </p>
 *
 * @author ddmonk
 * @since 2023-01-13
 */
@Getter
@Setter
@Accessors(chain = true)
@TableName("corp_sec_product_info")
@ApiModel(value = "CorpSecProductInfo对象", description = "安全生产基础信息")
public class CorpSecProductInfo implements Serializable {

    private static final long serialVersionUID = 1L;

    private Integer id;

    @ApiModelProperty("公司id")
    @Excel(name = "公司id")
    private String corpId;

    @ApiModelProperty("主要负责人")
    @Excel(name = "主要负责人")
    private String majorName;

    @ApiModelProperty("安全负责人")
    @Excel(name = "安全负责人")
    private String safeName;

    @ApiModelProperty("职工人数")
    @Excel(name = "职工人数")
    private Integer staffNum;

    @ApiModelProperty("安全管理机构")
    @Excel(name = "安全管理机构")
    private String safetyManagementOrg;

    @ApiModelProperty("安全生产标准化")
    @Excel(name = "安全生产标准化")
    private String safetyStandard;

    @ApiModelProperty("重点行业领域")
    @Excel(name = "重点行业领域")
    private String vipIndustryArea;

    @ApiModelProperty("备注")
    @Excel(name = "备注")
    private String remark;

    @ApiModelProperty(value = "创建时间", hidden = true)
    @TableField(fill = FieldFill.INSERT)
    private LocalDateTime createTime;

    @ApiModelProperty(value = "修改时间", hidden = true)
    @TableField(fill = FieldFill.INSERT_UPDATE)
    private LocalDateTime updateTime;

    @ApiModelProperty(value = "创建人员", hidden = true)
    @TableField(fill = FieldFill.INSERT)
    private String createBy;

    @ApiModelProperty(value = "修改人员", hidden = true)
    @TableField(fill = FieldFill.INSERT_UPDATE)
    private String modifyBy;


}
