package com.enjoychain.www.security.constant;

public class RoleTypeConstant {

    public static final String ADMIN = "admin";

    public static final String COMMON = "common";

    public static final String ENVIRONMENT_PATROL = "environment";

    public static final String SECURITY_PATROL = "security";

    public static final String CUSTOM = "custom";

    public static final String MANAGER = "manager";

}
