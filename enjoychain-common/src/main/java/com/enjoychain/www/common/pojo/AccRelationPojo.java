package com.enjoychain.www.common.pojo;

import lombok.Data;

@Data
public class AccRelationPojo {

    private String taskId;

    private String accId;

}
