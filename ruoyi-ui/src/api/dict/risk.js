import request from '@/utils/request'

// 查询较大风险目录字典列表
export function listData(query) {
  return request({
    url: '/security/sysRiskDictData/list',
    method: 'get',
    params: query
  })
}

// 查询较大风险目录字典详细
export function getData(id) {
  return request({
    url: '/security/sysRiskDictData/' + id,
    method: 'get'
  })
}

// 新增较大风险目录字典
export function addData(data) {
  return request({
    url: '/security/sysRiskDictData',
    method: 'post',
    data: data
  })
}

// 修改较大风险目录字典
export function updateData(data) {
  return request({
    url: '/security/sysRiskDictData',
    method: 'put',
    data: data
  })
}

// 删除较大风险目录字典
export function delData(id) {
  return request({
    url: '/security/sysRiskDictData/' + id,
    method: 'delete'
  })
}
