package com.enjoychain.www.security.controller.business;

import org.springframework.web.bind.annotation.RequestMapping;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.common.utils.StringUtils;
import com.ruoyi.common.utils.poi.ExcelUtil;
import io.swagger.annotations.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import javax.servlet.http.HttpServletResponse;
import java.util.List;
import java.util.UUID;
import java.util.Arrays;
import com.enjoychain.www.security.entity.NeedInfo;
import com.enjoychain.www.security.service.INeedInfoService;
import org.springframework.web.bind.annotation.RestController;

/**
 * <p>
 * 用户需求信息 前端控制器
 * </p>
 *
 * @author ddmonk
 * @since 2023-01-29
 */
@Api(tags = "用户需求信息")
@RestController
@RequestMapping("/security/needInfo")
public class NeedInfoController {

    @Autowired
    INeedInfoService service;

    @PreAuthorize("@ss.hasPermi('need:basic:list')")
    @GetMapping(path = "/list", produces = {"application/json"})
    @ApiOperation(value = "获取用户需求信息列表")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "corpName", value = "企业名称",paramType = "query", dataType = "String"),
            @ApiImplicitParam(name = "status", value = "当前状态",paramType = "query", dataType = "String"),
            @ApiImplicitParam(name = "type", value = "需求类型",paramType = "query", dataType = "String"),
            @ApiImplicitParam(name = "startTime", value = "开始时间",paramType = "query",  dataType = "String"),
            @ApiImplicitParam(name = "endTime", value = "结束时间",paramType = "query",  dataType = "String"),
            @ApiImplicitParam(name = "pageNum", value = "页码", paramType = "query", dataType = "long"),
            @ApiImplicitParam(name = "pageSize", value = "页面记录条数", paramType = "query", dataType = "long")
    })
    @ApiResponses({
        @ApiResponse(code = 200, message = "ok", response= NeedInfo.class),
    })
    public AjaxResult list(
                            @RequestParam(name = "corpName", required = false) String corpName,
                            @RequestParam(name = "startTime", required = false) String startTime,
                            @RequestParam(name = "endTime", required = false) String endTime,
                            @RequestParam(name = "type", required = false) String type,
                            @RequestParam(name = "status", required = false) String status,
                            @RequestParam(name = "pageNum", defaultValue = "1") long pageNo,
                            @RequestParam(name = "pageSize",defaultValue = "10") long pageSize){
        IPage<NeedInfo> entityIPage = new Page<>(pageNo, pageSize);
        QueryWrapper<NeedInfo> queryWrapper = new QueryWrapper<>();
        if (StringUtils.isNotEmpty(corpName)) {
            queryWrapper.like("corp_name",corpName);
        }
        if (StringUtils.isNotEmpty(type)) {
            queryWrapper.eq("type",type);
        }
        if (StringUtils.isNotEmpty(status)) {
            queryWrapper.eq("status",status);
        }
        if (StringUtils.isNotEmpty(startTime) && StringUtils.isNotEmpty(endTime)) {
            queryWrapper.between("create_time",startTime,endTime);
        }
        queryWrapper.orderByAsc("create_time");
        return AjaxResult.success("list-need_info",service.page(entityIPage,queryWrapper));
    }

    @PreAuthorize("@ss.hasPermi('need:basic:query')")
    @GetMapping(value = "{id}")
    @ApiOperation("获取用户需求信息详情")
    @ApiImplicitParams({
        @ApiImplicitParam(name = "id", value = "id",paramType = "path", dataType = "String")
    })
    @ApiResponses({
        @ApiResponse(code = 200, message = "ok", response = NeedInfo.class)
    })
    public AjaxResult getNeedInfoInfo(@PathVariable(value = "id", required = false) String id)
    {
        return AjaxResult.success("get-need_info",service.getById(id));
    }

    @PreAuthorize("@ss.hasPermi('need:basic:import')")
    @ApiOperation("导入用户需求信息")
    @Log(title = "用户需求信息", businessType = BusinessType.IMPORT)
    @PostMapping("/importData")
    public AjaxResult importData(MultipartFile file) throws Exception{
        ExcelUtil<NeedInfo> util = new ExcelUtil<NeedInfo>(NeedInfo.class);
        List<NeedInfo> infos = util.importExcel(file.getInputStream());
        return service.saveOrUpdateBatch(infos)?AjaxResult.success("批量插入成功"):AjaxResult.error("批量插入失败");
    }


    @ApiOperation("用户需求信息导出模板下载")
    @PostMapping("/importTemplate")
    public void importTemplate(HttpServletResponse response){
        ExcelUtil<NeedInfo> util = new ExcelUtil<NeedInfo>(NeedInfo.class);
        util.importTemplateExcel(response,"用户需求信息");
    }

    @PreAuthorize("@ss.hasPermi('need:basic:add')")
    @PostMapping(consumes = {"application/json"}, produces = {"application/json"})
    @ApiOperation("新增用户需求信息")
    @ApiResponses({
        @ApiResponse(code = 200, message = "ok", response = NeedInfo.class),
    })
    @Log(title = "用户需求信息", businessType = BusinessType.INSERT)
    public AjaxResult add(@RequestBody NeedInfo entity){
        return service.save(entity)? AjaxResult.success("add-need_info",entity): AjaxResult.error("新增用户需求信息失败");
    }


    @PreAuthorize("@ss.hasPermi('need:basic:update')")
    @PutMapping(consumes = {"application/json"}, produces = {"application/json"})
    @ApiOperation("修改用户需求信息")
    @ApiResponses({
        @ApiResponse(code = 200, message = "ok", response = NeedInfo.class)
    })
    @Log(title = "用户需求信息", businessType = BusinessType.UPDATE)
    public AjaxResult update(@RequestBody NeedInfo corpBasicInfo){
        String operate = "update-need_info";
        return service.updateById(corpBasicInfo)?AjaxResult.success(operate,service.getById(corpBasicInfo.getId())) : AjaxResult.error("修改客户基础信息失败");
    }

    @PreAuthorize("@ss.hasPermi('need:basic:delete')")
    @DeleteMapping(value = "{id}", produces = {"application/json"})
    @ApiOperation("删除用户需求信息")
    @ApiResponses({
        @ApiResponse(code = 200, message = "ok", response = AjaxResult.class),
    })
    @Log(title = "用户需求信息", businessType = BusinessType.DELETE)
    public AjaxResult delete(@PathVariable(value = "id", required = false) String id){
        String operate = "delete-need_info";
        String[] ids = id.split(",");
        return service.removeBatchByIds(Arrays.asList(ids))?AjaxResult.success(operate) : AjaxResult.error("修改用户需求信息失败");
    }


    @Log(title = "用户需求信息", businessType = BusinessType.EXPORT)
    @PreAuthorize("@ss.hasPermi('need:basic:export')")
    @PostMapping("/export")
    @ApiOperation("导出用户需求信息")
    @ApiResponses({
        @ApiResponse(code = 200, message = "ok", response = AjaxResult.class),
    })
    public void export(HttpServletResponse response,@RequestParam(name = "corpId", required = false) String corpId){
        QueryWrapper<NeedInfo> queryWrapper = new QueryWrapper<>();
        if (StringUtils.isNotEmpty(corpId)) {
            queryWrapper.eq("corp_id",corpId);
        }
        List<NeedInfo> list = service.list(queryWrapper);
        ExcelUtil<NeedInfo> util = new ExcelUtil<NeedInfo>(NeedInfo.class);
        util.exportExcel(response,list, "导出用户需求信息");
    }
}
