package com.enjoychain.www.security.service;

import com.enjoychain.www.security.entity.TaskHwData;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author ddmonk
 * @since 2023-02-24
 */
public interface ITaskHwDataService extends ITaskService<TaskHwData> {

}
