package com.enjoychain.www.security.service.impl;

import com.enjoychain.www.security.entity.CorpRiksItem;
import com.enjoychain.www.security.mapper.CorpRiksItemMapper;
import com.enjoychain.www.security.service.ICorpRiksItemService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 公司较大以上安全风险信息表 服务实现类
 * </p>
 *
 * @author ddmonk
 * @since 2023-01-13
 */
@Service
public class CorpRiksItemServiceImpl extends ServiceImpl<CorpRiksItemMapper, CorpRiksItem> implements ICorpRiksItemService {

}
