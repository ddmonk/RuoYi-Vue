// 企业清洁生产情况
import request from '@/utils/request'

export function listClean(query) {
  return request({
    url: '/security/corpEpCleanProduct/list',
    method: 'get',
    params: query
  })
}

export function getClean(id) {
  return request({
    url: '/security/corpEpCleanProduct/' + id,
    method: 'get'
  })
}

export function addClean(data) {
  return request({
    url: '/security/corpEpCleanProduct',
    method: 'post',
    data: data
  })
}

export function updateClean(data) {
  return request({
    url: '/security/corpEpCleanProduct',
    method: 'put',
    data: data
  })
}

export function delClean(id) {
  return request({
    url: '/security/corpEpCleanProduct/' + id,
    method: 'delete'
  })
}

export function importClean(data) {
  return request({
    url: '/security/corpEpCleanProduct/importData',
    method: 'post',
    data: data
  })
}
