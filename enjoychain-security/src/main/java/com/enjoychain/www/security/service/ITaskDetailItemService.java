package com.enjoychain.www.security.service;

import com.enjoychain.www.security.entity.accident.AccidentInfo;
import com.enjoychain.www.security.entity.task.TaskDetailItem;
import com.baomidou.mybatisplus.extension.service.IService;

import java.io.IOException;
import java.util.List;

/**
 * <p>
 * 巡查任务详情信息 服务类
 * </p>
 *
 * @author ddmonk
 * @since 2023-01-20
 */
public interface ITaskDetailItemService extends IService<TaskDetailItem> {

    List<TaskDetailItem> getItems(String taskId);

    void removeByTaskId(String taskId);


    boolean removeBatchByTaskIds(List<String> asList);

    TaskDetailItem findItemWithTaskIdAndAccidentInfo(String taskId, AccidentInfo info);

    boolean setAccidentByTaskItemId(String id) throws IOException;


    TaskDetailItem getItem(String id);

}
