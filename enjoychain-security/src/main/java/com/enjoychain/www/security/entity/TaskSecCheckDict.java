package com.enjoychain.www.security.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import java.io.Serializable;
import com.ruoyi.common.annotation.Excel;
import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;
import lombok.experimental.Accessors;

/**
 * <p>
 *
 * </p>
 *
 * @author ddmonk
 * @since 2023-04-21
 */
@Getter
@Setter
@Accessors(chain = true)
@TableName("task_sec_check_dict")
@ApiModel(value = "TaskSecCheckDict对象", description = "")
public class TaskSecCheckDict implements Serializable {

    private static final long serialVersionUID = 1L;

    @TableId(value = "id", type = IdType.AUTO)
    private Integer id;

    private String classType;

    private String checkPoint;

    private String content;

    private String keyWord;

    private String description;


}
