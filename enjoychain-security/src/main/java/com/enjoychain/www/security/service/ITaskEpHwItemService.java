package com.enjoychain.www.security.service;

import com.enjoychain.www.security.entity.TaskEpHwItem;
import com.baomidou.mybatisplus.extension.service.IService;

import java.util.List;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author ddmonk
 * @since 2023-03-07
 */
public interface ITaskEpHwItemService extends ITaskService<TaskEpHwItem> {

}
