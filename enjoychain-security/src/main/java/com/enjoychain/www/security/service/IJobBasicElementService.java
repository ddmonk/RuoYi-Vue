package com.enjoychain.www.security.service;

import com.enjoychain.www.security.entity.JobBasicElement;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 * 核查任务要素信息 服务类
 * </p>
 *
 * @author ddmonk
 * @since 2023-01-30
 */
public interface IJobBasicElementService extends IService<JobBasicElement> {

}
