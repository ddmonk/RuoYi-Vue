// 较大以上安全风险信息
import request from '@/utils/request'

export function listRisk(query) {
  return request({
    url: '/security/corpSecBigRiskInfo/list',
    method: 'get',
    params: query
  })
}

export function getRisk(id) {
  return request({
    url: '/security/corpSecBigRiskInfo/' + id,
    method: 'get'
  })
}

export function addRisk(data) {
  return request({
    url: '/security/corpSecBigRiskInfo',
    method: 'post',
    data: data
  })
}

export function updateRisk(data) {
  return request({
    url: '/security/corpSecBigRiskInfo',
    method: 'put',
    data: data
  })
}

export function delRisk(id) {
  return request({
    url: '/security/corpSecBigRiskInfo/' + id,
    method: 'delete'
  })
}

export function importRisk(data) {
  return request({
    url: '/security/corpSecBigRiskInfo/importData',
    method: 'post',
    data: data
  })
}
