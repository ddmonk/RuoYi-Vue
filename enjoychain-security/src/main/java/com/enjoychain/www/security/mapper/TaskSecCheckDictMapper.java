package com.enjoychain.www.security.mapper;

import com.enjoychain.www.security.entity.TaskSecCheckDict;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author ddmonk
 * @since 2023-04-21
 */
public interface TaskSecCheckDictMapper extends BaseMapper<TaskSecCheckDict> {

}
