import request from '@/utils/request'

export function listTask(query) {
  return request({
    url: '/security/taskInfo/list',
    method: 'get',
    params: query
  })
}

export function delTask(id) {
  return request({
    url: '/security/taskInfo/' + id,
    method: 'delete'
  })
}

export function getTask(id) {
  return request({
    url: '/security/taskInfo/' + id,
    method: 'get'
  })
}

export function addTask(data) {
  return request({
    url: '/security/taskInfo',
    method: 'post',
    data: data
  })
}

export function updateTask(data, params) {
  return request({
    url: '/security/taskInfo',
    method: 'put',
    data: data,
    params: params
  })
}

export function listTaskDetailItem(query) {
  return request({
    url: '/security/taskDetailItem/list',
    method: 'get',
    params: query
  })
}

export function addTaskDetailItem(data) {
  return request({
    url: '/security/taskDetailItem',
    method: 'post',
    data: data
  })
}

export function setEvent(id) {
  return request({
    url: '/security/taskDetailItem/event/' + id,
    method: 'get'
  })
}

export function updateTaskDetailItem(data) {
  return request({
    url: '/security/taskDetailItem',
    method: 'put',
    data: data
  })
}

export function delTaskDetailItem(id) {
  return request({
    url: '/security/taskDetailItem/' + id,
    method: 'delete'
  })
}

export function getNewTask(id) {
  return request({
    url: '/security/taskInfo/new/' + id,
    method: 'get'
  })
}

export function updateNewTask(data) {
  return request({
    url: '/security/taskInfo/new',
    method: 'put',
    data: data
  })
}

export function correctNewTask(data) {
  return request({
    url: '/security/taskInfo/new/correct',
    method: 'put',
    data: data
  })
}

export function exportCorpInfo(id) {
  return request({
    url: '/report/basic/task/' + id,
    method: 'get'
  })
}

export function exportReportInfo(id) {
  return request({
    url: '/report/task/' + id,
    method: 'get'
  })
}

export function exportTaskAllInfo(id) {
  return request({
    url: '/report/task/all/' + id,
    method: 'get'
  })
}

export function exportPromiseInfo(id) {
  return request({
    url: '/report/company/promise/' + id,
    method: 'get'
  })
}

export function exportCheckInfo(id) {
  return request({
    url: '/report/task/check/' + id,
    method: 'get'
  })
}

export function searchHwCode(params) {
  return request({
    url: '/security/basicDictHw/list',
    method: 'get',
    params
  })
}

export function searchRiskCode(params) {
  return request({
    url: '/security/sysRiskDictData/list',
    method: 'get',
    params
  })
}

export function searchIndustry(params) {
  return request({
    url: '/security/industry/list',
    method: 'get',
    params
  })
}

export function getEnvCorpInfo(id) {
  return request({
    url: '/security/corpEnvBasicInfo/' + id,
    method: 'get'
  })
}

export function updateEnvCorpInfo(data) {
  return request({
    url: '/security/corpEnvBasicInfo',
    method: 'put',
    data: data
  })
}

export function exportEnvCorpInfo(id) {
  return request({
    url: '/report/basic/corp/' + id,
    method: 'get'
  })
}
