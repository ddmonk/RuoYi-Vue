package com.enjoychain.www.security.service;

import com.enjoychain.www.security.pojo.StatisticsPojo;

public interface IDashBoardService {
    StatisticsPojo getPanel();
}
