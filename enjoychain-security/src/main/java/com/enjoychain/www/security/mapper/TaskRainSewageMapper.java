package com.enjoychain.www.security.mapper;

import com.enjoychain.www.security.entity.TaskRainSewage;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * 雨污水情况 Mapper 接口
 * </p>
 *
 * @author ddmonk
 * @since 2023-02-24
 */
public interface TaskRainSewageMapper extends BaseMapper<TaskRainSewage> {

}
