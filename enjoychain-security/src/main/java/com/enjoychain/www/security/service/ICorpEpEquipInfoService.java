package com.enjoychain.www.security.service;

import com.enjoychain.www.security.entity.CorpEpEquipInfo;
import com.baomidou.mybatisplus.extension.service.IService;

import java.util.List;

/**
 * <p>
 * 主要设备情况 服务类
 * </p>
 *
 * @author ddmonk
 * @since 2023-01-13
 */
public interface ICorpEpEquipInfoService extends IService<CorpEpEquipInfo> {

    List<CorpEpEquipInfo> listWithStatus(String status, String corpId);

    void saveOrUpdateBatch(List<CorpEpEquipInfo> equipItems, String id);

    void deleteByCorpId(String id);
}
