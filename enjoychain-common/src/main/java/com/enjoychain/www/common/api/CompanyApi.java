package com.enjoychain.www.common.api;


import com.enjoychain.www.common.config.FeignLogConfiguration;
import com.enjoychain.www.common.pojo.CompanyBasicInfo;
import com.enjoychain.www.common.pojo.DataApiResponse;
import com.enjoychain.www.common.pojo.PojoParams;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

@FeignClient(name = "demo", url = "https://dwzxl.dwstock.com/ibwechat/apidev/",configuration = FeignLogConfiguration.class)
public interface CompanyApi {

    /**
     * 工商信息
     * @param params
     * @return
     */
    @RequestMapping(value = "COMPANY_BASICINFO_ALL",method = RequestMethod.POST, produces = "application/json;charset=utf-8")
    public DataApiResponse<CompanyBasicInfo> getCompanyDetail(@RequestBody PojoParams params);

    @RequestMapping(value = "COMPANY_BASICINFO_ALL",method = RequestMethod.POST, produces = "application/json;charset=utf-8")
    public Object getCompBasicInfo(@RequestBody PojoParams params);

    /**
     * 模糊搜索企业
     * @param params
     * @return
     */
    @RequestMapping(value = "querycompanyinfoES", method = RequestMethod.POST)
    public Object findCompanyList(@RequestBody PojoParams params);


    /**
     * 诚信风险
     * @param params
     * @return
     */
    @RequestMapping(value = "company_honesty", method = RequestMethod.POST)
    public Object getCompanyHonesty(@RequestBody Object params);

    /**
     * 分支机构
     * @param params
     * @return
     */
    @RequestMapping(value = "company_branch", method = RequestMethod.POST)
    public Object getCompanyBranch(@RequestBody Object params);

    /**
     * 股东信息
     * @param params
     * @return
     */
    @RequestMapping(value = "compy_shareholder", method = RequestMethod.POST)
    public Object getCompShareholder(@RequestBody Object params);

    /**
     * 高管人员
     * @param params
     * @return
     */
    @RequestMapping(value = "company_manager", method = RequestMethod.POST)
    public Object getCompManager(@RequestBody Object params);

    /**
     * 司法风险
     * @param params
     * @return
     */
    @RequestMapping(value = "company_dishonest", method = RequestMethod.POST)
    public Object getCompDisHonest(@RequestBody Object params);

    /**
     * 经营风险
     * @param params
     * @return
     */
    @RequestMapping(value = "company_manage", method = RequestMethod.POST)
    public Object getCompManage(@RequestBody Object params);

    /**
     * 经营风险
     * @param params
     * @return
     */
    @RequestMapping(value = "company_penalty", method = RequestMethod.POST)
    public Object getCompPenalty(@RequestBody Object params);

    @RequestMapping(value = "company_info", method = RequestMethod.POST)
    public Object getCompInfo(@RequestBody Object params);


    @RequestMapping(value = "LAW_LITIGANT_BASICINFO_JX", method = RequestMethod.POST)
    public Object getLawLitigantBasicInfoJx(@RequestBody Object params);

    /**
     * 经营风险
     * @param params
     * @return
     */
    @RequestMapping(value = "LAW_DISHONEST_BASICINFO_JX", method = RequestMethod.POST)
    public Object getLawDisHonestBasicInfoJx(@RequestBody Object params);

    /**
     * 经营风险
     * @param params
     * @return
     */
    @RequestMapping(value = "LAW_JUDICIAL_BASICINFO_JX", method = RequestMethod.POST)
    public Object getLawJudicialBasicInfoJx(@RequestBody Object params);

    @RequestMapping(value = "LAW_COURT_ANNE_BASICINFO_JX", method = RequestMethod.POST)
    public Object getLawCourtAnneBasicInfoJx(@RequestBody Object params);

    @RequestMapping(value = "COMPANY_OPEREXCEPT_XYGS", method = RequestMethod.POST)
    Object getCompOperExcepXygs(Object params);
}
