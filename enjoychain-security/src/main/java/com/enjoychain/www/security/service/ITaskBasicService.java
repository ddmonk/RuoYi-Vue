package com.enjoychain.www.security.service;

import com.enjoychain.www.security.entity.task.TaskBasic;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 * 巡查任务基础信息 服务类
 * </p>
 *
 * @author ddmonk
 * @since 2023-01-20
 */
public interface ITaskBasicService extends IService<TaskBasic> {

    TaskBasic getBasicByTaskId(String taskId);

    void removeByTaskId(String taskId);

}
