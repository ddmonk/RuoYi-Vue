package com.enjoychain.www.security.entity;

import com.baomidou.mybatisplus.annotation.FieldFill;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableName;
import java.io.Serializable;
import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.List;

import com.ruoyi.common.annotation.Excel;
import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;
import lombok.experimental.Accessors;
import org.apache.commons.lang3.StringUtils;

/**
 * <p>
 * 巡查任务—设备情况
 * </p>
 *
 * @author ddmonk
 * @since 2023-02-24
 */
@Getter
@Setter
@Accessors(chain = true)
@TableName("task_env_equip_item")
@ApiModel(value = "TaskEquipItem对象", description = "巡查任务—设备情况")
public class TaskEquipItem implements Serializable {

    private static final long serialVersionUID = 1L;

    private String id;

    @ApiModelProperty("任务id")
    @Excel(name = "任务id")
    private String taskId;

    @ApiModelProperty("设备名称")
    @Excel(name = "设备名称")
    private String equipName;

    @ApiModelProperty("环评报告设备数量")
    @Excel(name = "环评报告设备数量")
    private String equipHisNum;

    @ApiModelProperty("实际数量")
    @Excel(name = "实际数量")
    private String equipCurNum;

    public void setEquipHisNum(String equipHisNum) {
        this.equipHisNum = equipHisNum;
        if (StringUtils.isNotEmpty(this.equipHisNum) && StringUtils.isNotEmpty(this.equipCurNum)){
            this.numChange = (Integer.valueOf(this.equipCurNum) - Integer.valueOf(this.equipHisNum)) + "";
        }
    }

    public void setEquipCurNum(String equipCurNum) {
        this.equipCurNum = equipCurNum;
        if (StringUtils.isNotEmpty(this.equipHisNum) && StringUtils.isNotEmpty(this.equipCurNum)){
            this.numChange = (Integer.valueOf(this.equipCurNum) - Integer.valueOf(this.equipHisNum)) + "";
        }
    }

    @TableField(exist = false)
    private String numChange;

    @ApiModelProperty("备注")
    @Excel(name = "备注")
    private String remark;

    @ApiModelProperty(value = "创建时间", hidden = true)
    @TableField(fill = FieldFill.INSERT)
    private LocalDateTime createTime;

    @ApiModelProperty(value = "修改时间", hidden = true)
    @TableField(fill = FieldFill.INSERT_UPDATE)
    private LocalDateTime updateTime;

    @ApiModelProperty(value = "创建人员", hidden = true)
    @TableField(fill = FieldFill.INSERT)
    private String createBy;

    @ApiModelProperty(value = "修改人员", hidden = true)
    @TableField(fill = FieldFill.INSERT_UPDATE)
    private String modifyBy;

    @TableField(exist = false)
    private List<BasicFile> files;


}
