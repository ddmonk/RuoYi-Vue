package com.enjoychain.www.security.service;

import com.enjoychain.www.security.entity.TaskRainSewage;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 * 雨污水情况 服务类
 * </p>
 *
 * @author ddmonk
 * @since 2023-02-24
 */
public interface ITaskRainSewageService extends ITaskService<TaskRainSewage> {

    TaskRainSewage getDetailByTaskId(String taskId);

    void removeDetailByTaskId(String taskId);
}
