package com.enjoychain.www.patrol.entity.vo;

import lombok.Data;

import java.util.List;

@Data
public class DailyTableVo {

    private List<UploaderComponentVo> image;

    private String advice;

}
