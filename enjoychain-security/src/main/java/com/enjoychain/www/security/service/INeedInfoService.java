package com.enjoychain.www.security.service;

import com.enjoychain.www.security.entity.NeedInfo;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 * 用户需求信息 服务类
 * </p>
 *
 * @author ddmonk
 * @since 2023-01-29
 */
public interface INeedInfoService extends IService<NeedInfo> {

}
