package com.enjoychain.www.security.mapper;

import com.enjoychain.www.security.entity.TaskProductProcess;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * 生产工艺 Mapper 接口
 * </p>
 *
 * @author ddmonk
 * @since 2023-02-24
 */
public interface TaskProductProcessMapper extends BaseMapper<TaskProductProcess> {

}
