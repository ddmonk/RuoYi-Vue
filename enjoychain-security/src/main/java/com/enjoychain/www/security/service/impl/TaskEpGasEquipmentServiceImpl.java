package com.enjoychain.www.security.service.impl;

import com.enjoychain.www.security.entity.TaskEpGasEquipment;
import com.enjoychain.www.security.mapper.TaskEpGasEquipmentMapper;
import com.enjoychain.www.security.service.ITaskEpGasEquipmentService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author ddmonk
 * @since 2023-03-07
 */
@Service
public class TaskEpGasEquipmentServiceImpl extends ServiceImpl<TaskEpGasEquipmentMapper, TaskEpGasEquipment> implements ITaskEpGasEquipmentService {

}
