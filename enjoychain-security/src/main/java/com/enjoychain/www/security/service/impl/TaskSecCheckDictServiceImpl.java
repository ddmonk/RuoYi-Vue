package com.enjoychain.www.security.service.impl;

import com.enjoychain.www.security.entity.TaskSecCheckDict;
import com.enjoychain.www.security.mapper.TaskSecCheckDictMapper;
import com.enjoychain.www.security.service.ITaskSecCheckDictService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author ddmonk
 * @since 2023-04-21
 */
@Service
public class TaskSecCheckDictServiceImpl extends ServiceImpl<TaskSecCheckDictMapper, TaskSecCheckDict> implements ITaskSecCheckDictService {

}
