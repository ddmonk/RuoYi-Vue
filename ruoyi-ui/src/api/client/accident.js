import request from '@/utils/request'

export function listAccident(query) {
  return request({
    url: '/security/accidentInfo/list',
    method: 'get',
    params: query
  })
}

export function getAccident(id) {
  return request({
    url: '/security/accidentInfo/' + id,
    method: 'get'
  })
}

export function getAccidentDetail(query) {
  return request({
    url: '/security/accidentInfo/detail',
    method: 'get',
    params: query
  })
}

export function addAccident(data) {
  return request({
    url: '/security/accidentInfo',
    method: 'post',
    data: data
  })
}

export function updateAccident(data) {
  return request({
    url: '/security/accidentInfo',
    method: 'put',
    data: data
  })
}

export function delAccident(id) {
  return request({
    url: '/security/accidentInfo/' + id,
    method: 'delete'
  })
}

export function importAccident(data) {
  return request({
    url: '/security/accidentInfo/importData',
    method: 'post',
    data: data
  })
}
