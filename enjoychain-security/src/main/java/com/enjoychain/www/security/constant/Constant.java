package com.enjoychain.www.security.constant;

public class Constant {

    public static final String DATE_FORMATTER = "yyyyMMdd";

    public static final String REPORT_DEFAULT_VALUE = "——";

    public static final String DEFAULT_PASSWORD = "123456";
}
