package com.enjoychain.www.security.mapper;

import com.enjoychain.www.security.entity.CorpEpGasEquipment;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * 废气处理设备情况 Mapper 接口
 * </p>
 *
 * @author ddmonk
 * @since 2023-01-13
 */
public interface CorpEpGasEquipmentMapper extends BaseMapper<CorpEpGasEquipment> {

}
