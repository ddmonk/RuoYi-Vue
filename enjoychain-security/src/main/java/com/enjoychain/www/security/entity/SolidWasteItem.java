package com.enjoychain.www.security.entity;

import com.baomidou.mybatisplus.annotation.FieldFill;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableName;
import java.io.Serializable;
import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

import com.ruoyi.common.annotation.Excel;
import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;
import lombok.experimental.Accessors;

import static com.enjoychain.www.security.constant.Constant.DATE_FORMATTER;
import static com.enjoychain.www.security.constant.Constant.REPORT_DEFAULT_VALUE;

/**
 * <p>
 * 一般固废详情
 * </p>
 *
 * @author ddmonk
 * @since 2023-02-21
 */
@Getter
@Setter
@Accessors(chain = true)
@TableName("solid_waste_item")
@ApiModel(value = "SolidWasteItem对象", description = "一般固废详情")
public class SolidWasteItem implements Serializable {

    private static final long serialVersionUID = 1L;

    private String id;

    @ApiModelProperty("公司id")
    @Excel(name = "公司id")
    private String corpId;

    @ApiModelProperty("一般固废名称")
    @Excel(name = "一般固废名称")
    private String solidName;

    @ApiModelProperty("数量")
    @Excel(name = "数量")
    private String solidNum;

    @ApiModelProperty("状态")
    @Excel(name = "状态")
    private String status;

    @ApiModelProperty("到期时间")
    @Excel(name = "到期时间", width = 30, dateFormat = "yyyy-MM-dd")
    private LocalDateTime deadline;

    @TableField(exist = false)
    private String deadlineString;

    public void setDeadline(LocalDateTime deadline) {
        this.deadline = deadline;
        this.deadlineString = this.deadline == null ? REPORT_DEFAULT_VALUE : deadline.format(DateTimeFormatter.ofPattern(DATE_FORMATTER));
    }

    @ApiModelProperty("备注")
    @Excel(name = "备注")
    private String remark;

    @ApiModelProperty(value = "创建时间", hidden = true)
    @TableField(fill = FieldFill.INSERT)
    private LocalDateTime createTime;

    @ApiModelProperty(value = "修改时间", hidden = true)
    @TableField(fill = FieldFill.INSERT_UPDATE)
    private LocalDateTime updateTime;

    @ApiModelProperty(value = "创建人员", hidden = true)
    @TableField(fill = FieldFill.INSERT)
    private String createBy;

    @ApiModelProperty(value = "修改人员", hidden = true)
    @TableField(fill = FieldFill.INSERT_UPDATE)
    private String modifyBy;


}
