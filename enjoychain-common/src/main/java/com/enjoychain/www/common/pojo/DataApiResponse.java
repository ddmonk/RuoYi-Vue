package com.enjoychain.www.common.pojo;

import lombok.Data;

import java.util.List;

@Data
public class DataApiResponse<T> {
//    Object data;
    List<T> data;
}
