package com.enjoychain.www.security.service.impl;

import com.enjoychain.www.security.entity.TaskWasteWater;
import com.enjoychain.www.security.mapper.TaskWasteWaterMapper;
import com.enjoychain.www.security.service.ITaskWasteWaterService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 废水产生 服务实现类
 * </p>
 *
 * @author ddmonk
 * @since 2023-02-24
 */
@Service
public class TaskWasteWaterServiceImpl extends ServiceImpl<TaskWasteWaterMapper, TaskWasteWater> implements ITaskWasteWaterService {

}
