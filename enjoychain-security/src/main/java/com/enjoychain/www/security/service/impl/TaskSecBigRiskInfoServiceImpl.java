package com.enjoychain.www.security.service.impl;

import com.enjoychain.www.security.entity.TaskSecBigRiskInfo;
import com.enjoychain.www.security.mapper.TaskSecBigRiskInfoMapper;
import com.enjoychain.www.security.service.ITaskSecBigRiskInfoService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author ddmonk
 * @since 2023-04-18
 */
@Service
public class TaskSecBigRiskInfoServiceImpl extends ServiceImpl<TaskSecBigRiskInfoMapper, TaskSecBigRiskInfo> implements ITaskSecBigRiskInfoService {

}
