// 土壤和地下水自行监测情况
import request from '@/utils/request'

export function listMonitor(query) {
  return request({
    url: '/security/corpEpWaterMonitor/list',
    method: 'get',
    params: query
  })
}

export function getMonitor(id) {
  return request({
    url: '/security/corpEpWaterMonitor/' + id,
    method: 'get'
  })
}

export function addMonitor(data) {
  return request({
    url: '/security/corpEpWaterMonitor',
    method: 'post',
    data: data
  })
}

export function updateMonitor(data) {
  return request({
    url: '/security/corpEpWaterMonitor',
    method: 'put',
    data: data
  })
}

export function delMonitor(id) {
  return request({
    url: '/security/corpEpWaterMonitor/' + id,
    method: 'delete'
  })
}

export function importMonitor(data) {
  return request({
    url: '/security/corpEpWaterMonitor/importData',
    method: 'post',
    data: data
  })
}
