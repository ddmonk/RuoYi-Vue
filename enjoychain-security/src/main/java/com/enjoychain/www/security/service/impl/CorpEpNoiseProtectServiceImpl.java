package com.enjoychain.www.security.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.enjoychain.www.security.entity.CorpEpNoiseProtect;
import com.enjoychain.www.security.mapper.CorpEpNoiseProtectMapper;
import com.enjoychain.www.security.service.ICorpEpNoiseProtectService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * <p>
 * 企业噪声管理情况 服务实现类
 * </p>
 *
 * @author ddmonk
 * @since 2023-01-13
 */
@Service
public class CorpEpNoiseProtectServiceImpl extends ServiceImpl<CorpEpNoiseProtectMapper, CorpEpNoiseProtect> implements ICorpEpNoiseProtectService {

    @Override
    public List<CorpEpNoiseProtect> listWithStatus(int status, String corpId) {
        QueryWrapper<CorpEpNoiseProtect> queryWrapper = new QueryWrapper<>();
        queryWrapper.eq("status",status);
        queryWrapper.eq("corp_id",corpId);
        return list(queryWrapper);
    }
}
