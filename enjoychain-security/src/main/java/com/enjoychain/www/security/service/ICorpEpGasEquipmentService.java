package com.enjoychain.www.security.service;

import com.enjoychain.www.security.entity.CorpEpGasEquipment;
import com.baomidou.mybatisplus.extension.service.IService;

import java.util.List;

/**
 * <p>
 * 废气处理设备情况 服务类
 * </p>
 *
 * @author ddmonk
 * @since 2023-01-13
 */
public interface ICorpEpGasEquipmentService extends IService<CorpEpGasEquipment> {

    List<CorpEpGasEquipment> listWithStatus(String status, String corpId);

    List<CorpEpGasEquipment> listSection(int i, String corpId);

    void saveOrUpdateBatch(List<CorpEpGasEquipment> gasEquipmentItems, String id);

    void deleteByCorpId(String id);
}
