package com.enjoychain.www.security.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.enjoychain.www.common.api.CompanyApi;
import com.enjoychain.www.common.pojo.CompanyBasicInfo;
import com.enjoychain.www.common.pojo.DataApiResponse;
import com.enjoychain.www.common.pojo.FieldPojo;
import com.enjoychain.www.common.pojo.PojoParams;
import com.enjoychain.www.security.entity.CorpBasicInfo;
import com.enjoychain.www.security.mapper.CorpBasicInfoMapper;
import com.enjoychain.www.security.service.ICorpBasicInfoService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.ruoyi.common.utils.uuid.UUID;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;


/**
 * <p>
 * 客户基础信息表 服务实现类
 * </p>
 *
 * @author baomidou
 * @since 2022-12-30
 */
@Service
public class CorpBasicInfoServiceImpl extends ServiceImpl<CorpBasicInfoMapper, CorpBasicInfo> implements ICorpBasicInfoService {

    @Autowired
    CompanyApi companyApi;

    @Override
    public CorpBasicInfo buildBasicInfoByCertNo(String certNo) {
        PojoParams params = PojoParams.builder().inFields(FieldPojo.builder().ORGNUM(certNo).build()).build();
        DataApiResponse<CompanyBasicInfo> result = companyApi.getCompanyDetail(params);
        if (result.getData().size() > 0) {
            CorpBasicInfo info = new CorpBasicInfo();
            info.setCertNo(certNo);
            info.setAddress(result.getData().get(0).getREG_ADDR());
            info.setName(result.getData().get(0).getCOMPANY_NM());
            info.setEnglishName(result.getData().get(0).getCOMPANY_EM());
            info.setJuridical(result.getData().get(0).getLEG_REPRESENT());
            info.setRegion(result.getData().get(0).getREGION());
            return info;
        }else {
            return null;
        }
    }

    @Override
    public CorpBasicInfo rebuild(CorpBasicInfo basicInfo) {
        QueryWrapper<CorpBasicInfo> queryWrapper = new QueryWrapper<>();
        queryWrapper.eq("cert_no",basicInfo.getCertNo());
        CorpBasicInfo info = baseMapper.selectOne(queryWrapper);
        if(info == null ){
            String id = UUID.randomUUID().toString().replace("-","");
            basicInfo.setId(id);
            return basicInfo;
        }else {
            return info;
        }
    }
}
