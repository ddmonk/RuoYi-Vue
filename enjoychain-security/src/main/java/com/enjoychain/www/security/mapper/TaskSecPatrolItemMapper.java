package com.enjoychain.www.security.mapper;

import com.enjoychain.www.security.entity.TaskSecPatrolItem;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * 安全核查基础信息表 Mapper 接口
 * </p>
 *
 * @author ddmonk
 * @since 2023-04-18
 */
public interface TaskSecPatrolItemMapper extends BaseMapper<TaskSecPatrolItem> {

}
