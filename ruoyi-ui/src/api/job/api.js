import request from '@/utils/request'

// 查询作业基础列表
export function listInfo(query) {
  return request({
    url: '/security/jobInfo/list',
    method: 'get',
    params: query
  })
}

// 查询作业基础详细
export function getInfo(id) {
  return request({
    url: '/security/jobInfo/' + id,
    method: 'get'
  })
}

// 新增作业基础
export function addInfo(data) {
  return request({
    url: '/security/jobInfo',
    method: 'post',
    data: data
  })
}

// 修改作业基础
export function updateInfo(data) {
  return request({
    url: '/security/jobInfo',
    method: 'put',
    data: data
  })
}

// 删除作业基础
export function delInfo(id) {
  return request({
    url: '/security/jobInfo/' + id,
    method: 'delete'
  })
}

// 查询作业考核详情内容列表
export function listItem(query) {
  return request({
    url: '/security/jobDetailItem/list',
    method: 'get',
    params: query
  })
}

// 查询作业考核详情内容详细
export function getItem(id) {
  return request({
    url: '/security/jobDetailItem/' + id,
    method: 'get'
  })
}

// 新增作业考核详情内容
export function addItem(data) {
  return request({
    url: '/security/jobDetailItem',
    method: 'post',
    data: data
  })
}

// 修改作业考核详情内容
export function updateItem(data) {
  return request({
    url: '/security/jobDetailItem',
    method: 'put',
    data: data
  })
}

// 删除作业考核详情内容
export function delItem(id) {
  return request({
    url: '/security/jobDetailItem/' + id,
    method: 'delete'
  })
}

export function start(id) {
  return request({
    url: '/security/jobInfo/start/' + id,
    method: 'get'
  })
}
