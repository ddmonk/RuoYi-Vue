package com.enjoychain.www.security.service;

import com.enjoychain.www.security.entity.job.JobDetailItem;
import com.baomidou.mybatisplus.extension.service.IService;

import java.util.List;

/**
 * <p>
 * 作业考核详情内容 服务类
 * </p>
 *
 * @author ddmonk
 * @since 2023-01-29
 */
public interface IJobDetailItemService extends IService<JobDetailItem> {

    List<JobDetailItem> getJobItems(String jobId);

}
