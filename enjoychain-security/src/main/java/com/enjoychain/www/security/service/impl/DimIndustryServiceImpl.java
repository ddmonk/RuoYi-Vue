package com.enjoychain.www.security.service.impl;

import com.enjoychain.www.security.entity.DimIndustry;
import com.enjoychain.www.security.mapper.DimIndustryMapper;
import com.enjoychain.www.security.service.IDimIndustryService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 国民经济行业分类 服务实现类
 * </p>
 *
 * @author ddmonk
 * @since 2023-03-11
 */
@Service
public class DimIndustryServiceImpl extends ServiceImpl<DimIndustryMapper, DimIndustry> implements IDimIndustryService {

}
