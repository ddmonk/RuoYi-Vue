package com.enjoychain.www.security.service;

import com.enjoychain.www.security.entity.CorpSecPromiseInfo;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 * 安全承诺信息 服务类
 * </p>
 *
 * @author ddmonk
 * @since 2023-01-13
 */
public interface ICorpSecPromiseInfoService extends IService<CorpSecPromiseInfo> {

}
