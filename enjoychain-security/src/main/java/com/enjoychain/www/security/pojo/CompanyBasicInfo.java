package com.enjoychain.www.security.pojo;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonValue;
import lombok.Data;

@Data
public class CompanyBasicInfo {

    @JsonProperty("COMPANY_NM")
    private String COMPANY_NM;

    @JsonProperty("COMPANY_FAX")
    private String COMPANY_FAX;

    @JsonProperty("INFO_NEWS")
    private String INFO_NEWS;

    @JsonProperty("ACTUAL_CAPITAL")
    private String ACTUAL_CAPITAL;
    @JsonProperty("ORIG_ORG_FORM")
    private String ORIG_ORG_FORM;
    @JsonProperty("START_DT")
    private String START_DT;
    @JsonProperty("LEG_REPRESENT")
    private String LEG_REPRESENT;
    @JsonProperty("STATUS")
    private int STATUS;
    @JsonProperty("COUNTRY")
    private String COUNTRY;
    @JsonProperty("BLNUMB")
    private String BLNUMB;
    @JsonProperty("FOUND_DT")
    private String FOUND_DT;
    @JsonProperty("IS_DEL")
    private int IS_DEL;
    @JsonProperty("OFFICE_POST_CD")
    private String OFFICE_POST_CD;
    @JsonProperty("COMPANY_EM")
    private String COMPANY_EM;
    @JsonProperty("LTRNUM")
    private String LTRNUM;
    @JsonProperty("ACCOUNTING_FIRM")
    private String ACCOUNTING_FIRM;
    @JsonProperty("REGION")
    private String REGION;
    @JsonProperty("CHAIRMAN")
    private String CHAIRMAN;
    @JsonProperty("REG_CAPITAL")
    private double REG_CAPITAL;
    @JsonProperty("REG_GOV")
    private String REG_GOV;
    @JsonProperty("IS_CORE")
    private int IS_CORE;
    @JsonProperty("CREATE_BY")
    private int CREATE_BY;
    @JsonProperty("END_DT")
    private String END_DT;
    @JsonProperty("UPDT_DT")
    private String UPDT_DT;
    @JsonProperty("MAIN_BUSIN")
    private String MAIN_BUSIN;
    @JsonProperty("BUSIN_SCOPE")
    private String BUSIN_SCOPE;
    @JsonProperty("COMPANY_WEB")
    private String COMPANY_WEB;
    @JsonProperty("EMPLOY_NUM")
    private String EMPLOY_NUM;
    @JsonProperty("ORGNUM")
    private String ORGNUM;
    @JsonProperty("COMPANY_PROFILE")
    private String COMPANY_PROFILE;

    private String CURRENCY;

    private String REG_DT;

    private String COMPANY_ST;

    private String LEGAL_ADVISOR;

    private String BSECRETARY;

    private String ORIG_COMPANY_ST;

    private String COMPANY_SNM;

    private String SRC_UPDT_DT;

    private String INFO_URL;

    private String FEN_NM;

    private String REVOKE_DT;

    private String GMANAGER;

    private int UPDT_BY;

    private String CREATE_DT;
    @JsonProperty("REG_ADDR")
    private String REG_ADDR;

    private String CITY;

    private String ORG_FORM_ID;

    private int VERSION;

    private String OFFICE_ADDR;

    private String COMPANY_PH;

    private String SRC_CD;

    private String NTRNUM;

    private String SRC_COMPANY_CD;
}
