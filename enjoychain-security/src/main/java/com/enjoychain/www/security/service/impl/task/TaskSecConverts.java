package com.enjoychain.www.security.service.impl.task;

import com.enjoychain.www.security.entity.*;

import java.util.ArrayList;
import java.util.List;

/**
 * BasicInfo基础信息与TaskBasicInfo相互转化
 */
public class TaskSecConverts {

    public static TaskCorpSecBasicInfo  convertSecBasicInfoToTaskCorpBasicInfo(CorpSecCorpBasic basic, String taskId){
        TaskCorpSecBasicInfo taskCorpSecBasicInfo = new TaskCorpSecBasicInfo();
        taskCorpSecBasicInfo.setCorpId(basic.getId());
        taskCorpSecBasicInfo.setCorpName(basic.getCorpName());
        taskCorpSecBasicInfo.setTaskId(taskId);
        taskCorpSecBasicInfo.setCorpScale(basic.getCorpScale());
        taskCorpSecBasicInfo.setRemark(basic.getRemark());
        taskCorpSecBasicInfo.setCertNo(basic.getCertNo());
        taskCorpSecBasicInfo.setAdminArea(basic.getAdminArea());
        taskCorpSecBasicInfo.setBusinessType(basic.getBusinessType());
        taskCorpSecBasicInfo.setMajorName(basic.getMajorName());
        taskCorpSecBasicInfo.setCorpAddress(basic.getCorpAddress());
        taskCorpSecBasicInfo.setSafeName(basic.getSafeName());
        taskCorpSecBasicInfo.setVipIndustryArea(basic.getVipIndustryArea());
        taskCorpSecBasicInfo.setStaffNum(basic.getStaffNum());
        taskCorpSecBasicInfo.setSafetyStandard(basic.getSafetyStandard());
        taskCorpSecBasicInfo.setSafetyManagementOrg(basic.getSafetyManagementOrg());
        taskCorpSecBasicInfo.setMergencyRecordTime(basic.getMergencyRecordTime());
        taskCorpSecBasicInfo.setMergencyStatus(basic.getMergencyStatus());
        taskCorpSecBasicInfo.setMergencyRegularExercise(basic.getMergencyRegularExercise());
        taskCorpSecBasicInfo.setMergencyRecordStatus(basic.getMergencyRecordStatus());
        taskCorpSecBasicInfo.setDualPreventionMechanismStatus(basic.getDualPreventionMechanismStatus());

        taskCorpSecBasicInfo.setBigRiskInfoList(convertTaskSecBigRiskToCorpSecBigRiskInfoWithList(basic.getBigRisks(),taskId));
        taskCorpSecBasicInfo.setEquipInfos(convertEquipInfoToTaskEquipInfo(basic.getEquipInfos(),taskId));
        return taskCorpSecBasicInfo;
    }


    public static TaskCorpSecBasicInfo convertBasicInfoToTaskSecBasicInfo(CorpBasicInfo basicInfo, String taskId){
        TaskCorpSecBasicInfo secBasicInfo = new TaskCorpSecBasicInfo();
        secBasicInfo.setCertNo(basicInfo.getCertNo());
        secBasicInfo.setId(basicInfo.getId());
        secBasicInfo.setCorpAddress(basicInfo.getAddress());
        secBasicInfo.setCorpName(basicInfo.getName());
        secBasicInfo.setMajorName(basicInfo.getJuridical());
        secBasicInfo.setTaskId(taskId);
        return secBasicInfo;
    }


    public static CorpSecCorpBasic convertTaskSecCorpBasicToSecBasicInfo(TaskCorpSecBasicInfo taskCorpSecBasicInfo){
        CorpSecCorpBasic corpSecBasicInfo = new CorpSecCorpBasic();
        corpSecBasicInfo.setId(taskCorpSecBasicInfo.getCorpId());
        corpSecBasicInfo.setCorpName(taskCorpSecBasicInfo.getCorpName());
        corpSecBasicInfo.setCorpScale(taskCorpSecBasicInfo.getCorpScale());
        corpSecBasicInfo.setRemark(taskCorpSecBasicInfo.getRemark());
        corpSecBasicInfo.setCertNo(taskCorpSecBasicInfo.getCertNo());
        corpSecBasicInfo.setAdminArea(taskCorpSecBasicInfo.getAdminArea());
        corpSecBasicInfo.setBusinessType(taskCorpSecBasicInfo.getBusinessType());
        corpSecBasicInfo.setMajorName(taskCorpSecBasicInfo.getMajorName());
        corpSecBasicInfo.setCorpAddress(taskCorpSecBasicInfo.getCorpAddress());
        corpSecBasicInfo.setSafeName(taskCorpSecBasicInfo.getSafeName());
        corpSecBasicInfo.setVipIndustryArea(taskCorpSecBasicInfo.getVipIndustryArea());
        corpSecBasicInfo.setStaffNum(taskCorpSecBasicInfo.getStaffNum());
        corpSecBasicInfo.setSafetyStandard(taskCorpSecBasicInfo.getSafetyStandard());
        corpSecBasicInfo.setSafetyManagementOrg(taskCorpSecBasicInfo.getSafetyManagementOrg());
        corpSecBasicInfo.setMergencyRecordTime(taskCorpSecBasicInfo.getMergencyRecordTime());
        corpSecBasicInfo.setDualPreventionMechanismStatus(taskCorpSecBasicInfo.getDualPreventionMechanismStatus());
        corpSecBasicInfo.setMergencyStatus(taskCorpSecBasicInfo.getMergencyStatus());
        corpSecBasicInfo.setMergencyRegularExercise(taskCorpSecBasicInfo.getMergencyRegularExercise());
        corpSecBasicInfo.setMergencyRecordStatus(taskCorpSecBasicInfo.getMergencyRecordStatus());
        return corpSecBasicInfo;
    }

    public static CorpSecBigRiskInfo convertTaskSecBigRiskToCorpSecBigRiskInfo(TaskSecBigRiskInfo taskSecBigRiskInfo){
        CorpSecBigRiskInfo corpSecBigRiskInfo = new CorpSecBigRiskInfo();
        corpSecBigRiskInfo.setCorpId(taskSecBigRiskInfo.getCorpId());
        corpSecBigRiskInfo.setRiskCode(taskSecBigRiskInfo.getRiskCode());
        corpSecBigRiskInfo.setRiskName(taskSecBigRiskInfo.getRiskName());
        corpSecBigRiskInfo.setRiskPoint(taskSecBigRiskInfo.getRiskPoint());
        corpSecBigRiskInfo.setRiskPosition(taskSecBigRiskInfo.getRiskPosition());
        corpSecBigRiskInfo.setRemark(taskSecBigRiskInfo.getRemark());
        return corpSecBigRiskInfo;
    }

    public static List<CorpSecBigRiskInfo> convertTaskSecBigRiskToCorpSecBigRiskInfoWithList(List<TaskSecBigRiskInfo> taskSecBigRiskInfoList){
        List<CorpSecBigRiskInfo> aRes = new ArrayList<>();
        for (TaskSecBigRiskInfo taskSecBigRiskInfo: taskSecBigRiskInfoList){
            aRes.add(convertTaskSecBigRiskToCorpSecBigRiskInfo(taskSecBigRiskInfo));
        }
        return aRes;
    }

    private static TaskSecBigRiskInfo convertCorpSecBigRiskToTaskSecBigRiskInfo(CorpSecBigRiskInfo corpSecBigRiskInfo, String taskId){
        TaskSecBigRiskInfo taskSecBigRiskInfo = new TaskSecBigRiskInfo();
        taskSecBigRiskInfo.setTaskId(taskId);
        taskSecBigRiskInfo.setCorpId(corpSecBigRiskInfo.getCorpId());
        taskSecBigRiskInfo.setRiskCode(corpSecBigRiskInfo.getRiskCode());
        taskSecBigRiskInfo.setRiskName(corpSecBigRiskInfo.getRiskName());
        taskSecBigRiskInfo.setRiskPoint(corpSecBigRiskInfo.getRiskPoint());
        taskSecBigRiskInfo.setRiskPosition(corpSecBigRiskInfo.getRiskPosition());
        taskSecBigRiskInfo.setRemark(corpSecBigRiskInfo.getRemark());
        return taskSecBigRiskInfo;
    }

    private static List<TaskSecBigRiskInfo> convertTaskSecBigRiskToCorpSecBigRiskInfoWithList(List<CorpSecBigRiskInfo> CorpSecBigRiskInfoList, String taskId){
        List<TaskSecBigRiskInfo> aRes = new ArrayList<>();
        for (CorpSecBigRiskInfo corpSecBigRiskInfo: CorpSecBigRiskInfoList){
            aRes.add(convertCorpSecBigRiskToTaskSecBigRiskInfo(corpSecBigRiskInfo, taskId));
        }
        return aRes;
    }

    public static List<CorpSecEquipInfo> convertTaskSecEquipInfoToCorpSecEquipInfoWithList(List<TaskSecEquipInfo> taskSecEquipInfos){
        List<CorpSecEquipInfo> aRes = new ArrayList<>();
        for (TaskSecEquipInfo taskSecEquipInfo : taskSecEquipInfos){
            aRes.add(convertTaskSecEquipInfoToCorpSecEquipInfo(taskSecEquipInfo));
        }
        return aRes;
    }

    private static CorpSecEquipInfo convertTaskSecEquipInfoToCorpSecEquipInfo(TaskSecEquipInfo item) {
        CorpSecEquipInfo epEquipItem = new CorpSecEquipInfo();
        epEquipItem.setCorpId(item.getCorpId());
        epEquipItem.setEquipName(item.getEquipName());
        epEquipItem.setEquipNum(item.getEquipNum());
        epEquipItem.setEquipSource(item.getEquipSource());
        epEquipItem.setRemark(item.getRemark());
        epEquipItem.setStatus(item.getStatus());
        return epEquipItem;
    }


    private static List<TaskSecEquipInfo> convertEquipInfoToTaskEquipInfo(List<CorpSecEquipInfo> equipItems, String taskId) {
        List<TaskSecEquipInfo> aRes = new ArrayList<>();
        for (CorpSecEquipInfo item : equipItems){
            if (item != null){
                aRes.add(convertEquipInfoToTaskEquipInfo(item,taskId));
            }
        }
        return aRes;
    }

    private static TaskSecEquipInfo convertEquipInfoToTaskEquipInfo(CorpSecEquipInfo item, String taskId) {
        TaskSecEquipInfo taskEpEquipItem = new TaskSecEquipInfo();
        taskEpEquipItem.setCorpId(item.getCorpId());
        taskEpEquipItem.setEquipName(item.getEquipName());
        taskEpEquipItem.setEquipNum(item.getEquipNum());
        taskEpEquipItem.setEquipSource(item.getEquipSource());
        taskEpEquipItem.setTaskId(taskId);
        taskEpEquipItem.setRemark(item.getRemark());
        taskEpEquipItem.setStatus(item.getStatus());
        return taskEpEquipItem;
    }


}
