package com.enjoychain.www.security.mapper;

import com.enjoychain.www.security.entity.TaskSolidWaste;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * 一般固废仓库 Mapper 接口
 * </p>
 *
 * @author ddmonk
 * @since 2023-02-24
 */
public interface TaskSolidWasteMapper extends BaseMapper<TaskSolidWaste> {

}
