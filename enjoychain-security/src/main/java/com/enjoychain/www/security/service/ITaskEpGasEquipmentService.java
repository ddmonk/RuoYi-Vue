package com.enjoychain.www.security.service;

import com.enjoychain.www.security.entity.TaskEpGasEquipment;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author ddmonk
 * @since 2023-03-07
 */
public interface ITaskEpGasEquipmentService extends ITaskService<TaskEpGasEquipment> {

}
