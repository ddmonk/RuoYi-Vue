package com.enjoychain.www.security.service.impl;

import com.enjoychain.www.security.entity.*;
import com.enjoychain.www.security.pojo.TaskPojo;
import com.enjoychain.www.security.service.*;
import com.enjoychain.www.security.service.impl.task.TaskSecConverts;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class TaskSecServiceImpl implements ITaskSecService {

    @Autowired
    ITaskCorpSecBasicInfoService taskCorpSecBasicInfoService;


    @Autowired
    ITaskSecPatrolItemService taskSecDetailService;


    @Autowired
    ITaskSecBigRiskInfoService taskSecBigRiskInfoService;


    @Autowired
    ICorpSecCorpBasicService corpSecCorpBasicService;

    @Autowired
    ITaskSecEquipInfoService taskSecEquipInfoService;

    @Override
    public TaskCorpSecBasicInfo getCorpSecBasicInfoByTaskId(String taskId) {
        TaskCorpSecBasicInfo basicInfo =  taskCorpSecBasicInfoService.getByTaskId(taskId);
        basicInfo.setBigRiskInfoList(taskSecBigRiskInfoService.listByTaskId(taskId));
        basicInfo.setEquipInfos(taskSecEquipInfoService.listByTaskId(taskId));
        return basicInfo;
    }

    @Override
    public List<TaskSecPatrolItem> getSecTaskDetailByTaskId(String taskId) {
        return taskSecDetailService.listDetailByTaskId(taskId);
    }

    @Override
    public void removeByTaskId(String taskId) {
        taskCorpSecBasicInfoService.removeByTaskId(taskId);
        taskSecDetailService.removeByTaskId(taskId);
    }

    @Override
    public void saveSecCorpBasicInfo(TaskPojo taskPojo) {
        taskCorpSecBasicInfoService.saveDetailBasicInfo(taskPojo.getSecBasicInfo());
        //更新巡查信息内的默认值
        taskSecDetailService.updateByCorpSecBasicInfo(taskPojo.getSecBasicInfo());
    }

    @Override
    public void saveSecTaskDetail(TaskPojo taskPojo) {
        taskSecDetailService.updatePatrolItems(taskPojo.getSecBasicInfo(),taskPojo.getSecTaskDetail());
    }

    @Override
    public void buildSecTask(CorpBasicInfo basicInfo, String taskId) {
        CorpSecCorpBasic secCorpBasic = corpSecCorpBasicService.getById(basicInfo.getId());
        if (secCorpBasic == null) {
            corpSecCorpBasicService.inital(basicInfo);
        }
        TaskCorpSecBasicInfo secBasicInfo = TaskSecConverts.convertSecBasicInfoToTaskCorpBasicInfo(corpSecCorpBasicService.getDetailById(basicInfo.getId()),taskId);
        taskCorpSecBasicInfoService.saveDetailBasicInfo(secBasicInfo);
        taskSecDetailService.inital(secBasicInfo,taskId);
    }




}
