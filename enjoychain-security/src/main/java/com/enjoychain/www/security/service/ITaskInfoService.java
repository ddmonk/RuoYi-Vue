package com.enjoychain.www.security.service;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.enjoychain.www.security.constant.BusinessTaskType;
import com.enjoychain.www.security.entity.task.TaskInfo;
import com.baomidou.mybatisplus.extension.service.IService;
import com.enjoychain.www.security.pojo.TaskPojo;

import java.util.List;

/**
 * <p>
 * 任务基础信息表 服务类
 * </p>
 *
 * @author ddmonk
 * @since 2023-01-20
 */
public interface ITaskInfoService extends IService<TaskInfo> {

    TaskInfo getTaskDetailById(String id);

    boolean removeBatchByIdsWithItems(List<String> asList);

    IPage<TaskInfo> getPageByAccId(Page<TaskInfo> page, String accId);

    boolean buildTask(String jobId, String taskId);

    TaskPojo buildNewTask(String certNo, BusinessTaskType type, String taskType);

    TaskPojo update(TaskPojo taskPojo, boolean isCorrect);

    TaskPojo getTaskPojo(String taskId);

    void removeByTaskId(String taskId);

}
