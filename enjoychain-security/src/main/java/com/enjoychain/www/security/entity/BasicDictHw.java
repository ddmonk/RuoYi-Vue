package com.enjoychain.www.security.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import java.io.Serializable;
import com.ruoyi.common.annotation.Excel;
import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;
import lombok.experimental.Accessors;

/**
 * <p>
 * 危险废物名录字典
 * </p>
 *
 * @author ddmonk
 * @since 2023-03-08
 */
@Getter
@Setter
@Accessors(chain = true)
@TableName("basic_dict_hw")
@ApiModel(value = "BasicDictHw对象", description = "危险废物名录字典")
public class BasicDictHw implements Serializable {

    private static final long serialVersionUID = 1L;

    @TableId(value = "id", type = IdType.AUTO)
    private Integer id;

    @ApiModelProperty("物类别")
    @Excel(name = "物类别")
    private String category;

    @ApiModelProperty("行业来源")
    @Excel(name = "行业来源")
    private String source;

    @ApiModelProperty("危废代码")
    @Excel(name = "危废代码")
    private String hwCode;

    @ApiModelProperty("危险废物")
    @Excel(name = "危险废物")
    private String hwName;

    @ApiModelProperty("危废特性")
    @Excel(name = "危废特性")
    private String property;


}
