package com.enjoychain.www.security.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.enjoychain.www.security.entity.TaskCorpEnvBasicInfo;
import com.enjoychain.www.security.entity.TaskEpHwItem;
import com.enjoychain.www.security.mapper.TaskEpHwItemMapper;
import com.enjoychain.www.security.service.ITaskEpHwItemService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author ddmonk
 * @since 2023-03-07
 */
@Service
public class TaskEpHwItemServiceImpl extends ServiceImpl<TaskEpHwItemMapper, TaskEpHwItem> implements ITaskEpHwItemService {

}
