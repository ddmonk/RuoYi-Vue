package com.enjoychain.www.security.service;

import com.enjoychain.www.security.entity.CorpEpGasEquipment;
import com.enjoychain.www.security.entity.TaskProductSectionItem;
import com.baomidou.mybatisplus.extension.service.IService;

import java.util.List;

/**
 * <p>
 * 任务_工段明细 服务类
 * </p>
 *
 * @author ddmonk
 * @since 2023-02-24
 */
public interface ITaskProductSectionItemService extends ITaskService<TaskProductSectionItem> {

    List<TaskProductSectionItem> listByTaskId(String id);

    void removeDetailByTaskId(String taskId);
}
