package com.enjoychain.www.security.service;

import com.enjoychain.www.security.entity.CorpBasicInfo;
import com.enjoychain.www.security.entity.job.JobInfo;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 * 作业基础表 服务类
 * </p>
 *
 * @author ddmonk
 * @since 2023-01-29
 */
public interface IJobInfoService extends IService<JobInfo> {

    JobInfo getJobDetail(String id);


    boolean start(String id);

    boolean buildSingleTask(CorpBasicInfo corp, JobInfo info);
}
