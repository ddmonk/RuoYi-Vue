package com.enjoychain.www.security.service.impl;

import com.enjoychain.www.security.entity.CorpSecPromiseInfo;
import com.enjoychain.www.security.mapper.CorpSecPromiseInfoMapper;
import com.enjoychain.www.security.service.ICorpSecPromiseInfoService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 安全承诺信息 服务实现类
 * </p>
 *
 * @author ddmonk
 * @since 2023-01-13
 */
@Service
public class CorpSecPromiseInfoServiceImpl extends ServiceImpl<CorpSecPromiseInfoMapper, CorpSecPromiseInfo> implements ICorpSecPromiseInfoService {

}
