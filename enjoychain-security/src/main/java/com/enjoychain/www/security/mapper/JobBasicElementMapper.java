package com.enjoychain.www.security.mapper;

import com.enjoychain.www.security.entity.JobBasicElement;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * 核查任务要素信息 Mapper 接口
 * </p>
 *
 * @author ddmonk
 * @since 2023-01-30
 */
public interface JobBasicElementMapper extends BaseMapper<JobBasicElement> {

}
