package com.enjoychain.www.security.mapper;

import com.enjoychain.www.security.entity.TaskCorpSecBasicInfo;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author ddmonk
 * @since 2023-04-18
 */
public interface TaskCorpSecBasicInfoMapper extends BaseMapper<TaskCorpSecBasicInfo> {

}
