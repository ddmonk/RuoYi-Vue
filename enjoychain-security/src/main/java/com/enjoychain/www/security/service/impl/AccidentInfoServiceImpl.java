package com.enjoychain.www.security.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.enjoychain.www.security.entity.accident.AccidentDetailItem;
import com.enjoychain.www.security.entity.accident.AccidentInfo;
import com.enjoychain.www.security.entity.task.TaskDetailItem;
import com.enjoychain.www.security.entity.task.TaskInfo;
import com.enjoychain.www.security.mapper.AccidentInfoMapper;
import com.enjoychain.www.security.service.IAccidentDetailItemService;
import com.enjoychain.www.security.service.IAccidentInfoService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.enjoychain.www.security.service.ITaskInfoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.io.IOException;

/**
 * <p>
 * 问题基础表 服务实现类
 * </p>
 *
 * @author ddmonk
 * @since 2023-01-29
 */
@Service
public class AccidentInfoServiceImpl extends ServiceImpl<AccidentInfoMapper, AccidentInfo> implements IAccidentInfoService {

    @Autowired
    private IAccidentDetailItemService itemService;

    @Autowired
    private ITaskInfoService taskInfoService;



    @Override
    public AccidentInfo getDetailByAccId(String id) {
        AccidentInfo info = getById(id);
        info.setItems(itemService.listByAccId(id));
        return info;
    }

    @Override
    public boolean saveByTaskDetailItem(TaskDetailItem item) throws IOException {
        TaskInfo taskInfo = taskInfoService.getById(item.getTaskId());
        StringBuilder accName = new StringBuilder();
        accName.append(taskInfo.getName()).append("_").append(item.getTargetName()).append("_").append(item.getKeyName());
        AccidentInfo info = AccidentInfo.builder()
                .accName(accName.toString())
                .status(0)
                .taskId(item.getTaskId())
                .accLevel(item.getItemLevel())
                .basicInfo(item.getBasicInfo())
                .content(item.getContent())
                .corpId(taskInfo.getCorpId())
                .corpName(taskInfo.getCorpName())
                .keyName(item.getKeyName())
                .keyId(item.getKeyId())
                .taskName(taskInfo.getName())
                .targetId(item.getTargetId())
                .targetName(item.getTargetName())
                .build();
        this.baseMapper.insert(info);
        return itemService.saveByTaskItem(item,info.getId());
    }
}
