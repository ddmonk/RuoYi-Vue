package com.enjoychain.www.security.mapper;

import com.enjoychain.www.security.entity.accident.AccidentDetailItem;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author ddmonk
 * @since 2023-01-29
 */
public interface AccidentDetailItemMapper extends BaseMapper<AccidentDetailItem> {

}
