package com.enjoychain.www.security.service;

import com.enjoychain.www.security.entity.CorpEpNoiseProtect;
import com.baomidou.mybatisplus.extension.service.IService;

import java.util.List;

/**
 * <p>
 * 企业噪声管理情况 服务类
 * </p>
 *
 * @author ddmonk
 * @since 2023-01-13
 */
public interface ICorpEpNoiseProtectService extends IService<CorpEpNoiseProtect> {

    List<CorpEpNoiseProtect> listWithStatus(int status, String corpId);

}
