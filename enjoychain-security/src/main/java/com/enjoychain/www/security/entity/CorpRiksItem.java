package com.enjoychain.www.security.entity;

import com.baomidou.mybatisplus.annotation.TableName;
import java.io.Serializable;
import com.ruoyi.common.annotation.Excel;
import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;
import lombok.experimental.Accessors;

/**
 * <p>
 * 公司较大以上安全风险信息表
 * </p>
 *
 * @author ddmonk
 * @since 2023-01-13
 */
@Getter
@Setter
@Accessors(chain = true)
@TableName("corp_riks_item")
@ApiModel(value = "CorpRiksItem对象", description = "公司较大以上安全风险信息表")
public class CorpRiksItem implements Serializable {

    private static final long serialVersionUID = 1L;

    @ApiModelProperty("主键")
    @Excel(name = "主键")
    private String id;

    @ApiModelProperty("风险代码")
    @Excel(name = "风险代码")
    private String riskCode;

    @ApiModelProperty("风险名称")
    @Excel(name = "风险名称")
    private String riskName;

    @ApiModelProperty("风险所在位置")
    @Excel(name = "风险所在位置")
    private String position;

    @ApiModelProperty("风险点")
    @Excel(name = "风险点")
    private String riskPoint;


}
