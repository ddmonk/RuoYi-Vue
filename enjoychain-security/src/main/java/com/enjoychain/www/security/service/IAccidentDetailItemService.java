package com.enjoychain.www.security.service;

import com.enjoychain.www.security.entity.accident.AccidentDetailItem;
import com.baomidou.mybatisplus.extension.service.IService;
import com.enjoychain.www.security.entity.task.TaskDetailItem;

import java.io.IOException;
import java.util.List;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author ddmonk
 * @since 2023-01-29
 */
public interface IAccidentDetailItemService extends IService<AccidentDetailItem> {

    List<AccidentDetailItem> listByAccId(String accId);


    AccidentDetailItem getItemDetail(String id);

    boolean relateTaskByTaskId(String accId, String taskId) throws IOException;

    boolean removeRelationTask(String id);

    boolean saveOrUpdateAndChangeStatus(AccidentDetailItem entity);

    boolean saveByTaskItem(TaskDetailItem item, String id) throws IOException;
}
