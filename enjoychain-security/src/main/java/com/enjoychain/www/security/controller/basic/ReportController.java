package com.enjoychain.www.security.controller.basic;

import com.enjoychain.www.security.constant.BasicInfoType;
import com.enjoychain.www.security.entity.CorpSecProductInfo;
import com.enjoychain.www.security.service.IReportService;
import com.ruoyi.common.core.domain.AjaxResult;
import io.swagger.annotations.*;
import org.aspectj.weaver.loadtime.Aj;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.Map;

@Api(tags = "报表信息")
@RestController
@RequestMapping("/report")
public class ReportController {


    @Autowired
    IReportService reportService;

    @ApiOperation(value = "获取当前企业基础信息报表")
    @ApiResponses({
            @ApiResponse(code = 200, message = "ok", response= AjaxResult.class),
    })
    @GetMapping("/basic/corp/{corpId}")
    public AjaxResult downloadBasicInfoByCorpId(@PathVariable("corpId") String corpId){
        try {
            return AjaxResult.success(reportService.buildBasicInfoById(corpId, BasicInfoType.CORP));
        } catch (Exception e) {
            e.printStackTrace();
            return AjaxResult.error();
        }
    }

    @ApiOperation(value = "获取当前任务企业信息报表")
    @ApiResponses({
            @ApiResponse(code = 200, message = "ok", response= CorpSecProductInfo.class),
    })
    @GetMapping(value = "/basic/task/{taskId}", produces = {"application/json"})
    public AjaxResult downloadBasicInfoByTaskId(@PathVariable("taskId") String taskId){
        try {

            return AjaxResult.success(reportService.buildBasicInfoById(taskId,BasicInfoType.TASK));
        } catch (Exception e) {
            e.printStackTrace();
            return AjaxResult.error();
        }
    }

    @ApiOperation(value = "获取当前巡查结果报表")
    @ApiResponses({
            @ApiResponse(code = 200, message = "ok", response= AjaxResult.class),
    })
    @GetMapping("/task/{taskId}")
    public AjaxResult downloadTaskInfoByTaskId(@PathVariable("taskId") String taskId){
        try {
            return AjaxResult.success(reportService.buildTaskInfoByTaskId(taskId));
        } catch (Exception e) {
            e.printStackTrace();
            return AjaxResult.error();
        }
    }

    @ApiOperation(value = "获取当前企业核查承诺书")
    @ApiResponses({
            @ApiResponse(code = 200, message = "ok", response= AjaxResult.class),
    })
    @GetMapping("/company/promise/{taskId}")
    public AjaxResult downloadCompanyPromiseByTaskId(@PathVariable("taskId") String taskId){
        try {
            return AjaxResult.success(reportService.buildCompanyPromiseByTaskId(taskId));
        } catch (Exception e) {
            e.printStackTrace();
            return AjaxResult.error();
        }
    }

    @ApiOperation(value = "获取当前企业核查表")
    @ApiResponses({
            @ApiResponse(code = 200, message = "ok", response= AjaxResult.class),
    })
    @GetMapping("/task/check/{taskId}")
    public AjaxResult downloadTaskCheckByTaskId(@PathVariable("taskId") String taskId){
        try {
            return AjaxResult.success(reportService.buildCheckListByTaskId(taskId));
        } catch (Exception e) {
            e.printStackTrace();
            return AjaxResult.error();
        }
    }


    @ApiOperation(value = "获取当前任务报表")
    @ApiResponses({
            @ApiResponse(code = 200, message = "ok", response= AjaxResult.class),
    })
    @GetMapping("/task/all/{taskId}")
    public AjaxResult downloadAllByTaskId(@PathVariable("taskId") String taskId){
        try {
            return AjaxResult.success(reportService.buildTaskAllByTaskId(taskId));
        } catch (Exception e) {
            e.printStackTrace();
            return AjaxResult.error();
        }
    }


    @ApiOperation(value = "获取常规企业核查报告")
    @ApiResponses({
            @ApiResponse(code = 200, message = "ok", response= AjaxResult.class),
    })
    @GetMapping("/common/{taskId}")
    public AjaxResult downloadCompanyDailyTaskByTaskId(@PathVariable("taskId") String taskId){
        try {
            return AjaxResult.success(reportService.buildDailyTaskReportByTaskId(taskId));
        } catch (Exception e) {
            e.printStackTrace();
            return AjaxResult.error();
        }
    }

}
