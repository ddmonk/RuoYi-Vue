package com.enjoychain.www.security.service.impl;

import com.enjoychain.www.security.entity.*;
import com.enjoychain.www.security.mapper.TaskCorpSecBasicInfoMapper;
import com.enjoychain.www.security.service.ITaskCorpSecBasicInfoService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.enjoychain.www.security.service.ITaskSecBigRiskInfoService;
import com.enjoychain.www.security.service.ITaskSecEquipInfoService;
import com.enjoychain.www.security.service.impl.task.TaskSecConverts;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author ddmonk
 * @since 2023-04-18
 */
@Service
public class TaskCorpSecBasicInfoServiceImpl extends ServiceImpl<TaskCorpSecBasicInfoMapper, TaskCorpSecBasicInfo> implements ITaskCorpSecBasicInfoService {

    @Autowired
    ITaskSecBigRiskInfoService bigRiskInfoService;

    @Autowired
    ITaskSecEquipInfoService taskSecEquipInfoService;

    @Override
    public TaskCorpSecBasicInfo inital(CorpBasicInfo basicInfo, String taskId) {
        this.save(TaskSecConverts.convertBasicInfoToTaskSecBasicInfo(basicInfo,taskId));
        return this.getByTaskId(taskId);
    }

    @Override
    @Transactional
    public void saveDetailBasicInfo(TaskCorpSecBasicInfo basic) {
        this.saveOrUpdate(basic);
        bigRiskInfoService.removeByTaskId(basic.getTaskId());
        if(basic.getBigRiskInfoList() != null) {
            for (TaskSecBigRiskInfo bigRiskInfo : basic.getBigRiskInfoList()) {
                if (StringUtils.isEmpty(bigRiskInfo.getTaskId())) {
                    bigRiskInfo.setTaskId(basic.getTaskId());
                    bigRiskInfo.setCorpId(basic.getCorpId());
                }
            }
        }
        bigRiskInfoService.saveOrUpdateBatch(basic.getBigRiskInfoList());

        taskSecEquipInfoService.removeByTaskId(basic.getTaskId());
        if(basic.getEquipInfos() != null) {
            for (TaskSecEquipInfo equipInfo : basic.getEquipInfos()) {
                if (StringUtils.isEmpty(equipInfo.getTaskId())) {
                    equipInfo.setTaskId(basic.getTaskId());
                    equipInfo.setCorpId(basic.getCorpId());
                }
            }
        }
        taskSecEquipInfoService.saveOrUpdateBatch(basic.getEquipInfos());

    }
}
