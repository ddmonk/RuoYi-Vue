package com.enjoychain.www.security.controller.business.accident;

import org.springframework.web.bind.annotation.RequestMapping;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.common.utils.StringUtils;
import com.ruoyi.common.utils.poi.ExcelUtil;
import io.swagger.annotations.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import javax.servlet.http.HttpServletResponse;
import java.util.List;
import java.util.Arrays;
import com.enjoychain.www.security.entity.accident.AccidentInfo;
import com.enjoychain.www.security.service.IAccidentInfoService;
import org.springframework.web.bind.annotation.RestController;

/**
 * <p>
 * 问题基础表 前端控制器
 * </p>
 *
 * @author ddmonk
 * @since 2023-01-29
 */
@Api(tags = "问题基础表")
@RestController
@RequestMapping("/security/accidentInfo")
public class AccidentInfoController {

    @Autowired
    IAccidentInfoService service;

    @PreAuthorize("@ss.hasPermi('accident:basic:list')")
    @GetMapping(path = "/list", produces = {"application/json"})
    @ApiOperation(value = "获取问题基础表列表")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "name", value = "name",paramType = "query", dataType = "String"),
            @ApiImplicitParam(name = "corpName", value = "corpName",paramType = "query", dataType = "String"),
            @ApiImplicitParam(name = "corpId", value = "corpId",paramType = "query", dataType = "String"),
            @ApiImplicitParam(name = "level", value = "等级",paramType = "query", dataType = "String"),
            @ApiImplicitParam(name = "status", value = "当前状态",paramType = "query", dataType = "String"),
            @ApiImplicitParam(name = "beginTime", value = "事件发生时间[开始时间]",paramType = "query", dataType = "String"),
            @ApiImplicitParam(name = "endTime", value = "事件发生时间[结束时间]",paramType = "query", dataType = "String"),
            @ApiImplicitParam(name = "pageNo", value = "页码", paramType = "query", dataType = "long"),
            @ApiImplicitParam(name = "pageSize", value = "页面记录条数", paramType = "query", dataType = "long")
    })
    @ApiResponses({
        @ApiResponse(code = 200, message = "ok", response= AccidentInfo.class),
    })
    public AjaxResult list(
                            @RequestParam(name = "corpId", required = false) String corpId,
                            @RequestParam(name = "corpName", required = false) String corpName,
                            @RequestParam(name = "name", required = false) String name,
                            @RequestParam(name = "level", required = false) String level,
                            @RequestParam(name = "status", required = false) String status,
                            @RequestParam(name = "beginTime", required = false) String beginTime,
                            @RequestParam(name = "endTime", required = false) String endTime,
                            @RequestParam(name = "pageNo", defaultValue = "1") long pageNo,
                            @RequestParam(name = "pageSize",defaultValue = "10") long pageSize){
        IPage<AccidentInfo> entityIPage = new Page<>(pageNo, pageSize);
        QueryWrapper<AccidentInfo> queryWrapper = new QueryWrapper<>();
        if (StringUtils.isNotEmpty(corpId)) {
            queryWrapper.eq("corp_id",corpId);
        }
        if (StringUtils.isNotEmpty(corpName)) {
            queryWrapper.like("corp_name",corpName);
        }
        if (StringUtils.isNotEmpty(name)) {
            queryWrapper.like("acc_name",name);
        }
        if (StringUtils.isNotEmpty(level)) {
            queryWrapper.eq("acc_level",level);
        }
        if (StringUtils.isNotEmpty(status)) {
            queryWrapper.eq("status",status);
        }
        if (StringUtils.isNotEmpty(beginTime) && StringUtils.isNotEmpty(endTime)) {
            queryWrapper.between("create_time",beginTime,endTime);
        }
        return AjaxResult.success("list-accident_info",service.page(entityIPage,queryWrapper));
    }

    @PreAuthorize("@ss.hasPermi('accident:basic:query')")
    @GetMapping(value = "{id}")
    @ApiOperation("获取问题基础表详情")
    @ApiImplicitParams({
        @ApiImplicitParam(name = "id", value = "id",paramType = "path", dataType = "String")
    })
    @ApiResponses({
        @ApiResponse(code = 200, message = "ok", response = AccidentInfo.class)
    })
    public AjaxResult getAccidentInfoInfo(@PathVariable(value = "id", required = false) String id)
    {
        return AjaxResult.success("get-accident_info",service.getDetailByAccId(id));
    }

    @PreAuthorize("@ss.hasPermi('accident:basic:import')")
    @ApiOperation("导入问题基础表")
    @Log(title = "问题基础表", businessType = BusinessType.IMPORT)
    @PostMapping("/importData")
    public AjaxResult importData(MultipartFile file) throws Exception{
        ExcelUtil<AccidentInfo> util = new ExcelUtil<AccidentInfo>(AccidentInfo.class);
        List<AccidentInfo> infos = util.importExcel(file.getInputStream());
        return service.saveOrUpdateBatch(infos)?AjaxResult.success("批量插入成功"):AjaxResult.error("批量插入失败");
    }


    @ApiOperation("问题基础表导出模板下载")
    @PostMapping("/importTemplate")
    public void importTemplate(HttpServletResponse response){
        ExcelUtil<AccidentInfo> util = new ExcelUtil<AccidentInfo>(AccidentInfo.class);
        util.importTemplateExcel(response,"问题基础表");
    }

    @PreAuthorize("@ss.hasPermi('accident:basic:add')")
    @PostMapping(consumes = {"application/json"}, produces = {"application/json"})
    @ApiOperation("新增问题基础表")
    @ApiResponses({
        @ApiResponse(code = 200, message = "ok", response = AccidentInfo.class),
    })
    @Log(title = "问题基础表", businessType = BusinessType.INSERT)
    public AjaxResult add(@RequestBody AccidentInfo entity){
        return service.save(entity)? AjaxResult.success("add-accident_info",entity): AjaxResult.error("新增问题基础表失败");
    }


    @PreAuthorize("@ss.hasPermi('accident:basic:update')")
    @PutMapping(consumes = {"application/json"}, produces = {"application/json"})
    @ApiOperation("修改问题基础表")
    @ApiResponses({
        @ApiResponse(code = 200, message = "ok", response = AccidentInfo.class)
    })
    @Log(title = "问题基础表", businessType = BusinessType.UPDATE)
    public AjaxResult update(@RequestBody AccidentInfo corpBasicInfo){
        String operate = "update-accident_info";
        return service.updateById(corpBasicInfo)?AjaxResult.success(operate,service.getById(corpBasicInfo.getId())) : AjaxResult.error("修改客户基础信息失败");
    }

    @PreAuthorize("@ss.hasPermi('accident:basic:delete')")
    @DeleteMapping(value = "{id}", produces = {"application/json"})
    @ApiOperation("删除问题基础表")
    @ApiResponses({
        @ApiResponse(code = 200, message = "ok", response = AjaxResult.class),
    })
    @Log(title = "问题基础表", businessType = BusinessType.DELETE)
    public AjaxResult delete(@PathVariable(value = "id", required = false) String id){
        String operate = "delete-accident_info";
        String[] ids = id.split(",");
        return service.removeBatchByIds(Arrays.asList(ids))?AjaxResult.success(operate) : AjaxResult.error("修改问题基础表失败");
    }


    @Log(title = "问题基础表", businessType = BusinessType.EXPORT)
    @PreAuthorize("@ss.hasPermi('accident:basic:export')")
    @PostMapping("/export")
    @ApiOperation("导出问题基础表")
    @ApiResponses({
        @ApiResponse(code = 200, message = "ok", response = AjaxResult.class),
    })
    public void export(HttpServletResponse response,@RequestParam(name = "corpId", required = false) String corpId){
        QueryWrapper<AccidentInfo> queryWrapper = new QueryWrapper<>();
        if (StringUtils.isNotEmpty(corpId)) {
            queryWrapper.eq("corp_id",corpId);
        }
        List<AccidentInfo> list = service.list(queryWrapper);
        ExcelUtil<AccidentInfo> util = new ExcelUtil<AccidentInfo>(AccidentInfo.class);
        util.exportExcel(response,list, "导出问题基础表");
    }




}
