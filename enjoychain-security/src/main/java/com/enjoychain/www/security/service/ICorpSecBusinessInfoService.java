package com.enjoychain.www.security.service;

import com.enjoychain.www.security.entity.CorpSecBusinessInfo;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 * 安全企业行业类型信息 服务类
 * </p>
 *
 * @author ddmonk
 * @since 2023-01-13
 */
public interface ICorpSecBusinessInfoService extends IService<CorpSecBusinessInfo> {

}
