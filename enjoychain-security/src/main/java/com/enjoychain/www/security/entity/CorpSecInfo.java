package com.enjoychain.www.security.entity;

import com.baomidou.mybatisplus.annotation.TableName;
import java.io.Serializable;
import com.ruoyi.common.annotation.Excel;
import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;
import lombok.experimental.Accessors;

/**
 * <p>
 * 企业安全生产基础信息表
 * </p>
 *
 * @author ddmonk
 * @since 2023-01-13
 */
@Getter
@Setter
@Accessors(chain = true)
@TableName("corp_sec_info")
@ApiModel(value = "CorpSecInfo对象", description = "企业安全生产基础信息表")
public class CorpSecInfo implements Serializable {

    private static final long serialVersionUID = 1L;

    @ApiModelProperty("主键id")
    @Excel(name = "主键id")
    private String id;

    @ApiModelProperty("公司主键")
    @Excel(name = "公司主键")
    private String corpId;

    @ApiModelProperty("主要负责人")
    @Excel(name = "主要负责人")
    private String principal;

    @ApiModelProperty("安全负责人")
    @Excel(name = "安全负责人")
    private String secMaster;

    @ApiModelProperty("安全管理机构")
    @Excel(name = "安全管理机构")
    private String adOrg;

    @ApiModelProperty("职工人数")
    @Excel(name = "职工人数")
    private Integer empNum;

    @ApiModelProperty("安全生产标准化等级")
    @Excel(name = "安全生产标准化等级")
    private String productCrit;

    @ApiModelProperty("重点领域标签")
    @Excel(name = "重点领域标签")
    private String keyAreaFlag;

    @ApiModelProperty("安全生产承诺书")
    @Excel(name = "安全生产承诺书")
    private String safetyPromiseNote;


}
