package com.enjoychain.www.security.service;

import com.enjoychain.www.security.entity.CorpBasicInfo;
import com.enjoychain.www.security.entity.TaskCorpSecBasicInfo;
import com.enjoychain.www.security.entity.TaskSecPatrolItem;
import com.enjoychain.www.security.pojo.TaskPojo;

import java.util.List;

public interface ITaskSecService {


    TaskCorpSecBasicInfo getCorpSecBasicInfoByTaskId(String taskId);

    List<TaskSecPatrolItem> getSecTaskDetailByTaskId(String taskId);

    void removeByTaskId(String taskId);

    void saveSecCorpBasicInfo(TaskPojo taskPojo);

    void saveSecTaskDetail(TaskPojo taskPojo);

    void buildSecTask(CorpBasicInfo basicInfo, String taskId);
}
