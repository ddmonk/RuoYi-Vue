package com.enjoychain.www.security.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.enjoychain.www.security.entity.CorpEpMergencyPlan;
import com.enjoychain.www.security.mapper.CorpEpMergencyPlanMapper;
import com.enjoychain.www.security.service.ICorpEpMergencyPlanService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * <p>
 * 应急预案情况 服务实现类
 * </p>
 *
 * @author ddmonk
 * @since 2023-01-13
 */
@Service
public class CorpEpMergencyPlanServiceImpl extends ServiceImpl<CorpEpMergencyPlanMapper, CorpEpMergencyPlan> implements ICorpEpMergencyPlanService {

    @Override
    public List<CorpEpMergencyPlan> listWithStatus(int status, String corpId) {
        QueryWrapper<CorpEpMergencyPlan> queryWrapper = new QueryWrapper<>();
        queryWrapper.eq("status",status);
        queryWrapper.eq("corp_id",corpId);
        return list(queryWrapper);
    }
}
