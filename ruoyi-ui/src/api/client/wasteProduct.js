// 危废生产情况
import request from '@/utils/request'

export function listWasteProduct(query) {
  return request({
    url: '/security/corpEpHw/list',
    method: 'get',
    params: query
  })
}

export function getWasteProduct(id) {
  return request({
    url: '/security/corpEpHw/' + id,
    method: 'get'
  })
}

export function addWasteProduct(data) {
  return request({
    url: '/security/corpEpHw',
    method: 'post',
    data: data
  })
}

export function updateWasteProduct(data) {
  return request({
    url: '/security/corpEpHw',
    method: 'put',
    data: data
  })
}

export function delWasteProduct(id) {
  return request({
    url: '/security/corpEpHw/' + id,
    method: 'delete'
  })
}

export function importWasteProduct(data) {
  return request({
    url: '/security/corpEpHw/importData',
    method: 'post',
    data: data
  })
}
