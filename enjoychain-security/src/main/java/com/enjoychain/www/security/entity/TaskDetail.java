package com.enjoychain.www.security.entity;

import com.baomidou.mybatisplus.annotation.TableName;
import java.io.Serializable;
import com.ruoyi.common.annotation.Excel;
import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;
import lombok.experimental.Accessors;

/**
 * <p>
 *
 * </p>
 *
 * @author ddmonk
 * @since 2023-02-24
 */
@Getter
@Setter
@Accessors(chain = true)
@TableName("task_detail")
@ApiModel(value = "TaskDetail对象", description = "")
public class TaskDetail implements Serializable {

    private static final long serialVersionUID = 1L;

    private String id;

    @ApiModelProperty("任务id")
    @Excel(name = "任务id")
    private String taskId;

    @ApiModelProperty("自行监测完成情况")
    @Excel(name = "自行监测完成情况")
    private String ownerMonitorStatus;

    @ApiModelProperty("执行报告发布情况")
    @Excel(name = "执行报告发布情况")
    private String planPublishStatus;



}
