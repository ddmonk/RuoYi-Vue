package com.enjoychain.www.security.service;

import com.enjoychain.www.security.entity.TaskSolidWaste;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 * 一般固废仓库 服务类
 * </p>
 *
 * @author ddmonk
 * @since 2023-02-24
 */
public interface ITaskSolidWasteService extends ITaskService<TaskSolidWaste> {

    TaskSolidWaste getDetailByTaskId(String taskId);

    void removeDetailByTaskId(String taskId);
}
