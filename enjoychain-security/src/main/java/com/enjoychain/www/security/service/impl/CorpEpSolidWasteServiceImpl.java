package com.enjoychain.www.security.service.impl;

import com.enjoychain.www.security.entity.CorpEpSolidWaste;
import com.enjoychain.www.security.mapper.CorpEpSolidWasteMapper;
import com.enjoychain.www.security.service.ICorpEpSolidWasteService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 一般固废生产情况 服务实现类
 * </p>
 *
 * @author ddmonk
 * @since 2023-01-13
 */
@Service
public class CorpEpSolidWasteServiceImpl extends ServiceImpl<CorpEpSolidWasteMapper, CorpEpSolidWaste> implements ICorpEpSolidWasteService {

}
