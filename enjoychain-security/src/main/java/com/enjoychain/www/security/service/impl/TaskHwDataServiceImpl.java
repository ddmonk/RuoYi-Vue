package com.enjoychain.www.security.service.impl;

import com.enjoychain.www.security.entity.TaskHwData;
import com.enjoychain.www.security.mapper.TaskHwDataMapper;
import com.enjoychain.www.security.service.ITaskHwDataService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author ddmonk
 * @since 2023-02-24
 */
@Service
public class TaskHwDataServiceImpl extends ServiceImpl<TaskHwDataMapper, TaskHwData> implements ITaskHwDataService {

}
