package com.enjoychain.www.security.pojo;

import lombok.Data;

@Data
public class StatisticsPojo {

    /**
     * 服务客户数
     */
    private Long customNum;

    /**
     * 发现问题数
     */
    private Long problemNum;

    /**
     * 作业完成数
     */
    private Long taskNum;

    /**
     * 客户需求数
     */
    private Long needNum;

}
