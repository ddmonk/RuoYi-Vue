// 排污许可证申领情况
import request from '@/utils/request'

export function listSewage(query) {
  return request({
    url: '/security/corpEpUnloading/list',
    method: 'get',
    params: query
  })
}

export function getSewage(id) {
  return request({
    url: '/security/corpEpUnloading/' + id,
    method: 'get'
  })
}

export function addSewage(data) {
  return request({
    url: '/security/corpEpUnloading',
    method: 'post',
    data: data
  })
}

export function updateSewage(data) {
  return request({
    url: '/security/corpEpUnloading',
    method: 'put',
    data: data
  })
}

export function delSewage(id) {
  return request({
    url: '/security/corpEpUnloading/' + id,
    method: 'delete'
  })
}

export function importSewage(data) {
  return request({
    url: '/security/corpEpUnloading/importData',
    method: 'post',
    data: data
  })
}
