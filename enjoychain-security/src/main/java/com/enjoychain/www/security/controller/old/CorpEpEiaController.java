package com.enjoychain.www.security.controller.old;

import org.springframework.web.bind.annotation.RequestMapping;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.common.utils.StringUtils;
import com.ruoyi.common.utils.poi.ExcelUtil;
import io.swagger.annotations.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import javax.servlet.http.HttpServletResponse;
import java.util.List;
import java.util.UUID;
import java.util.Arrays;
import com.enjoychain.www.security.entity.CorpEpEia;
import com.enjoychain.www.security.service.ICorpEpEiaService;
import org.springframework.web.bind.annotation.RestController;

/**
 * <p>
 * 环评情况表 前端控制器
 * </p>
 *
 * @author ddmonk
 * @since 2023-01-20
 */
@Api(tags = "环评情况表")
@RestController
@RequestMapping("/security/corpEpEia")
public class CorpEpEiaController {

    @Autowired
    ICorpEpEiaService service;

    @PreAuthorize("@ss.hasPermi('client:corp-ep-eia:list')")
    @GetMapping(path = "/list", produces = {"application/json"})
    @ApiOperation(value = "获取环评情况表列表")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "corpId", value = "企业Id",paramType = "query", required = false, dataType = "String"),
            @ApiImplicitParam(name = "pageNo", value = "页码", paramType = "query", dataType = "long"),
            @ApiImplicitParam(name = "pageSize", value = "页面记录条数", paramType = "query", dataType = "long")
    })
    @ApiResponses({
        @ApiResponse(code = 200, message = "ok", response= CorpEpEia.class),
    })
    public AjaxResult list(
                            @RequestParam(name = "corpId", required = false) String corpId,
                            @RequestParam(name = "pageNum", defaultValue = "1") long pageNum,
                            @RequestParam(name = "pageSize",defaultValue = "10") long pageSize){
        IPage<CorpEpEia> entityIPage = new Page<>(pageNum, pageSize);
        QueryWrapper<CorpEpEia> queryWrapper = new QueryWrapper<>();
        if (StringUtils.isNotEmpty(corpId)) {
            queryWrapper.eq("corp_id",corpId);
        }
        return AjaxResult.success("list-corp_ep_eia",service.page(entityIPage,queryWrapper));
    }

    @PreAuthorize("@ss.hasPermi('client:corp-ep-eia:query')")
    @GetMapping(value = "{id}")
    @ApiOperation("获取环评情况表详情")
    @ApiImplicitParams({
        @ApiImplicitParam(name = "id", value = "id",paramType = "path", dataType = "String")
    })
    @ApiResponses({
        @ApiResponse(code = 200, message = "ok", response = CorpEpEia.class)
    })
    public AjaxResult getCorpEpEiaInfo(@PathVariable(value = "id", required = false) String id)
    {
        return AjaxResult.success("get-corp_ep_eia",service.getById(id));
    }

    @PreAuthorize("@ss.hasPermi('client:corp-ep-eia:import')")
    @ApiOperation("导入环评情况表")
    @Log(title = "环评情况表", businessType = BusinessType.IMPORT)
    @PostMapping("/importData")
    public AjaxResult importData(MultipartFile file) throws Exception{
        ExcelUtil<CorpEpEia> util = new ExcelUtil<CorpEpEia>(CorpEpEia.class);
        List<CorpEpEia> infos = util.importExcel(file.getInputStream());
        return service.saveOrUpdateBatch(infos)?AjaxResult.success("批量插入成功"):AjaxResult.error("批量插入失败");
    }


    @ApiOperation("环评情况表导出模板下载")
    @PostMapping("/importTemplate")
    public void importTemplate(HttpServletResponse response){
        ExcelUtil<CorpEpEia> util = new ExcelUtil<CorpEpEia>(CorpEpEia.class);
        util.importTemplateExcel(response,"环评情况表");
    }

    @PreAuthorize("@ss.hasPermi('client:corp-ep-eia:add')")
    @PostMapping(consumes = {"application/json"}, produces = {"application/json"})
    @ApiOperation("新增环评情况表")
    @ApiResponses({
        @ApiResponse(code = 200, message = "ok", response = CorpEpEia.class),
    })
    @Log(title = "环评情况表", businessType = BusinessType.INSERT)
    public AjaxResult add(@RequestBody CorpEpEia entity){
        return service.save(entity)? AjaxResult.success("add-corp_ep_eia",entity): AjaxResult.error("新增环评情况表失败");
    }


    @PreAuthorize("@ss.hasPermi('client:corp-ep-eia:update')")
    @PutMapping(consumes = {"application/json"}, produces = {"application/json"})
    @ApiOperation("修改环评情况表")
    @ApiResponses({
        @ApiResponse(code = 200, message = "ok", response = CorpEpEia.class)
    })
    @Log(title = "环评情况表", businessType = BusinessType.UPDATE)
    public AjaxResult update(@RequestBody CorpEpEia corpBasicInfo){
        String operate = "update-corp_ep_eia";
        return service.updateById(corpBasicInfo)?AjaxResult.success(operate,service.getById(corpBasicInfo.getId())) : AjaxResult.error("修改客户基础信息失败");
    }

    @PreAuthorize("@ss.hasPermi('client:corp-ep-eia:delete')")
    @DeleteMapping(value = "{id}", produces = {"application/json"})
    @ApiOperation("删除环评情况表")
    @ApiResponses({
        @ApiResponse(code = 200, message = "ok", response = AjaxResult.class),
    })
    @Log(title = "环评情况表", businessType = BusinessType.DELETE)
    public AjaxResult delete(@PathVariable(value = "id", required = false) String id){
        String operate = "delete-corp_ep_eia";
        String[] ids = id.split(",");
        return service.removeBatchByIds(Arrays.asList(ids))?AjaxResult.success(operate) : AjaxResult.error("修改环评情况表失败");
    }


    @Log(title = "环评情况表", businessType = BusinessType.EXPORT)
    @PreAuthorize("@ss.hasPermi('client:corp-ep-eia:export')")
    @PostMapping("/export")
    @ApiOperation("导出环评情况表")
    @ApiResponses({
        @ApiResponse(code = 200, message = "ok", response = AjaxResult.class),
    })
    public void export(HttpServletResponse response,@RequestParam(name = "corpId", required = false) String corpId){
        QueryWrapper<CorpEpEia> queryWrapper = new QueryWrapper<>();
        if (StringUtils.isNotEmpty(corpId)) {
            queryWrapper.eq("corp_id",corpId);
        }
        List<CorpEpEia> list = service.list(queryWrapper);
        ExcelUtil<CorpEpEia> util = new ExcelUtil<CorpEpEia>(CorpEpEia.class);
        util.exportExcel(response,list, "导出环评情况表");
    }
}
