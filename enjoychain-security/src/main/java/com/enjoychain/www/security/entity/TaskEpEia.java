package com.enjoychain.www.security.entity;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableName;
import java.io.Serializable;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

import com.ruoyi.common.annotation.Excel;
import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;
import lombok.experimental.Accessors;

import static com.enjoychain.www.security.constant.Constant.DATE_FORMATTER;
import static com.enjoychain.www.security.constant.Constant.REPORT_DEFAULT_VALUE;

/**
 * <p>
 *
 * </p>
 *
 * @author ddmonk
 * @since 2023-03-07
 */
@Getter
@Setter
@Accessors(chain = true)
@TableName("task_ep_eia")
@ApiModel(value = "TaskEpEia对象", description = "")
public class TaskEpEia implements Serializable {

    private static final long serialVersionUID = 1L;

    @ApiModelProperty("主键")
    @Excel(name = "主键")
    private String id;

    @ApiModelProperty("任务id")
    @Excel(name = "任务id")
    private String taskId;

    @ApiModelProperty("公司id")
    @Excel(name = "公司id")
    private String corpId;

    @ApiModelProperty("项目名称")
    @Excel(name = "项目名称")
    private String projectName;

    @ApiModelProperty("报告类型")
    @Excel(name = "报告类型")
    private String reportType;

    @ApiModelProperty("批复时间")
    @Excel(name = "批复时间", width = 30, dateFormat = "yyyy-MM-dd")
    private LocalDateTime officialTime;

    @ApiModelProperty("批复文号")
    @Excel(name = "批复文号")
    private String officialNo;

    @ApiModelProperty("是否验收")
    @Excel(name = "是否验收")
    private String status;

    @ApiModelProperty("验收时间")
    @Excel(name = "验收时间", width = 30, dateFormat = "yyyy-MM-dd")
    private LocalDateTime checkTime;

    @ApiModelProperty("备注")
    @Excel(name = "备注")
    private String remark;

    @TableField(exist = false)
    private String officialTimeString;

    @TableField(exist = false)
    private String checkTimeString;

    public void setOfficialTime(LocalDateTime officialTime) {
        this.officialTime = officialTime;
        this.officialTimeString = this.officialTime == null ? REPORT_DEFAULT_VALUE : officialTime.format(DateTimeFormatter.ofPattern(DATE_FORMATTER));
    }

    public void setCheckTime(LocalDateTime checkTime) {
        this.checkTime = checkTime;
        this.checkTimeString = this.checkTime == null ? REPORT_DEFAULT_VALUE : checkTime.format(DateTimeFormatter.ofPattern(DATE_FORMATTER));
    }
}
