package com.enjoychain.www.security.mapper;

import com.enjoychain.www.security.entity.CorpSecPromiseInfo;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * 安全承诺信息 Mapper 接口
 * </p>
 *
 * @author ddmonk
 * @since 2023-01-13
 */
public interface CorpSecPromiseInfoMapper extends BaseMapper<CorpSecPromiseInfo> {

}
