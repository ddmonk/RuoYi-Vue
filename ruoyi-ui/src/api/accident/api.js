import request from '@/utils/request'

// 查询问题基础列表
export function listInfo(query) {
  return request({
    url: '/security/accidentInfo/list',
    method: 'get',
    params: query
  })
}

// 查询问题基础详细
export function getInfo(id) {
  return request({
    url: '/security/accidentInfo/' + id,
    method: 'get'
  })
}

// 新增问题基础
export function addInfo(data) {
  return request({
    url: '/security/accidentInfo',
    method: 'post',
    data: data
  })
}

// 修改问题基础
export function updateInfo(data) {
  return request({
    url: '/security/accidentInfo',
    method: 'put',
    data: data
  })
}

// 删除问题基础
export function delInfo(id) {
  return request({
    url: '/security/accidentInfo/' + id,
    method: 'delete'
  })
}
