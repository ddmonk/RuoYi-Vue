package com.enjoychain.www.security.service.impl;

import com.enjoychain.www.security.entity.CorpEpWaterMonitor;
import com.enjoychain.www.security.mapper.CorpEpWaterMonitorMapper;
import com.enjoychain.www.security.service.ICorpEpWaterMonitorService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 土壤和地下水自行监测情况 服务实现类
 * </p>
 *
 * @author ddmonk
 * @since 2023-01-13
 */
@Service
public class CorpEpWaterMonitorServiceImpl extends ServiceImpl<CorpEpWaterMonitorMapper, CorpEpWaterMonitor> implements ICorpEpWaterMonitorService {

}
