package com.enjoychain.www.security.controller.business.task;

import com.enjoychain.www.security.pojo.TaskDetailPojo;
import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.enums.BusinessType;
import org.springframework.web.bind.annotation.RequestMapping;
import com.ruoyi.common.core.domain.AjaxResult;
import io.swagger.annotations.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;
import com.enjoychain.www.security.service.ITaskDetailService;
import org.springframework.web.bind.annotation.RestController;

import java.util.Arrays;

/**
 * <p>
 * 巡查任务详情信息 前端控制器
 * </p>
 *
 * @author ddmonk
 * @since 2023-01-20
 */
@Api(tags = "巡查任务详情信息")
@RestController
@RequestMapping("/security/taskDetail")
public class TaskDetailController {

    @Autowired
    ITaskDetailService service;


//    @PreAuthorize("@ss.hasPermi('security:task-detail:query')")
    @GetMapping(value = "{id}")
    @ApiOperation("获取巡查任务详情信息详情")
    @ApiImplicitParams({
        @ApiImplicitParam(name = "id", value = "id",paramType = "path", dataType = "String")
    })
    @ApiResponses({
        @ApiResponse(code = 200, message = "ok", response = TaskDetailPojo.class)
    })
    public AjaxResult getTaskDetailInfo(@PathVariable(value = "id", required = false) String id)
    {
        return AjaxResult.success("get-task_detail",service.getById(id));
    }

//    @PreAuthorize("@ss.hasPermi('security:task-detail:delete')")
    @DeleteMapping(value = "{taskIds}", produces = {"application/json"})
    @ApiOperation("删除巡查任务信息（基础+详情）")
    @ApiResponses({
            @ApiResponse(code = 200, message = "ok", response = AjaxResult.class),
    })
    @Log(title = "删除巡查任务", businessType = BusinessType.DELETE)
    public AjaxResult delete(@PathVariable(value = "taskIds", required = false) String taskIds){
        String operate = "delete-task_basic";
        String[] ids = taskIds.split(",");
        service.removeByTaskIds(Arrays.asList(ids));
        return AjaxResult.success(operate);
    }







}
