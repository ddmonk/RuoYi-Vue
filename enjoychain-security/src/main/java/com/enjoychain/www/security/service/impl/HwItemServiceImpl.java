package com.enjoychain.www.security.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.enjoychain.www.security.entity.HwItem;
import com.enjoychain.www.security.mapper.HwItemMapper;
import com.enjoychain.www.security.service.IHwItemService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.apache.commons.lang3.StringUtils;
import org.apache.poi.util.StringUtil;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author ddmonk
 * @since 2023-02-21
 */
@Service
public class HwItemServiceImpl extends ServiceImpl<HwItemMapper, HwItem> implements IHwItemService {

    private QueryWrapper<HwItem> buildQueryWrapper(String id){
        QueryWrapper<HwItem> queryWrapper = new QueryWrapper<>();
        queryWrapper.eq("corp_id",id);
        return queryWrapper;
    }

    @Override
    public List<HwItem> listByCorpId(String id) {
        return list(buildQueryWrapper(id));
    }

    @Override
    public void saveOrUpdateBatch(List<HwItem> hwItems, String id) {

        for (HwItem item : hwItems){
            if (StringUtils.isEmpty(item.getCorpId())){
                item.setCorpId(id);
            }
        }
        this.saveOrUpdateBatch(hwItems);
    }

    @Override
    public void deleteByCorpId(String corpId) {
        remove(buildQueryWrapper(corpId));
    }


}
