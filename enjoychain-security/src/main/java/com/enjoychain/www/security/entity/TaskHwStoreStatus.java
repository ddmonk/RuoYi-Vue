package com.enjoychain.www.security.entity;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableName;
import java.io.Serializable;
import java.util.List;

import com.ruoyi.common.annotation.Excel;
import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;
import lombok.experimental.Accessors;

/**
 * <p>
 *
 * </p>
 *
 * @author ddmonk
 * @since 2023-02-24
 */
@Getter
@Setter
@Accessors(chain = true)
@TableName("task_hw_store_status")
@ApiModel(value = "TaskHwStoreStatus对象", description = "")
public class TaskHwStoreStatus implements Serializable {

    private static final long serialVersionUID = 1L;

    private String id;

    private String taskId;

    @ApiModelProperty("考察点")
    @Excel(name = "考察点")
    private String checkPoint;

    @ApiModelProperty("结果")
    @Excel(name = "结果")
    private String result;

    @ApiModelProperty("备注")
    @Excel(name = "备注")
    private String remark;

    @ApiModelProperty("建议")
    @Excel(name = "建议")
    private String advice;

    @ApiModelProperty("排序字段")
    @Excel(name = "排序字段")
    private int sortFlag;

    @TableField(exist = false)
    private List<BasicFile> files;

}
