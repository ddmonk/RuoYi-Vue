// 安全企业行业类型信息
import request from '@/utils/request'

export function listBusinessType(query) {
  return request({
    url: '/security/corpSecBusinessInfo/list',
    method: 'get',
    params: query
  })
}

export function getBusinessType(id) {
  return request({
    url: '/security/corpSecBusinessInfo/' + id,
    method: 'get'
  })
}

export function addBusinessType(data) {
  return request({
    url: '/security/corpSecBusinessInfo',
    method: 'post',
    data: data
  })
}

export function updateBusinessType(data) {
  return request({
    url: '/security/corpSecBusinessInfo',
    method: 'put',
    data: data
  })
}

export function delBusinessType(id) {
  return request({
    url: '/security/corpSecBusinessInfo/' + id,
    method: 'delete'
  })
}

export function importBusinessType(data) {
  return request({
    url: '/security/corpSecBusinessInfo/importData',
    method: 'post',
    data: data
  })
}
