package com.enjoychain.www.security.entity;

import com.baomidou.mybatisplus.annotation.TableName;
import java.io.Serializable;
import java.sql.Blob;
import java.sql.Date;
import java.time.LocalDateTime;

import com.ruoyi.common.annotation.Excel;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * <p>
 * 客户基础信息表
 * </p>
 *
 * @author baomidou
 * @since 2022-12-30
 */
@TableName("corp_basic_info")
@ApiModel(value = "CorpBasicInfo对象", description = "客户基础信息表")
@Data
public class CorpBasicInfo implements Serializable {

    private static final long serialVersionUID = 1L;

    @ApiModelProperty("主键")
    @Excel(name = "主键")
    private String id;

    @ApiModelProperty("名称")
    @Excel(name = "名称")
    private String name;

    @ApiModelProperty("英文名称")
    @Excel(name = "英文名称")
    private String englishName;

    @ApiModelProperty("地址")
    @Excel(name = "地址")
    private String address;

    @ApiModelProperty("统一社会信用代码")
    @Excel(name = "统一社会信用代码")
    private String certNo;

    @ApiModelProperty("类型（企业工商信息）")
    @Excel(name = "类型")
    private String type;

    @ApiModelProperty("法人")
    @Excel(name = "法人")
    private String juridical;

    @ApiModelProperty("注册资金")
    @Excel(name = "注册资金")
    private String regCapital;


    @ApiModelProperty("注册地")
    @Excel(name = "注册地")
    private String region;

    @ApiModelProperty("经营范围")
    @Excel(name = "经营范围")
    private String scope;

    @ApiModelProperty("联系人名称")
    @Excel(name = "联系人名称")
    private String linkName;

    @ApiModelProperty("联系人电话")
    @Excel(name = "联系人电话")
    private String linkTel;

    @ApiModelProperty("行业类别")
    @Excel(name = "行业类别")
    private String flag;

    @ApiModelProperty("备注")
    @Excel(name = "备注")
    private String remark;

    @ApiModelProperty("业务类型字典")
    @Excel(name = "业务类型字典", dictType = "sys_business_type")
    private String busiType;

    @ApiModelProperty("最近服务时间")
    @Excel(name = "最近服务时间")
    private LocalDateTime lastServiceTime;


    private String adminArea;

}
