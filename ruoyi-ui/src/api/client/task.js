import request from '@/utils/request'

export function listClient(query) {
  return request({
    url: '/corpBasicInfo/list',
    method: 'get',
    params: query
  })
}

export function getClient(id) {
  return request({
    url: '/corpBasicInfo/' + id,
    method: 'get'
  })
}

export function getClientDetail(query) {
  return request({
    url: '/corpBasicInfo/detail',
    method: 'get',
    params: query
  })
}

export function addClient(data) {
  return request({
    url: '/corpBasicInfo',
    method: 'post',
    data: data
  })
}

export function updateClient(data) {
  return request({
    url: '/corpBasicInfo',
    method: 'put',
    data: data
  })
}

export function delClient(id) {
  return request({
    url: '/corpBasicInfo/' + id,
    method: 'delete'
  })
}

export function importClient(data) {
  return request({
    url: '/corpBasicInfo/importData',
    method: 'post',
    data: data
  })
}
