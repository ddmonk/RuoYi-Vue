package com.enjoychain.www.security.service.impl;

import com.enjoychain.www.security.entity.CorpSecProductInfo;
import com.enjoychain.www.security.mapper.CorpSecProductInfoMapper;
import com.enjoychain.www.security.service.ICorpSecProductInfoService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 安全生产基础信息 服务实现类
 * </p>
 *
 * @author ddmonk
 * @since 2023-01-13
 */
@Service
public class CorpSecProductInfoServiceImpl extends ServiceImpl<CorpSecProductInfoMapper, CorpSecProductInfo> implements ICorpSecProductInfoService {

}
