package com.enjoychain.www.security.mapper;

import com.enjoychain.www.security.entity.CorpEpNoiseProtect;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * 企业噪声管理情况 Mapper 接口
 * </p>
 *
 * @author ddmonk
 * @since 2023-01-13
 */
public interface CorpEpNoiseProtectMapper extends BaseMapper<CorpEpNoiseProtect> {

}
