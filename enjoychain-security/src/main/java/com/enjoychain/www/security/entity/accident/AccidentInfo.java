package com.enjoychain.www.security.entity.accident;

import com.baomidou.mybatisplus.annotation.FieldFill;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import java.io.Serializable;
import java.time.LocalDateTime;
import java.util.List;

import com.ruoyi.common.annotation.Excel;
import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.*;
import lombok.experimental.Accessors;

/**
 * <p>
 * 问题基础表
 * </p>
 *
 * @author ddmonk
 * @since 2023-01-29
 */
@Getter
@Setter
@Accessors(chain = true)
@TableName("accident_info")
@ApiModel(value = "AccidentInfo对象", description = "问题基础表")
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class AccidentInfo implements Serializable {

    private static final long serialVersionUID = 1L;

    @ApiModelProperty("主键")
    @Excel(name = "主键")
    private String id;

    @ApiModelProperty("事件名称")
    @Excel(name = "事件名称")
    private String accName;

    @ApiModelProperty("公司名称")
    @Excel(name = "公司名称")
    private String corpName;

    @ApiModelProperty("公司id")
    @Excel(name = "公司id")
    private String corpId;

    @ApiModelProperty("问题级别")
    @Excel(name = "问题级别")
    private String accLevel;

    @ApiModelProperty("当前状态")
    @Excel(name = "当前状态")
    private Integer status;

    @ApiModelProperty(value = "创建时间", hidden = true)
    @TableField(fill = FieldFill.INSERT)
    private LocalDateTime createTime;

    @ApiModelProperty(value = "修改时间", hidden = true)
    @TableField(fill = FieldFill.INSERT_UPDATE)
    private LocalDateTime updateTime;

    @ApiModelProperty(value = "创建人员", hidden = true)
    @TableField(fill = FieldFill.INSERT)
    private String createBy;

    @ApiModelProperty(value = "修改人员", hidden = true)
    @TableField(fill = FieldFill.INSERT_UPDATE)
    private String modifyBy;

    @ApiModelProperty("发生问题任务id")
    @Excel(name = "发生问题任务id")
    private String taskId;

    @ApiModelProperty("发生问题任务名")
    @Excel(name = "发生问题任务名")
    private String taskName;

    @ApiModelProperty("核查指标")
    @Excel(name = "核查指标")
    private String targetName;

    @ApiModelProperty("核查指标id")
    @Excel(name = "核查指标id")
    private String targetId;

    @ApiModelProperty("核查要点id")
    @Excel(name = "核查要点id")
    private String keyId;

    @ApiModelProperty("核查要点")
    @Excel(name = "核查要点")
    private String keyName;

    @ApiModelProperty("核查内容")
    @Excel(name = "核查内容")
    private String content;

    @ApiModelProperty("核查基准数据")
    @Excel(name = "核查基准数据")
    private String basicInfo;

    @TableField(exist = false)
    private List<AccidentDetailItem> items;
}
