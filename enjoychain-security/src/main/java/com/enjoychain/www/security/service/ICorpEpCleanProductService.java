package com.enjoychain.www.security.service;

import com.enjoychain.www.security.entity.CorpEpCleanProduct;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 * 企业清洁生产情况 服务类
 * </p>
 *
 * @author ddmonk
 * @since 2023-01-13
 */
public interface ICorpEpCleanProductService extends IService<CorpEpCleanProduct> {

}
