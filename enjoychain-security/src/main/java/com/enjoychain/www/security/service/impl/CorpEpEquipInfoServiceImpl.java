package com.enjoychain.www.security.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.enjoychain.www.security.entity.CorpEpEquipInfo;
import com.enjoychain.www.security.mapper.CorpEpEquipInfoMapper;
import com.enjoychain.www.security.service.ICorpEpEquipInfoService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.ruoyi.common.utils.StringUtils;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * <p>
 * 主要设备情况 服务实现类
 * </p>
 *
 * @author ddmonk
 * @since 2023-01-13
 */
@Service
public class CorpEpEquipInfoServiceImpl extends ServiceImpl<CorpEpEquipInfoMapper, CorpEpEquipInfo> implements ICorpEpEquipInfoService {

    private QueryWrapper<CorpEpEquipInfo> buildQueryWrapper(String id){
        QueryWrapper<CorpEpEquipInfo> queryWrapper = new QueryWrapper<>();
        queryWrapper.eq("corp_id",id);
        return queryWrapper;
    }

    @Override
    public List<CorpEpEquipInfo> listWithStatus(String status, String corpId) {
        QueryWrapper<CorpEpEquipInfo> queryWrapper = new QueryWrapper<>();
        if (StringUtils.isNotEmpty(status))  queryWrapper.eq("status",status);
        queryWrapper.eq("corp_id",corpId);
        return list(queryWrapper);
    }

    @Override
    public void saveOrUpdateBatch(List<CorpEpEquipInfo> equipItems, String id) {
        for (CorpEpEquipInfo epEquipInfo: equipItems){
            if (StringUtils.isEmpty(epEquipInfo.getCorpId())){
                epEquipInfo.setCorpId(id);
            }
        }
        saveOrUpdateBatch(equipItems);
    }

    @Override
    public void deleteByCorpId(String id) {
        remove(buildQueryWrapper(id));
    }
}
