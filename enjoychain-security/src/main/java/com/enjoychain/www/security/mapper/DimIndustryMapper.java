package com.enjoychain.www.security.mapper;


import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.enjoychain.www.security.entity.DimIndustry;

/**
 * 国民经济行业分类Mapper接口
 *
 * @author ruoyi
 * @date 2022-12-02
 */
public interface DimIndustryMapper extends BaseMapper<DimIndustry> {

}
