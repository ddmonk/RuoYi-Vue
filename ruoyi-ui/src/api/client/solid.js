// 一般固废生产情况
import request from '@/utils/request'

export function listSolid(query) {
  return request({
    url: '/security/corpEpSolidWaste/list',
    method: 'get',
    params: query
  })
}

export function getSolid(id) {
  return request({
    url: '/security/corpEpSolidWaste/' + id,
    method: 'get'
  })
}

export function addSolid(data) {
  return request({
    url: '/security/corpEpSolidWaste',
    method: 'post',
    data: data
  })
}

export function updateSolid(data) {
  return request({
    url: '/security/corpEpSolidWaste',
    method: 'put',
    data: data
  })
}

export function delSolid(id) {
  return request({
    url: '/security/corpEpSolidWaste/' + id,
    method: 'delete'
  })
}

export function importSolid(data) {
  return request({
    url: '/security/corpEpSolidWaste/importData',
    method: 'post',
    data: data
  })
}
