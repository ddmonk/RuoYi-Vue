package com.enjoychain.www.security.service.impl;

import com.enjoychain.www.security.entity.TaskMergencyInfo;
import com.enjoychain.www.security.mapper.TaskMergencyInfoMapper;
import com.enjoychain.www.security.service.ITaskMergencyInfoService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author ddmonk
 * @since 2023-02-24
 */
@Service
public class TaskMergencyInfoServiceImpl extends ServiceImpl<TaskMergencyInfoMapper, TaskMergencyInfo> implements ITaskMergencyInfoService {

}
