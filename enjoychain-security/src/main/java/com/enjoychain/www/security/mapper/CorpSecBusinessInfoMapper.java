package com.enjoychain.www.security.mapper;

import com.enjoychain.www.security.entity.CorpSecBusinessInfo;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * 安全企业行业类型信息 Mapper 接口
 * </p>
 *
 * @author ddmonk
 * @since 2023-01-13
 */
public interface CorpSecBusinessInfoMapper extends BaseMapper<CorpSecBusinessInfo> {

}
