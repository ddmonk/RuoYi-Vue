package com.enjoychain.www.security.entity;

import com.baomidou.mybatisplus.annotation.TableName;
import java.io.Serializable;
import com.ruoyi.common.annotation.Excel;
import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;
import lombok.experimental.Accessors;

/**
 * <p>
 * 
 * </p>
 *
 * @author ddmonk
 * @since 2023-02-24
 */
@Getter
@Setter
@Accessors(chain = true)
@TableName("task_hw_data")
@ApiModel(value = "TaskHwData对象", description = "")
public class TaskHwData implements Serializable {

    private static final long serialVersionUID = 1L;

    private String id;

    private String taskId;

    @ApiModelProperty("是否签订了危废处置合同")
    @Excel(name = "是否签订了危废处置合同")
    private String signContract;

    @ApiModelProperty("是否到期")
    @Excel(name = "是否到期")
    private String deadlineStatus;

    @ApiModelProperty("台帐是否齐全")
    @Excel(name = "台帐是否齐全")
    private String standbookStatus;

    @ApiModelProperty("危废种类是否发生变化")
    @Excel(name = "危废种类是否发生变化")
    private String typeChangeStatus;


}
