package com.enjoychain.www.security.mapper;

import com.enjoychain.www.security.entity.CorpRiksItem;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * 公司较大以上安全风险信息表 Mapper 接口
 * </p>
 *
 * @author ddmonk
 * @since 2023-01-13
 */
public interface CorpRiksItemMapper extends BaseMapper<CorpRiksItem> {

}
