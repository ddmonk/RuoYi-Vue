package com.enjoychain.www.security.entity;

import com.baomidou.mybatisplus.annotation.FieldFill;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import java.io.Serializable;
import java.math.BigDecimal;
import java.time.LocalDateTime;
import com.ruoyi.common.annotation.Excel;
import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;
import lombok.experimental.Accessors;

/**
 * <p>
 * 危废生产情况
 * </p>
 *
 * @author ddmonk
 * @since 2023-01-13
 */
@Getter
@Setter
@Accessors(chain = true)
@TableName("corp_ep_hw")
@ApiModel(value = "CorpEpHw对象", description = "危废生产情况")
public class CorpEpHw implements Serializable {

    private static final long serialVersionUID = 1L;

    @TableId(value = "id", type = IdType.AUTO)
    private Integer id;

    @ApiModelProperty("企业id")
    @Excel(name = "企业id")
    private String corpId;

    @ApiModelProperty("仓库面积")
    @Excel(name = "仓库面积")
    private String hwArea;

    @ApiModelProperty("危废名称")
    @Excel(name = "危废名称")
    private String hwName;

    @ApiModelProperty("数量")
    @Excel(name = "数量")
    private BigDecimal num;

    @ApiModelProperty("单位")
    @Excel(name = "单位")
    private String unit;

    @ApiModelProperty("危废代码")
    @Excel(name = "危废代码")
    private String hwCode;

    @ApiModelProperty("合同签订情况")
    @Excel(name = "合同签订情况")
    private String contractDesc;

    @ApiModelProperty("到期时间")
    @Excel(name = "到期时间", width = 30, dateFormat = "yyyy-MM-dd")
    private LocalDateTime deadline;

    @ApiModelProperty("备注")
    @Excel(name = "备注")
    private String remark;

    @ApiModelProperty(value = "创建时间", hidden = true)
    @TableField(fill = FieldFill.INSERT)
    private LocalDateTime createTime;

    @ApiModelProperty(value = "修改时间", hidden = true)
    @TableField(fill = FieldFill.INSERT_UPDATE)
    private LocalDateTime updateTime;

    @ApiModelProperty(value = "创建人员", hidden = true)
    @TableField(fill = FieldFill.INSERT)
    private String createBy;

    @ApiModelProperty(value = "修改人员", hidden = true)
    @TableField(fill = FieldFill.INSERT_UPDATE)
    private String modifyBy;


}
