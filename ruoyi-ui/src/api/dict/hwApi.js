import request from '@/utils/request'

// 查询危险废物名录字典列表
export function listBasicDictHw(query) {
  return request({
    url: '/security/basicDictHw/list',
    method: 'get',
    params: query
  })
}

// 查询危险废物名录字典详细
export function getBasicDictHw(id) {
  return request({
    url: '/security/basicDictHw/' + id,
    method: 'get'
  })
}

// 新增危险废物名录字典
export function addBasicDictHw(data) {
  return request({
    url: '/security/basicDictHw',
    method: 'post',
    data: data
  })
}

// 修改危险废物名录字典
export function updateBasicDictHw(data) {
  return request({
    url: '/security/basicDictHw',
    method: 'put',
    data: data
  })
}

// 删除危险废物名录字典
export function delBasicDictHw(id) {
  return request({
    url: '/security/basicDictHw/' + id,
    method: 'delete'
  })
}
