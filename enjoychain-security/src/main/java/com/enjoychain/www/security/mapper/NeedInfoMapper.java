package com.enjoychain.www.security.mapper;

import com.enjoychain.www.security.entity.NeedInfo;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * 用户需求信息 Mapper 接口
 * </p>
 *
 * @author ddmonk
 * @since 2023-01-29
 */
public interface NeedInfoMapper extends BaseMapper<NeedInfo> {

}
