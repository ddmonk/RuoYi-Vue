package com.enjoychain.www.security.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.enjoychain.www.security.constant.BusinessTaskType;
import com.enjoychain.www.security.entity.BasicFile;
import com.enjoychain.www.security.entity.accident.AccidentDetailItem;
import com.enjoychain.www.security.entity.accident.AccidentInfo;
import com.enjoychain.www.security.entity.task.TaskDetailItem;
import com.enjoychain.www.security.mapper.AccidentDetailItemMapper;
import com.enjoychain.www.security.service.*;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.ruoyi.common.config.RuoYiConfig;
import com.ruoyi.common.utils.file.FileUploadUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.io.IOException;
import java.util.List;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author ddmonk
 * @since 2023-01-29
 */
@Service
public class AccidentDetailItemServiceImpl extends ServiceImpl<AccidentDetailItemMapper, AccidentDetailItem> implements IAccidentDetailItemService {

    @Autowired
    IBasicFileService fileService;

    @Autowired
    IAccidentInfoService accidentInfoService;

    @Autowired
    ITaskDetailItemService taskDetailItemService;

    private QueryWrapper<AccidentDetailItem> buildQueryWrapper(String id){
        QueryWrapper<AccidentDetailItem> queryWrapper = new QueryWrapper<>();
        queryWrapper.eq("acc_id",id);
        return queryWrapper;
    }


    @Override
    public List<AccidentDetailItem> listByAccId(String accId) {
        return list(buildQueryWrapper(accId));
    }

    @Override
    public AccidentDetailItem getItemDetail(String id) {
        AccidentDetailItem item = getById(id);
        item.setFileList(fileService.getListByTypeAndId(BusinessTaskType.ACCIDENT,item.getId()));
        //基于Item id查询。
        //todo 获取文件列表
        return item;
    }

    @Override
    @Transactional
    public boolean relateTaskByTaskId(String accId, String taskId) throws IOException {
        //1、获取accinfo
        AccidentInfo info = accidentInfoService.getById(accId);
        //2、 查看该问题中是否存在。
        TaskDetailItem taskItem = taskDetailItemService.findItemWithTaskIdAndAccidentInfo(taskId, info);
        if( taskItem == null ){
            return false;
        }else {
            //1、处理数据
            saveByTaskItem(taskItem,info.getId());
            return true;
        }
    }

    @Override
    public boolean removeRelationTask(String taskId) {
        List<AccidentDetailItem> items = listByTaskId(taskId);
        boolean flag = true;
        for (AccidentDetailItem item : items){
            flag = flag && removeAccDetailItem(item);
        }
        return flag;
    }

    @Override
    public boolean saveOrUpdateAndChangeStatus(AccidentDetailItem entity) {
        AccidentInfo accidentInfo = accidentInfoService.getById(entity.getAccId());
        accidentInfo.setStatus(entity.getStatus());
        return accidentInfoService.updateById(accidentInfo) && this.saveOrUpdate(entity);
    }

    @Override
    public boolean saveByTaskItem(TaskDetailItem taskItem, String accId) throws IOException {
        AccidentDetailItem item = AccidentDetailItem.builder()
                .accId(accId)
                .fileList(taskItem.getFileList())
                .modifiedMethod(taskItem.getModifiedMethod())
                .description(taskItem.getReason())
                .remark(taskItem.getRemark())
                .taskId(taskItem.getTaskId())
                .build();
        baseMapper.insert(item);
        if (taskItem.getFileList() != null) {
            //2、插入文件
            for (BasicFile file : taskItem.getFileList()) {
                String path = FileUploadUtils.copyFile(RuoYiConfig.getUploadPath(), file.getPath());
                BasicFile temp = BasicFile.builder()
                        .busiType(BusinessTaskType.ACCIDENT)
                        .connectId(item.getId())
                        .path(RuoYiConfig.getProfile() + path)
                        .build();
                fileService.save(temp);
            }
        }
        return true;
    }

    private List<AccidentDetailItem> listByTaskId(String taskId) {
        QueryWrapper<AccidentDetailItem> queryWrapper = new QueryWrapper<>();
        queryWrapper.eq("task_id",taskId);
        return list(queryWrapper);

    }

    boolean removeAccDetailItem(AccidentDetailItem item){
        fileService.removeFileByConnectIdAndBusiType(BusinessTaskType.ACCIDENT,item.getId());
        return removeById(item);
    }

}
