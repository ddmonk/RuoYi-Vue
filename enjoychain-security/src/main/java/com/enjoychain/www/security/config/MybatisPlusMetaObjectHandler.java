package com.enjoychain.www.security.config;

import com.baomidou.mybatisplus.core.handlers.MetaObjectHandler;
import com.ruoyi.common.utils.SecurityUtils;
import org.apache.ibatis.reflection.MetaObject;
import org.springframework.stereotype.Component;

import java.time.LocalDateTime;
import java.util.Date;

/**
 * 自动补充插入或更新时的值
 * @author lr
 * @date 2019-06-03 18:26
 */
@Component
public class MybatisPlusMetaObjectHandler implements MetaObjectHandler {


    @Override
    public void insertFill(MetaObject metaObject) {
        this.setFieldValByName("createBy", SecurityUtils.getUsername(),metaObject);
        this.setFieldValByName("createTime", LocalDateTime.now(),metaObject);
        this.setFieldValByName("updateBy", SecurityUtils.getUsername(),metaObject);
        this.setFieldValByName("modifyBy", SecurityUtils.getUsername(),metaObject);
        this.setFieldValByName("updateTime", LocalDateTime.now(),metaObject);
        this.setFieldValByName("version", Long.valueOf(1),metaObject);
//        this.setFieldValByName("appSecret", UUID.randomUUID().toString().replaceAll("-",""),metaObject);
    }

    @Override
    public void updateFill(MetaObject metaObject) {
        this.setFieldValByName("updateBy", SecurityUtils.getUsername(),metaObject);
        this.setFieldValByName("modifyBy", SecurityUtils.getUsername(),metaObject);
        this.setFieldValByName("updateTime", LocalDateTime.now(),metaObject);
        this.setFieldValByName("version", this.getFieldValByName("version",metaObject),metaObject);
    }
}
