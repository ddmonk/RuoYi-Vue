package com.enjoychain.www.security.mapper;

import com.enjoychain.www.security.entity.job.JobInfo;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * 作业基础表 Mapper 接口
 * </p>
 *
 * @author ddmonk
 * @since 2023-01-29
 */
public interface JobInfoMapper extends BaseMapper<JobInfo> {

}
