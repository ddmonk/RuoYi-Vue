package com.enjoychain.www.security.service;

import com.enjoychain.www.security.entity.CorpBasicInfo;
import com.enjoychain.www.security.entity.CorpSecCorpBasic;
import com.enjoychain.www.security.entity.TaskCorpSecBasicInfo;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author ddmonk
 * @since 2023-04-18
 */
public interface ITaskCorpSecBasicInfoService extends ITaskService<TaskCorpSecBasicInfo> {


    TaskCorpSecBasicInfo inital(CorpBasicInfo basicInfo, String taskId);

    void saveDetailBasicInfo(TaskCorpSecBasicInfo basic);
}
