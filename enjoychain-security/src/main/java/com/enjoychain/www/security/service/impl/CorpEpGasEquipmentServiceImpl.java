package com.enjoychain.www.security.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.enjoychain.www.security.entity.CorpEpEia;
import com.enjoychain.www.security.entity.CorpEpGasEquipment;
import com.enjoychain.www.security.mapper.CorpEpGasEquipmentMapper;
import com.enjoychain.www.security.service.ICorpEpGasEquipmentService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.ruoyi.common.utils.StringUtils;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * <p>
 * 废气处理设备情况 服务实现类
 * </p>
 *
 * @author ddmonk
 * @since 2023-01-13
 */
@Service
public class CorpEpGasEquipmentServiceImpl extends ServiceImpl<CorpEpGasEquipmentMapper, CorpEpGasEquipment> implements ICorpEpGasEquipmentService {

    private QueryWrapper<CorpEpGasEquipment> buildQueryWrapper(String corpId){
        QueryWrapper<CorpEpGasEquipment> queryWrapper = new QueryWrapper<>();
        queryWrapper.eq("corp_id",corpId);
        return queryWrapper;
    }

    @Override
    public List<CorpEpGasEquipment> listWithStatus(String status, String corpId) {
        QueryWrapper<CorpEpGasEquipment> queryWrapper = new QueryWrapper<>();
        if (StringUtils.isNotEmpty(status)) queryWrapper.eq("status",status);
        queryWrapper.eq("corp_id",corpId);
        return list(queryWrapper);
    }

    @Override
    public List<CorpEpGasEquipment> listSection(int status, String corpId) {
        QueryWrapper<CorpEpGasEquipment> queryWrapper = new QueryWrapper<>();
        queryWrapper.eq("status",status);
        queryWrapper.eq("corp_id",corpId);
        queryWrapper.select("DISTINCT product_section");
        return list(queryWrapper);
    }

    @Override
    public void saveOrUpdateBatch(List<CorpEpGasEquipment> gasEquipmentItems, String id) {
        for (CorpEpGasEquipment gasEquipment: gasEquipmentItems){
            if (StringUtils.isEmpty(gasEquipment.getCorpId())){
                gasEquipment.setCorpId(id);
            }
        }
        saveOrUpdateBatch(gasEquipmentItems);
    }

    @Override
    public void deleteByCorpId(String id) {
        remove(buildQueryWrapper(id));
    }
}
