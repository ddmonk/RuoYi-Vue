import request from '@/utils/request'

// 查询核查任务要素信息列表
export function listElement(query) {
  return request({
    url: '/security/jobBasicElement/list',
    method: 'get',
    params: query
  })
}

// 查询核查任务要素信息详细
export function getElement(id) {
  return request({
    url: '/security/jobBasicElement/' + id,
    method: 'get'
  })
}

// 新增核查任务要素信息
export function addElement(data) {
  return request({
    url: '/security/jobBasicElement',
    method: 'post',
    data: data
  })
}

// 修改核查任务要素信息
export function updateElement(data) {
  return request({
    url: '/security/jobBasicElement',
    method: 'put',
    data: data
  })
}

// 删除核查任务要素信息
export function delElement(id) {
  return request({
    url: '/security/jobBasicElement/' + id,
    method: 'delete'
  })
}

// 查询核查任务要素信息列表(all)
export function listElementAll(query) {
  return request({
    url: '/security/jobBasicElement/list/all',
    method: 'get',
    params: query
  })
}
