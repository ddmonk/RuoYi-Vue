package com.enjoychain.www.security.service.impl;

import com.enjoychain.www.security.entity.BasicDictHw;
import com.enjoychain.www.security.mapper.BasicDictHwMapper;
import com.enjoychain.www.security.service.IBasicDictHwService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 危险废物名录字典 服务实现类
 * </p>
 *
 * @author ddmonk
 * @since 2023-03-08
 */
@Service
public class BasicDictHwServiceImpl extends ServiceImpl<BasicDictHwMapper, BasicDictHw> implements IBasicDictHwService {

}
