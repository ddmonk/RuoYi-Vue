package com.enjoychain.www.security.pojo;

import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class GasItems {

    private String exhaustFunnelNo;

    private String pollutionFactor;

    private String key;

    private String value;

    private String advice;

}
