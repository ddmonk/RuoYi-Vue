package com.enjoychain.www.security.service.impl;

import com.enjoychain.www.security.entity.task.TaskBasic;
import com.enjoychain.www.security.entity.task.TaskDetailItem;
import com.enjoychain.www.security.pojo.TaskDetailPojo;
import com.enjoychain.www.security.service.ITaskBasicService;
import com.enjoychain.www.security.service.ITaskDetailItemService;
import com.enjoychain.www.security.service.ITaskDetailService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.annotation.Transient;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * <p>
 * 服务实现类
 * </p>
 *
 * @author ddmonk
 * @since 2023-01-20
 */
@Service
public class TaskDetailServiceImpl implements ITaskDetailService {

    @Autowired
    ITaskBasicService taskBasicService;

    @Autowired
    ITaskDetailItemService itemService;

    @Override
    public TaskDetailPojo getById(String id) {
        TaskDetailPojo pojo = new TaskDetailPojo();
        TaskBasic basic = taskBasicService.getBasicByTaskId(id);
        List<TaskDetailItem> itemList = itemService.getItems(id);
        pojo.setBasic(basic);
        pojo.setDetails(itemList);
        return pojo;
    }

    @Override
    @Transient
    public void removeByTaskId(String taskId) {
        taskBasicService.removeByTaskId(taskId);
        itemService.removeByTaskId(taskId);
    }

    @Override
    public void removeByTaskIds(List<String> taskIds) {
        for (String taskId : taskIds) {
            removeByTaskId(taskId);
        }
    }
}
