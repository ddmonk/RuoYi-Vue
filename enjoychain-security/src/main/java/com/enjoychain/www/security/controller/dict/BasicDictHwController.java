package com.enjoychain.www.security.controller.dict;

import org.springframework.web.bind.annotation.RequestMapping;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.common.utils.StringUtils;
import com.ruoyi.common.utils.poi.ExcelUtil;
import io.swagger.annotations.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import javax.servlet.http.HttpServletResponse;
import java.util.List;
import java.util.UUID;
import java.util.Arrays;
import com.enjoychain.www.security.entity.BasicDictHw;
import com.enjoychain.www.security.service.IBasicDictHwService;
import org.springframework.web.bind.annotation.RestController;

/**
 * <p>
 * 危险废物名录字典 前端控制器
 * </p>
 *
 * @author ddmonk
 * @since 2023-03-08
 */
@Api(tags = "危险废物名录字典")
@RestController
@RequestMapping("/security/basicDictHw")
public class BasicDictHwController {

    @Autowired
    IBasicDictHwService service;

    @PreAuthorize("@ss.hasPermi('security:basic-dict-hw:list')")
    @GetMapping(path = "/list", produces = {"application/json"})
    @ApiOperation(value = "获取危险废物名录字典列表")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "category", value = "企业Id",paramType = "query", required = false, dataType = "String"),
            @ApiImplicitParam(name = "source", value = "企业Id",paramType = "query", required = false, dataType = "String"),
            @ApiImplicitParam(name = "hwCode", value = "企业Id",paramType = "query", required = false, dataType = "String"),
            @ApiImplicitParam(name = "hwName", value = "企业Id",paramType = "query", required = false, dataType = "String"),
            @ApiImplicitParam(name = "pageNum", value = "页码", paramType = "query", dataType = "long"),
            @ApiImplicitParam(name = "pageSize", value = "页面记录条数", paramType = "query", dataType = "long")
    })
    @ApiResponses({
        @ApiResponse(code = 200, message = "ok", response= BasicDictHw.class),
    })
    public AjaxResult list(
                            @RequestParam(name = "category", required = false) String category,
                            @RequestParam(name = "source", required = false) String source,
                            @RequestParam(name = "hwCode", required = false) String hwCode,
                            @RequestParam(name = "hwName", required = false) String hwName,
                            @RequestParam(name = "keyWord", required = false) String keyWord,
                            @RequestParam(name = "pageNum", defaultValue = "1") long pageNo,
                            @RequestParam(name = "pageSize",defaultValue = "10") long pageSize){
        IPage<BasicDictHw> entityIPage = new Page<>(pageNo, pageSize);
        QueryWrapper<BasicDictHw> queryWrapper = new QueryWrapper<>();
        if (StringUtils.isNotEmpty(category)) {
            queryWrapper.like("category",category);
        }
        if (StringUtils.isNotEmpty(source)) {
            queryWrapper.like("source",source);
        }
        if (StringUtils.isNotEmpty(hwCode)) {
            queryWrapper.like("hw_code",hwCode);
        }
        if (StringUtils.isNotEmpty(hwName)) {
            queryWrapper.like("hw_name",hwName);
        }
        if (StringUtils.isNotEmpty(keyWord)) {
            queryWrapper.like("hw_name",keyWord).or().like("hw_code",keyWord).or().like("source",keyWord);

        }
        return AjaxResult.success("list-basic_dict_hw",service.page(entityIPage,queryWrapper));
    }

    @PreAuthorize("@ss.hasPermi('security:basic-dict-hw:query')")
    @GetMapping(value = "{id}")
    @ApiOperation("获取危险废物名录字典详情")
    @ApiImplicitParams({
        @ApiImplicitParam(name = "id", value = "id",paramType = "path", dataType = "String")
    })
    @ApiResponses({
        @ApiResponse(code = 200, message = "ok", response = BasicDictHw.class)
    })
    public AjaxResult getBasicDictHwInfo(@PathVariable(value = "id", required = false) String id)
    {
        return AjaxResult.success("get-basic_dict_hw",service.getById(id));
    }

    @PreAuthorize("@ss.hasPermi('security:basic-dict-hw:import')")
    @ApiOperation("导入危险废物名录字典")
    @Log(title = "危险废物名录字典", businessType = BusinessType.IMPORT)
    @PostMapping("/importData")
    public AjaxResult importData(MultipartFile file) throws Exception{
        ExcelUtil<BasicDictHw> util = new ExcelUtil<BasicDictHw>(BasicDictHw.class);
        List<BasicDictHw> infos = util.importExcel(file.getInputStream());
        return service.saveOrUpdateBatch(infos)?AjaxResult.success("批量插入成功"):AjaxResult.error("批量插入失败");
    }


    @ApiOperation("危险废物名录字典导出模板下载")
    @PostMapping("/importTemplate")
    public void importTemplate(HttpServletResponse response){
        ExcelUtil<BasicDictHw> util = new ExcelUtil<BasicDictHw>(BasicDictHw.class);
        util.importTemplateExcel(response,"危险废物名录字典");
    }

    @PreAuthorize("@ss.hasPermi('security:basic-dict-hw:add')")
    @PostMapping(consumes = {"application/json"}, produces = {"application/json"})
    @ApiOperation("新增危险废物名录字典")
    @ApiResponses({
        @ApiResponse(code = 200, message = "ok", response = BasicDictHw.class),
    })
    @Log(title = "危险废物名录字典", businessType = BusinessType.INSERT)
    public AjaxResult add(@RequestBody BasicDictHw entity){
        return service.save(entity)? AjaxResult.success("add-basic_dict_hw",entity): AjaxResult.error("新增危险废物名录字典失败");
    }


    @PreAuthorize("@ss.hasPermi('security:basic-dict-hw:update')")
    @PutMapping(consumes = {"application/json"}, produces = {"application/json"})
    @ApiOperation("修改危险废物名录字典")
    @ApiResponses({
        @ApiResponse(code = 200, message = "ok", response = BasicDictHw.class)
    })
    @Log(title = "危险废物名录字典", businessType = BusinessType.UPDATE)
    public AjaxResult update(@RequestBody BasicDictHw corpBasicInfo){
        String operate = "update-basic_dict_hw";
        return service.updateById(corpBasicInfo)?AjaxResult.success(operate,service.getById(corpBasicInfo.getId())) : AjaxResult.error("修改客户基础信息失败");
    }

    @PreAuthorize("@ss.hasPermi('security:basic-dict-hw:delete')")
    @DeleteMapping(value = "{id}", produces = {"application/json"})
    @ApiOperation("删除危险废物名录字典")
    @ApiResponses({
        @ApiResponse(code = 200, message = "ok", response = AjaxResult.class),
    })
    @Log(title = "危险废物名录字典", businessType = BusinessType.DELETE)
    public AjaxResult delete(@PathVariable(value = "id", required = false) String id){
        String operate = "delete-basic_dict_hw";
        String[] ids = id.split(",");
        return service.removeBatchByIds(Arrays.asList(ids))?AjaxResult.success(operate) : AjaxResult.error("修改危险废物名录字典失败");
    }


    @Log(title = "危险废物名录字典", businessType = BusinessType.EXPORT)
    @PreAuthorize("@ss.hasPermi('security:basic-dict-hw:export')")
    @PostMapping("/export")
    @ApiOperation("导出危险废物名录字典")
    @ApiResponses({
        @ApiResponse(code = 200, message = "ok", response = AjaxResult.class),
    })
    public void export(HttpServletResponse response,@RequestParam(name = "corpId", required = false) String corpId){
        QueryWrapper<BasicDictHw> queryWrapper = new QueryWrapper<>();
        if (StringUtils.isNotEmpty(corpId)) {
            queryWrapper.eq("corp_id",corpId);
        }
        List<BasicDictHw> list = service.list(queryWrapper);
        ExcelUtil<BasicDictHw> util = new ExcelUtil<BasicDictHw>(BasicDictHw.class);
        util.exportExcel(response,list, "导出危险废物名录字典");
    }
}
