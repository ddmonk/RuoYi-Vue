// 企业废水情况
import request from '@/utils/request'

export function listWaste(query) {
  return request({
    url: '/security/corpEpHw/list',
    method: 'get',
    params: query
  })
}

export function getWaste(id) {
  return request({
    url: '/security/corpEpHw/' + id,
    method: 'get'
  })
}

export function addWaste(data) {
  return request({
    url: '/security/corpEpHw',
    method: 'post',
    data: data
  })
}

export function updateWaste(data) {
  return request({
    url: '/security/corpEpHw',
    method: 'put',
    data: data
  })
}

export function delWaste(id) {
  return request({
    url: '/security/corpEpHw/' + id,
    method: 'delete'
  })
}

export function importWaste(data) {
  return request({
    url: '/security/corpEpHw/importData',
    method: 'post',
    data: data
  })
}
