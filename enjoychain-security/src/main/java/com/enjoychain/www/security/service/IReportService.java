package com.enjoychain.www.security.service;

import com.enjoychain.www.security.constant.BasicInfoType;

import java.io.IOException;
import java.util.Map;

public interface IReportService {


    Map<String,String> buildBasicInfoById(String id, BasicInfoType type);

    Map<String,String> buildTaskInfoByTaskId(String taskId) throws IOException;

    Map<String,String> buildTaskAllByTaskId(String taskId) throws IOException;

    Map<String,String> buildCompanyPromiseByTaskId(String taskId);


    Map<String,String> buildCheckListByTaskId(String taskId);

    Map<String,String> buildDailyTaskReportByTaskId(String taskId);
}

