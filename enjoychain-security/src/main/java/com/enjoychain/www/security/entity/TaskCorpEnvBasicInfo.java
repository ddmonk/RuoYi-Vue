package com.enjoychain.www.security.entity;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableName;
import java.io.Serializable;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.List;

import com.ruoyi.common.annotation.Excel;
import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;
import lombok.experimental.Accessors;

import static com.enjoychain.www.security.constant.Constant.DATE_FORMATTER;
import static com.enjoychain.www.security.constant.Constant.REPORT_DEFAULT_VALUE;

/**
 * <p>
 *
 * </p>
 *
 * @author ddmonk
 * @since 2023-03-07
 */
@Getter
@Setter
@Accessors(chain = true)
@TableName("task_corp_env_basic_info")
@ApiModel(value = "TaskCorpEnvBasicInfo对象", description = "")
public class TaskCorpEnvBasicInfo implements Serializable {

    private static final long serialVersionUID = 1L;

    private String id;

    @ApiModelProperty("公司id")
    @Excel(name = "公司id")
    private String corpId;

    @ApiModelProperty("任务id")
    @Excel(name = "任务id")
    private String taskId;

    @ApiModelProperty("公司名称")
    @Excel(name = "公司名称")
    private String corpName;

    @ApiModelProperty("统一社会信用代码")
    @Excel(name = "统一社会信用代码")
    private String certNo;

    @ApiModelProperty("行业类别")
    @Excel(name = "行业类别")
    private String busiType;

    @ApiModelProperty("企业地址")
    @Excel(name = "企业地址")
    private String corpAddress;

    @ApiModelProperty("法人姓名")
    @Excel(name = "法人姓名")
    private String juridicalName;

    @ApiModelProperty("法人联系方式")
    @Excel(name = "法人联系方式")
    private String juridicalTel;

    @ApiModelProperty("联系人")
    @Excel(name = "联系人")
    private String linkName;

    @ApiModelProperty("联系方式")
    @Excel(name = "联系方式")
    private String linkTel;

    @ApiModelProperty("主要生产工艺")
    @Excel(name = "主要生产工艺")
    private String productProcess;

    @ApiModelProperty("环评报告情况")
    @Excel(name = "环评报告情况")
    private String reportStatus;

    @ApiModelProperty("排污许可证申领情况")
    @Excel(name = "排污许可证申领情况")
    private String unloadingStatus;

    @ApiModelProperty("排污许可证类型")
    @Excel(name = "排污许可证类型")
    private String unloadingType;

    private String adminArea;

    @ApiModelProperty("排污许可证日期")
    @Excel(name = "排污许可证日期", width = 30, dateFormat = "yyyy-MM-dd")
    private LocalDateTime unloadingDate;

    @ApiModelProperty("排污许可证有效期[开始]")
    @Excel(name = "排污许可证有效期[开始]", width = 30, dateFormat = "yyyy-MM-dd")
    private LocalDateTime unloadingValidityBegin;

    @ApiModelProperty("排污许可证有效期[结束]")
    @Excel(name = "排污许可证有效期[结束]", width = 30, dateFormat = "yyyy-MM-dd")
    private LocalDateTime unloadingValidityEnd;

    @ApiModelProperty("应急预案状态")
    @Excel(name = "应急预案状态")
    private String mergencyStatus;

    @ApiModelProperty("应急预案备案状态")
    @Excel(name = "应急预案备案状态")
    private String mergencyRecordStatus;

    @ApiModelProperty("应急预案备案时间")
    @Excel(name = "应急预案备案时间", width = 30, dateFormat = "yyyy-MM-dd")
    private LocalDateTime mergencyRecordTime;

    @ApiModelProperty("应急池情况")
    @Excel(name = "应急池情况")
    private String mergencyLagoonStatus;

    @ApiModelProperty("土壤和地下水自行监测情况")
    @Excel(name = "土壤和地下水自行监测情况")
    private String waterMonitorStatus;

    @ApiModelProperty("土壤和地下水自行监测时间")
    @Excel(name = "土壤和地下水自行监测时间", width = 30, dateFormat = "yyyy-MM-dd")
    private LocalDateTime waterMonitorTime;

    @ApiModelProperty("清洁生产情况")
    @Excel(name = "清洁生产情况")
    private String cleanProductStatus;

    @ApiModelProperty("污水排水去向")
    @Excel(name = "污水排水去向")
    private String sewageTraceStatus;

    @ApiModelProperty("污水排水去向备注")
    @Excel(name = "污水排水去向备注")
    private String sewageTraceRemark;

    @ApiModelProperty("排水体制")
    @Excel(name = "排水体制")
    private String drainageSystem;

    @ApiModelProperty("污水性质")
    @Excel(name = "污水性质")
    private String sewageType;

    @ApiModelProperty("废水处理设备")
    @Excel(name = "废水处理设备")
    private String sewageEquipStatus;

    @ApiModelProperty("废水处理设备备注")
    @Excel(name = "废水处理设备备注")
    private String sewageEquipRemark;

    @ApiModelProperty("固废生产情况")
    @Excel(name = "固废生产情况")
    private String sewageSolidStatus;

    @ApiModelProperty("固废生产情况备注")
    @Excel(name = "固废生产情况备注")
    private String sewageSolidRemark;

    @ApiModelProperty("废气排放情况")
    @Excel(name = "废气排放情况")
    private String wasteGasStatus;

    @ApiModelProperty("防噪措施")
    @Excel(name = "防噪措施")
    private String noiseMethodStatus;

    @ApiModelProperty("防噪措施备注")
    @Excel(name = "防噪措施备注")
    private String noiseMethodRemark;

    @ApiModelProperty("一般固废情况")
    @Excel(name = "一般固废情况")
    private String solidWasteStatus;

    @ApiModelProperty("一般固废仓库面积")
    @Excel(name = "一般固废仓库面积")
    private String solidWasteArea;

    @ApiModelProperty("危废情况")
    @Excel(name = "危废情况")
    private String hwStatus;

    @ApiModelProperty("危废仓库面积")
    @Excel(name = "危废仓库面积")
    private String hwArea;

    @ApiModelProperty("备注")
    @Excel(name = "备注")
    private String remark;


    @TableField(exist = false)
    private List<TaskEpHwItem> hwItems;
    @TableField(exist = false)
    private List<TaskEpSolidWasteItem> solidWasteItems;
    @TableField(exist = false)
    private List<TaskEpEquipInfo> equipItems;
    @TableField(exist = false)
    private List<TaskEpEia> reportItems;
    @TableField(exist = false)
    private List<TaskEpGasEquipment> gasEquipmentItems;


    @TableField(exist = false)
    private String unloadingDateString;

    @TableField(exist = false)
    private String unloadingValidityBeginString ;

    @TableField(exist = false)
    private String unloadingValidityEndString ;

    @TableField(exist = false)
    private String mergencyRecordTimeString ;

    @TableField(exist = false)
    private String waterMonitorTimeString ;

    public void setUnloadingDate(LocalDateTime unloadingDate) {
        this.unloadingDate = unloadingDate;
        this.unloadingDateString = this.unloadingDate == null ? REPORT_DEFAULT_VALUE : unloadingDate.format(DateTimeFormatter.ofPattern(DATE_FORMATTER));
    }

    public void setUnloadingValidityBegin(LocalDateTime unloadingValidityBegin) {
        this.unloadingValidityBegin = unloadingValidityBegin;
        this.unloadingValidityBeginString = this.unloadingValidityBegin == null ? REPORT_DEFAULT_VALUE : unloadingValidityBegin.format(DateTimeFormatter.ofPattern(DATE_FORMATTER));
    }

    public void setUnloadingValidityEnd(LocalDateTime unloadingValidityEnd) {
        this.unloadingValidityEnd = unloadingValidityEnd;
        this.unloadingValidityEndString = this.unloadingValidityEnd == null ? REPORT_DEFAULT_VALUE : unloadingValidityEnd.format(DateTimeFormatter.ofPattern(DATE_FORMATTER));
    }

    public void setMergencyRecordTime(LocalDateTime mergencyRecordTime) {
        this.mergencyRecordTime = mergencyRecordTime;
        this.mergencyRecordTimeString = this.mergencyRecordTime == null ? REPORT_DEFAULT_VALUE : mergencyRecordTime.format(DateTimeFormatter.ofPattern(DATE_FORMATTER));
    }

    public void setWaterMonitorTime(LocalDateTime waterMonitorTime) {
        this.waterMonitorTime = waterMonitorTime;
        this.waterMonitorTimeString = this.waterMonitorTime == null ? REPORT_DEFAULT_VALUE : waterMonitorTime.format(DateTimeFormatter.ofPattern(DATE_FORMATTER));
    }

}
