package com.enjoychain.www.security.entity.accident;

import com.baomidou.mybatisplus.annotation.FieldFill;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import java.io.Serializable;
import java.time.LocalDateTime;
import java.util.List;

import com.enjoychain.www.security.entity.BasicFile;
import com.ruoyi.common.annotation.Excel;
import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.*;
import lombok.experimental.Accessors;

/**
 * <p>
 *
 * </p>
 *
 * @author ddmonk
 * @since 2023-01-29
 */
@Getter
@Setter
@Accessors(chain = true)
@TableName("accident_detail_item")
@ApiModel(value = "AccidentDetailItem对象", description = "")
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class AccidentDetailItem implements Serializable {

    private static final long serialVersionUID = 1L;

    @TableId(value = "id")
    private String id;

    @ApiModelProperty("问题id")
    @Excel(name = "问题id")
    private String accId;

    @ApiModelProperty("状态")
    @Excel(name = "状态")
    private Integer status;

    @ApiModelProperty("整改措施")
    @Excel(name = "整改措施")
    private String modifiedMethod;

    @ApiModelProperty("描述")
    @Excel(name = "描述")
    private String description;

    @ApiModelProperty("备注")
    @Excel(name = "备注")
    private String remark;

    @ApiModelProperty("类型")
    @Excel(name = "类型")
    private Integer type;

    @ApiModelProperty(value = "创建时间", hidden = true)
    @TableField(fill = FieldFill.INSERT)
    private LocalDateTime createTime;

    @ApiModelProperty(value = "修改时间", hidden = true)
    @TableField(fill = FieldFill.INSERT_UPDATE)
    private LocalDateTime updateTime;

    @ApiModelProperty(value = "创建人员", hidden = true)
    @TableField(fill = FieldFill.INSERT)
    private String createBy;

    @ApiModelProperty(value = "修改人员", hidden = true)
    @TableField(fill = FieldFill.INSERT_UPDATE)
    private String modifyBy;


    @TableField(exist = false)
    @ApiModelProperty(value = "附件", hidden = true)
    private List<BasicFile> fileList;

    private String taskId;


}
