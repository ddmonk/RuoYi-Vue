package com.enjoychain.www.common.pojo;

import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class PojoParams {

    private int pageNo;

    private int pageSize;

    private FieldPojo inFields;

}
