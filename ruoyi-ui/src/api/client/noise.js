// 企业噪声管理情况
import request from '@/utils/request'

export function listNoise(query) {
  return request({
    url: '/security/corpEpNoiseProtect/list',
    method: 'get',
    params: query
  })
}

export function getNoise(id) {
  return request({
    url: '/security/corpEpNoiseProtect/' + id,
    method: 'get'
  })
}

export function addNoise(data) {
  return request({
    url: '/security/corpEpNoiseProtect',
    method: 'post',
    data: data
  })
}

export function updateNoise(data) {
  return request({
    url: '/security/corpEpNoiseProtect',
    method: 'put',
    data: data
  })
}

export function delNoise(id) {
  return request({
    url: '/security/corpEpNoiseProtect/' + id,
    method: 'delete'
  })
}

export function importNoise(data) {
  return request({
    url: '/security/corpEpNoiseProtect/importData',
    method: 'post',
    data: data
  })
}
