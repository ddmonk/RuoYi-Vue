package com.enjoychain.www.security.service;

import com.enjoychain.www.security.entity.TaskSecCheckDict;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author ddmonk
 * @since 2023-04-21
 */
public interface ITaskSecCheckDictService extends IService<TaskSecCheckDict> {

}
