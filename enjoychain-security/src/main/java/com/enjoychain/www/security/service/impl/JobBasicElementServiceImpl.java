package com.enjoychain.www.security.service.impl;

import com.enjoychain.www.security.entity.JobBasicElement;
import com.enjoychain.www.security.mapper.JobBasicElementMapper;
import com.enjoychain.www.security.service.IJobBasicElementService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 核查任务要素信息 服务实现类
 * </p>
 *
 * @author ddmonk
 * @since 2023-01-30
 */
@Service
public class JobBasicElementServiceImpl extends ServiceImpl<JobBasicElementMapper, JobBasicElement> implements IJobBasicElementService {

}
