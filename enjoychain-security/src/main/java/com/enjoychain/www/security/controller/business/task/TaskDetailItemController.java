package com.enjoychain.www.security.controller.business.task;

import com.enjoychain.www.security.entity.accident.AccidentDetailItem;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.RequestMapping;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.common.utils.StringUtils;
import com.ruoyi.common.utils.poi.ExcelUtil;
import io.swagger.annotations.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;
import java.util.Arrays;
import com.enjoychain.www.security.entity.task.TaskDetailItem;
import com.enjoychain.www.security.service.ITaskDetailItemService;
import org.springframework.web.bind.annotation.RestController;

/**
 * <p>
 * 巡查任务核查细项信息 前端控制器
 * </p>
 *
 * @author ddmonk
 * @since 2023-01-20
 */
@Api(tags = "巡查任务核查细项信息")
@RestController
@RequestMapping("/security/taskDetailItem")
@Slf4j
public class TaskDetailItemController {

    @Autowired
    ITaskDetailItemService service;

    @PreAuthorize("@ss.hasPermi('task:detail-item:list')")
    @GetMapping(path = "/list", produces = {"application/json"})
    @ApiOperation(value = "获取巡查任务核查细项信息列表")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "taskId", value = "taskId",paramType = "query", required = false, dataType = "String"),
            @ApiImplicitParam(name = "pageNo", value = "页码", paramType = "query", dataType = "long"),
            @ApiImplicitParam(name = "pageSize", value = "页面记录条数", paramType = "query", dataType = "long")
    })
    @ApiResponses({
        @ApiResponse(code = 200, message = "ok", response= TaskDetailItem.class),
    })
    public AjaxResult list(
                            @RequestParam(name = "taskId", required = false) String taskId,
                            @RequestParam(name = "pageNo", defaultValue = "1") long pageNo,
                            @RequestParam(name = "pageSize",defaultValue = "10") long pageSize){

        return AjaxResult.success("list-task_detail_item",service.getItems(taskId));
    }

    @PreAuthorize("@ss.hasPermi('task:detail-item:query')")
    @GetMapping(value = "{id}")
    @ApiOperation("获取巡查任务核查细项信息核查细项")
    @ApiImplicitParams({
        @ApiImplicitParam(name = "id", value = "id",paramType = "path", dataType = "String")
    })
    @ApiResponses({
        @ApiResponse(code = 200, message = "ok", response = TaskDetailItem.class)
    })
    public AjaxResult getTaskDetailItemInfo(@PathVariable(value = "id", required = false) String id)
    {
        return AjaxResult.success("get-task_detail_item",service.getItem(id));
    }

//    @PreAuthorize("@ss.hasPermi('task:detail-item:import')")
//    @ApiOperation("导入巡查任务核查细项信息")
//    @Log(title = "巡查任务核查细项信息", businessType = BusinessType.IMPORT)
//    @PostMapping("/importData")
//    public AjaxResult importData(MultipartFile file) throws Exception{
//        ExcelUtil<TaskDetailItem> util = new ExcelUtil<TaskDetailItem>(TaskDetailItem.class);
//        List<TaskDetailItem> infos = util.importExcel(file.getInputStream());
//        return service.saveOrUpdateBatch(infos)?AjaxResult.success("批量插入成功"):AjaxResult.error("批量插入失败");
//    }
//
//
//    @ApiOperation("巡查任务核查细项信息导出模板下载")
//    @PostMapping("/importTemplate")
//    public void importTemplate(HttpServletResponse response){
//        ExcelUtil<TaskDetailItem> util = new ExcelUtil<TaskDetailItem>(TaskDetailItem.class);
//        util.importTemplateExcel(response,"巡查任务核查细项信息");
//    }

    @PreAuthorize("@ss.hasPermi('task:detail-item:add')")
    @PostMapping(consumes = {"application/json"}, produces = {"application/json"})
    @ApiOperation("新增巡查任务核查细项信息")
    @ApiResponses({
        @ApiResponse(code = 200, message = "ok", response = TaskDetailItem.class),
    })
    @Log(title = "巡查任务核查细项信息", businessType = BusinessType.INSERT)
    public AjaxResult add(@RequestBody TaskDetailItem entity){
        return service.save(entity)? AjaxResult.success("add-task_detail_item",entity): AjaxResult.error("新增巡查任务核查细项信息失败");
    }


    @PreAuthorize("@ss.hasPermi('task:detail-item:update')")
    @PutMapping(consumes = {"application/json"}, produces = {"application/json"})
    @ApiOperation("修改巡查任务核查细项信息")
    @ApiResponses({
        @ApiResponse(code = 200, message = "ok", response = TaskDetailItem.class)
    })
    @Log(title = "巡查任务核查细项信息", businessType = BusinessType.UPDATE)
    public AjaxResult update(@RequestBody TaskDetailItem corpBasicInfo){
        String operate = "update-task_detail_item";
        return service.updateById(corpBasicInfo)?AjaxResult.success(operate,service.getById(corpBasicInfo.getId())) : AjaxResult.error("修改客户基础信息失败");
    }

    @PreAuthorize("@ss.hasPermi('task:detail-item:delete')")
    @DeleteMapping(value = "{id}", produces = {"application/json"})
    @ApiOperation("删除巡查任务核查细项信息")
    @ApiResponses({
        @ApiResponse(code = 200, message = "ok", response = AjaxResult.class),
    })
    @Log(title = "巡查任务核查细项信息", businessType = BusinessType.DELETE)
    public AjaxResult delete(@PathVariable(value = "id", required = false) String id){
        String operate = "delete-task_detail_item";
        String[] ids = id.split(",");
        return service.removeBatchByIds(Arrays.asList(ids))?AjaxResult.success(operate) : AjaxResult.error("修改巡查任务核查细项信息失败");
    }


//    @Log(title = "巡查任务核查细项信息", businessType = BusinessType.EXPORT)
//    @PreAuthorize("@ss.hasPermi('task:detail-item:export')")
//    @PostMapping("/export")
//    @ApiOperation("导出巡查任务核查细项信息")
//    @ApiResponses({
//        @ApiResponse(code = 200, message = "ok", response = AjaxResult.class),
//    })
//    public void export(HttpServletResponse response,@RequestParam(name = "corpId", required = false) String corpId){
//        QueryWrapper<TaskDetailItem> queryWrapper = new QueryWrapper<>();
//        if (StringUtils.isNotEmpty(corpId)) {
//            queryWrapper.eq("corp_id",corpId);
//        }
//        List<TaskDetailItem> list = service.list(queryWrapper);
//        ExcelUtil<TaskDetailItem> util = new ExcelUtil<TaskDetailItem>(TaskDetailItem.class);
//        util.exportExcel(response,list, "导出巡查任务核查细项信息");
//    }

    @PreAuthorize("@ss.hasPermi('task:detail-item:event')")
    @GetMapping(path = "/event/{id}", produces = {"application/json"})
    @ApiOperation(value = "绑定业务列表")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "accId", value = "事件id",paramType = "query", required = false, dataType = "String"),
            @ApiImplicitParam(name = "pageNo", value = "页码", paramType = "query", dataType = "long"),
            @ApiImplicitParam(name = "pageSize", value = "页面记录条数", paramType = "query", dataType = "long")
    })
    @ApiResponses({
            @ApiResponse(code = 200, message = "ok", response= AccidentDetailItem.class),
    })
    public AjaxResult delRelationTask(@PathVariable(value = "id", required = false) String id){
        try {
            return service.setAccidentByTaskItemId(id)?AjaxResult.success():AjaxResult.error();
        } catch (IOException e) {
            log.error("EVENT ERROR", e);
            return AjaxResult.error();
        }
    }
}
