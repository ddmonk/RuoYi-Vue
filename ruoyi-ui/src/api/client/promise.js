// 安全承诺信息
import request from '@/utils/request'

export function listPromise(query) {
  return request({
    url: '/security/corpSecPromiseInfo/list',
    method: 'get',
    params: query
  })
}

export function getPromise(id) {
  return request({
    url: '/security/corpSecPromiseInfo/' + id,
    method: 'get'
  })
}

export function addPromise(data) {
  return request({
    url: '/security/corpSecPromiseInfo',
    method: 'post',
    data: data
  })
}

export function updatePromise(data) {
  return request({
    url: '/security/corpSecPromiseInfo',
    method: 'put',
    data: data
  })
}

export function delPromise(id) {
  return request({
    url: '/security/corpSecPromiseInfo/' + id,
    method: 'delete'
  })
}

export function importPromise(data) {
  return request({
    url: '/security/corpSecPromiseInfo/importData',
    method: 'post',
    data: data
  })
}
