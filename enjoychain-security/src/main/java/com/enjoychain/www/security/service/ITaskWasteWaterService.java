package com.enjoychain.www.security.service;

import com.enjoychain.www.security.entity.TaskWasteWater;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 * 废水产生 服务类
 * </p>
 *
 * @author ddmonk
 * @since 2023-02-24
 */
public interface ITaskWasteWaterService extends ITaskService<TaskWasteWater> {

}
