package com.enjoychain.www.security.service.impl;

import com.enjoychain.www.security.entity.TaskEpEquipItem;
import com.enjoychain.www.security.mapper.TaskEpEquipItemMapper;
import com.enjoychain.www.security.service.ITaskEpEquipItemService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 巡查任务—设备情况 服务实现类
 * </p>
 *
 * @author ddmonk
 * @since 2023-03-07
 */
@Service
public class TaskEpEquipItemServiceImpl extends ServiceImpl<TaskEpEquipItemMapper, TaskEpEquipItem> implements ITaskEpEquipItemService {

}
