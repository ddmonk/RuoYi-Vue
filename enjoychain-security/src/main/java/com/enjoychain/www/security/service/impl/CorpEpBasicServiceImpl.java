package com.enjoychain.www.security.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.enjoychain.www.security.entity.CorpEpBasic;
import com.enjoychain.www.security.mapper.CorpEpBasicMapper;
import com.enjoychain.www.security.service.ICorpEpBasicService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * <p>
 * 环保业务基础信息 服务实现类
 * </p>
 *
 * @author ddmonk
 * @since 2023-01-13
 */
@Service
public class CorpEpBasicServiceImpl extends ServiceImpl<CorpEpBasicMapper, CorpEpBasic> implements ICorpEpBasicService {

    @Override
    public List<CorpEpBasic> listByCorpId(String corpId) {
        QueryWrapper<CorpEpBasic> queryWrapper = new QueryWrapper<>();
        queryWrapper.eq("corp_id",corpId);
        return list(queryWrapper);
    }
}
