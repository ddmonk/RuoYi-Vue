package com.enjoychain.www.security.mapper;

import com.enjoychain.www.security.entity.TaskEpEquipItem;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * 巡查任务—设备情况 Mapper 接口
 * </p>
 *
 * @author ddmonk
 * @since 2023-03-07
 */
public interface TaskEpEquipItemMapper extends BaseMapper<TaskEpEquipItem> {

}
