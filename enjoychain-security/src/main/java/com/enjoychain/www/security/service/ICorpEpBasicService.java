package com.enjoychain.www.security.service;

import com.enjoychain.www.security.entity.CorpEpBasic;
import com.baomidou.mybatisplus.extension.service.IService;

import java.util.List;

/**
 * <p>
 * 环保业务基础信息 服务类
 * </p>
 *
 * @author ddmonk
 * @since 2023-01-13
 */
public interface ICorpEpBasicService extends IService<CorpEpBasic> {

    List<CorpEpBasic> listByCorpId(String corpId);
}
