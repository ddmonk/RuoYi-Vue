package com.enjoychain.www.security.mapper;

import com.enjoychain.www.security.entity.BasicFile;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * 基础文件信息表 Mapper 接口
 * </p>
 *
 * @author ddmonk
 * @since 2023-01-29
 */
public interface BasicFileMapper extends BaseMapper<BasicFile> {

}
