package com.enjoychain.www.security.service;

import com.enjoychain.www.security.entity.TaskEpEquipItem;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 * 巡查任务—设备情况 服务类
 * </p>
 *
 * @author ddmonk
 * @since 2023-03-07
 */
public interface ITaskEpEquipItemService extends IService<TaskEpEquipItem> {

}
