import request from '@/utils/request'

export function listRequirement(query) {
  return request({
    url: '/security/needInfo/list',
    method: 'get',
    params: query
  })
}

export function getRequirement(id) {
  return request({
    url: '/security/needInfo/' + id,
    method: 'get'
  })
}

export function addRequirement(data) {
  return request({
    url: '/security/needInfo',
    method: 'post',
    data: data
  })
}

export function updateRequirement(data) {
  return request({
    url: '/security/needInfo',
    method: 'put',
    data: data
  })
}

export function delRequirement(id) {
  return request({
    url: '/security/needInfo/' + id,
    method: 'delete'
  })
}

export function importRequirement(data) {
  return request({
    url: '/security/needInfo/importData',
    method: 'post',
    data: data
  })
}
