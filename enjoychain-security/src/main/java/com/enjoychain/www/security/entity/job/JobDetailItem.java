package com.enjoychain.www.security.entity.job;

import com.baomidou.mybatisplus.annotation.FieldFill;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableName;
import java.io.Serializable;
import java.time.LocalDateTime;

import com.ruoyi.common.annotation.Excel;
import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;
import lombok.experimental.Accessors;

/**
 * <p>
 * 作业考核详情内容
 * </p>
 *
 * @author ddmonk
 * @since 2023-01-29
 */
@Getter
@Setter
@Accessors(chain = true)
@TableName("job_detail_item")
@ApiModel(value = "JobDetailItem对象", description = "作业考核详情内容")
public class JobDetailItem implements Serializable {

    private static final long serialVersionUID = 1L;

    private Integer id;

    @ApiModelProperty("作业id")
    @Excel(name = "作业id")
    private String jobId;

    @ApiModelProperty("核查指标")
    @Excel(name = "核查指标")
    private String targetName;

    @ApiModelProperty("核查指标id")
    @Excel(name = "核查指标id")
    private String targetId;

    @ApiModelProperty("核查要点id")
    @Excel(name = "核查要点id")
    private String keyId;

    @ApiModelProperty("核查要点")
    @Excel(name = "核查要点")
    private String keyName;

    @ApiModelProperty("核查要素")
    @Excel(name = "核查要素")
    private String element;

    @ApiModelProperty("核查基准数据")
    @Excel(name = "核查基准数据")
    private String basicInfo;

    @ApiModelProperty("核查内容")
    @Excel(name = "核查内容")
    private String content;


    @ApiModelProperty("备注")
    @Excel(name = "备注")
    private String remark;

    @ApiModelProperty("排序字段")
    @Excel(name = "排序字段")
    private Integer itemSort;

    @ApiModelProperty("问题等级")
    @Excel(name = "问题等级")
    private String itemLevel;

    @ApiModelProperty("显示类型（单选、多选、文本框）")
    @Excel(name = "显示类型（单选、多选、文本框）")
    private String type;

    @ApiModelProperty("显示内容描述（单选值、多选值、文本框默认值）")
    @Excel(name = "显示内容描述（单选值、多选值、文本框默认值）")
    private String typeDesc;

    @ApiModelProperty("是否需要上传照片")
    @Excel(name = "是否需要上传照片")
    private Integer needImage;

    @ApiModelProperty(value = "创建时间", hidden = true)
    @TableField(fill = FieldFill.INSERT)
    private LocalDateTime createTime;

    @ApiModelProperty(value = "修改时间", hidden = true)
    @TableField(fill = FieldFill.INSERT_UPDATE)
    private LocalDateTime updateTime;

    @ApiModelProperty(value = "创建人员", hidden = true)
    @TableField(fill = FieldFill.INSERT)
    private String createBy;

    @ApiModelProperty(value = "修改人员", hidden = true)
    @TableField(fill = FieldFill.INSERT_UPDATE)
    private String modifyBy;


}
