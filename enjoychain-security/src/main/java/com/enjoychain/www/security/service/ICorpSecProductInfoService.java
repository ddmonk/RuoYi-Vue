package com.enjoychain.www.security.service;

import com.enjoychain.www.security.entity.CorpSecProductInfo;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 * 安全生产基础信息 服务类
 * </p>
 *
 * @author ddmonk
 * @since 2023-01-13
 */
public interface ICorpSecProductInfoService extends IService<CorpSecProductInfo> {

}
