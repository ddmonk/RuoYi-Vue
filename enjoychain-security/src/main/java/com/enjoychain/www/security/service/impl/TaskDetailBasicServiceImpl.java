package com.enjoychain.www.security.service.impl;

import com.enjoychain.www.security.entity.TaskDetailBasic;
import com.enjoychain.www.security.mapper.TaskDetailBasicMapper;
import com.enjoychain.www.security.service.ITaskDetailBasicService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author ddmonk
 * @since 2023-02-24
 */
@Service
public class TaskDetailBasicServiceImpl extends ServiceImpl<TaskDetailBasicMapper, TaskDetailBasic> implements ITaskDetailBasicService {

}
