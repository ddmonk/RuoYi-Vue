package com.enjoychain.www.security.mapper;

import com.enjoychain.www.security.entity.CorpEpEquipInfo;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * 主要设备情况 Mapper 接口
 * </p>
 *
 * @author ddmonk
 * @since 2023-01-13
 */
public interface CorpEpEquipInfoMapper extends BaseMapper<CorpEpEquipInfo> {

}
