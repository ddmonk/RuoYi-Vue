package com.enjoychain.www.security.entity;

import com.baomidou.mybatisplus.annotation.FieldFill;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import java.io.Serializable;
import java.time.LocalDateTime;
import com.ruoyi.common.annotation.Excel;
import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;
import lombok.experimental.Accessors;

/**
 * <p>
 * 
 * </p>
 *
 * @author ddmonk
 * @since 2023-04-18
 */
@Getter
@Setter
@Accessors(chain = true)
@TableName("task_sec_big_risk_info")
@ApiModel(value = "TaskSecBigRiskInfo对象", description = "")
public class TaskSecBigRiskInfo implements Serializable {

    private static final long serialVersionUID = 1L;

    @TableId(value = "id", type = IdType.AUTO)
    private Integer id;

    @ApiModelProperty("公司id")
    @Excel(name = "公司id")
    private String corpId;

    @ApiModelProperty("风险所在位置")
    @Excel(name = "风险所在位置")
    private String riskPosition;

    @ApiModelProperty("风险代码")
    @Excel(name = "风险代码")
    private String riskCode;

    @ApiModelProperty("风险名称")
    @Excel(name = "风险名称")
    private String riskName;

    @ApiModelProperty("风险点")
    @Excel(name = "风险点")
    private String riskPoint;

    @ApiModelProperty("状态")
    @Excel(name = "状态")
    private Integer status;

    @ApiModelProperty("备注")
    @Excel(name = "备注")
    private String remark;

    @ApiModelProperty(value = "创建时间", hidden = true)
    @TableField(fill = FieldFill.INSERT)
    private LocalDateTime createTime;

    @ApiModelProperty(value = "修改时间", hidden = true)
    @TableField(fill = FieldFill.INSERT_UPDATE)
    private LocalDateTime updateTime;

    @ApiModelProperty(value = "创建人员", hidden = true)
    @TableField(fill = FieldFill.INSERT)
    private String createBy;

    @ApiModelProperty(value = "修改人员", hidden = true)
    @TableField(fill = FieldFill.INSERT_UPDATE)
    private String modifyBy;

    @ApiModelProperty("任务ID")
    @Excel(name = "任务ID")
    private String taskId;


}
