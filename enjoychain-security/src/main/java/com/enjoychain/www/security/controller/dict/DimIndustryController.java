package com.enjoychain.www.security.controller.dict;

import org.springframework.web.bind.annotation.RequestMapping;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.common.utils.StringUtils;
import com.ruoyi.common.utils.poi.ExcelUtil;
import io.swagger.annotations.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import javax.servlet.http.HttpServletResponse;
import java.util.List;
import java.util.Arrays;
import com.enjoychain.www.security.entity.DimIndustry;
import com.enjoychain.www.security.service.IDimIndustryService;
import org.springframework.web.bind.annotation.RestController;

/**
 * <p>
 * 国民经济行业分类 前端控制器
 * </p>
 *
 * @author ddmonk
 * @since 2023-03-09
 */
@Api(tags = "国民经济行业分类")
@RestController
@RequestMapping("/security/industry")
public class DimIndustryController {

    @Autowired
    IDimIndustryService service;

    @PreAuthorize("@ss.hasPermi('security:dim-industry:list')")
    @GetMapping(path = "/list", produces = {"application/json"})
    @ApiOperation(value = "获取国民经济行业分类列表")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "keywords", value = "关键字",paramType = "query", required = false, dataType = "String"),
            @ApiImplicitParam(name = "pageNo", value = "页码", paramType = "query", dataType = "long"),
            @ApiImplicitParam(name = "pageSize", value = "页面记录条数", paramType = "query", dataType = "long")
    })
    @ApiResponses({
        @ApiResponse(code = 200, message = "ok", response= DimIndustry.class),
    })
    public AjaxResult list(
                            @RequestParam(name = "largeClassName", required = false) String largeClassName,
                            @RequestParam(name = "midClassName", required = false) String midClassName,
                            @RequestParam(name = "smallClassName", required = false) String smallClassName,
                            @RequestParam(name = "categoryName", required = false) String categoryName,
                            @RequestParam(name = "keywords", required = false) String keywords,
                            @RequestParam(name = "pageNum", defaultValue = "1") long pageNo,
                            @RequestParam(name = "pageSize",defaultValue = "10") long pageSize){
        IPage<DimIndustry> entityIPage = new Page<>(pageNo, pageSize);
        QueryWrapper<DimIndustry> queryWrapper = new QueryWrapper<>();
        if (StringUtils.isNotEmpty(largeClassName)) {
            queryWrapper.like("large_class_name",largeClassName);
        }
        if (StringUtils.isNotEmpty(midClassName)) {
            queryWrapper.like("mid_class_name",midClassName);
        }
        if (StringUtils.isNotEmpty(smallClassName)) {
            queryWrapper.like("small_class_name",smallClassName);
        }
        if (StringUtils.isNotEmpty(categoryName)) {
            queryWrapper.like("category_name",categoryName);
        }
        if (StringUtils.isNotEmpty(keywords)) {
            queryWrapper.like("class_code",keywords).or().like("small_class_name",keywords);
        }
        return AjaxResult.success("list-dim_industry",service.page(entityIPage,queryWrapper));
    }

    @PreAuthorize("@ss.hasPermi('security:dim-industry:query')")
    @GetMapping(value = "{id}")
    @ApiOperation("获取国民经济行业分类详情")
    @ApiImplicitParams({
        @ApiImplicitParam(name = "id", value = "id",paramType = "path", dataType = "String")
    })
    @ApiResponses({
        @ApiResponse(code = 200, message = "ok", response = DimIndustry.class)
    })
    public AjaxResult getDimIndustryInfo(@PathVariable(value = "id", required = false) String id)
    {
        return AjaxResult.success("get-dim_industry",service.getById(id));
    }

    @PreAuthorize("@ss.hasPermi('security:dim-industry:import')")
    @ApiOperation("导入国民经济行业分类")
    @Log(title = "国民经济行业分类", businessType = BusinessType.IMPORT)
    @PostMapping("/importData")
    public AjaxResult importData(MultipartFile file) throws Exception{
        ExcelUtil<DimIndustry> util = new ExcelUtil<DimIndustry>(DimIndustry.class);
        List<DimIndustry> infos = util.importExcel(file.getInputStream());
        return service.saveOrUpdateBatch(infos)?AjaxResult.success("批量插入成功"):AjaxResult.error("批量插入失败");
    }


    @ApiOperation("国民经济行业分类导出模板下载")
    @PostMapping("/importTemplate")
    public void importTemplate(HttpServletResponse response){
        ExcelUtil<DimIndustry> util = new ExcelUtil<DimIndustry>(DimIndustry.class);
        util.importTemplateExcel(response,"国民经济行业分类");
    }

    @PreAuthorize("@ss.hasPermi('security:dim-industry:add')")
    @PostMapping(consumes = {"application/json"}, produces = {"application/json"})
    @ApiOperation("新增国民经济行业分类")
    @ApiResponses({
        @ApiResponse(code = 200, message = "ok", response = DimIndustry.class),
    })
    @Log(title = "国民经济行业分类", businessType = BusinessType.INSERT)
    public AjaxResult add(@RequestBody DimIndustry entity){
        return service.save(entity)? AjaxResult.success("add-dim_industry",entity): AjaxResult.error("新增国民经济行业分类失败");
    }


    @PreAuthorize("@ss.hasPermi('security:dim-industry:update')")
    @PutMapping(consumes = {"application/json"}, produces = {"application/json"})
    @ApiOperation("修改国民经济行业分类")
    @ApiResponses({
        @ApiResponse(code = 200, message = "ok", response = DimIndustry.class)
    })
    @Log(title = "国民经济行业分类", businessType = BusinessType.UPDATE)
    public AjaxResult update(@RequestBody DimIndustry corpBasicInfo){
        String operate = "update-dim_industry";
        return service.updateById(corpBasicInfo)?AjaxResult.success(operate,service.getById(corpBasicInfo.getId())) : AjaxResult.error("修改客户基础信息失败");
    }

    @PreAuthorize("@ss.hasPermi('security:dim-industry:delete')")
    @DeleteMapping(value = "{id}", produces = {"application/json"})
    @ApiOperation("删除国民经济行业分类")
    @ApiResponses({
        @ApiResponse(code = 200, message = "ok", response = AjaxResult.class),
    })
    @Log(title = "国民经济行业分类", businessType = BusinessType.DELETE)
    public AjaxResult delete(@PathVariable(value = "id", required = false) String id){
        String operate = "delete-dim_industry";
        String[] ids = id.split(",");
        return service.removeBatchByIds(Arrays.asList(ids))?AjaxResult.success(operate) : AjaxResult.error("修改国民经济行业分类失败");
    }


    @Log(title = "国民经济行业分类", businessType = BusinessType.EXPORT)
    @PreAuthorize("@ss.hasPermi('security:dim-industry:export')")
    @PostMapping("/export")
    @ApiOperation("导出国民经济行业分类")
    @ApiResponses({
        @ApiResponse(code = 200, message = "ok", response = AjaxResult.class),
    })
    public void export(HttpServletResponse response,@RequestParam(name = "corpId", required = false) String corpId){
        QueryWrapper<DimIndustry> queryWrapper = new QueryWrapper<>();
        if (StringUtils.isNotEmpty(corpId)) {
            queryWrapper.eq("corp_id",corpId);
        }
        List<DimIndustry> list = service.list(queryWrapper);
        ExcelUtil<DimIndustry> util = new ExcelUtil<DimIndustry>(DimIndustry.class);
        util.exportExcel(response,list, "导出国民经济行业分类");
    }
}
