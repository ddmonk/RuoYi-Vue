package com.enjoychain.www.security.controller.old;

import org.springframework.web.bind.annotation.RequestMapping;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.common.utils.StringUtils;
import com.ruoyi.common.utils.poi.ExcelUtil;
import io.swagger.annotations.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import javax.servlet.http.HttpServletResponse;
import java.util.List;
import java.util.UUID;
import java.util.Arrays;
import com.enjoychain.www.security.entity.CorpRiksItem;
import com.enjoychain.www.security.service.ICorpRiksItemService;
import org.springframework.web.bind.annotation.RestController;

/**
 * <p>
 * 公司较大以上安全风险信息表 前端控制器
 * </p>
 *
 * @author ddmonk
 * @since 2023-01-20
 */
@Api(tags = "公司较大以上安全风险信息表")
@RestController
@RequestMapping("/security/corpRiksItem")
public class CorpRiksItemController {

    @Autowired
    ICorpRiksItemService service;

    @PreAuthorize("@ss.hasPermi('security:corp-riks-item:list')")
    @GetMapping(path = "/list", produces = {"application/json"})
    @ApiOperation(value = "获取公司较大以上安全风险信息表列表")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "corpId", value = "企业Id",paramType = "query", required = false, dataType = "String"),
            @ApiImplicitParam(name = "pageNo", value = "页码", paramType = "query", dataType = "long"),
            @ApiImplicitParam(name = "pageSize", value = "页面记录条数", paramType = "query", dataType = "long")
    })
    @ApiResponses({
        @ApiResponse(code = 200, message = "ok", response= CorpRiksItem.class),
    })
    public AjaxResult list(
                            @RequestParam(name = "corpId", required = false) String corpId,
                            @RequestParam(name = "pageNum", defaultValue = "1") long pageNum,
                            @RequestParam(name = "pageSize",defaultValue = "10") long pageSize){
        IPage<CorpRiksItem> entityIPage = new Page<>(pageNum, pageSize);
        QueryWrapper<CorpRiksItem> queryWrapper = new QueryWrapper<>();
        if (StringUtils.isNotEmpty(corpId)) {
            queryWrapper.eq("corp_id",corpId);
        }
        return AjaxResult.success("list-corp_riks_item",service.page(entityIPage,queryWrapper));
    }

    @PreAuthorize("@ss.hasPermi('security:corp-riks-item:query')")
    @GetMapping(value = "{id}")
    @ApiOperation("获取公司较大以上安全风险信息表详情")
    @ApiImplicitParams({
        @ApiImplicitParam(name = "id", value = "id",paramType = "path", dataType = "String")
    })
    @ApiResponses({
        @ApiResponse(code = 200, message = "ok", response = CorpRiksItem.class)
    })
    public AjaxResult getCorpRiksItemInfo(@PathVariable(value = "id", required = false) String id)
    {
        return AjaxResult.success("get-corp_riks_item",service.getById(id));
    }

    @PreAuthorize("@ss.hasPermi('security:corp-riks-item:import')")
    @ApiOperation("导入公司较大以上安全风险信息表")
    @Log(title = "公司较大以上安全风险信息表", businessType = BusinessType.IMPORT)
    @PostMapping("/importData")
    public AjaxResult importData(MultipartFile file) throws Exception{
        ExcelUtil<CorpRiksItem> util = new ExcelUtil<CorpRiksItem>(CorpRiksItem.class);
        List<CorpRiksItem> infos = util.importExcel(file.getInputStream());
        return service.saveOrUpdateBatch(infos)?AjaxResult.success("批量插入成功"):AjaxResult.error("批量插入失败");
    }


    @ApiOperation("公司较大以上安全风险信息表导出模板下载")
    @PostMapping("/importTemplate")
    public void importTemplate(HttpServletResponse response){
        ExcelUtil<CorpRiksItem> util = new ExcelUtil<CorpRiksItem>(CorpRiksItem.class);
        util.importTemplateExcel(response,"公司较大以上安全风险信息表");
    }

    @PreAuthorize("@ss.hasPermi('security:corp-riks-item:add')")
    @PostMapping(consumes = {"application/json"}, produces = {"application/json"})
    @ApiOperation("新增公司较大以上安全风险信息表")
    @ApiResponses({
        @ApiResponse(code = 200, message = "ok", response = CorpRiksItem.class),
    })
    @Log(title = "公司较大以上安全风险信息表", businessType = BusinessType.INSERT)
    public AjaxResult add(@RequestBody CorpRiksItem entity){
        return service.save(entity)? AjaxResult.success("add-corp_riks_item",entity): AjaxResult.error("新增公司较大以上安全风险信息表失败");
    }


    @PreAuthorize("@ss.hasPermi('security:corp-riks-item:update')")
    @PutMapping(consumes = {"application/json"}, produces = {"application/json"})
    @ApiOperation("修改公司较大以上安全风险信息表")
    @ApiResponses({
        @ApiResponse(code = 200, message = "ok", response = CorpRiksItem.class)
    })
    @Log(title = "公司较大以上安全风险信息表", businessType = BusinessType.UPDATE)
    public AjaxResult update(@RequestBody CorpRiksItem corpBasicInfo){
        String operate = "update-corp_riks_item";
        return service.updateById(corpBasicInfo)?AjaxResult.success(operate,service.getById(corpBasicInfo.getId())) : AjaxResult.error("修改客户基础信息失败");
    }

    @PreAuthorize("@ss.hasPermi('security:corp-riks-item:delete')")
    @DeleteMapping(value = "{id}", produces = {"application/json"})
    @ApiOperation("删除公司较大以上安全风险信息表")
    @ApiResponses({
        @ApiResponse(code = 200, message = "ok", response = AjaxResult.class),
    })
    @Log(title = "公司较大以上安全风险信息表", businessType = BusinessType.DELETE)
    public AjaxResult delete(@PathVariable(value = "id", required = false) String id){
        String operate = "delete-corp_riks_item";
        String[] ids = id.split(",");
        return service.removeBatchByIds(Arrays.asList(ids))?AjaxResult.success(operate) : AjaxResult.error("修改公司较大以上安全风险信息表失败");
    }


    @Log(title = "公司较大以上安全风险信息表", businessType = BusinessType.EXPORT)
    @PreAuthorize("@ss.hasPermi('security:corp-riks-item:export')")
    @PostMapping("/export")
    @ApiOperation("导出公司较大以上安全风险信息表")
    @ApiResponses({
        @ApiResponse(code = 200, message = "ok", response = AjaxResult.class),
    })
    public void export(HttpServletResponse response,@RequestParam(name = "corpId", required = false) String corpId){
        QueryWrapper<CorpRiksItem> queryWrapper = new QueryWrapper<>();
        if (StringUtils.isNotEmpty(corpId)) {
            queryWrapper.eq("corp_id",corpId);
        }
        List<CorpRiksItem> list = service.list(queryWrapper);
        ExcelUtil<CorpRiksItem> util = new ExcelUtil<CorpRiksItem>(CorpRiksItem.class);
        util.exportExcel(response,list, "导出公司较大以上安全风险信息表");
    }
}
