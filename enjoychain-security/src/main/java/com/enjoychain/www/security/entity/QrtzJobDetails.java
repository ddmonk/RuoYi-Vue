package com.enjoychain.www.security.entity;

import com.baomidou.mybatisplus.annotation.TableName;
import java.io.Serializable;
import java.sql.Blob;
import com.ruoyi.common.annotation.Excel;
import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;
import lombok.experimental.Accessors;

/**
 * <p>
 * 任务详细信息表
 * </p>
 *
 * @author ddmonk
 * @since 2023-01-29
 */
@Getter
@Setter
@Accessors(chain = true)
@TableName("QRTZ_JOB_DETAILS")
@ApiModel(value = "QrtzJobDetails对象", description = "任务详细信息表")
public class QrtzJobDetails implements Serializable {

    private static final long serialVersionUID = 1L;

    @ApiModelProperty("调度名称")
    @Excel(name = "调度名称")
    private String schedName;

    @ApiModelProperty("任务名称")
    @Excel(name = "任务名称")
    private String jobName;

    @ApiModelProperty("任务组名")
    @Excel(name = "任务组名")
    private String jobGroup;

    @ApiModelProperty("相关介绍")
    @Excel(name = "相关介绍")
    private String description;

    @ApiModelProperty("执行任务类名称")
    @Excel(name = "执行任务类名称")
    private String jobClassName;

    @ApiModelProperty("是否持久化")
    @Excel(name = "是否持久化")
    private String isDurable;

    @ApiModelProperty("是否并发")
    @Excel(name = "是否并发")
    private String isNonconcurrent;

    @ApiModelProperty("是否更新数据")
    @Excel(name = "是否更新数据")
    private String isUpdateData;

    @ApiModelProperty("是否接受恢复执行")
    @Excel(name = "是否接受恢复执行")
    private String requestsRecovery;

    @ApiModelProperty("存放持久化job对象")
    @Excel(name = "存放持久化job对象")
    private Blob jobData;


}
