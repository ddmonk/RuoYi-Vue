package com.enjoychain.www.security.controller.business.job;

import org.springframework.web.bind.annotation.RequestMapping;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.common.utils.StringUtils;
import com.ruoyi.common.utils.poi.ExcelUtil;
import io.swagger.annotations.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import javax.servlet.http.HttpServletResponse;
import java.util.List;
import java.util.Arrays;
import com.enjoychain.www.security.entity.job.JobDetailItem;
import com.enjoychain.www.security.service.IJobDetailItemService;
import org.springframework.web.bind.annotation.RestController;

/**
 * <p>
 * 作业考核详情内容 前端控制器
 * </p>
 *
 * @author ddmonk
 * @since 2023-01-29
 */
@Api(tags = "作业考核详情内容")
@RestController
@RequestMapping("/security/jobDetailItem")
public class JobDetailItemController {

    @Autowired
    IJobDetailItemService service;

    @PreAuthorize("@ss.hasPermi('job:detail-item:list')")
    @GetMapping(path = "/list", produces = {"application/json"})
    @ApiOperation(value = "获取作业考核详情内容列表")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "jobId", value = "任务Id",paramType = "query", dataType = "String"),
            @ApiImplicitParam(name = "pageNum", value = "页码", paramType = "query", dataType = "long"),
            @ApiImplicitParam(name = "pageSize", value = "页面记录条数", paramType = "query", dataType = "long")
    })
    @ApiResponses({
        @ApiResponse(code = 200, message = "ok", response= JobDetailItem.class),
    })
    public AjaxResult list(
                            @RequestParam(name = "jobId", required = false) String jobId,
                            @RequestParam(name = "pageNum", defaultValue = "1") long pageNo,
                            @RequestParam(name = "pageSize",defaultValue = "10") long pageSize){
        IPage<JobDetailItem> entityIPage = new Page<>(pageNo, pageSize);
        QueryWrapper<JobDetailItem> queryWrapper = new QueryWrapper<>();
        if (StringUtils.isNotEmpty(jobId)) {
            queryWrapper.eq("job_id",jobId);
        }
        queryWrapper.orderByAsc("item_sort","update_time");
        return AjaxResult.success("list-job_detail_item",service.page(entityIPage,queryWrapper));
    }

    @PreAuthorize("@ss.hasPermi('job:detail-item:query')")
    @GetMapping(value = "{id}")
    @ApiOperation("获取作业考核详情内容详情")
    @ApiImplicitParams({
        @ApiImplicitParam(name = "id", value = "id",paramType = "path", dataType = "String")
    })
    @ApiResponses({
        @ApiResponse(code = 200, message = "ok", response = JobDetailItem.class)
    })
    public AjaxResult getJobDetailItemInfo(@PathVariable(value = "id", required = false) String id)
    {
        return AjaxResult.success("get-job_detail_item",service.getById(id));
    }

    @PreAuthorize("@ss.hasPermi('job:detail-item:import')")
    @ApiOperation("导入作业考核详情内容")
    @Log(title = "作业考核详情内容", businessType = BusinessType.IMPORT)
    @PostMapping("/importData")
    public AjaxResult importData(MultipartFile file) throws Exception{
        ExcelUtil<JobDetailItem> util = new ExcelUtil<JobDetailItem>(JobDetailItem.class);
        List<JobDetailItem> infos = util.importExcel(file.getInputStream());
        return service.saveOrUpdateBatch(infos)?AjaxResult.success("批量插入成功"):AjaxResult.error("批量插入失败");
    }


    @ApiOperation("作业考核详情内容导出模板下载")
    @PostMapping("/importTemplate")
    public void importTemplate(HttpServletResponse response){
        ExcelUtil<JobDetailItem> util = new ExcelUtil<JobDetailItem>(JobDetailItem.class);
        util.importTemplateExcel(response,"作业考核详情内容");
    }

    @PreAuthorize("@ss.hasPermi('job:detail-item:add')")
    @PostMapping(consumes = {"application/json"}, produces = {"application/json"})
    @ApiOperation("新增作业考核详情内容")
    @ApiResponses({
        @ApiResponse(code = 200, message = "ok", response = JobDetailItem.class),
    })
    @Log(title = "作业考核详情内容", businessType = BusinessType.INSERT)
    public AjaxResult add(@RequestBody JobDetailItem entity){
        return service.save(entity)? AjaxResult.success("add-job_detail_item",entity): AjaxResult.error("新增作业考核详情内容失败");
    }


    @PreAuthorize("@ss.hasPermi('job:detail-item:update')")
    @PutMapping(consumes = {"application/json"}, produces = {"application/json"})
    @ApiOperation("修改作业考核详情内容")
    @ApiResponses({
        @ApiResponse(code = 200, message = "ok", response = JobDetailItem.class)
    })
    @Log(title = "作业考核详情内容", businessType = BusinessType.UPDATE)
    public AjaxResult update(@RequestBody JobDetailItem corpBasicInfo){
        String operate = "update-job_detail_item";
        return service.updateById(corpBasicInfo)?AjaxResult.success(operate,service.getById(corpBasicInfo.getId())) : AjaxResult.error("修改客户基础信息失败");
    }

    @PreAuthorize("@ss.hasPermi('job:detail-item:delete')")
    @DeleteMapping(value = "{id}", produces = {"application/json"})
    @ApiOperation("删除作业考核详情内容")
    @ApiResponses({
        @ApiResponse(code = 200, message = "ok", response = AjaxResult.class),
    })
    @Log(title = "作业考核详情内容", businessType = BusinessType.DELETE)
    public AjaxResult delete(@PathVariable(value = "id", required = false) String id){
        String operate = "delete-job_detail_item";
        String[] ids = id.split(",");
        return service.removeBatchByIds(Arrays.asList(ids))?AjaxResult.success(operate) : AjaxResult.error("修改作业考核详情内容失败");
    }


    @Log(title = "作业考核详情内容", businessType = BusinessType.EXPORT)
    @PreAuthorize("@ss.hasPermi('job:detail-item:export')")
    @PostMapping("/export")
    @ApiOperation("导出作业考核详情内容")
    @ApiResponses({
        @ApiResponse(code = 200, message = "ok", response = AjaxResult.class),
    })
    public void export(HttpServletResponse response,@RequestParam(name = "corpId", required = false) String corpId){
        QueryWrapper<JobDetailItem> queryWrapper = new QueryWrapper<>();
        if (StringUtils.isNotEmpty(corpId)) {
            queryWrapper.eq("corp_id",corpId);
        }
        List<JobDetailItem> list = service.list(queryWrapper);
        ExcelUtil<JobDetailItem> util = new ExcelUtil<JobDetailItem>(JobDetailItem.class);
        util.exportExcel(response,list, "导出作业考核详情内容");
    }
}
