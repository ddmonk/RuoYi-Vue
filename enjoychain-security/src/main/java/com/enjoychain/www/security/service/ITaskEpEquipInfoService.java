package com.enjoychain.www.security.service;

import com.enjoychain.www.security.entity.TaskEpEquipInfo;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author ddmonk
 * @since 2023-03-08
 */
public interface ITaskEpEquipInfoService extends ITaskService<TaskEpEquipInfo> {

}
