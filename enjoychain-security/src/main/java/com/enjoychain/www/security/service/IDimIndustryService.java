package com.enjoychain.www.security.service;

import com.enjoychain.www.security.entity.DimIndustry;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 * 国民经济行业分类 服务类
 * </p>
 *
 * @author ddmonk
 * @since 2023-03-11
 */
public interface IDimIndustryService extends IService<DimIndustry> {

}
