package com.enjoychain.www.security.entity;

import com.baomidou.mybatisplus.annotation.TableName;
import java.io.Serializable;
import com.ruoyi.common.annotation.Excel;
import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;
import lombok.experimental.Accessors;

/**
 * <p>
 * 
 * </p>
 *
 * @author ddmonk
 * @since 2023-02-24
 */
@Getter
@Setter
@Accessors(chain = true)
@TableName("task_mergency_info")
@ApiModel(value = "TaskMergencyInfo对象", description = "")
public class TaskMergencyInfo implements Serializable {

    private static final long serialVersionUID = 1L;

    private String id;

    private String taskId;

    @ApiModelProperty("应急预案状态")
    @Excel(name = "应急预案状态")
    private String mergencyPlanStatus;

    @ApiModelProperty("应急预案是否需要变更")
    @Excel(name = "应急预案是否需要变更")
    private String mergencyPlanChange;

    @ApiModelProperty("应急池状态")
    @Excel(name = "应急池状态")
    private String mergencyLagoonStatus;


}
