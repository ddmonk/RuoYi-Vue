package com.enjoychain.www.security.service.impl;

import com.enjoychain.www.security.entity.CorpEpCleanProduct;
import com.enjoychain.www.security.mapper.CorpEpCleanProductMapper;
import com.enjoychain.www.security.service.ICorpEpCleanProductService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 企业清洁生产情况 服务实现类
 * </p>
 *
 * @author ddmonk
 * @since 2023-01-13
 */
@Service
public class CorpEpCleanProductServiceImpl extends ServiceImpl<CorpEpCleanProductMapper, CorpEpCleanProduct> implements ICorpEpCleanProductService {

}
