package com.enjoychain.www.security.mapper;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.enjoychain.www.security.entity.task.TaskInfo;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;

/**
 * <p>
 * 任务基础信息表 Mapper 接口
 * </p>
 *
 * @author ddmonk
 * @since 2023-01-20
 */
public interface TaskInfoMapper extends BaseMapper<TaskInfo> {

    @Select("SELECT\n" +
            "\t* \n" +
            "FROM\n" +
            "\ttask_info \n" +
            "WHERE\n" +
            "\tid IN (\n" +
            "\tSELECT DISTINCT\n" +
            "\t\t( d.task_id ) \n" +
            "\tFROM\n" +
            "\t\taccident_detail_item d \n" +
            "WHERE\n" +
            "\td.task_id IS NOT NULL and d.acc_id = #{acc_id})")
    IPage<TaskInfo> pageByAccId(Page<TaskInfo> page, @Param("acc_id") String accId);


    @Select("SELECT COUNT(*) FROM task_info t WHERE YEAR(t.end_time) = YEAR(NOW()) and t.corp_id = #{corp_id}")
    int countFinishTaskByCorpId(@Param("corp_id")String corpId);

//    @Select()
//    TaskInfo getRecentlyFinishTaskByCertNo(String certNo);
}
