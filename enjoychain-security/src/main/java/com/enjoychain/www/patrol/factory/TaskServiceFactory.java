package com.enjoychain.www.patrol.factory;

import com.enjoychain.www.patrol.service.IEcTaskCategoryService;
import com.enjoychain.www.patrol.service.IEcTaskColumnService;
import com.enjoychain.www.patrol.service.IEcTaskItemService;
import com.enjoychain.www.patrol.service.IEcTaskService;
import lombok.Getter;
import org.springframework.stereotype.Component;

import javax.annotation.Resource;

@Component
@Getter
public class TaskServiceFactory {

    @Resource
    protected IEcTaskService ecTaskService;

    @Resource
    protected IEcTaskCategoryService ecTaskCategoryService;

    @Resource
    protected IEcTaskItemService ecTaskItemService;

    @Resource
    protected IEcTaskColumnService ecTaskColumnService;

}
