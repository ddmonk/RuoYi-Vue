package com.enjoychain.www.security.service.impl;

import com.enjoychain.www.security.entity.TaskEpEquipInfo;
import com.enjoychain.www.security.mapper.TaskEquipInfoMapper;
import com.enjoychain.www.security.service.ITaskEpEquipInfoService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author ddmonk
 * @since 2023-03-08
 */
@Service
public class TaskEquipInfoServiceImpl extends ServiceImpl<TaskEquipInfoMapper, TaskEpEquipInfo> implements ITaskEpEquipInfoService {

}
