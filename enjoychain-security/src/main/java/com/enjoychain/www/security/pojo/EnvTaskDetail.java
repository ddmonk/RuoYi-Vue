package com.enjoychain.www.security.pojo;

import com.enjoychain.www.security.entity.*;
import com.ruoyi.common.annotation.Excel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.util.List;

@Data
public class EnvTaskDetail {


    private List<TaskEquipItem> equipItems;

    private TaskProductProcess productProcessInfo;


    @ApiModelProperty("自行监测完成情况")
    @Excel(name = "自行监测完成情况")
    private String ownerMonitorStatus;

    @ApiModelProperty("执行报告发布情况")
    @Excel(name = "执行报告发布情况")
    private String planPublishStatus;

    private TaskMergencyInfo mergencyInfo;

    private TaskWasteWater wasteWaterInfo;

    private TaskRainSewage rainSewageInfo;

    private List<TaskProductSectionItem> gasSectionItems;

    private List<TaskGasPollutionItem> gasPollutionItems;

    private TaskNoise taskNoiseInfo;

    private TaskSolidWaste solidWasteInfo;

    private List<TaskHwStoreStatus> hwStoreStatusInfos;

    private TaskHwData hwData;



}
