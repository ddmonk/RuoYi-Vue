package com.enjoychain.www.security.pojo;
import com.enjoychain.www.patrol.entity.EcTask;
import com.enjoychain.www.security.entity.TaskCorpEnvBasicInfo;
import com.enjoychain.www.security.entity.TaskCorpSecBasicInfo;
import com.enjoychain.www.security.entity.TaskSecPatrolItem;
import com.enjoychain.www.security.entity.task.TaskInfo;
import lombok.Data;

import java.util.List;

@Data
public class TaskPojo {

    String id;

    TaskInfo taskInfo;

    /**
     * 环保企业基础信息
     */
    TaskCorpEnvBasicInfo envCorpInfo;

    TaskCorpEnvBasicInfo corpInfo;

    EnvTaskDetail taskDetail;

    /**
     * 环保任务详情
     */
    EnvTaskDetail envTaskDetail;

    TaskCorpSecBasicInfo secBasicInfo;

    List<TaskSecPatrolItem> secTaskDetail;

    EcTask commonTask;


    public TaskCorpEnvBasicInfo getCorpInfo() {
        return envCorpInfo;
    }

    public void setCorpInfo(TaskCorpEnvBasicInfo corpInfo) {
        this.envCorpInfo = corpInfo;
    }

    public EnvTaskDetail getTaskDetail() {
        return envTaskDetail;
    }

    public void setTaskDetail(EnvTaskDetail taskDetail) {
        this.envTaskDetail = taskDetail;
    }
}
