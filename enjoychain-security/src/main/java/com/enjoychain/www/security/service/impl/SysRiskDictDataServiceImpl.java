package com.enjoychain.www.security.service.impl;

import com.enjoychain.www.security.entity.SysRiskDictData;
import com.enjoychain.www.security.mapper.SysRiskDictDataMapper;
import com.enjoychain.www.security.service.ISysRiskDictDataService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 较大风险目录字典 服务实现类
 * </p>
 *
 * @author ddmonk
 * @since 2023-04-11
 */
@Service
public class SysRiskDictDataServiceImpl extends ServiceImpl<SysRiskDictDataMapper, SysRiskDictData> implements ISysRiskDictDataService {

}
