package com.enjoychain.www.security.constant;

import com.baomidou.mybatisplus.annotation.EnumValue;

public enum NeedType {


    PLAN(0,"计划中"),

    DOING(1,"正在进行"),

    FINISH(2, "完成"),

    STOP(3, "终止");

    @EnumValue
    private int id;

    private String description;

    NeedType(int id, String description) {
        this.id = id;
        this.description = description;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

}
