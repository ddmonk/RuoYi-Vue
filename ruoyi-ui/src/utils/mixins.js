const parSettingMixin = {
  methods: {
    handleChange() {
      this.$emit('input', this.data)
      this.$emit('change', this.data)
    },
    handleRemove(index) {
      if (this.data.length === 1) {
        this.data = [this.getEmptyRow()]
      } else {
        this.data.splice(index, 1)
      }
      this.handleChange()
    },
    handleAdd(index) {
      this.data.splice(index + 1, 0, this.getEmptyRow())
      this.handleChange()
    },
    getEmptyRow() {
      const row = {}
      this.keys.forEach(key => {
        row[key] = ''
      })
      return row
    }
  }
}

export { parSettingMixin }
