package com.enjoychain.www.security.constant;

import com.baomidou.mybatisplus.annotation.EnumValue;
import com.fasterxml.jackson.annotation.JsonValue;
import org.apache.commons.lang3.StringUtils;

public enum BusinessTaskType {

    ENVIRONMENT(0,"环保作业", "environment"),

    SECURITY(1,"安全作业", "security"),

    ACCIDENT(2, "问题", "problem"),

    COMMON(3,"通用任务","common"),

    OTHER(4, "其他", "other");

    @EnumValue
    @JsonValue
    private int id;

    private String description;

    private String value;

    BusinessTaskType(int id, String description, String value) {
        this.id = id;
        this.description = description;
        this.value = value;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public static BusinessTaskType convertFromId(Integer value){
        if (value == null){
            return null;
        }
        for (BusinessTaskType val: values()){
            if (val.getId() == value){
                return val;
            }
        }
        throw new IllegalArgumentException();
    }

    public static BusinessTaskType convertFromString(String value){
        if (StringUtils.isNotEmpty(value)){
            switch (value){
                case "environment": return ENVIRONMENT;
                case "security": return SECURITY;
                default: return OTHER;
            }
        }else return null;
    }

}
