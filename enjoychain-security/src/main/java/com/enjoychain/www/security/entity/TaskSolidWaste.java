package com.enjoychain.www.security.entity;

import com.baomidou.mybatisplus.annotation.FieldFill;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableName;
import java.io.Serializable;
import java.time.LocalDateTime;
import java.util.List;

import com.ruoyi.common.annotation.Excel;
import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;
import lombok.experimental.Accessors;

/**
 * <p>
 * 一般固废仓库
 * </p>
 *
 * @author ddmonk
 * @since 2023-02-24
 */
@Getter
@Setter
@Accessors(chain = true)
@TableName("task_solid_waste")
@ApiModel(value = "TaskSolidWaste对象", description = "一般固废仓库")
public class TaskSolidWaste implements Serializable {

    private static final long serialVersionUID = 1L;

    private String id;

    @ApiModelProperty("任务id")
    @Excel(name = "任务id")
    private String taskId;

    @ApiModelProperty("一般固废仓库是否满足要求")
    @Excel(name = "一般固废仓库是否满足要求")
    private String solidWasteStatus;

    @ApiModelProperty("一般固废台帐内容是否齐全")
    @Excel(name = "一般固废台帐内容是否齐全")
    private String solidWasteBookStatus;

    @ApiModelProperty("一般固废标识是否张贴")
    @Excel(name = "一般固废标识是否张贴")
    private String solidWasteNoteStatus;

    @ApiModelProperty("一般固废标识是否张贴建议")
    @Excel(name = "一般固废标识是否张贴建议")
    private String solidWasteNoteAdvice;

    @ApiModelProperty(value = "创建时间", hidden = true)
    @TableField(fill = FieldFill.INSERT)
    private LocalDateTime createTime;

    @ApiModelProperty(value = "修改时间", hidden = true)
    @TableField(fill = FieldFill.INSERT_UPDATE)
    private LocalDateTime updateTime;

    @ApiModelProperty(value = "创建人员", hidden = true)
    @TableField(fill = FieldFill.INSERT)
    private String createBy;

    @ApiModelProperty(value = "修改人员", hidden = true)
    @TableField(fill = FieldFill.INSERT_UPDATE)
    private String modifyBy;

    @TableField(exist = false)
    private List<BasicFile> files;

}
