package com.enjoychain.www.security.controller.basic;

import org.springframework.web.bind.annotation.RequestMapping;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.common.utils.StringUtils;
import com.ruoyi.common.utils.poi.ExcelUtil;
import io.swagger.annotations.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletResponse;
import java.util.List;
import java.util.Arrays;
import com.enjoychain.www.security.entity.CorpEnvBasicInfo;
import com.enjoychain.www.security.service.ICorpEnvBasicInfoService;
import org.springframework.web.bind.annotation.RestController;

/**
 * <p>
 * 环保企业信息 前端控制器
 * </p>
 *
 * @author ddmonk
 * @since 2023-02-21
 */
@Api(tags = "环保企业信息")
@RestController
@RequestMapping("/security/corpEnvBasicInfo")
public class CorpEnvBasicInfoController {

    @Autowired
    ICorpEnvBasicInfoService service;

    @PreAuthorize("@ss.hasPermi('client:service:list')")
    @GetMapping(path = "/list", produces = {"application/json"})
    @ApiOperation(value = "获取环保企业信息列表")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "corpId", value = "企业Id",paramType = "query", required = false, dataType = "String"),
            @ApiImplicitParam(name = "pageNo", value = "页码", paramType = "query", dataType = "long"),
            @ApiImplicitParam(name = "pageSize", value = "页面记录条数", paramType = "query", dataType = "long")
    })
    @ApiResponses({
        @ApiResponse(code = 200, message = "ok", response= CorpEnvBasicInfo.class),
    })
    public AjaxResult list(
                            @RequestParam(name = "corpId", required = false) String corpId,
                            @RequestParam(name = "pageNum", defaultValue = "1") long pageNo,
                            @RequestParam(name = "pageSize",defaultValue = "10") long pageSize){
        IPage<CorpEnvBasicInfo> entityIPage = new Page<>(pageNo, pageSize);
        QueryWrapper<CorpEnvBasicInfo> queryWrapper = new QueryWrapper<>();
        if (StringUtils.isNotEmpty(corpId)) {
            queryWrapper.eq("corp_id",corpId);
        }
        return AjaxResult.success("list-corp_env_basic_info",service.page(entityIPage,queryWrapper));
    }

    @PreAuthorize("@ss.hasPermi('client:service:query')")
    @GetMapping(value = "{id}")
    @ApiOperation("获取环保企业信息详情")
    @ApiImplicitParams({
        @ApiImplicitParam(name = "id", value = "id",paramType = "path", dataType = "String")
    })
    @ApiResponses({
        @ApiResponse(code = 200, message = "ok", response = CorpEnvBasicInfo.class)
    })
    public AjaxResult getCorpEnvBasicInfoInfo(@PathVariable(value = "id", required = false) String id)
    {
        return AjaxResult.success("get-corp_env_basic_info",service.getDetailById(id));
    }

//    @PreAuthorize("@ss.hasPermi('security:corp-env-basic-info:import')")
//    @ApiOperation("导入环保企业信息")
//    @Log(title = "环保企业信息", businessType = BusinessType.IMPORT)
//    @PostMapping("/importData")
//    public AjaxResult importData(MultipartFile file) throws Exception{
//        ExcelUtil<CorpEnvBasicInfo> util = new ExcelUtil<CorpEnvBasicInfo>(CorpEnvBasicInfo.class);
//        List<CorpEnvBasicInfo> infos = util.importExcel(file.getInputStream());
//        return service.saveOrUpdateBatch(infos)?AjaxResult.success("批量插入成功"):AjaxResult.error("批量插入失败");
//    }
//
//
//    @ApiOperation("环保企业信息导出模板下载")
//    @PostMapping("/importTemplate")
//    public void importTemplate(HttpServletResponse response){
//        ExcelUtil<CorpEnvBasicInfo> util = new ExcelUtil<CorpEnvBasicInfo>(CorpEnvBasicInfo.class);
//        util.importTemplateExcel(response,"环保企业信息");
//    }

    @PreAuthorize("@ss.hasPermi('client:service:add')")
    @PostMapping(consumes = {"application/json"}, produces = {"application/json"})
    @ApiOperation("新增环保企业信息")
    @ApiResponses({
        @ApiResponse(code = 200, message = "ok", response = CorpEnvBasicInfo.class),
    })
    @Log(title = "环保企业信息", businessType = BusinessType.INSERT)
    public AjaxResult add(@RequestBody CorpEnvBasicInfo entity){
        return service.save(entity)? AjaxResult.success("add-corp_env_basic_info",entity): AjaxResult.error("新增环保企业信息失败");
    }


    @PreAuthorize("@ss.hasPermi('client:service:update')")
    @PutMapping(consumes = {"application/json"}, produces = {"application/json"})
    @ApiOperation("修改环保企业信息")
    @ApiResponses({
        @ApiResponse(code = 200, message = "ok", response = CorpEnvBasicInfo.class)
    })
    @Log(title = "环保企业信息", businessType = BusinessType.UPDATE)
    public AjaxResult update(@RequestBody CorpEnvBasicInfo corpBasicInfo){
        String operate = "update-corp_env_basic_info";
        return service.saveOrUpdateDetail(corpBasicInfo)?AjaxResult.success(operate,service.getDetailById(corpBasicInfo.getId())) : AjaxResult.error("修改客户基础信息失败");
    }

    @PreAuthorize("@ss.hasPermi('client:service:delete')")
    @DeleteMapping(value = "{id}", produces = {"application/json"})
    @ApiOperation("删除环保企业信息")
    @ApiResponses({
        @ApiResponse(code = 200, message = "ok", response = AjaxResult.class),
    })
    @Log(title = "环保企业信息", businessType = BusinessType.DELETE)
    public AjaxResult delete(@PathVariable(value = "id", required = false) String id){
        String operate = "delete-corp_env_basic_info";
        String[] ids = id.split(",");
        return service.removeBasicInfoByIds(Arrays.asList(ids))?AjaxResult.success(operate) : AjaxResult.error("修改环保企业信息失败");
    }

    @Log(title = "环保企业信息", businessType = BusinessType.EXPORT)
    @PreAuthorize("@ss.hasPermi('client:service:export')")
    @PostMapping("/export")
    @ApiOperation("导出环保企业信息报表")
    @ApiResponses({
        @ApiResponse(code = 200, message = "ok", response = AjaxResult.class),
    })
    public void export(HttpServletResponse response,@RequestParam(name = "corpId", required = false) String corpId){
        QueryWrapper<CorpEnvBasicInfo> queryWrapper = new QueryWrapper<>();
        if (StringUtils.isNotEmpty(corpId)) {
            queryWrapper.eq("corp_id",corpId);
        }
        List<CorpEnvBasicInfo> list = service.list(queryWrapper);
        ExcelUtil<CorpEnvBasicInfo> util = new ExcelUtil<CorpEnvBasicInfo>(CorpEnvBasicInfo.class);
        util.exportExcel(response,list, "导出环保企业信息");
    }
}
