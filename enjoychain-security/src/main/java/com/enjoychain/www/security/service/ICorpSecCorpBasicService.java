package com.enjoychain.www.security.service;

import com.enjoychain.www.security.entity.CorpBasicInfo;
import com.enjoychain.www.security.entity.CorpSecCorpBasic;
import com.baomidou.mybatisplus.extension.service.IService;
import com.enjoychain.www.security.entity.TaskCorpSecBasicInfo;

/**
 * <p>
 * 安全生产基础信息 服务类
 * </p>
 *
 * @author ddmonk
 * @since 2023-04-14
 */
public interface ICorpSecCorpBasicService extends IService<CorpSecCorpBasic> {

    void saveOrUpdateDetail(TaskCorpSecBasicInfo secBasicInfo);

    boolean saveOrUpdateDetail(CorpSecCorpBasic secBasicInfo);

    void inital(CorpBasicInfo basicInfo);

    CorpSecCorpBasic getDetailById(String id);
}
