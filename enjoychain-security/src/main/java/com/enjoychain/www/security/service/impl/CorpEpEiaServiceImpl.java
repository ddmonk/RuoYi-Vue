package com.enjoychain.www.security.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.enjoychain.www.security.entity.CorpEpEia;
import com.enjoychain.www.security.entity.CorpEpEquipInfo;
import com.enjoychain.www.security.mapper.CorpEpEiaMapper;
import com.enjoychain.www.security.service.ICorpEpEiaService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.ruoyi.common.utils.StringUtils;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * <p>
 * 环评情况表 服务实现类
 * </p>
 *
 * @author ddmonk
 * @since 2023-01-13
 */
@Service
public class CorpEpEiaServiceImpl extends ServiceImpl<CorpEpEiaMapper, CorpEpEia> implements ICorpEpEiaService {

    private QueryWrapper<CorpEpEia> buildQueryWrapper(String id){
        QueryWrapper<CorpEpEia> queryWrapper = new QueryWrapper<>();
        queryWrapper.eq("corp_id",id);
        return queryWrapper;
    }

    @Override
    public List<CorpEpEia> listByCorpId(String corpId) {
        QueryWrapper<CorpEpEia> queryWrapper = new QueryWrapper<>();
        queryWrapper.eq("corp_id",corpId);
        return list(buildQueryWrapper(corpId));
    }

    @Override
    public void saveOrUpdateBatch(List<CorpEpEia> reportItems, String id) {
        for (CorpEpEia report: reportItems){
            if (StringUtils.isEmpty(report.getCorpId())){
                report.setCorpId(id);
            }
        }
        saveOrUpdateBatch(reportItems);
    }

    @Override
    public void deleteByCorpId(String id) {
        remove(buildQueryWrapper(id));
    }
}
