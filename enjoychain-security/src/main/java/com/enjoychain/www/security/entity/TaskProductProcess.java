package com.enjoychain.www.security.entity;

import com.baomidou.mybatisplus.annotation.FieldFill;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableName;
import java.io.Serializable;
import java.time.LocalDateTime;
import com.ruoyi.common.annotation.Excel;
import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;
import lombok.experimental.Accessors;

/**
 * <p>
 * 生产工艺
 * </p>
 *
 * @author ddmonk
 * @since 2023-02-24
 */
@Getter
@Setter
@Accessors(chain = true)
@TableName("task_product_process")
@ApiModel(value = "TaskProductProcess对象", description = "生产工艺")
public class TaskProductProcess implements Serializable {

    private static final long serialVersionUID = 1L;

    private String id;

    @ApiModelProperty("任务id")
    @Excel(name = "任务id")
    private String taskId;

    @ApiModelProperty("生产工艺变化情况")
    @Excel(name = "生产工艺变化情况")
    private String productProcessStatus;

    @ApiModelProperty("生产工艺变化情况备注")
    @Excel(name = "生产工艺变化情况备注")
    private String productProcessRemark;

    @ApiModelProperty("新污染物情况")
    @Excel(name = "新污染物情况")
    private String newWasteStatus;

    @ApiModelProperty("新污染物备注")
    @Excel(name = "新污染物备注")
    private String newWasteRemark;

    @ApiModelProperty("针对变化的建议")
    @Excel(name = "针对变化的建议")
    private String processAdvice;

    @ApiModelProperty(value = "创建时间", hidden = true)
    @TableField(fill = FieldFill.INSERT)
    private LocalDateTime createTime;

    @ApiModelProperty(value = "修改时间", hidden = true)
    @TableField(fill = FieldFill.INSERT_UPDATE)
    private LocalDateTime updateTime;

    @ApiModelProperty(value = "创建人员", hidden = true)
    @TableField(fill = FieldFill.INSERT)
    private String createBy;

    @ApiModelProperty(value = "修改人员", hidden = true)
    @TableField(fill = FieldFill.INSERT_UPDATE)
    private String modifyBy;


}
