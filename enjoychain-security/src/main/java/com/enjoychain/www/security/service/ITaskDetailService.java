package com.enjoychain.www.security.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.enjoychain.www.security.pojo.TaskDetailPojo;

import java.util.List;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author ddmonk
 * @since 2023-01-20
 */
public interface ITaskDetailService {

    TaskDetailPojo getById(String taskId);

    void removeByTaskId(String taskId);

    void removeByTaskIds(List<String> taskIds);

}
