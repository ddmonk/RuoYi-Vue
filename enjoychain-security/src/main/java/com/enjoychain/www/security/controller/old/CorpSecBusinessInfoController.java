package com.enjoychain.www.security.controller.old;

import org.springframework.web.bind.annotation.RequestMapping;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.common.utils.StringUtils;
import com.ruoyi.common.utils.poi.ExcelUtil;
import io.swagger.annotations.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import javax.servlet.http.HttpServletResponse;
import java.util.List;
import java.util.UUID;
import java.util.Arrays;
import com.enjoychain.www.security.entity.CorpSecBusinessInfo;
import com.enjoychain.www.security.service.ICorpSecBusinessInfoService;
import org.springframework.web.bind.annotation.RestController;

/**
 * <p>
 * 安全企业行业类型信息 前端控制器
 * </p>
 *
 * @author ddmonk
 * @since 2023-01-20
 */
@Api(tags = "安全企业行业类型信息")
@RestController
@RequestMapping("/security/corpSecBusinessInfo")
public class CorpSecBusinessInfoController {

    @Autowired
    ICorpSecBusinessInfoService service;

    @PreAuthorize("@ss.hasPermi('security:corp-sec-business-info:list')")
    @GetMapping(path = "/list", produces = {"application/json"})
    @ApiOperation(value = "获取安全企业行业类型信息列表")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "corpId", value = "企业Id",paramType = "query", required = false, dataType = "String"),
            @ApiImplicitParam(name = "pageNo", value = "页码", paramType = "query", dataType = "long"),
            @ApiImplicitParam(name = "pageSize", value = "页面记录条数", paramType = "query", dataType = "long")
    })
    @ApiResponses({
        @ApiResponse(code = 200, message = "ok", response= CorpSecBusinessInfo.class),
    })
    public AjaxResult list(
                            @RequestParam(name = "corpId", required = false) String corpId,
                            @RequestParam(name = "pageNum", defaultValue = "1") long pageNum,
                            @RequestParam(name = "pageSize",defaultValue = "10") long pageSize){
        IPage<CorpSecBusinessInfo> entityIPage = new Page<>(pageNum, pageSize);
        QueryWrapper<CorpSecBusinessInfo> queryWrapper = new QueryWrapper<>();
        if (StringUtils.isNotEmpty(corpId)) {
            queryWrapper.eq("corp_id",corpId);
        }
        return AjaxResult.success("list-corp_sec_business_info",service.page(entityIPage,queryWrapper));
    }

    @PreAuthorize("@ss.hasPermi('security:corp-sec-business-info:query')")
    @GetMapping(value = "{id}")
    @ApiOperation("获取安全企业行业类型信息详情")
    @ApiImplicitParams({
        @ApiImplicitParam(name = "id", value = "id",paramType = "path", dataType = "String")
    })
    @ApiResponses({
        @ApiResponse(code = 200, message = "ok", response = CorpSecBusinessInfo.class)
    })
    public AjaxResult getCorpSecBusinessInfoInfo(@PathVariable(value = "id", required = false) String id)
    {
        return AjaxResult.success("get-corp_sec_business_info",service.getById(id));
    }

    @PreAuthorize("@ss.hasPermi('security:corp-sec-business-info:import')")
    @ApiOperation("导入安全企业行业类型信息")
    @Log(title = "安全企业行业类型信息", businessType = BusinessType.IMPORT)
    @PostMapping("/importData")
    public AjaxResult importData(MultipartFile file) throws Exception{
        ExcelUtil<CorpSecBusinessInfo> util = new ExcelUtil<CorpSecBusinessInfo>(CorpSecBusinessInfo.class);
        List<CorpSecBusinessInfo> infos = util.importExcel(file.getInputStream());
        return service.saveOrUpdateBatch(infos)?AjaxResult.success("批量插入成功"):AjaxResult.error("批量插入失败");
    }


    @ApiOperation("安全企业行业类型信息导出模板下载")
    @PostMapping("/importTemplate")
    public void importTemplate(HttpServletResponse response){
        ExcelUtil<CorpSecBusinessInfo> util = new ExcelUtil<CorpSecBusinessInfo>(CorpSecBusinessInfo.class);
        util.importTemplateExcel(response,"安全企业行业类型信息");
    }

    @PreAuthorize("@ss.hasPermi('security:corp-sec-business-info:add')")
    @PostMapping(consumes = {"application/json"}, produces = {"application/json"})
    @ApiOperation("新增安全企业行业类型信息")
    @ApiResponses({
        @ApiResponse(code = 200, message = "ok", response = CorpSecBusinessInfo.class),
    })
    @Log(title = "安全企业行业类型信息", businessType = BusinessType.INSERT)
    public AjaxResult add(@RequestBody CorpSecBusinessInfo entity){
        return service.save(entity)? AjaxResult.success("add-corp_sec_business_info",entity): AjaxResult.error("新增安全企业行业类型信息失败");
    }


    @PreAuthorize("@ss.hasPermi('security:corp-sec-business-info:update')")
    @PutMapping(consumes = {"application/json"}, produces = {"application/json"})
    @ApiOperation("修改安全企业行业类型信息")
    @ApiResponses({
        @ApiResponse(code = 200, message = "ok", response = CorpSecBusinessInfo.class)
    })
    @Log(title = "安全企业行业类型信息", businessType = BusinessType.UPDATE)
    public AjaxResult update(@RequestBody CorpSecBusinessInfo corpBasicInfo){
        String operate = "update-corp_sec_business_info";
        return service.updateById(corpBasicInfo)?AjaxResult.success(operate,service.getById(corpBasicInfo.getId())) : AjaxResult.error("修改客户基础信息失败");
    }

    @PreAuthorize("@ss.hasPermi('security:corp-sec-business-info:delete')")
    @DeleteMapping(value = "{id}", produces = {"application/json"})
    @ApiOperation("删除安全企业行业类型信息")
    @ApiResponses({
        @ApiResponse(code = 200, message = "ok", response = AjaxResult.class),
    })
    @Log(title = "安全企业行业类型信息", businessType = BusinessType.DELETE)
    public AjaxResult delete(@PathVariable(value = "id", required = false) String id){
        String operate = "delete-corp_sec_business_info";
        String[] ids = id.split(",");
        return service.removeBatchByIds(Arrays.asList(ids))?AjaxResult.success(operate) : AjaxResult.error("修改安全企业行业类型信息失败");
    }


    @Log(title = "安全企业行业类型信息", businessType = BusinessType.EXPORT)
    @PreAuthorize("@ss.hasPermi('security:corp-sec-business-info:export')")
    @PostMapping("/export")
    @ApiOperation("导出安全企业行业类型信息")
    @ApiResponses({
        @ApiResponse(code = 200, message = "ok", response = AjaxResult.class),
    })
    public void export(HttpServletResponse response,@RequestParam(name = "corpId", required = false) String corpId){
        QueryWrapper<CorpSecBusinessInfo> queryWrapper = new QueryWrapper<>();
        if (StringUtils.isNotEmpty(corpId)) {
            queryWrapper.eq("corp_id",corpId);
        }
        List<CorpSecBusinessInfo> list = service.list(queryWrapper);
        ExcelUtil<CorpSecBusinessInfo> util = new ExcelUtil<CorpSecBusinessInfo>(CorpSecBusinessInfo.class);
        util.exportExcel(response,list, "导出安全企业行业类型信息");
    }
}
