package com.enjoychain.www.security.mapper;

import com.enjoychain.www.security.entity.CorpEpCleanProduct;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * 企业清洁生产情况 Mapper 接口
 * </p>
 *
 * @author ddmonk
 * @since 2023-01-13
 */
public interface CorpEpCleanProductMapper extends BaseMapper<CorpEpCleanProduct> {

}
