package com.enjoychain.www.security.mapper;

import com.enjoychain.www.security.entity.TaskMergencyInfo;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author ddmonk
 * @since 2023-02-24
 */
public interface TaskMergencyInfoMapper extends BaseMapper<TaskMergencyInfo> {

}
