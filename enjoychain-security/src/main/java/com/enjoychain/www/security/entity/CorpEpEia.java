package com.enjoychain.www.security.entity;

import com.baomidou.mybatisplus.annotation.FieldFill;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import java.io.Serializable;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

import com.ruoyi.common.annotation.Excel;
import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.experimental.Accessors;

import static com.enjoychain.www.security.constant.Constant.DATE_FORMATTER;
import static com.enjoychain.www.security.constant.Constant.REPORT_DEFAULT_VALUE;

/**
 * <p>
 * 环评情况表
 * </p>
 *
 * @author ddmonk
 * @since 2023-01-13
 */
@Getter
@Setter
@Accessors(chain = true)
@TableName("corp_ep_eia")
@ApiModel(value = "CorpEpEia对象", description = "环评情况表")
@NoArgsConstructor
public class CorpEpEia implements Serializable {

    private static final long serialVersionUID = 1L;



    public CorpEpEia(String defaultValue) {
        this.projectName = defaultValue;
        this.reportType = defaultValue;
        this.officialNo = defaultValue;
        this.status = defaultValue;
        this.officialTimeString = defaultValue;
        this.checkTimeString = defaultValue;
    }

    @ApiModelProperty("主键")
    @Excel(name = "主键")
    @TableId(value = "id", type = IdType.AUTO)
    private Integer id;

    @ApiModelProperty("公司id")
    @Excel(name = "公司id")
    private String corpId;

    @ApiModelProperty("项目名称")
    @Excel(name = "项目名称")
    private String projectName;

    @ApiModelProperty("报告类型")
    @Excel(name = "报告类型")
    private String reportType;

    @ApiModelProperty("批复时间")
    @Excel(name = "批复时间", width = 30, dateFormat = "yyyy-MM-dd")
    private LocalDateTime officialTime;

    @TableField(exist = false)
    private String officialTimeString;

    @ApiModelProperty("批复文号")
    @Excel(name = "批复文号")
    private String officialNo;

    @ApiModelProperty("是否验收")
    @Excel(name = "是否验收")
    private String status;

    @ApiModelProperty("验收时间")
    @Excel(name = "验收时间", width = 30, dateFormat = "yyyy-MM-dd")
    private LocalDateTime checkTime;

    @TableField(exist = false)
    private String checkTimeString;

    public void setOfficialTime(LocalDateTime officialTime) {
        this.officialTime = officialTime;
        this.officialTimeString = this.officialTime == null ? REPORT_DEFAULT_VALUE : officialTime.format(DateTimeFormatter.ofPattern(DATE_FORMATTER));
    }

    public void setCheckTime(LocalDateTime checkTime) {
        this.checkTime = checkTime;
        this.checkTimeString = this.checkTime == null ? REPORT_DEFAULT_VALUE : checkTime.format(DateTimeFormatter.ofPattern(DATE_FORMATTER));
    }

    @ApiModelProperty("备注")
    @Excel(name = "备注")
    private String remark;

    @ApiModelProperty(value = "创建时间", hidden = true)
    @TableField(fill = FieldFill.INSERT)
    private LocalDateTime createTime;

    @ApiModelProperty(value = "修改时间", hidden = true)
    @TableField(fill = FieldFill.INSERT_UPDATE)
    private LocalDateTime updateTime;

    @ApiModelProperty(value = "创建人员", hidden = true)
    @TableField(fill = FieldFill.INSERT)
    private String createBy;

    @ApiModelProperty(value = "修改人员", hidden = true)
    @TableField(fill = FieldFill.INSERT_UPDATE)
    private String modifyBy;


}
