package com.enjoychain.www.security.service;

import com.enjoychain.www.security.entity.TaskHwStoreStatus;
import com.baomidou.mybatisplus.extension.service.IService;

import java.util.List;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author ddmonk
 * @since 2023-02-24
 */
public interface ITaskHwStoreStatusService extends ITaskService<TaskHwStoreStatus> {

    List<TaskHwStoreStatus> initial(String taskId);

    List<TaskHwStoreStatus> listByTaskId(String taskId);

    void removeDetailByTaskId(String taskId);
}
