package com.enjoychain.www.security.mapper;

import com.enjoychain.www.security.entity.SolidWasteItem;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * 一般固废详情 Mapper 接口
 * </p>
 *
 * @author ddmonk
 * @since 2023-02-21
 */
public interface SolidWasteItemMapper extends BaseMapper<SolidWasteItem> {

}
