package com.enjoychain.www.security.service;

import com.enjoychain.www.security.constant.BusinessTaskType;
import com.enjoychain.www.security.entity.BasicFile;
import com.baomidou.mybatisplus.extension.service.IService;

import java.io.IOException;
import java.util.List;

/**
 * <p>
 * 基础文件信息表 服务类
 * </p>
 *
 * @author ddmonk
 * @since 2023-01-29
 */
public interface IBasicFileService extends IService<BasicFile> {

    List<BasicFile> getListByTypeAndId(BusinessTaskType type, String id);

    boolean removeFileById(int id) throws IOException;

    boolean removeFileByConnectIdAndBusiType(BusinessTaskType businessType, String connectId);

    BasicFile getSingleByTypeAndId(BusinessTaskType common, String managerSign);
}
