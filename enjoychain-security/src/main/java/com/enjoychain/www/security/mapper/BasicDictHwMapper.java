package com.enjoychain.www.security.mapper;

import com.enjoychain.www.security.entity.BasicDictHw;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * 危险废物名录字典 Mapper 接口
 * </p>
 *
 * @author ddmonk
 * @since 2023-03-08
 */
public interface BasicDictHwMapper extends BaseMapper<BasicDictHw> {

}
