package com.enjoychain.www.security.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.enjoychain.www.security.entity.CorpEpHw;
import com.enjoychain.www.security.mapper.CorpEpHwMapper;
import com.enjoychain.www.security.service.ICorpEpHwService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * <p>
 * 危废生产情况 服务实现类
 * </p>
 *
 * @author ddmonk
 * @since 2023-01-13
 */
@Service
public class CorpEpHwServiceImpl extends ServiceImpl<CorpEpHwMapper, CorpEpHw> implements ICorpEpHwService {

    @Override
    public List<CorpEpHw> listWithStatus(int i, String corpId) {
        QueryWrapper<CorpEpHw> queryWrapper = new QueryWrapper<>();
        queryWrapper.eq("status",i);
        queryWrapper.eq("corp_id",corpId);
        return list(queryWrapper);
    }
}
