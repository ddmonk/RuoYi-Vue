package com.enjoychain.www.security.service;
import com.enjoychain.www.security.entity.TaskCorpSecBasicInfo;
import com.enjoychain.www.security.entity.TaskSecPatrolItem;

import java.util.List;

/**
 * <p>
 * 安全核查基础信息表 服务类
 * </p>
 *
 * @author ddmonk
 * @since 2023-04-18
 */
public interface ITaskSecPatrolItemService extends ITaskService<TaskSecPatrolItem> {

    void inital(TaskCorpSecBasicInfo basicInfo, String taskId);

    void updateByCorpSecBasicInfo(TaskCorpSecBasicInfo secBasicInfo);

    List<TaskSecPatrolItem> listDetailByTaskId(String taskId);

    void updatePatrolItems(TaskCorpSecBasicInfo basicInfo, List<TaskSecPatrolItem> items);
}
