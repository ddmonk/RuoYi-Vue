package com.enjoychain.www.security.entity;

import com.baomidou.mybatisplus.annotation.FieldFill;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import java.io.Serializable;
import java.time.LocalDateTime;
import com.ruoyi.common.annotation.Excel;
import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;
import lombok.experimental.Accessors;

/**
 * <p>
 * 土壤和地下水自行监测情况
 * </p>
 *
 * @author ddmonk
 * @since 2023-01-13
 */
@Getter
@Setter
@Accessors(chain = true)
@TableName("corp_ep_water_monitor")
@ApiModel(value = "CorpEpWaterMonitor对象", description = "土壤和地下水自行监测情况")
public class CorpEpWaterMonitor implements Serializable {

    private static final long serialVersionUID = 1L;

    @ApiModelProperty("主键")
    @Excel(name = "主键")
    @TableId(value = "id", type = IdType.AUTO)
    private Integer id;

    @ApiModelProperty("公司id")
    @Excel(name = "公司id")
    private String corpId;

    @ApiModelProperty("监测时间")
    @Excel(name = "监测时间", width = 30, dateFormat = "yyyy-MM-dd")
    private LocalDateTime monitorTime;

    @ApiModelProperty("监测情况")
    @Excel(name = "监测情况")
    private String monitorDesc;

    @ApiModelProperty("状态")
    @Excel(name = "状态")
    private Integer status;

    @ApiModelProperty("是否存在")
    @Excel(name = "是否存在")
    private String existMonitor;

    @ApiModelProperty("备注")
    @Excel(name = "备注")
    private String remark;

    @ApiModelProperty(value = "创建时间", hidden = true)
    @TableField(fill = FieldFill.INSERT)
    private LocalDateTime createTime;

    @ApiModelProperty(value = "修改时间", hidden = true)
    @TableField(fill = FieldFill.INSERT_UPDATE)
    private LocalDateTime updateTime;

    @ApiModelProperty(value = "创建人员", hidden = true)
    @TableField(fill = FieldFill.INSERT)
    private String createBy;

    @ApiModelProperty(value = "修改人员", hidden = true)
    @TableField(fill = FieldFill.INSERT_UPDATE)
    private String modifyBy;


}
