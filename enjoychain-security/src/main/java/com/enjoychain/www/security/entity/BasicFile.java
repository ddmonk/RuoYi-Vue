package com.enjoychain.www.security.entity;

import com.baomidou.mybatisplus.annotation.*;

import java.io.Serializable;
import java.time.LocalDateTime;

import com.enjoychain.www.security.constant.BusinessTaskType;
import com.ruoyi.common.annotation.Excel;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.*;
import lombok.experimental.Accessors;

/**
 * <p>
 * 基础文件信息表
 * </p>
 *
 * @author ddmonk
 * @since 2023-01-29
 */
@Getter
@Setter
@Accessors(chain = true)
@TableName("sys_basic_file")
@ApiModel(value = "BasicFile对象", description = "基础文件信息表")
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class BasicFile implements Serializable {

    private static final long serialVersionUID = 1L;

    @TableId(value = "id", type = IdType.AUTO)
    private Integer id;

    @ApiModelProperty("文件类型")
    @Excel(name = "文件类型")
    private Integer fileType;

    @ApiModelProperty("业务类型")
    @Excel(name = "业务类型")
    private BusinessTaskType busiType;

    @ApiModelProperty("业务id")
    @Excel(name = "业务id")
    private String connectId;

    @ApiModelProperty("路径")
    @Excel(name = "路径")
    private String path;

    @ApiModelProperty("文件名")
    @Excel(name = "文件名")
    private String name;

    @ApiModelProperty("备注")
    @Excel(name = "备注")
    private String remark;

    @ApiModelProperty("文件链接")
    @Excel(name = "文件链接")
    private String url;

    @ApiModelProperty(value = "创建时间", hidden = true)
    @TableField(fill = FieldFill.INSERT)
    private LocalDateTime createTime;

    @ApiModelProperty(value = "修改时间", hidden = true)
    @TableField(fill = FieldFill.INSERT_UPDATE)
    private LocalDateTime updateTime;

    @ApiModelProperty(value = "创建人员", hidden = true)
    @TableField(fill = FieldFill.INSERT)
    private String createBy;

    @ApiModelProperty(value = "修改人员", hidden = true)
    @TableField(fill = FieldFill.INSERT_UPDATE)
    private String modifyBy;
}
