// 环评情况表
import request from '@/utils/request'

export function listReport(query) {
  return request({
    url: '/security/corpEpEia/list',
    method: 'get',
    params: query
  })
}

export function getReport(id) {
  return request({
    url: '/security/corpEpEia/' + id,
    method: 'get'
  })
}

export function addReport(data) {
  return request({
    url: '/security/corpEpEia',
    method: 'post',
    data: data
  })
}

export function updateReport(data) {
  return request({
    url: '/security/corpEpEia',
    method: 'put',
    data: data
  })
}

export function delReport(id) {
  return request({
    url: '/security/corpEpEia/' + id,
    method: 'delete'
  })
}

export function importReport(data) {
  return request({
    url: '/security/corpEpEia/importData',
    method: 'post',
    data: data
  })
}
