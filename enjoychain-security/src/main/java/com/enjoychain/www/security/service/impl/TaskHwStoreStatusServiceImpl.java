package com.enjoychain.www.security.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.enjoychain.www.security.constant.BusinessTaskType;
import com.enjoychain.www.security.entity.TaskHwStoreStatus;
import com.enjoychain.www.security.mapper.TaskHwStoreStatusMapper;
import com.enjoychain.www.security.service.IBasicFileService;
import com.enjoychain.www.security.service.ITaskHwStoreStatusService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author ddmonk
 * @since 2023-02-24
 */
@Service
public class TaskHwStoreStatusServiceImpl extends ServiceImpl<TaskHwStoreStatusMapper, TaskHwStoreStatus> implements ITaskHwStoreStatusService {

    @Autowired
    IBasicFileService fileService;

    private QueryWrapper<TaskHwStoreStatus> buildQueryWrapperWithTaskId(String taskId){
        QueryWrapper<TaskHwStoreStatus> queryWrapper = new QueryWrapper<>();
        queryWrapper.eq("task_id",taskId);
        return queryWrapper;
    }

    List<String> targets = new ArrayList<String>(){{
        add("危废仓库要独立、密闭，上锁防盗，仓库内要有安全照明设施和观察窗口，必要时安装监控，危废仓库管理责任制要上墙。");
        add("仓库地面要防渗，顶部防水、防晒；地面与裙脚要用坚固、防渗的材料建造，建筑材料必须与危险废物相容，门口要设置围堰。");
        add("存放危废为液体的仓库内必须有泄漏液体收集装置（例如托盘、导流沟、收集池），存放危废为具有挥发性气体的仓库内必须有导出口及气体净化装置。");
        add("仓库门上要张贴包含所有危废的标识、标牌，仓库内对应墙上有标志标识，无法装入常用容器的危险废物可用防漏胶袋等盛装，包装桶、袋上有标签。");
        add("危废和一般固废不能混存，不同危废分开存放并设置隔断隔离。");
        add("仓库现场要有危废产生台账和转移联单，联单保存期限为五年。贮存危险废物的，其联单保存期限与危险废物贮存期限相同。");
        add("装载液体、半固体危险废物的容器内须留足够空间，容器顶部与液体表面之间保留100毫米以上的空间。用以存放装载液体、半固体危险废物容器的地方，必须有耐腐蚀的硬化地面，且表面无裂隙。");
        add("及时通过企业网站等途径依法公开当年危险废物污染防治信息。");
    }};


    @Override
    public List<TaskHwStoreStatus> initial(String taskId) {
        int sort = 1;
        for (String target: targets){
            TaskHwStoreStatus hwStoreStatus = new TaskHwStoreStatus();
            hwStoreStatus.setTaskId(taskId);
            hwStoreStatus.setCheckPoint(target);
            hwStoreStatus.setSortFlag(sort);
            sort++;
            save(hwStoreStatus);
        }
        return list(buildQueryWrapperWithTaskId(taskId));
    }

    @Override
    public List<TaskHwStoreStatus> listByTaskId(String taskId) {
        List<TaskHwStoreStatus> items = list(buildQueryWithTaskId(taskId));
        for (TaskHwStoreStatus item : items){
            item.setFiles(fileService.getListByTypeAndId(BusinessTaskType.ENVIRONMENT,item.getId()));
        }
        return items;
    }

    @Override
    public void removeDetailByTaskId(String taskId) {
        List<TaskHwStoreStatus> items = list(buildQueryWithTaskId(taskId));
        for (TaskHwStoreStatus item : items){
            fileService.removeFileByConnectIdAndBusiType(BusinessTaskType.ENVIRONMENT,item.getId());
            removeById(item);
        }
    }
}
