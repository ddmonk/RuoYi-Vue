package com.enjoychain.www.security.entity;

import com.baomidou.mybatisplus.annotation.TableName;
import java.io.Serializable;
import com.ruoyi.common.annotation.Excel;
import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;
import lombok.experimental.Accessors;

/**
 * <p>
 * 
 * </p>
 *
 * @author ddmonk
 * @since 2023-03-07
 */
@Getter
@Setter
@Accessors(chain = true)
@TableName("task_ep_gas_equipment")
@ApiModel(value = "TaskEpGasEquipment对象", description = "")
public class TaskEpGasEquipment implements Serializable {

    private static final long serialVersionUID = 1L;

    @ApiModelProperty("主键")
    @Excel(name = "主键")
    private String id;

    @ApiModelProperty("任务id")
    @Excel(name = "任务id")
    private String taskId;

    @ApiModelProperty("公司id")
    @Excel(name = "公司id")
    private String corpId;

    @ApiModelProperty("生产工段")
    @Excel(name = "生产工段")
    private String productSection;

    @ApiModelProperty("处置设备名称")
    @Excel(name = "处置设备名称")
    private String handleEquipName;

    @ApiModelProperty("排气筒编号")
    @Excel(name = "排气筒编号")
    private String exhaustFunnelNo;

    @ApiModelProperty("污染因子")
    @Excel(name = "污染因子")
    private String pollutionFactor;

    @ApiModelProperty("状态")
    @Excel(name = "状态")
    private Integer status;

    @ApiModelProperty("类型")
    @Excel(name = "类型")
    private String type;

    @ApiModelProperty("废气排放情况")
    @Excel(name = "废气排放情况")
    private String dischargeStatus;

    @ApiModelProperty("备注")
    @Excel(name = "备注")
    private String remark;


}
