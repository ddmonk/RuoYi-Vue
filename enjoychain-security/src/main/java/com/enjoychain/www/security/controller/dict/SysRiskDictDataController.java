package com.enjoychain.www.security.controller.dict;

import org.springframework.web.bind.annotation.RequestMapping;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.common.utils.StringUtils;
import com.ruoyi.common.utils.poi.ExcelUtil;
import io.swagger.annotations.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import javax.servlet.http.HttpServletResponse;
import java.util.List;
import java.util.UUID;
import java.util.Arrays;
import com.enjoychain.www.security.entity.SysRiskDictData;
import com.enjoychain.www.security.service.ISysRiskDictDataService;
import org.springframework.web.bind.annotation.RestController;

/**
 * <p>
 * 较大风险目录字典 前端控制器
 * </p>
 *
 * @author ddmonk
 * @since 2023-04-11
 */
@Api(tags = "较大风险目录字典")
@RestController
@RequestMapping("/security/sysRiskDictData")
public class SysRiskDictDataController {

    @Autowired
    ISysRiskDictDataService service;

    @PreAuthorize("@ss.hasPermi('security:risk_dict:list')")
    @GetMapping(path = "/list", produces = {"application/json"})
    @ApiOperation(value = "获取较大风险目录字典列表")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "pageNum", value = "页码", paramType = "query", dataType = "long"),
            @ApiImplicitParam(name = "pageSize", value = "页面记录条数", paramType = "query", dataType = "long")
    })
    @ApiResponses({
        @ApiResponse(code = 200, message = "ok", response= SysRiskDictData.class),
    })
    public AjaxResult list(
                            @RequestParam(name = "keyword", required = false) String keyword,
                            @RequestParam(name = "managerType", required = false) String managerType,
                            @RequestParam(name = "riskCode", required = false) String riskCode,
                            @RequestParam(name = "riskName", required = false) String riskName,
                            @RequestParam(name = "accType", required = false) String accType,
                            @RequestParam(name = "riskPoint", required = false) String riskPoint,
                            @RequestParam(name = "status", required = false) String status,
                            @RequestParam(name = "pageNum", defaultValue = "1") long pageNo,
                            @RequestParam(name = "pageSize",defaultValue = "10") long pageSize){
        IPage<SysRiskDictData> entityIPage = new Page<>(pageNo, pageSize);
        QueryWrapper<SysRiskDictData> queryWrapper = new QueryWrapper<>();
        if (StringUtils.isNotEmpty(managerType)) {
            queryWrapper.like("manager_type",managerType);
        }
        if (StringUtils.isNotEmpty(riskCode)) {
            queryWrapper.like("risk_code",riskCode);
        }
        if (StringUtils.isNotEmpty(riskName)) {
            queryWrapper.like("risk_name",riskName);
        }
        if (StringUtils.isNotEmpty(accType)) {
            queryWrapper.like("acc_type",accType);
        }
        if (StringUtils.isNotEmpty(riskPoint)) {
            queryWrapper.like("risk_point",riskPoint);
        }
        if (StringUtils.isNotEmpty(status)) {
            queryWrapper.eq("status",status);
        }
        if (StringUtils.isNotEmpty(keyword)){
            queryWrapper.like("risk_code",keyword).or().like("risk_name",keyword);
        }
        return AjaxResult.success("list-sys_risk_dict_data",service.page(entityIPage,queryWrapper));
    }

    @PreAuthorize("@ss.hasPermi('security:risk_dict:query')")
    @GetMapping(value = "{id}")
    @ApiOperation("获取较大风险目录字典详情")
    @ApiImplicitParams({
        @ApiImplicitParam(name = "id", value = "id",paramType = "path", dataType = "String")
    })
    @ApiResponses({
        @ApiResponse(code = 200, message = "ok", response = SysRiskDictData.class)
    })
    public AjaxResult getSysRiskDictDataInfo(@PathVariable(value = "id", required = false) String id)
    {
        return AjaxResult.success("get-sys_risk_dict_data",service.getById(id));
    }

    @PreAuthorize("@ss.hasPermi('security:risk_dict:import')")
    @ApiOperation("导入较大风险目录字典")
    @Log(title = "较大风险目录字典", businessType = BusinessType.IMPORT)
    @PostMapping("/importData")
    public AjaxResult importData(MultipartFile file) throws Exception{
        ExcelUtil<SysRiskDictData> util = new ExcelUtil<SysRiskDictData>(SysRiskDictData.class);
        List<SysRiskDictData> infos = util.importExcel(file.getInputStream());
        return service.saveOrUpdateBatch(infos)?AjaxResult.success("批量插入成功"):AjaxResult.error("批量插入失败");
    }


    @ApiOperation("较大风险目录字典导出模板下载")
    @PostMapping("/importTemplate")
    public void importTemplate(HttpServletResponse response){
        ExcelUtil<SysRiskDictData> util = new ExcelUtil<SysRiskDictData>(SysRiskDictData.class);
        util.importTemplateExcel(response,"较大风险目录字典");
    }

    @PreAuthorize("@ss.hasPermi('security:risk_dict:add')")
    @PostMapping(consumes = {"application/json"}, produces = {"application/json"})
    @ApiOperation("新增较大风险目录字典")
    @ApiResponses({
        @ApiResponse(code = 200, message = "ok", response = SysRiskDictData.class),
    })
    @Log(title = "较大风险目录字典", businessType = BusinessType.INSERT)
    public AjaxResult add(@RequestBody SysRiskDictData entity){
        return service.save(entity)? AjaxResult.success("add-sys_risk_dict_data",entity): AjaxResult.error("新增较大风险目录字典失败");
    }


    @PreAuthorize("@ss.hasPermi('security:risk_dict:edit')")
    @PutMapping(consumes = {"application/json"}, produces = {"application/json"})
    @ApiOperation("修改较大风险目录字典")
    @ApiResponses({
        @ApiResponse(code = 200, message = "ok", response = SysRiskDictData.class)
    })
    @Log(title = "较大风险目录字典", businessType = BusinessType.UPDATE)
    public AjaxResult update(@RequestBody SysRiskDictData corpBasicInfo){
        String operate = "update-sys_risk_dict_data";
        return service.updateById(corpBasicInfo)?AjaxResult.success(operate,service.getById(corpBasicInfo.getId())) : AjaxResult.error("修改客户基础信息失败");
    }

    @PreAuthorize("@ss.hasPermi('security:risk_dict:remove')")
    @DeleteMapping(value = "{id}", produces = {"application/json"})
    @ApiOperation("删除较大风险目录字典")
    @ApiResponses({
        @ApiResponse(code = 200, message = "ok", response = AjaxResult.class),
    })
    @Log(title = "较大风险目录字典", businessType = BusinessType.DELETE)
    public AjaxResult delete(@PathVariable(value = "id", required = false) String id){
        String operate = "delete-sys_risk_dict_data";
        String[] ids = id.split(",");
        return service.removeBatchByIds(Arrays.asList(ids))?AjaxResult.success(operate) : AjaxResult.error("修改较大风险目录字典失败");
    }


    @Log(title = "较大风险目录字典", businessType = BusinessType.EXPORT)
    @PreAuthorize("@ss.hasPermi('security:risk_dict:export')")
    @PostMapping("/export")
    @ApiOperation("导出较大风险目录字典")
    @ApiResponses({
        @ApiResponse(code = 200, message = "ok", response = AjaxResult.class),
    })
    public void export(HttpServletResponse response,@RequestParam(name = "corpId", required = false) String corpId){
        QueryWrapper<SysRiskDictData> queryWrapper = new QueryWrapper<>();
        if (StringUtils.isNotEmpty(corpId)) {
            queryWrapper.eq("corp_id",corpId);
        }
        List<SysRiskDictData> list = service.list(queryWrapper);
        ExcelUtil<SysRiskDictData> util = new ExcelUtil<SysRiskDictData>(SysRiskDictData.class);
        util.exportExcel(response,list, "导出较大风险目录字典");
    }
}
