package com.enjoychain.www.security.service.impl;

import com.enjoychain.www.security.entity.CorpEpWasteWater;
import com.enjoychain.www.security.mapper.CorpEpWasteWaterMapper;
import com.enjoychain.www.security.service.ICorpEpWasteWaterService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 企业废水情况 服务实现类
 * </p>
 *
 * @author ddmonk
 * @since 2023-01-13
 */
@Service
public class CorpEpWasteWaterServiceImpl extends ServiceImpl<CorpEpWasteWaterMapper, CorpEpWasteWater> implements ICorpEpWasteWaterService {

}
