package com.enjoychain.www.security.service;

import com.enjoychain.www.security.entity.CorpEpSolidWaste;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 * 一般固废生产情况 服务类
 * </p>
 *
 * @author ddmonk
 * @since 2023-01-13
 */
public interface ICorpEpSolidWasteService extends IService<CorpEpSolidWaste> {

}
