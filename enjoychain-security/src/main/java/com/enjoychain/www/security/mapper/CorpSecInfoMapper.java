package com.enjoychain.www.security.mapper;

import com.enjoychain.www.security.entity.CorpSecInfo;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * 企业安全生产基础信息表 Mapper 接口
 * </p>
 *
 * @author ddmonk
 * @since 2023-01-13
 */
public interface CorpSecInfoMapper extends BaseMapper<CorpSecInfo> {

}
