package com.enjoychain.www.security.mapper;

import com.enjoychain.www.security.entity.SysRiskDictData;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * 较大风险目录字典 Mapper 接口
 * </p>
 *
 * @author ddmonk
 * @since 2023-04-11
 */
public interface SysRiskDictDataMapper extends BaseMapper<SysRiskDictData> {

}
