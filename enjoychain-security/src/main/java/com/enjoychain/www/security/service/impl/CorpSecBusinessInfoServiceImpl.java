package com.enjoychain.www.security.service.impl;

import com.enjoychain.www.security.entity.CorpSecBusinessInfo;
import com.enjoychain.www.security.mapper.CorpSecBusinessInfoMapper;
import com.enjoychain.www.security.service.ICorpSecBusinessInfoService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 安全企业行业类型信息 服务实现类
 * </p>
 *
 * @author ddmonk
 * @since 2023-01-13
 */
@Service
public class CorpSecBusinessInfoServiceImpl extends ServiceImpl<CorpSecBusinessInfoMapper, CorpSecBusinessInfo> implements ICorpSecBusinessInfoService {

}
