package com.enjoychain.www.security.mapper;

import com.enjoychain.www.security.entity.CorpEpUnloading;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * 排污许可证申领情况 Mapper 接口
 * </p>
 *
 * @author ddmonk
 * @since 2023-01-13
 */
public interface CorpEpUnloadingMapper extends BaseMapper<CorpEpUnloading> {

}
