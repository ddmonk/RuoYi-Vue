package com.enjoychain.www.security.service;

import com.enjoychain.www.security.entity.TaskMergencyInfo;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author ddmonk
 * @since 2023-02-24
 */
public interface ITaskMergencyInfoService extends ITaskService<TaskMergencyInfo> {

}
