package com.enjoychain.www.security.service;

import com.enjoychain.www.security.entity.SysRiskDictData;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 * 较大风险目录字典 服务类
 * </p>
 *
 * @author ddmonk
 * @since 2023-04-11
 */
public interface ISysRiskDictDataService extends IService<SysRiskDictData> {

}
