package com.enjoychain.www.security.service.impl;

import com.enjoychain.www.security.entity.NeedInfo;
import com.enjoychain.www.security.mapper.NeedInfoMapper;
import com.enjoychain.www.security.service.INeedInfoService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 用户需求信息 服务实现类
 * </p>
 *
 * @author ddmonk
 * @since 2023-01-29
 */
@Service
public class NeedInfoServiceImpl extends ServiceImpl<NeedInfoMapper, NeedInfo> implements INeedInfoService {

}
