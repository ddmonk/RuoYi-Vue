package com.enjoychain.www.security.service.impl;

import com.enjoychain.www.security.entity.TaskEpEia;
import com.enjoychain.www.security.mapper.TaskEpEiaMapper;
import com.enjoychain.www.security.service.ITaskEpEiaService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author ddmonk
 * @since 2023-03-07
 */
@Service
public class TaskEpEiaServiceImpl extends ServiceImpl<TaskEpEiaMapper, TaskEpEia> implements ITaskEpEiaService {

}
