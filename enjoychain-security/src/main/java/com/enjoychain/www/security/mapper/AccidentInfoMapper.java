package com.enjoychain.www.security.mapper;

import com.enjoychain.www.security.entity.accident.AccidentInfo;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * 问题基础表 Mapper 接口
 * </p>
 *
 * @author ddmonk
 * @since 2023-01-29
 */
public interface AccidentInfoMapper extends BaseMapper<AccidentInfo> {

}
