package com.enjoychain.www.security.mapper;

import com.enjoychain.www.security.entity.CorpSecCorpBasic;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * 安全生产基础信息 Mapper 接口
 * </p>
 *
 * @author ddmonk
 * @since 2023-04-14
 */
public interface CorpSecCorpBasicMapper extends BaseMapper<CorpSecCorpBasic> {

}
