// 应急预案情况
import request from '@/utils/request'

export function listEmergency(query) {
  return request({
    url: '/security/corpEpMergencyPlan/list',
    method: 'get',
    params: query
  })
}

export function getEmergency(id) {
  return request({
    url: '/security/corpEpMergencyPlan/' + id,
    method: 'get'
  })
}

export function addEmergency(data) {
  return request({
    url: '/security/corpEpMergencyPlan',
    method: 'post',
    data: data
  })
}

export function updateEmergency(data) {
  return request({
    url: '/security/corpEpMergencyPlan',
    method: 'put',
    data: data
  })
}

export function delEmergency(id) {
  return request({
    url: '/security/corpEpMergencyPlan/' + id,
    method: 'delete'
  })
}

export function importEmergency(data) {
  return request({
    url: '/security/corpEpMergencyPlan/importData',
    method: 'post',
    data: data
  })
}
