package com.enjoychain.www.security.controller.basic;

import com.enjoychain.www.security.service.IDashBoardService;
import com.ruoyi.common.core.domain.AjaxResult;
import io.swagger.annotations.Api;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@Api(tags = "仪表盘接口")
@RestController
@RequestMapping("/security/dashboard")
@Slf4j
public class DashBoardController {

    @Autowired
    IDashBoardService dashBoardService;

    @GetMapping("/panel")
    public AjaxResult getTotalStatic(){
        return AjaxResult.success().put("statistic",dashBoardService.getPanel());
    }


}
