package com.enjoychain.www.security.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.enjoychain.www.security.entity.XcBasic;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author ddmonk
 * @since 2020-12-03
 */
public interface XcBasicMapper extends BaseMapper<XcBasic> {

}
