package com.enjoychain.www.security.mapper;

import com.enjoychain.www.security.entity.CorpEpWaterMonitor;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * 土壤和地下水自行监测情况 Mapper 接口
 * </p>
 *
 * @author ddmonk
 * @since 2023-01-13
 */
public interface CorpEpWaterMonitorMapper extends BaseMapper<CorpEpWaterMonitor> {

}
