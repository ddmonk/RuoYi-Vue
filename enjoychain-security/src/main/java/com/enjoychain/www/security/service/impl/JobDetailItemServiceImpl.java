package com.enjoychain.www.security.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.enjoychain.www.security.entity.job.JobDetailItem;
import com.enjoychain.www.security.mapper.JobDetailItemMapper;
import com.enjoychain.www.security.service.IJobDetailItemService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * <p>
 * 作业考核详情内容 服务实现类
 * </p>
 *
 * @author ddmonk
 * @since 2023-01-29
 */
@Service
public class JobDetailItemServiceImpl extends ServiceImpl<JobDetailItemMapper, JobDetailItem> implements IJobDetailItemService {


    private QueryWrapper<JobDetailItem> buildQueryWrapper(String jobId){
        QueryWrapper<JobDetailItem> queryWrapper = new QueryWrapper<>();
        queryWrapper.eq("job_id",jobId);
        queryWrapper.orderByAsc("item_sort");
        return queryWrapper;
    }

    @Override
    public List<JobDetailItem> getJobItems(String jobId) {
        return list(buildQueryWrapper(jobId));
    }
}
