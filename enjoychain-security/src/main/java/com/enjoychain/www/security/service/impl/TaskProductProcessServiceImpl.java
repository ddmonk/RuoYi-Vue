package com.enjoychain.www.security.service.impl;

import com.enjoychain.www.security.entity.TaskProductProcess;
import com.enjoychain.www.security.mapper.TaskProductProcessMapper;
import com.enjoychain.www.security.service.ITaskProductProcessService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 生产工艺 服务实现类
 * </p>
 *
 * @author ddmonk
 * @since 2023-02-24
 */
@Service
public class TaskProductProcessServiceImpl extends ServiceImpl<TaskProductProcessMapper, TaskProductProcess> implements ITaskProductProcessService {

}
