package com.enjoychain.www.security.mapper;

import com.enjoychain.www.security.entity.CorpEpBasic;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * 环保业务基础信息 Mapper 接口
 * </p>
 *
 * @author ddmonk
 * @since 2023-01-13
 */
public interface CorpEpBasicMapper extends BaseMapper<CorpEpBasic> {

}
