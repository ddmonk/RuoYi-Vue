package com.enjoychain.www.security.pojo;

import lombok.Data;

import java.util.ArrayList;
import java.util.List;

@Data
public class TableMeta {

    private String tableName;

    private List<String> fields;

    public void setField(String field) {
        if (fields == null ){
            fields = new ArrayList<>();
        }
        this.fields.add(field);
    }
}
