package com.enjoychain.www.security.service;

import com.enjoychain.www.security.entity.BasicDictHw;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 * 危险废物名录字典 服务类
 * </p>
 *
 * @author ddmonk
 * @since 2023-03-08
 */
public interface IBasicDictHwService extends IService<BasicDictHw> {

}
