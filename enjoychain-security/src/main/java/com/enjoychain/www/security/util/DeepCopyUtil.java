package com.enjoychain.www.security.util;

import org.apache.commons.lang3.SerializationUtils;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class DeepCopyUtil<T extends Serializable> {

    private static DeepCopyUtil instance = new DeepCopyUtil();

    //让构造函数为 private，这样该类就不会被实例化
    private DeepCopyUtil(){}

    //获取唯一可用的对象
    public static DeepCopyUtil getInstance(){
        return instance;
    }

    public List<T> deepCopy(List<T> sources){
        List<T> aRes = new ArrayList<>();
        for (T source : sources){
            aRes.add(SerializationUtils.clone(source));
        }
        return aRes;
    }
}
