package com.enjoychain.www.security.pojo;

import com.deepoove.poi.data.PictureRenderData;
import lombok.Data;

import java.util.List;
import java.util.Map;

@Data
public class PicPojo {

    private String desc;

    private List<Map<String,Object>> pics;

}
