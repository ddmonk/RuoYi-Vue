import request from '@/utils/request'

// 查询【请填写功能名称】列表
export function listItem(query) {
  return request({
    url: '/security/accidentDetailItem/list',
    method: 'get',
    params: query
  })
}

// 查询【请填写功能名称】详细
export function getItem(id) {
  return request({
    url: '/security/accidentDetailItem/' + id,
    method: 'get'
  })
}

// 新增【请填写功能名称】
export function addItem(data) {
  return request({
    url: '/security/accidentDetailItem',
    method: 'post',
    data: data
  })
}

// 修改【请填写功能名称】
export function updateItem(data) {
  return request({
    url: '/security/accidentDetailItem',
    method: 'put',
    data: data
  })
}

// 删除【请填写功能名称】
export function delItem(id) {
  return request({
    url: '/security/accidentDetailItem/' + id,
    method: 'delete'
  })
}

export function listRelation(query) {
  return request({
    url: '/security/accidentDetailItem/relation/list',
    method: 'get',
    params: query
  })
}

export function delRelation(id) {
  return request({
    url: '/security/accidentDetailItem/relation/' + id,
    method: 'delete'
  })
}

// 新增【请填写功能名称】
export function addRelation(data) {
  return request({
    url: '/security/accidentDetailItem/relation',
    method: 'post',
    data: data
  })
}
