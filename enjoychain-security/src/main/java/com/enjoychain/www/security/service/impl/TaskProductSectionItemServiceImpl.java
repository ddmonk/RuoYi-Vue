package com.enjoychain.www.security.service.impl;

import com.enjoychain.www.security.constant.BusinessTaskType;
import com.enjoychain.www.security.entity.TaskProductSectionItem;
import com.enjoychain.www.security.mapper.TaskProductSectionItemMapper;
import com.enjoychain.www.security.service.IBasicFileService;
import com.enjoychain.www.security.service.ITaskProductSectionItemService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * <p>
 * 任务_工段明细 服务实现类
 * </p>
 *
 * @author ddmonk
 * @since 2023-02-24
 */
@Service
public class TaskProductSectionItemServiceImpl extends ServiceImpl<TaskProductSectionItemMapper, TaskProductSectionItem> implements ITaskProductSectionItemService {

    @Autowired
    IBasicFileService fileService;

    @Override
    public List<TaskProductSectionItem> listByTaskId(String id) {
        List<TaskProductSectionItem> items = list(buildQueryWithTaskId(id));
        for (TaskProductSectionItem item : items){
            item.setFiles(fileService.getListByTypeAndId(BusinessTaskType.ENVIRONMENT,item.getId()));
        }
        return items;
    }

    @Override
    public void removeDetailByTaskId(String taskId) {
        List<TaskProductSectionItem> items = list(buildQueryWithTaskId(taskId));
        for (TaskProductSectionItem item : items){
            fileService.removeFileByConnectIdAndBusiType(BusinessTaskType.ENVIRONMENT,item.getId());
            removeById(item);
        }
    }
}
