package com.enjoychain.www.security.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.enjoychain.www.security.constant.BusinessTaskType;
import com.enjoychain.www.security.entity.TaskEquipItem;
import com.enjoychain.www.security.mapper.TaskEquipItemMapper;
import com.enjoychain.www.security.service.IBasicFileService;
import com.enjoychain.www.security.service.ITaskEquipItemService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * <p>
 * 巡查任务—设备情况 服务实现类
 * </p>
 *
 * @author ddmonk
 * @since 2023-02-24
 */
@Service
public class TaskEquipItemServiceImpl extends ServiceImpl<TaskEquipItemMapper, TaskEquipItem> implements ITaskEquipItemService {

    @Autowired
    IBasicFileService fileService;

    private QueryWrapper<TaskEquipItem> buildQueryWrapper(String taskId){
        QueryWrapper<TaskEquipItem> queryWrapper = new QueryWrapper<>();
        queryWrapper.eq("task_id",taskId);
        return queryWrapper;
    }

    @Override
    public List<TaskEquipItem> listByTaskId(String id) {
        List<TaskEquipItem> items = list(buildQueryWrapper(id));
        for (TaskEquipItem item : items){
            item.setFiles(fileService.getListByTypeAndId(BusinessTaskType.ENVIRONMENT,item.getId()));
        }
        return items;
    }

    @Override
    public void removeAllByTaskId(String taskId) {
        List<TaskEquipItem> items = list(buildQueryWrapper(taskId));
        for (TaskEquipItem item : items){
            fileService.removeFileByConnectIdAndBusiType(BusinessTaskType.ENVIRONMENT,item.getId());
            removeById(item);
        }
    }
}
