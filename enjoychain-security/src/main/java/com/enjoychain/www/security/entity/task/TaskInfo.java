package com.enjoychain.www.security.entity.task;

import com.baomidou.mybatisplus.annotation.FieldFill;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableName;
import java.io.Serializable;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.List;

import com.enjoychain.www.common.constrants.TaskStatus;
import com.ruoyi.common.annotation.Excel;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;
import lombok.experimental.Accessors;

import static com.enjoychain.www.security.constant.Constant.DATE_FORMATTER;
import static com.enjoychain.www.security.constant.Constant.REPORT_DEFAULT_VALUE;

/**
 * <p>
 * 任务基础信息表
 * </p>
 *
 * @author ddmonk
 * @since 2023-01-20
 */
@Getter
@Setter
@Accessors(chain = true)
@TableName("task_info")
@ApiModel(value = "TaskInfo对象", description = "任务基础信息表")
public class TaskInfo implements Serializable {

    private static final long serialVersionUID = 1L;

    @ApiModelProperty("主键")
    @Excel(name = "主键")
    private String id;

    @ApiModelProperty("任务名称")
    @Excel(name = "任务名称")
    private String name;

    @ApiModelProperty("任务编号")
    @Excel(name = "任务编号")
    private String taskNo;

    @ApiModelProperty("公司id")
    @Excel(name = "公司id")
    private String corpId;

    @ApiModelProperty("公司名称")
    @Excel(name = "公司名称")
    private String corpName;

    @ApiModelProperty(value = "创建时间", hidden = true)
    @TableField(fill = FieldFill.INSERT)
    private LocalDateTime createTime;

    @ApiModelProperty(value = "更新时间", hidden = true)
    @TableField(fill = FieldFill.INSERT_UPDATE)
    private LocalDateTime updateTime;

    @ApiModelProperty("任务开始时间")
    @Excel(name = "任务开始时间", width = 30, dateFormat = "yyyy-MM-dd")
    private LocalDateTime startTime;

    public void setStartTime(LocalDateTime startTime) {
        this.startTime = startTime;
        this.startTimeString = this.startTime == null? REPORT_DEFAULT_VALUE:this.startTime.format(DateTimeFormatter.ofPattern(DATE_FORMATTER));
    }

    @TableField(exist = false)
    private String startTimeString;

    @ApiModelProperty("任务结束时间")
    @Excel(name = "任务结束时间", width = 30, dateFormat = "yyyy-MM-dd")
    private LocalDateTime endTime;

    @ApiModelProperty("任务状态")
    @Excel(name = "任务状态")

    private TaskStatus status;

    @ApiModelProperty("任务处理人id")
    @Excel(name = "任务处理人id")
    private String handlerId;

    @ApiModelProperty("业务类型")
    @Excel(name = "业务类型")
    private String type;

    @ApiModelProperty(value = "创建人员", hidden = true)
    @TableField(fill = FieldFill.INSERT)
    private String createBy;

    @ApiModelProperty(value = "修改人员", hidden = true)
    @TableField(fill = FieldFill.INSERT_UPDATE)
    private String modifyBy;

    @ApiModelProperty("任务处理人")
    @Excel(name = "任务处理人")
    private String handlerName;

    @ApiModelProperty("区域位置")
    @Excel(name = "区域位置")
    private String position;

    @ApiModelProperty("纬度")
    @Excel(name = "纬度")
    private String latitude;

    @ApiModelProperty("经度")
    @Excel(name = "经度")
    private String longitude;

    @ApiModelProperty("考核内容")
    @TableField(exist = false)
    private List<TaskDetailItem> itemList;

    @ApiModelProperty("巡查小结")
    @Excel(name = "巡查小结")
    private String conclusion;


    @ApiModelProperty("本年度巡查次数")
    @Excel(name = "本年度巡查次数")
    private String num;
}
