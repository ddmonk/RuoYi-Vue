package com.enjoychain.www.security.service;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.enjoychain.www.security.entity.CorpSecBigRiskInfo;
import com.baomidou.mybatisplus.extension.service.IService;

import java.util.List;

/**
 * <p>
 * 较大以上安全风险信息 服务类
 * </p>
 *
 * @author ddmonk
 * @since 2023-01-13
 */
public interface ICorpSecBigRiskInfoService extends IService<CorpSecBigRiskInfo> {



    boolean removeByCorpId(String corpId);

    List<CorpSecBigRiskInfo> listByCorpId(String corpId);

}
