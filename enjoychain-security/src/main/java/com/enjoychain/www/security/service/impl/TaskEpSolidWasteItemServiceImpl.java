package com.enjoychain.www.security.service.impl;

import com.enjoychain.www.security.entity.TaskEpSolidWasteItem;
import com.enjoychain.www.security.mapper.TaskEpSolidWasteItemMapper;
import com.enjoychain.www.security.service.ITaskEpSolidWasteItemService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author ddmonk
 * @since 2023-03-08
 */
@Service
public class TaskEpSolidWasteItemServiceImpl extends ServiceImpl<TaskEpSolidWasteItemMapper, TaskEpSolidWasteItem> implements ITaskEpSolidWasteItemService {

}
