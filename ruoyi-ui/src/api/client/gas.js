// 废气处理设备情况
import request from '@/utils/request'

export function listGas(query) {
  return request({
    url: '/security/corpEpGasEquipment/list',
    method: 'get',
    params: query
  })
}

export function getGas(id) {
  return request({
    url: '/security/corpEpGasEquipment/' + id,
    method: 'get'
  })
}

export function addGas(data) {
  return request({
    url: '/security/corpEpGasEquipment',
    method: 'post',
    data: data
  })
}

export function updateGas(data) {
  return request({
    url: '/security/corpEpGasEquipment',
    method: 'put',
    data: data
  })
}

export function delGas(id) {
  return request({
    url: '/security/corpEpGasEquipment/' + id,
    method: 'delete'
  })
}

export function importGas(data) {
  return request({
    url: '/security/corpEpGasEquipment/importData',
    method: 'post',
    data: data
  })
}
