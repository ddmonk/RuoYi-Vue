package com.enjoychain.www.security.service;

import com.enjoychain.www.security.entity.accident.AccidentInfo;
import com.baomidou.mybatisplus.extension.service.IService;
import com.enjoychain.www.security.entity.task.TaskDetailItem;

import java.io.IOException;

/**
 * <p>
 * 问题基础表 服务类
 * </p>
 *
 * @author ddmonk
 * @since 2023-01-29
 */
public interface IAccidentInfoService extends IService<AccidentInfo> {

    AccidentInfo getDetailByAccId(String id);

    boolean saveByTaskDetailItem(TaskDetailItem item) throws IOException;
}
