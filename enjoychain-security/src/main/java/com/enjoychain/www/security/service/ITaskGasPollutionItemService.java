package com.enjoychain.www.security.service;

import com.enjoychain.www.security.entity.CorpEpGasEquipment;
import com.enjoychain.www.security.entity.TaskEpGasEquipment;
import com.enjoychain.www.security.entity.TaskGasPollutionItem;
import com.baomidou.mybatisplus.extension.service.IService;

import java.util.List;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author ddmonk
 * @since 2023-02-24
 */
public interface ITaskGasPollutionItemService extends ITaskService<TaskGasPollutionItem> {

    List<TaskGasPollutionItem> listByTaskId(String id);

    void initial(TaskEpGasEquipment epGasEquipment, String id);

    void removeDetailByTaskId(String taskId);
}
