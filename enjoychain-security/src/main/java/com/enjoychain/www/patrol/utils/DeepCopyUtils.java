package com.enjoychain.www.patrol.utils;


import java.io.*;

public class DeepCopyUtils {
    public static <T,S> S deepCopy(T src) {
        try {
            ByteArrayOutputStream baos = new ByteArrayOutputStream();
            ObjectOutputStream oos = new ObjectOutputStream(baos);
            oos.writeObject(src);

            ByteArrayInputStream bais = new ByteArrayInputStream(baos.toByteArray());
            ObjectInputStream ois = new ObjectInputStream(bais);
            return (S) ois.readObject();

        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

}
