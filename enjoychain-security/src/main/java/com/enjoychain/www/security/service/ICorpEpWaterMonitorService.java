package com.enjoychain.www.security.service;

import com.enjoychain.www.security.entity.CorpEpWaterMonitor;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 * 土壤和地下水自行监测情况 服务类
 * </p>
 *
 * @author ddmonk
 * @since 2023-01-13
 */
public interface ICorpEpWaterMonitorService extends IService<CorpEpWaterMonitor> {

}
