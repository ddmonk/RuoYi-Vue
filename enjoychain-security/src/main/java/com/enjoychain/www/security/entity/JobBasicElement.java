package com.enjoychain.www.security.entity;

import com.baomidou.mybatisplus.annotation.*;

import java.io.Serializable;
import java.time.LocalDateTime;
import com.ruoyi.common.annotation.Excel;
import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;
import lombok.experimental.Accessors;

/**
 * <p>
 * 核查任务要素信息
 * </p>
 *
 * @author ddmonk
 * @since 2023-01-30
 */
@Getter
@Setter
@Accessors(chain = true)
@TableName("job_basic_element")
@ApiModel(value = "JobBasicElement对象", description = "核查任务要素信息")
public class JobBasicElement implements Serializable {

    private static final long serialVersionUID = 1L;

    @ApiModelProperty("主键")
    @Excel(name = "主键")
    @TableId(type = IdType.ASSIGN_ID)
    private String id;

    @ApiModelProperty("名称")
    @Excel(name = "名称")
    private String name;

    @ApiModelProperty("类型")
    @Excel(name = "类型")
    private Integer type;

    @ApiModelProperty("任务类型")
    @Excel(name = "任务类型")
    private String busiType;

    @ApiModelProperty("备注")
    @Excel(name = "备注")
    private String remark;

    @ApiModelProperty(value = "创建时间", hidden = true)
    @TableField(fill = FieldFill.INSERT)
    private LocalDateTime createTime;

    @ApiModelProperty(value = "修改时间", hidden = true)
    @TableField(fill = FieldFill.INSERT_UPDATE)
    private LocalDateTime updateTime;

    @ApiModelProperty(value = "创建人员", hidden = true)
    @TableField(fill = FieldFill.INSERT)
    private String createBy;

    @ApiModelProperty(value = "修改人员", hidden = true)
    @TableField(fill = FieldFill.INSERT_UPDATE)
    private String modifyBy;


}
