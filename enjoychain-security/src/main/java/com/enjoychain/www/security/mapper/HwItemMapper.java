package com.enjoychain.www.security.mapper;

import com.enjoychain.www.security.entity.HwItem;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author ddmonk
 * @since 2023-02-21
 */
public interface HwItemMapper extends BaseMapper<HwItem> {

}
