// 安全生产基础信息
import request from '@/utils/request'

export function listSecurityProduct(query) {
  return request({
    url: '/security/corpSecProductInfo/list',
    method: 'get',
    params: query
  })
}

export function getSecurityProduct(id) {
  return request({
    url: '/security/corpSecProductInfo/' + id,
    method: 'get'
  })
}

export function addSecurityProduct(data) {
  return request({
    url: '/security/corpSecProductInfo',
    method: 'post',
    data: data
  })
}

export function updateSecurityProduct(data) {
  return request({
    url: '/security/corpSecProductInfo',
    method: 'put',
    data: data
  })
}

export function delSecurityProduct(id) {
  return request({
    url: '/security/corpSecProductInfo/' + id,
    method: 'delete'
  })
}

export function importSecurityProduct(data) {
  return request({
    url: '/security/corpSecProductInfo/importData',
    method: 'post',
    data: data
  })
}
