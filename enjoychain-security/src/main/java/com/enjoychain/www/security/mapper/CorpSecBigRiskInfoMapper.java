package com.enjoychain.www.security.mapper;

import com.enjoychain.www.security.entity.CorpSecBigRiskInfo;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * 较大以上安全风险信息 Mapper 接口
 * </p>
 *
 * @author ddmonk
 * @since 2023-01-13
 */
public interface CorpSecBigRiskInfoMapper extends BaseMapper<CorpSecBigRiskInfo> {

}
