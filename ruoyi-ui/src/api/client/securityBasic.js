// 企业安全生产基础信息表
import request from '@/utils/request'

export function listSecurity(query) {
  return request({
    url: '/security/corpSecInfo/list',
    method: 'get',
    params: query
  })
}

export function getSecurity(id) {
  return request({
    url: '/security/corpSecInfo/' + id,
    method: 'get'
  })
}

export function addSecurity(data) {
  return request({
    url: '/security/corpSecInfo',
    method: 'post',
    data: data
  })
}

export function updateSecurity(data) {
  return request({
    url: '/security/corpSecInfo',
    method: 'put',
    data: data
  })
}

export function delSecurity(id) {
  return request({
    url: '/security/corpSecInfo/' + id,
    method: 'delete'
  })
}

export function importSecurity(data) {
  return request({
    url: '/security/corpSecInfo/importData',
    method: 'post',
    data: data
  })
}
