package com.enjoychain.www.security.mapper;

import com.enjoychain.www.security.entity.CorpEnvBasicInfo;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * 环保企业信息 Mapper 接口
 * </p>
 *
 * @author ddmonk
 * @since 2023-02-21
 */
public interface CorpEnvBasicInfoMapper extends BaseMapper<CorpEnvBasicInfo> {

}
