package com.enjoychain.www.security.entity.task;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import java.io.Serializable;
import com.ruoyi.common.annotation.Excel;
import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;
import lombok.experimental.Accessors;

/**
 * <p>
 * 巡查任务基础信息
 * </p>
 *
 * @author ddmonk
 * @since 2023-01-20
 */
@Getter
@Setter
@Accessors(chain = true)
@TableName("task_basic")
@ApiModel(value = "TaskBasic对象", description = "巡查任务基础信息")
public class TaskBasic implements Serializable {

    private static final long serialVersionUID = 1L;

    @ApiModelProperty("主键")
    @Excel(name = "主键")
    @TableId(value = "id", type = IdType.AUTO)
    private Integer id;

    @ApiModelProperty("任务id")
    @Excel(name = "任务id")
    private String taskId;

    @ApiModelProperty("任务名称")
    @Excel(name = "任务名称")
    private String name;

    @ApiModelProperty("显示名称")
    @Excel(name = "显示名称")
    private String label;

    @ApiModelProperty("公司id")
    @Excel(name = "公司id")
    private String corpId;

    @ApiModelProperty("公司名称")
    @Excel(name = "公司名称")
    private String corpName;

    @ApiModelProperty("描述")
    @Excel(name = "描述")
    private String description;

    @ApiModelProperty("巡查地点")
    @Excel(name = "巡查地点")
    private String position;

    @ApiModelProperty("巡查人员名称")
    @Excel(name = "巡查人员名称")
    private String handlerName;

    @ApiModelProperty("巡查人员id")
    @Excel(name = "巡查人员id")
    private String handlerId;


}
