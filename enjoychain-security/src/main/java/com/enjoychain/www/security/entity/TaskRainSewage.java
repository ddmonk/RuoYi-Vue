package com.enjoychain.www.security.entity;

import com.baomidou.mybatisplus.annotation.FieldFill;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableName;
import java.io.Serializable;
import java.time.LocalDateTime;
import java.util.List;

import com.ruoyi.common.annotation.Excel;
import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;
import lombok.experimental.Accessors;

/**
 * <p>
 * 雨污水情况
 * </p>
 *
 * @author ddmonk
 * @since 2023-02-24
 */
@Getter
@Setter
@Accessors(chain = true)
@TableName("task_rain_sewage")
@ApiModel(value = "TaskRainSewage对象", description = "雨污水情况")
public class TaskRainSewage implements Serializable {

    private static final long serialVersionUID = 1L;

    private String id;

    @ApiModelProperty("任务id")
    @Excel(name = "任务id")
    private String taskId;

    @ApiModelProperty("雨污水管网是否存在问题")
    @Excel(name = "雨污水管网是否存在问题")
    private String rainSewageStatus;

    @ApiModelProperty("雨污水管网备注")
    @Excel(name = "雨污水管网备注")
    private String rainSewageRemark;

    @ApiModelProperty("有无雨水排口标识牌")
    @Excel(name = "有无雨水排口标识牌")
    private String rainSewageSignStatus;

    @ApiModelProperty("是否有废水进入雨水管")
    @Excel(name = "是否有废水进入雨水管")
    private String rainSewageErrorStatus;

    @ApiModelProperty(value = "创建时间", hidden = true)
    @TableField(fill = FieldFill.INSERT)
    private LocalDateTime createTime;

    @ApiModelProperty(value = "修改时间", hidden = true)
    @TableField(fill = FieldFill.INSERT_UPDATE)
    private LocalDateTime updateTime;

    @ApiModelProperty(value = "创建人员", hidden = true)
    @TableField(fill = FieldFill.INSERT)
    private String createBy;

    @ApiModelProperty(value = "修改人员", hidden = true)
    @TableField(fill = FieldFill.INSERT_UPDATE)
    private String modifyBy;

    @TableField(exist = false)
    private List<BasicFile> files;

}
