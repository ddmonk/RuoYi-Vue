package com.enjoychain.www.security.service.impl;

import com.deepoove.poi.XWPFTemplate;
import com.deepoove.poi.config.Configure;
import com.deepoove.poi.data.PictureRenderData;
import com.deepoove.poi.data.PictureType;
import com.deepoove.poi.data.Pictures;
import com.deepoove.poi.plugin.table.LoopRowTableRenderPolicy;
import com.enjoychain.www.patrol.service.IEcTaskService;
import com.enjoychain.www.security.constant.BasicInfoType;
import com.enjoychain.www.security.entity.*;
import com.enjoychain.www.security.pojo.GasItems;
import com.enjoychain.www.security.pojo.PicPojo;
import com.enjoychain.www.security.pojo.TaskPojo;
import com.enjoychain.www.security.service.*;
import com.enjoychain.www.security.service.impl.report.policy.MergeCellLoopRowTableRenderPolicy;
import com.enjoychain.www.security.service.impl.report.policy.SingleLoopRowTableRenderPolicy;
import com.enjoychain.www.security.service.impl.task.TaskEnvConverts;
import com.ruoyi.common.config.RuoYiConfig;
import com.ruoyi.common.utils.file.ZipUtils;
import lombok.extern.java.Log;
import net.coobird.thumbnailator.Thumbnails;
import org.apache.commons.io.FileUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import java.awt.image.BufferedImage;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.text.DateFormat;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static com.ruoyi.common.utils.file.FileUploadUtils.*;

@Service
@Log
public class ReportServiceImpl implements IReportService {

    @Value("${url}")
    String url;

    @Value("${template.path}")
    String templatePath;

    @Value("${template.securityOwner}")
    String securityOwner;


    private String getSecurityPath(){
        return templatePath + "security" + File.separator;
    }


    @Autowired
    ICorpEnvBasicInfoService basicInfoService;

    @Autowired
    ITaskInfoService taskInfoService;

    @Autowired
    IEcTaskService ecTaskService;


    @Override
    public Map<String,String> buildBasicInfoById(String id, BasicInfoType type) {
        switch (type) {
            case TASK:
            return generateBasicInfoByTaskId(id);
            case CORP:
            return generateBasicInfoByCorpId(id);
            default:
                return null;
        }
    }

    @Override
    public Map<String,String> buildTaskInfoByTaskId(String taskId) throws IOException {
        TaskPojo taskPojo = taskInfoService.getTaskPojo(taskId);
        return generateTaskInfo(taskPojo);
    }

    @Override
    public Map<String,String> buildTaskAllByTaskId(String taskId) throws IOException {
        TaskPojo taskPojo = taskInfoService.getTaskPojo(taskId);
        //1、获取当前基础信息的文件路径
        String basicUrl = generateBasicInfo(TaskEnvConverts.convertTaskEnvBasicInfoToCorpEnvBasicInfo(taskPojo.getEnvCorpInfo())).get("path");
        String taskInfoUrl = generateTaskInfo(taskPojo).get("path");
        String originBasicPath = basicUrl.replace(url,"").replace("/profile/upload","");
        String originTaskInfoPath = taskInfoUrl.replace(url,"").replace("/profile/upload","");
        String dest = RuoYiConfig.getUploadPath();
        String dirPath = dest + File.separator + taskPojo.getTaskInfo().getName();
        String basicInfoPath = dirPath + File.separator + "基础信息.docx";
        String taskInfoPath = dirPath + File.separator + "巡查报告.docx";
        String imagePath = dirPath + File.separator + "图片" + File.separator;
        File basicInfo = new File(basicInfoPath);
        File taskInfo = new File(taskInfoPath);
        if (!basicInfo.exists()){
            FileUtils.createParentDirectories(basicInfo);
        }
        FileUtils.copyFile(new File(dest + originBasicPath),basicInfo);
        FileUtils.copyFile(new File(dest + originTaskInfoPath),taskInfo);

        //把图片全部放到文件夹下面去
        buildReportPictures(taskPojo,imagePath);
        ZipUtils.zipFile(dirPath,dest);
        FileUtils.deleteDirectory(new File(dirPath));
        String fileName = taskPojo.getTaskInfo().getName() + ".zip";
        String filePath = getPathFileName(RuoYiConfig.getUploadPath(), fileName);
        Map<String,String> aRes = new HashMap<>();
        aRes.put("path",url + filePath);
        aRes.put("fileName",fileName);
        return aRes;
    }

    @Override
    public Map<String, String> buildCompanyPromiseByTaskId(String taskId) {
        TaskPojo taskPojo = taskInfoService.getTaskPojo(taskId);
        Map<String, Object> params = new HashMap<>();
        params.put("corpName",taskPojo.getTaskInfo().getCorpName());
        params.put("ownerCorpName",securityOwner);
        Configure config = Configure.builder()
                .build();


        XWPFTemplate template = XWPFTemplate.compile(getSecurityPath() + "PromiseTemplate.docx",config).render(params);

        String dest = RuoYiConfig.getUploadPath();
        String fileName = taskPojo.getTaskInfo().getName()+ "-核查承诺书" + ".docx";
        String path = dest + File.separator + fileName ;

        try {
            File file = new File(path);
            if (!file.exists()) {
                FileUtils.createParentDirectories(file);
                file.createNewFile();
            }
            log.info("file path:" + path);
            template.write(new FileOutputStream(file));
            String filePath = getPathFileName(RuoYiConfig.getUploadPath(), fileName);
            Map<String, String> aRes = new HashMap<>();
            aRes.put("path",url + filePath);
            aRes.put("fileName", fileName);
            return aRes;
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    @Override
    public Map<String, String> buildCheckListByTaskId(String taskId) {
        DateTimeFormatter fmt = DateTimeFormatter.ofPattern("yyyy-MM-dd");
        TaskPojo taskPojo = taskInfoService.getTaskPojo(taskId);
        Map<String, Object> params = new HashMap<>();
        params.put("corpName",taskPojo.getTaskInfo().getCorpName());
        params.put("checkTime",taskPojo.getTaskInfo().getStartTime().format(fmt));
        params.put("checkList",taskPojo.getSecTaskDetail());
        Configure config = Configure.builder()
                .bind("checkList",new MergeCellLoopRowTableRenderPolicy())
                .build();


        XWPFTemplate template = XWPFTemplate.compile(getSecurityPath() + "CheckListTemplate.docx",config).render(params);

        String dest = RuoYiConfig.getUploadPath();
        String fileName = taskPojo.getTaskInfo().getName()+ "-核查表" + ".docx";
        String path = dest + File.separator + fileName ;

        try {
            File file = new File(path);
            if (!file.exists()) {
                FileUtils.createParentDirectories(file);
                file.createNewFile();
            }
            log.info("file path:" + path);
            template.write(new FileOutputStream(file));
            String filePath = getPathFileName(RuoYiConfig.getUploadPath(), fileName);
            Map<String, String> aRes = new HashMap<>();
            aRes.put("path",url + filePath);
            aRes.put("fileName", fileName);
            return aRes;
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    @Override
    public Map<String, String> buildDailyTaskReportByTaskId(String taskId) {
        return ecTaskService.buildReportByTaskId(taskId);
    }

    private void buildReportPictures(TaskPojo taskPojo, String imagePath) throws IOException {
        addEquipmentPicsToDir(taskPojo, imagePath);
        addGasItemsPics(taskPojo, imagePath);
        addHwStorePics(taskPojo, imagePath);
        addRainSewagePics(taskPojo, imagePath);
        addSolidWastePics(taskPojo, imagePath);
        addGasSectionsPic(taskPojo, imagePath);
    }

    private void addGasSectionsPic(TaskPojo taskPojo, String imagePath) throws IOException {
        String hwStoreParentPath = imagePath + "废气收集情况" + File.separator;
        for (TaskProductSectionItem item : taskPojo.getEnvTaskDetail().getGasSectionItems()){
            for (int i = 0;i< item.getFiles().size(); i++){
                StringBuilder path = new StringBuilder()
                        .append(hwStoreParentPath)
                        .append(item.getProductSection())
                        .append("_")
                        .append((i+1))
                        .append(".")
                        .append(PictureType.suggestFileType(item.getFiles().get(i).getPath()));
                String filePath = path.toString();
                File temp = new File(filePath);
                FileUtils.createParentDirectories(temp);
                FileUtils.copyFile(new File(item.getFiles().get(i).getPath()),temp);
            }
        }
    }

    private void addHwStorePics(TaskPojo taskPojo, String imagePath) throws IOException {
        String hwStoreParentPath = imagePath + "危废仓库管理情况" + File.separator;
        for (TaskHwStoreStatus item : taskPojo.getEnvTaskDetail().getHwStoreStatusInfos()){
            for (int i = 0;i< item.getFiles().size(); i++){
                StringBuilder path = new StringBuilder()
                        .append(hwStoreParentPath)
                        .append(item.getCheckPoint())
                        .append("_")
                        .append((i+1))
                        .append(".")
                        .append(PictureType.suggestFileType(item.getFiles().get(i).getPath()));
                String filePath = path.toString();
                File temp = new File(filePath);
                FileUtils.createParentDirectories(temp);
                FileUtils.copyFile(new File(item.getFiles().get(i).getPath()),temp);
            }
        }
    }

    private void addSolidWastePics(TaskPojo taskPojo, String imagePath) throws IOException {
        String rainSewageParentPath = imagePath + "一般固体废物情况" + File.separator;
        for (int i = 0;i< taskPojo.getEnvTaskDetail().getSolidWasteInfo().getFiles().size(); i++){
            String filePath = rainSewageParentPath + "一般固体废物情况_" + (i+1) + "." + PictureType.suggestFileType(taskPojo.getEnvTaskDetail().getSolidWasteInfo().getFiles().get(i).getPath());
            File temp = new File(filePath);
            FileUtils.createParentDirectories(temp);
            FileUtils.copyFile(new File(taskPojo.getEnvTaskDetail().getSolidWasteInfo().getFiles().get(i).getPath()),temp);
        }
    }

    private void addRainSewagePics(TaskPojo taskPojo, String imagePath) throws IOException {
        String rainSewageParentPath = imagePath + "雨污水情况" + File.separator;
            for (int i = 0;i< taskPojo.getEnvTaskDetail().getSolidWasteInfo().getFiles().size(); i++){
                String filePath = rainSewageParentPath + "雨污水情况_" + (i+1) + "." + PictureType.suggestFileType(taskPojo.getEnvTaskDetail().getSolidWasteInfo().getFiles().get(i).getPath());
                File temp = new File(filePath);
                FileUtils.createParentDirectories(temp);
                FileUtils.copyFile(new File(taskPojo.getEnvTaskDetail().getSolidWasteInfo().getFiles().get(i).getPath()),temp);
            }
    }

    private void addGasItemsPics(TaskPojo taskPojo, String imagePath) throws IOException {
        String gasItemParentPath = imagePath + "废气排放情况" + File.separator;
        for (TaskGasPollutionItem item : taskPojo.getEnvTaskDetail().getGasPollutionItems()){
            for (int i = 0;i< item.getFiles().size(); i++){
                StringBuilder path = new StringBuilder()
                        .append(gasItemParentPath)
                        .append(item.getExhaustFunnelNo())
                        .append("【")
                        .append(item.getPollutionFactor())
                        .append("】_")
                        .append((i+1))
                        .append(".")
                        .append(PictureType.suggestFileType(item.getFiles().get(i).getPath()));
                String filePath = path.toString();
                File temp = new File(filePath);
                FileUtils.createParentDirectories(temp);
                FileUtils.copyFile(new File(item.getFiles().get(i).getPath()),temp);
            }
        }
    }

    private void addEquipmentPicsToDir(TaskPojo taskPojo, String imagePath) throws IOException {
        String equipmentParentPath = imagePath + "设备情况" + File.separator;
        for (TaskEquipItem item : taskPojo.getEnvTaskDetail().getEquipItems()){
            for (int i = 0;i< item.getFiles().size(); i++){
                String filePath = equipmentParentPath + item.getEquipName() + "_" + (i+1) + "." +PictureType.suggestFileType(item.getFiles().get(i).getPath());
                File temp = new File(filePath);
                FileUtils.createParentDirectories(temp);
                FileUtils.copyFile(new File(item.getFiles().get(i).getPath()),temp);
            }
        }
    }

    private Map<String,String> generateTaskInfo(TaskPojo taskPojo) throws IOException {
        Map<String, Object> params = new HashMap<>();
        params.put("taskInfo",taskPojo.getTaskInfo());
        params.put("corpInfo",taskPojo.getEnvCorpInfo());
        params.put("equipments",taskPojo.getEnvTaskDetail().getEquipItems());
        params.put("process", taskPojo.getEnvTaskDetail().getProductProcessInfo());
        params.put("ownerMonitorStatus", taskPojo.getEnvTaskDetail().getOwnerMonitorStatus());
        params.put("planPublishStatus", taskPojo.getEnvTaskDetail().getPlanPublishStatus());
        params.put("mergency", taskPojo.getEnvTaskDetail().getMergencyInfo());
        params.put("wasteWater", taskPojo.getEnvTaskDetail().getWasteWaterInfo());
        params.put("rainSewage",taskPojo.getEnvTaskDetail().getRainSewageInfo());
        params.put("section", taskPojo.getEnvTaskDetail().getGasSectionItems());
        params.put("gasItems", buildGasItems(taskPojo.getEnvTaskDetail().getGasPollutionItems()));
        params.put("noise", taskPojo.getEnvTaskDetail().getTaskNoiseInfo());
        params.put("solid", taskPojo.getEnvTaskDetail().getSolidWasteInfo());
        params.put("hwItems", taskPojo.getEnvTaskDetail().getHwStoreStatusInfos());
        params.put("hw", taskPojo.getEnvTaskDetail().getHwData());
        buildEquipmentPics(params,taskPojo.getEnvTaskDetail().getEquipItems());
        buildGasItemsPics(params,taskPojo.getEnvTaskDetail().getGasPollutionItems());
        buildHwStorePics(params,taskPojo.getEnvTaskDetail().getHwStoreStatusInfos());
        buildRainSewagePics(params,taskPojo.getEnvTaskDetail().getRainSewageInfo());
        buildSolidWastePics(params,taskPojo.getEnvTaskDetail().getSolidWasteInfo());
        buildGasSectionsPic(params,taskPojo.getEnvTaskDetail().getGasSectionItems());

        LoopRowTableRenderPolicy policy = new LoopRowTableRenderPolicy();
        Configure config = Configure.builder()
                .bind("equipments", policy)
                .bind("section", policy)
                .bind("gasItems",new SingleLoopRowTableRenderPolicy())
                .bind("gasEquipmentItems",policy)
                .bind("hwItems",policy)
                .bind("equipmentPics",policy)
                .bind("gasPics",policy)
                .bind("hwPics",policy)
                .bind("gasSectionPics",policy)
                .build();


        XWPFTemplate template = XWPFTemplate.compile(templatePath + "TaskInfoTemplate.docx",config).render(params);

        String dest = RuoYiConfig.getUploadPath();
        String fileName = taskPojo.getTaskInfo().getName() + ".docx";
        String path = dest + File.separator + fileName ;

        try {
            File file = new File(path);
            if (!file.exists()) {
                FileUtils.createParentDirectories(file);
                file.createNewFile();
            }
            log.info("file path:" + path);
            template.write(new FileOutputStream(file));
            String filePath = getPathFileName(RuoYiConfig.getUploadPath(), fileName);
            Map<String, String> aRes = new HashMap<>();
            aRes.put("path",url + filePath);
            aRes.put("fileName", fileName);
            return aRes;
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    private void buildGasSectionsPic(Map<String, Object> params, List<TaskProductSectionItem> gasSectionItems) throws IOException {
        List<PicPojo> aRes = new ArrayList<>();
        for (TaskProductSectionItem item : gasSectionItems){
            if (item.getFiles() != null && item.getFiles().size() > 0){
                PicPojo temp = new PicPojo();
                temp.setDesc(item.getProductSection());
                List<Map<String,Object>> pics = new ArrayList<>();
                for (BasicFile basicFile : item.getFiles()){
                    pics.add(new HashMap<String,Object>(){{put("pic",buildPictureRenderData(basicFile));}});
                }
                temp.setPics(pics);
                aRes.add(temp);
            }
        }
        params.put("gasSectionPics",aRes);
        params.put("gasSectionFlag",aRes.size()>0);
    }

    private void buildSolidWastePics(Map<String, Object> params, TaskSolidWaste solidWasteInfo) throws IOException {
        List<Map<String,Object>> pics = new ArrayList<>();
        if (solidWasteInfo.getFiles() != null && solidWasteInfo.getFiles().size() > 0){
            for (BasicFile basicFile : solidWasteInfo.getFiles()){
                pics.add(new HashMap<String,Object>(){{put("pic",buildPictureRenderData(basicFile));}});
            }
        }
        params.put("solidWastePics",pics);
        params.put("solidWasteFlag",pics.size()>0);
    }

    private void buildRainSewagePics(Map<String, Object> params, TaskRainSewage rainSewageInfo) throws IOException {
        List<Map<String,Object>> pics = new ArrayList<>();
        if (rainSewageInfo.getFiles() != null && rainSewageInfo.getFiles().size() > 0){
            for (BasicFile basicFile : rainSewageInfo.getFiles()){
                pics.add(new HashMap<String,Object>(){{put("pic",buildPictureRenderData(basicFile));}});
            }
        }
        params.put("rainSewagePics",pics);
        params.put("rainSewageFlag",pics.size()>0);
    }

    private void buildHwStorePics(Map<String, Object> params, List<TaskHwStoreStatus> hwStoreStatusInfos) throws IOException {
        List<PicPojo> aRes = new ArrayList<>();
        for (TaskHwStoreStatus item : hwStoreStatusInfos){
            if (item.getFiles() != null && item.getFiles().size() > 0){
                PicPojo temp = new PicPojo();
                temp.setDesc(item.getCheckPoint());
                List<Map<String,Object>> pics = new ArrayList<>();
                for (BasicFile basicFile : item.getFiles()){
                    pics.add(new HashMap<String,Object>(){{put("pic",buildPictureRenderData(basicFile));}});
                }
                temp.setPics(pics);
                aRes.add(temp);
            }
        }
        params.put("hwPics",aRes);
        params.put("hwFlag",aRes.size()>0);
    }




    private void buildGasItemsPics(Map<String, Object> params, List<TaskGasPollutionItem> gasPollutionItems) throws IOException {
        List<PicPojo> aRes = new ArrayList<>();
        for (TaskGasPollutionItem item : gasPollutionItems){
            if (item.getFiles() != null && item.getFiles().size() > 0){
                PicPojo temp = new PicPojo();
                temp.setDesc(item.getExhaustFunnelNo() + "|" + item.getPollutionFactor());
                List<Map<String,Object>> pics = new ArrayList<>();
                for (BasicFile basicFile : item.getFiles()){
                    pics.add(new HashMap<String,Object>(){{put("pic",buildPictureRenderData(basicFile));}});
                }
                temp.setPics(pics);
                aRes.add(temp);
            }
        }
        params.put("gasPics",aRes);
        params.put("gasFlag",aRes.size()>0);
    }

    private void buildEquipmentPics(Map<String, Object> params, List<TaskEquipItem> equipItems) throws IOException {
        List<PicPojo> aRes = new ArrayList<>();
        for (TaskEquipItem item : equipItems){
            if (item.getFiles() != null && item.getFiles().size() > 0){
                PicPojo temp = new PicPojo();
                temp.setDesc(item.getEquipName());
                List<Map<String,Object>> pics = new ArrayList<>();
                for (BasicFile basicFile : item.getFiles()){
                    pics.add(new HashMap<String,Object>(){{put("pic",buildPictureRenderData(basicFile));}});
                }
                temp.setPics(pics);
                aRes.add(temp);
            }
        }
        params.put("equipmentPics",aRes);
        params.put("equipmentFlag",aRes.size()>0);
    }


    /**
     * 构建POI TL中的 图片数据
     * @param basicFile {@link BasicFile} 文件对象
     * @return {@link PictureRenderData} Poi tl 对象
     * @throws IOException
     */
    private PictureRenderData buildPictureRenderData(BasicFile basicFile) throws IOException {
        File pic = new File(basicFile.getPath());
        long size = FileUtils.sizeOf(pic)/1024/1024;
        BufferedImage bufferedImage = null;
        if (size > 10){
            bufferedImage = Thumbnails.of(basicFile.getPath()).outputQuality(0.5f).scale(0.5d).asBufferedImage();
        }else if (size > 5){
            bufferedImage = Thumbnails.of(basicFile.getPath()).outputQuality(0.5f).scale(0.7d).asBufferedImage();
        }else {
            bufferedImage = Thumbnails.of(basicFile.getPath()).scale(0.7d).asBufferedImage();
        }
        return Pictures.ofBufferedImage(
                        bufferedImage,
                        PictureType.suggestFileType(basicFile.getPath())).size((int)(150*(bufferedImage.getWidth() / (double) bufferedImage.getHeight())),150)
                .create();
    }


    private List<GasItems> buildGasItems(List<TaskGasPollutionItem> gasPollutionItems) {
        List<GasItems> items = new ArrayList<>();
        for (TaskGasPollutionItem item : gasPollutionItems){
            items.add(new GasItems(item.getExhaustFunnelNo(),
                    item.getPollutionFactor(),
                    "排气筒管道是否完好",
                    item.getExhaustPipelineStatus(),
                    item.getAdvice()));
            items.add(new GasItems(item.getExhaustFunnelNo(),
                    item.getPollutionFactor(),
                    "风机是否开启",
                    item.getExhaustPipelineStatus(),
                    item.getAdvice()));
            items.add(new GasItems(item.getExhaustFunnelNo(),
                    item.getPollutionFactor(),
                    "排气筒高度是否满足要求",
                    item.getFanStatus(),
                    item.getAdvice()));
            items.add(new GasItems(item.getExhaustFunnelNo(),
                    item.getPollutionFactor(),
                    "检测口是否满足要求",
                    item.getInspectionStatus(),
                    item.getAdvice()));
            items.add(new GasItems(item.getExhaustFunnelNo(),
                    item.getPollutionFactor(),
                    "处理设施是否开启",
                    item.getHandleEquipStatus(),
                    item.getAdvice()));
            items.add(new GasItems(item.getExhaustFunnelNo(),
                    item.getPollutionFactor(),
                    "是否按时更换处理材料",
                    item.getHandleSourceStatus(),
                    item.getAdvice()));
            items.add(new GasItems(item.getExhaustFunnelNo(),
                    item.getPollutionFactor(),
                    "是否按时维护",
                    item.getRepairStatus(),
                    item.getAdvice()));
            items.add(new GasItems(item.getExhaustFunnelNo(),
                    item.getPollutionFactor(),
                    "是否无其他异常",
                    item.getOtherUnusualStatus(),
                    item.getAdvice()));
            items.add(new GasItems(item.getExhaustFunnelNo(),
                    item.getPollutionFactor(),
                    "是否有标识牌",
                    item.getSignboardStatus(),
                    item.getAdvice()));
        }
        return items;
    }

    private Map<String,String> generateBasicInfoByCorpId(String id) {
        CorpEnvBasicInfo basicInfo = basicInfoService.getDetailById(id);
        return generateBasicInfo(basicInfo);
    }

    private Map<String,String> generateBasicInfoByTaskId(String id) {
        CorpEnvBasicInfo basicInfo = TaskEnvConverts.convertTaskEnvBasicInfoToCorpEnvBasicInfo(taskInfoService.getTaskPojo(id).getEnvCorpInfo());
        return generateBasicInfo(basicInfo);
    }

    private Map<String,String> generateBasicInfo(CorpEnvBasicInfo basicInfo)  {

        Map<String, Object> params = new HashMap<>();
        //时间格式更改
        if (basicInfo.getReportItems().size() < 1) {
            basicInfo.getReportItems().add(new CorpEpEia("——"));
        }
        params.put("basic",basicInfo);
        params.put("equipments",basicInfo.getEquipItems());
        params.put("reportItems", basicInfo.getReportItems());
        params.put("solidWasteItems", basicInfo.getSolidWasteItems());
        params.put("gasEquipmentItems", basicInfo.getGasEquipmentItems());
        params.put("hwItems", basicInfo.getHwItems());
        LoopRowTableRenderPolicy policy = new LoopRowTableRenderPolicy();
        Configure config = Configure.builder()
                .bind("equipments", policy)
                .bind("reportItems", policy)
                .bind("solidWasteItems",policy)
                .bind("gasEquipmentItems",policy)
                .bind("hwItems",policy)
                .build();


        XWPFTemplate template = XWPFTemplate.compile(templatePath + "CorpTemplate.docx",config).render(params);
        String dest = RuoYiConfig.getUploadPath();
        String fileName = basicInfo.getCorpName() + ".docx";
        String path = dest + File.separator + fileName ;


        try {
            File file = new File(path);
            if (!file.exists()) {
                FileUtils.createParentDirectories(file);
                file.createNewFile();
            }
            log.info("file path:" + path);
            template.write(new FileOutputStream(file));
            String filePath = getPathFileName(RuoYiConfig.getUploadPath(), fileName);
            Map<String, String> aRes = new HashMap<>();
            aRes.put("path", url + filePath);
            aRes.put("fileName", fileName);
            return aRes;
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }
}
