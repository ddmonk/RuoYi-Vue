package com.enjoychain.www.security.service.impl;

import com.enjoychain.www.security.pojo.StatisticsPojo;
import com.enjoychain.www.security.service.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class DashBoardServiceImpl implements IDashBoardService {

    @Autowired
    ITaskInfoService taskInfoService;

    @Autowired
    INeedInfoService needInfoService;

    @Autowired
    ICorpBasicInfoService corpBasicInfoService;

    @Autowired
    IAccidentInfoService accidentInfoService;

    @Override
    public StatisticsPojo getPanel() {
        StatisticsPojo statisticsPojo = new StatisticsPojo();
        statisticsPojo.setTaskNum(taskInfoService.count());
        statisticsPojo.setCustomNum(corpBasicInfoService.count());
        statisticsPojo.setNeedNum(needInfoService.count());
        statisticsPojo.setProblemNum(accidentInfoService.count());
        return statisticsPojo;
    }
}
