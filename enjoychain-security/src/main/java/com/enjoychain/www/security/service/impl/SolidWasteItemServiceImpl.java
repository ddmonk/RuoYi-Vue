package com.enjoychain.www.security.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.enjoychain.www.security.entity.SolidWasteItem;
import com.enjoychain.www.security.mapper.SolidWasteItemMapper;
import com.enjoychain.www.security.service.ISolidWasteItemService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * <p>
 * 一般固废详情 服务实现类
 * </p>
 *
 * @author ddmonk
 * @since 2023-02-21
 */
@Service
public class SolidWasteItemServiceImpl extends ServiceImpl<SolidWasteItemMapper, SolidWasteItem> implements ISolidWasteItemService {

    private QueryWrapper<SolidWasteItem> buildQueryWrapper(String id){
        QueryWrapper<SolidWasteItem> queryWrapper = new QueryWrapper<>();
        queryWrapper.eq("corp_id",id);
        return queryWrapper;
    }

    @Override
    public List<SolidWasteItem> listByCorpId(String id) {
        return list(buildQueryWrapper(id));
    }

    @Override
    public void saveOrUpdateBatch(List<SolidWasteItem> solidWasteItems, String id) {
        for (SolidWasteItem item: solidWasteItems){
            if (StringUtils.isEmpty(item.getCorpId())){
                item.setCorpId(id);
            }
        }
        saveOrUpdateBatch(solidWasteItems);
    }

    @Override
    public void deleteByCorpId(String id) {
        remove(buildQueryWrapper(id));
    }


}
