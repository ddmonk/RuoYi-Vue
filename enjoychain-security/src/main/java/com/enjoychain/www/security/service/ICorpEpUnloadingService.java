package com.enjoychain.www.security.service;

import com.enjoychain.www.security.entity.CorpEpUnloading;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 * 排污许可证申领情况 服务类
 * </p>
 *
 * @author ddmonk
 * @since 2023-01-13
 */
public interface ICorpEpUnloadingService extends IService<CorpEpUnloading> {

}
