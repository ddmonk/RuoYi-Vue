package com.enjoychain.www.security.service;

import com.enjoychain.www.security.entity.TaskSecBigRiskInfo;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author ddmonk
 * @since 2023-04-18
 */
public interface ITaskSecBigRiskInfoService extends ITaskService<TaskSecBigRiskInfo> {

}
