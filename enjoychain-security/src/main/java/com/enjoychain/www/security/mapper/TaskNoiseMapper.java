package com.enjoychain.www.security.mapper;

import com.enjoychain.www.security.entity.TaskNoise;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * 主要噪声源 Mapper 接口
 * </p>
 *
 * @author ddmonk
 * @since 2023-02-24
 */
public interface TaskNoiseMapper extends BaseMapper<TaskNoise> {

}
