package com.enjoychain.www.security.service.impl;

import com.enjoychain.www.security.constant.BusinessTaskType;
import com.enjoychain.www.security.entity.TaskRainSewage;
import com.enjoychain.www.security.mapper.TaskRainSewageMapper;
import com.enjoychain.www.security.service.IBasicFileService;
import com.enjoychain.www.security.service.ITaskRainSewageService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 雨污水情况 服务实现类
 * </p>
 *
 * @author ddmonk
 * @since 2023-02-24
 */
@Service
public class TaskRainSewageServiceImpl extends ServiceImpl<TaskRainSewageMapper, TaskRainSewage> implements ITaskRainSewageService {

    @Autowired
    IBasicFileService fileService;
    @Override
    public TaskRainSewage getDetailByTaskId(String taskId) {
        TaskRainSewage rainSewage = getByTaskId(taskId);
        if (rainSewage != null ){
            rainSewage.setFiles(fileService.getListByTypeAndId(BusinessTaskType.ENVIRONMENT,rainSewage.getId()));
        }
        return rainSewage;
    }

    @Override
    public void removeDetailByTaskId(String taskId) {
        TaskRainSewage rainSewage = getByTaskId(taskId);
        if (rainSewage != null ){
            fileService.removeFileByConnectIdAndBusiType(BusinessTaskType.ENVIRONMENT,rainSewage.getId());
        }
        removeById(rainSewage);
    }
}
