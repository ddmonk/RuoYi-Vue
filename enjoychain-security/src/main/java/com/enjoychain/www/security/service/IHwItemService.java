package com.enjoychain.www.security.service;

import com.enjoychain.www.security.entity.HwItem;
import com.baomidou.mybatisplus.extension.service.IService;

import java.util.List;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author ddmonk
 * @since 2023-02-21
 */
public interface IHwItemService extends IService<HwItem> {

    List<HwItem> listByCorpId(String id);

    void saveOrUpdateBatch(List<HwItem> hwItems, String id);


    void deleteByCorpId(String corpId);


}
