package com.enjoychain.www.security.pojo;

import lombok.Data;

@Data
public class AccRelationPojo {

    private String taskId;

    private String accId;

}
