package com.enjoychain.www.security.controller.business.job;

import org.springframework.web.bind.annotation.RequestMapping;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.common.utils.StringUtils;
import com.ruoyi.common.utils.poi.ExcelUtil;
import io.swagger.annotations.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import javax.servlet.http.HttpServletResponse;
import java.util.List;
import java.util.Arrays;
import com.enjoychain.www.security.entity.job.JobInfo;
import com.enjoychain.www.security.service.IJobInfoService;
import org.springframework.web.bind.annotation.RestController;

/**
 * <p>
 * 作业基础表 前端控制器
 * </p>
 *
 * @author ddmonk
 * @since 2023-01-29
 */
@Api(tags = "作业基础信息")
@RestController
@RequestMapping("/security/jobInfo")
public class JobInfoController {

    @Autowired
    IJobInfoService service;

    @PreAuthorize("@ss.hasPermi('job:basic:list')")
    @GetMapping(path = "/list", produces = {"application/json"})
    @ApiOperation(value = "获取作业基础列表")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "name", value = "名称",paramType = "query", dataType = "String"),
            @ApiImplicitParam(name = "type", value = "类型",paramType = "query", dataType = "String"),
            @ApiImplicitParam(name = "pageNo", value = "页码", paramType = "query", dataType = "long"),
            @ApiImplicitParam(name = "pageSize", value = "页面记录条数", paramType = "query", dataType = "long")
    })
    @ApiResponses({
        @ApiResponse(code = 200, message = "ok", response= JobInfo.class),
    })
    public AjaxResult list(
                            @RequestParam(name = "name", required = false) String name,
                            @RequestParam(name = "type", required = false) String type,
                            @RequestParam(name = "period", required = false) String period,
                            @RequestParam(name = "pageNum", defaultValue = "1") long pageNo,
                            @RequestParam(name = "pageSize",defaultValue = "10") long pageSize){
        IPage<JobInfo> entityIPage = new Page<>(pageNo, pageSize);
        QueryWrapper<JobInfo> queryWrapper = new QueryWrapper<>();
        if (StringUtils.isNotEmpty(name)) {
            queryWrapper.like("name",name);
        }
        if (StringUtils.isNotEmpty(type)) {
            queryWrapper.eq("type",type);
        }
        if (StringUtils.isNotEmpty(period)) {
            queryWrapper.eq("period",period);
        }
        return AjaxResult.success("list-job_info",service.page(entityIPage,queryWrapper));
    }

    @PreAuthorize("@ss.hasPermi('job:basic:query')")
    @GetMapping(value = "{id}")
    @ApiOperation("获取作业详情")
    @ApiImplicitParams({
        @ApiImplicitParam(name = "id", value = "id",paramType = "path", dataType = "String")
    })
    @ApiResponses({
        @ApiResponse(code = 200, message = "ok", response = JobInfo.class)
    })
    public AjaxResult getJobInfoInfo(@PathVariable(value = "id", required = false) String id)
    {
        return AjaxResult.success("get-job_info",service.getJobDetail(id));
    }

    @PreAuthorize("@ss.hasPermi('job:basic:add')")
    @PostMapping(consumes = {"application/json"}, produces = {"application/json"})
    @ApiOperation("新增作业基础表")
    @ApiResponses({
        @ApiResponse(code = 200, message = "ok", response = JobInfo.class),
    })
    @Log(title = "作业基础表", businessType = BusinessType.INSERT)
    public AjaxResult add(@RequestBody JobInfo entity){
        return service.save(entity)? AjaxResult.success("add-job_info",entity): AjaxResult.error("新增作业基础表失败");
    }


    @PreAuthorize("@ss.hasPermi('job:basic:update')")
    @PutMapping(consumes = {"application/json"}, produces = {"application/json"})
    @ApiOperation("修改作业基础表")
    @ApiResponses({
        @ApiResponse(code = 200, message = "ok", response = JobInfo.class)
    })
    @Log(title = "作业基础表", businessType = BusinessType.UPDATE)
    public AjaxResult update(@RequestBody JobInfo corpBasicInfo){
        String operate = "update-job_info";
        return service.updateById(corpBasicInfo)?AjaxResult.success(operate,service.getById(corpBasicInfo.getId())) : AjaxResult.error("修改客户基础信息失败");
    }

    @PreAuthorize("@ss.hasPermi('job:basic:delete')")
    @DeleteMapping(value = "{id}", produces = {"application/json"})
    @ApiOperation("删除作业基础表")
    @ApiResponses({
        @ApiResponse(code = 200, message = "ok", response = AjaxResult.class),
    })
    @Log(title = "作业基础表", businessType = BusinessType.DELETE)
    public AjaxResult delete(@PathVariable(value = "id", required = false) String id){
        String operate = "delete-job_info";
        String[] ids = id.split(",");
        return service.removeBatchByIds(Arrays.asList(ids))?AjaxResult.success(operate) : AjaxResult.error("修改作业基础表失败");
    }


    @PreAuthorize("@ss.hasPermi('job:basic:start')")
    @GetMapping("/start/{id}")
    @ApiOperation("开启作业任务")
    @ApiResponses({
            @ApiResponse(code = 200, message = "ok", response = AjaxResult.class),
    })
    @Log(title = "开启作业任务", businessType = BusinessType.RUNNING)
    public AjaxResult start(@PathVariable(value = "id") String id){
        return service.start(id)?AjaxResult.success() : AjaxResult.error("修改作业基础表失败");
    }


//    @Log(title = "作业基础表", businessType = BusinessType.EXPORT)
//    @PreAuthorize("@ss.hasPermi('job:basic:export')")
//    @PostMapping("/export")
//    @ApiOperation("导出作业基础表")
//    @ApiResponses({
//        @ApiResponse(code = 200, message = "ok", response = AjaxResult.class),
//    })
//    public void export(HttpServletResponse response,@RequestParam(name = "corpId", required = false) String corpId){
//        QueryWrapper<JobInfo> queryWrapper = new QueryWrapper<>();
//        if (StringUtils.isNotEmpty(corpId)) {
//            queryWrapper.eq("corp_id",corpId);
//        }
//        List<JobInfo> list = service.list(queryWrapper);
//        ExcelUtil<JobInfo> util = new ExcelUtil<JobInfo>(JobInfo.class);
//        util.exportExcel(response,list, "导出作业基础表");
//    }
}
