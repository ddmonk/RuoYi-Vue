package com.enjoychain.www.security.mapper;

import com.enjoychain.www.security.entity.task.TaskDetailItem;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * 巡查任务详情信息 Mapper 接口
 * </p>
 *
 * @author ddmonk
 * @since 2023-01-20
 */
public interface TaskDetailItemMapper extends BaseMapper<TaskDetailItem> {

}
