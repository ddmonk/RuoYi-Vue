// 主要设备情况
import request from '@/utils/request'

export function listEquipment(query) {
  return request({
    url: '/security/corpEpEquipInfo/list',
    method: 'get',
    params: query
  })
}

export function getEquipment(id) {
  return request({
    url: '/security/corpEpEquipInfo/' + id,
    method: 'get'
  })
}

export function addEquipment(data) {
  return request({
    url: '/security/corpEpEquipInfo',
    method: 'post',
    data: data
  })
}

export function updateEquipment(data) {
  return request({
    url: '/security/corpEpEquipInfo',
    method: 'put',
    data: data
  })
}

export function delEquipment(id) {
  return request({
    url: '/security/corpEpEquipInfo/' + id,
    method: 'delete'
  })
}

export function importEquipment(data) {
  return request({
    url: '/security/corpEpEquipInfo/importData',
    method: 'post',
    data: data
  })
}
