package com.enjoychain.www.security.mapper;

import com.enjoychain.www.security.entity.TaskEpEquipInfo;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author ddmonk
 * @since 2023-03-08
 */
public interface TaskEquipInfoMapper extends BaseMapper<TaskEpEquipInfo> {

}
