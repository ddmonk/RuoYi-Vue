package com.enjoychain.www.security.service;

import com.enjoychain.www.security.entity.CorpEpMergencyPlan;
import com.baomidou.mybatisplus.extension.service.IService;

import java.util.List;

/**
 * <p>
 * 应急预案情况 服务类
 * </p>
 *
 * @author ddmonk
 * @since 2023-01-13
 */
public interface ICorpEpMergencyPlanService extends IService<CorpEpMergencyPlan> {

    List<CorpEpMergencyPlan> listWithStatus(int status, String corpId);

}
