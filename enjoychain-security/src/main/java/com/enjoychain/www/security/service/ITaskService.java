package com.enjoychain.www.security.service;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.service.IService;
import com.baomidou.mybatisplus.extension.toolkit.SqlHelper;

import java.io.Serializable;
import java.util.List;

public interface ITaskService<T> extends IService<T> {

    default  QueryWrapper<T> buildQueryWithTaskId(Serializable taskId){
        QueryWrapper<T> queryWrapper = new QueryWrapper<>();
        queryWrapper.eq("task_id",taskId);
        return queryWrapper;
    }

    default T getByTaskId(Serializable id) {
        return getBaseMapper().selectOne(buildQueryWithTaskId(id));
    }

    default List<T> getBatchTaskId(Serializable id) {
        return getBaseMapper().selectList(buildQueryWithTaskId(id));
    }

    default boolean removeByTaskId(Serializable id) {return SqlHelper.retBool(getBaseMapper().delete(buildQueryWithTaskId(id)));}

    default List<T> listByTaskId(Serializable id) {return getBaseMapper().selectList(buildQueryWithTaskId(id));}

}
