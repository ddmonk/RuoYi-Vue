package com.enjoychain.www.security.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import java.io.Serializable;
import com.ruoyi.common.annotation.Excel;
import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;
import lombok.experimental.Accessors;

/**
 * <p>
 * 国民经济行业分类
 * </p>
 *
 * @author ddmonk
 * @since 2023-03-09
 */
@Getter
@Setter
@Accessors(chain = true)
@TableName("sys_dim_industry")
@ApiModel(value = "DimIndustry对象", description = "国民经济行业分类")
public class DimIndustry implements Serializable {

    private static final long serialVersionUID = 1L;

    @ApiModelProperty("Id")
    @Excel(name = "Id")
    @TableId(value = "id", type = IdType.AUTO)
    private Integer id;

    @ApiModelProperty("门类id ")
    @Excel(name = "门类id ")
    private String categoryIndustryId;

    @ApiModelProperty("门类名称")
    @Excel(name = "门类名称")
    private String categoryName;

    @ApiModelProperty("门类描述")
    @Excel(name = "门类描述")
    private String categoryDesc;

    @ApiModelProperty("大类id")
    @Excel(name = "大类id")
    private String largeClassIndustryId;

    @ApiModelProperty("大类名称")
    @Excel(name = "大类名称")
    private String largeClassName;

    @ApiModelProperty("大类描述")
    @Excel(name = "大类描述")
    private String largeClassDesc;

    @ApiModelProperty("中类id")
    @Excel(name = "中类id")
    private String midClassIndustryId;

    @ApiModelProperty("中类名称")
    @Excel(name = "中类名称")
    private String midClassName;

    @ApiModelProperty("中类描述")
    @Excel(name = "中类描述")
    private String midClassDesc;

    @ApiModelProperty("小类id")
    @Excel(name = "小类id")
    private String smallClassIndustryId;

    @ApiModelProperty("小类名称")
    @Excel(name = "小类名称")
    private String smallClassName;

    @ApiModelProperty("小类描述")
    @Excel(name = "小类描述")
    private String smallClassDesc;

    @ApiModelProperty("类型编码")
    @Excel(name = "类型编码")
    private String classCode;


}
