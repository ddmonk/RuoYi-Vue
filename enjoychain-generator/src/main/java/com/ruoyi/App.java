package com.ruoyi;

import com.baomidou.mybatisplus.annotation.FieldFill;
import com.baomidou.mybatisplus.core.enums.SqlLike;
import com.baomidou.mybatisplus.generator.FastAutoGenerator;
import com.baomidou.mybatisplus.generator.config.OutputFile;
import com.baomidou.mybatisplus.generator.config.TemplateType;
import com.baomidou.mybatisplus.generator.config.po.LikeTable;
import com.baomidou.mybatisplus.generator.engine.FreemarkerTemplateEngine;
import com.baomidou.mybatisplus.generator.engine.VelocityTemplateEngine;
import com.baomidou.mybatisplus.generator.fill.Column;
import com.baomidou.mybatisplus.generator.fill.Property;

import java.util.Collections;

/**
 * Hello world!
 *
 */
public class App
{
    public static void main( String[] args )
    {
        String url = "jdbc:mysql://localhost:3306/security_manager?useUnicode=true&characterEncoding=utf8";
        String userName = "root";
        String password = "gugang1234";
        FastAutoGenerator.create(url, userName, password)
                .globalConfig(builder -> {
                    builder.author("ddmonk") // 设置作者
                            .enableSwagger() // 开启 swagger 模式
                            .outputDir("/Users/gugang/IdeaProjects/ruoyi/RuoYi-Vue/enjoychain-security/src/main/java/"); // 指定输出目录
                })
                .packageConfig(builder -> {
                    builder.parent("com.enjoychain.www") // 设置父包名
                            .moduleName("patrol") // 设置父包模块名
                            .pathInfo(Collections.singletonMap(OutputFile.xml, "/Users/gugang/IdeaProjects/ruoyi/RuoYi-Vue/enjoychain-security/src/main/resources/mapper/patrol")); // 设置mapperXml生成路径
                })
                .strategyConfig(builder -> {
                    builder // 设置需要生成的表名
                            .likeTable(new LikeTable("ec_env_daily_task", SqlLike.RIGHT))
                            .entityBuilder()
                            .enableLombok()
                            .enableChainModel()
                            .addTableFills(new Column("create_time", FieldFill.INSERT),
                                    new Column("update_time", FieldFill.INSERT_UPDATE),
                                    new Column("modify_by", FieldFill.INSERT_UPDATE),
                                    new Column("create_by", FieldFill.INSERT))
                            .controllerBuilder()
                            .enableRestStyle()
                            ; // 设置过滤表前缀
                }).templateConfig(builder -> {
                    builder.disable(TemplateType.ENTITY,TemplateType.CONTROLLER)
                            .entity("/entity.java")
                            .controller("/controller.java")
                            .build();
                })
                .templateEngine(new VelocityTemplateEngine()) // 使用Freemarker引擎模板，默认的是Velocity引擎模板
                .execute();
    }
}
