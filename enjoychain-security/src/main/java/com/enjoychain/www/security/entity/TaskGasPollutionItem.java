package com.enjoychain.www.security.entity;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableName;
import java.io.Serializable;
import java.util.List;

import com.ruoyi.common.annotation.Excel;
import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;
import lombok.experimental.Accessors;

/**
 * <p>
 *
 * </p>
 *
 * @author ddmonk
 * @since 2023-02-25
 */
@Getter
@Setter
@Accessors(chain = true)
@TableName("task_gas_pollution_item")
@ApiModel(value = "TaskGasPollutionItem对象", description = "")
public class TaskGasPollutionItem implements Serializable {

    private static final long serialVersionUID = 1L;

    private String id;

    private String taskId;

    @ApiModelProperty("排气筒编号")
    @Excel(name = "排气筒编号")
    private String exhaustFunnelNo;

    @ApiModelProperty("污染因子")
    @Excel(name = "污染因子")
    private String pollutionFactor;

    @ApiModelProperty("产生工段")
    @Excel(name = "产生工段")
    private String productSection;

    @ApiModelProperty("排气筒管道是否完好")
    @Excel(name = "排气筒管道是否完好")
    private String exhaustPipelineStatus;

    @ApiModelProperty("风机是否开启")
    @Excel(name = "风机是否开启")
    private String fanStatus;

    @ApiModelProperty("建议")
    @Excel(name = "建议")
    private String advice;

    @ApiModelProperty("排气筒高度是否满足要求")
    @Excel(name = "排气筒高度是否满足要求")
    private String exhaustHeightStatus;

    @ApiModelProperty("检测口是否满足要求")
    @Excel(name = "检测口是否满足要求")
    private String inspectionStatus;

    @ApiModelProperty("处理设施是否开启")
    @Excel(name = "处理设施是否开启")
    private String handleEquipStatus;

    @ApiModelProperty("是否按时维护")
    @Excel(name = "是否按时维护")
    private String repairStatus;

    @ApiModelProperty("是否按时更换处理材料")
    @Excel(name = "是否按时更换处理材料")
    private String handleSourceStatus;

    @ApiModelProperty("是否无其他异常")
    @Excel(name = "是否无其他异常")
    private String otherUnusualStatus;

    @ApiModelProperty("是否有标识牌")
    @Excel(name = "是否有标识牌")
    private String signboardStatus;

    @TableField(exist = false)
    private List<BasicFile> files;

}
