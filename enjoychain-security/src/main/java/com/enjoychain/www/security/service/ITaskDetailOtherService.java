package com.enjoychain.www.security.service;

import com.enjoychain.www.security.entity.TaskDetailOther;
import com.baomidou.mybatisplus.extension.service.IService;
import com.enjoychain.www.security.pojo.TaskPojo;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author ddmonk
 * @since 2023-02-24
 */
public interface ITaskDetailOtherService extends ITaskService<TaskDetailOther> {

}
