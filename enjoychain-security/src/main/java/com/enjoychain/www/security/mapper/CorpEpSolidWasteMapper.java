package com.enjoychain.www.security.mapper;

import com.enjoychain.www.security.entity.CorpEpSolidWaste;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * 一般固废生产情况 Mapper 接口
 * </p>
 *
 * @author ddmonk
 * @since 2023-01-13
 */
public interface CorpEpSolidWasteMapper extends BaseMapper<CorpEpSolidWaste> {

}
