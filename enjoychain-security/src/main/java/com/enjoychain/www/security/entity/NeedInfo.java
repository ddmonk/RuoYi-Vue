package com.enjoychain.www.security.entity;

import com.baomidou.mybatisplus.annotation.FieldFill;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import java.io.Serializable;
import java.time.LocalDateTime;

import com.enjoychain.www.security.constant.NeedType;
import com.ruoyi.common.annotation.Excel;
import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;
import lombok.experimental.Accessors;

/**
 * <p>
 * 用户需求信息
 * </p>
 *
 * @author ddmonk
 * @since 2023-01-29
 */
@Getter
@Setter
@Accessors(chain = true)
@TableName("need_info")
@ApiModel(value = "NeedInfo对象", description = "用户需求信息")
public class NeedInfo implements Serializable {

    private static final long serialVersionUID = 1L;

    @TableId(value = "id", type = IdType.AUTO)
    private Integer id;

    @ApiModelProperty("需求名称")
    @Excel(name = "需求名称")
    private String needName;

    @ApiModelProperty("公司名")
    @Excel(name = "公司名")
    private String corpName;

    @ApiModelProperty("公司id")
    @Excel(name = "公司id")
    private String corpId;

    @ApiModelProperty("需求类型")
    @Excel(name = "需求类型")
    private Integer type;

    @ApiModelProperty("需求描述")
    @Excel(name = "需求描述")
    private String description;

    @ApiModelProperty("处理人id")
    @Excel(name = "处理人id")
    private String handlerId;

    @ApiModelProperty("处理人名称")
    @Excel(name = "处理人名称")
    private String handlerName;

    @ApiModelProperty("状态")
    @Excel(name = "状态")
    private Integer status;

    @ApiModelProperty(value = "创建时间", hidden = true)
    @TableField(fill = FieldFill.INSERT)
    private LocalDateTime createTime;

    @ApiModelProperty(value = "修改时间", hidden = true)
    @TableField(fill = FieldFill.INSERT_UPDATE)
    private LocalDateTime updateTime;

    @ApiModelProperty(value = "创建人员", hidden = true)
    @TableField(fill = FieldFill.INSERT)
    private String createBy;

    @ApiModelProperty(value = "修改人员", hidden = true)
    @TableField(fill = FieldFill.INSERT_UPDATE)
    private String modifyBy;


}
