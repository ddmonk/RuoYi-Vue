package com.enjoychain.www.security.mapper;

import com.enjoychain.www.security.entity.task.TaskBasic;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * 巡查任务基础信息 Mapper 接口
 * </p>
 *
 * @author ddmonk
 * @since 2023-01-20
 */
public interface TaskBasicMapper extends BaseMapper<TaskBasic> {

}
