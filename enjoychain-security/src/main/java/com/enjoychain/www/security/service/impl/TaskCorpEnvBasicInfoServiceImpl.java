package com.enjoychain.www.security.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.enjoychain.www.security.entity.TaskCorpEnvBasicInfo;
import com.enjoychain.www.security.mapper.TaskCorpEnvBasicInfoMapper;
import com.enjoychain.www.security.service.*;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import io.swagger.annotations.Authorization;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.xml.ws.Action;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author ddmonk
 * @since 2023-03-07
 */
@Service
public class TaskCorpEnvBasicInfoServiceImpl extends ServiceImpl<TaskCorpEnvBasicInfoMapper, TaskCorpEnvBasicInfo> implements ITaskCorpEnvBasicInfoService {

    @Autowired
    ITaskEpEquipInfoService epEquipInfoService;

    @Autowired
    ITaskEpHwItemService epHwItemService;

    @Autowired
    ITaskEpEiaService epEiaService;

    @Autowired
    ITaskEpGasEquipmentService epGasEquipmentService;

    @Autowired
    ITaskEpSolidWasteItemService epSolidWasteItemService;


    private QueryWrapper<TaskCorpEnvBasicInfo> buildQueryWithTaskId(String taskId){
        QueryWrapper<TaskCorpEnvBasicInfo> queryWrapper = new QueryWrapper<>();
        queryWrapper.eq("task_id",taskId);
        return queryWrapper;
    }

    @Override
    public TaskCorpEnvBasicInfo getDetailByTaskId(String taskId) {
        TaskCorpEnvBasicInfo aRes = this.getOne(buildQueryWithTaskId(taskId));
        aRes.setHwItems(epHwItemService.listByTaskId(taskId));
        aRes.setReportItems(epEiaService.listByTaskId(taskId));
        aRes.setGasEquipmentItems(epGasEquipmentService.listByTaskId(taskId));
        aRes.setEquipItems(epEquipInfoService.listByTaskId(taskId));
        aRes.setSolidWasteItems(epSolidWasteItemService.listByTaskId(taskId));
        return aRes;
    }
}
