package com.enjoychain.www.security.service;

import com.enjoychain.www.security.entity.TaskEquipItem;
import com.baomidou.mybatisplus.extension.service.IService;

import java.util.List;

/**
 * <p>
 * 巡查任务—设备情况 服务类
 * </p>
 *
 * @author ddmonk
 * @since 2023-02-24
 */
public interface ITaskEquipItemService extends ITaskService<TaskEquipItem> {

    List<TaskEquipItem> listByTaskId(String id);

    void removeAllByTaskId(String taskId);
}
