package com.enjoychain.www.security.service;

import com.enjoychain.www.security.entity.CorpBasicInfo;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 * 客户基础信息表 服务类
 * </p>
 *
 * @author baomidou
 * @since 2022-12-30
 */
public interface ICorpBasicInfoService extends IService<CorpBasicInfo> {


    CorpBasicInfo buildBasicInfoByCertNo(String certNo);


    CorpBasicInfo rebuild(CorpBasicInfo basicInfo);
}
