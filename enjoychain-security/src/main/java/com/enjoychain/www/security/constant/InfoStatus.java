package com.enjoychain.www.security.constant;

public enum InfoStatus {

    NORMAL(0,"正常"),

    LOSE(1,"失效")

    ;

    private int code;

    private String description;

    InfoStatus(int code, String description) {
        this.code = code;
        this.description = description;
    }

    public int getCode() {
        return code;
    }

    public String getDescription() {
        return description;
    }
}
