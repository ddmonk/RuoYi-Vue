package com.enjoychain.www.security.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.enjoychain.www.security.entity.XcBasic;
import com.enjoychain.www.security.mapper.XcBasicMapper;
import com.enjoychain.www.security.service.IXcBasicService;
import org.springframework.stereotype.Service;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.concurrent.atomic.AtomicInteger;


/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author ddmonk
 * @since 2020-12-03
 */
@Service
public class XcBasicServiceImpl extends ServiceImpl<XcBasicMapper, XcBasic> implements IXcBasicService {

    @Override
    public int getValue(String key) {
        QueryWrapper<XcBasic> queryWrapper = new QueryWrapper<>();
        queryWrapper.eq("item_key",key);
        XcBasic basic = baseMapper.selectOne(queryWrapper);
        if (null == basic){
            return -1;
        }
        return Integer.parseInt(basic.getItemValue());
    }

    @Override
    public boolean save(String key, String value) {
        QueryWrapper<XcBasic> queryWrapper = new QueryWrapper<>();
        queryWrapper.eq("item_key",key);
        XcBasic basic = baseMapper.selectOne(queryWrapper);
        if (null == basic){
            basic = new XcBasic();
            basic.setItemKey(key);
            basic.setItemValue(value);
        }else {
            basic.setItemValue(value);
        }
        return this.saveOrUpdate(basic);
    }

    @Override
    public String getTaskNo() {
        AtomicInteger productNo;
        int initNo = this.getValue("TASK_NO");
        if (initNo > 0){
            productNo = new AtomicInteger(initNo+1);
        }else {
            productNo = new AtomicInteger(1);
        }
        StringBuilder sb = new StringBuilder();
        sb.append("TASK").append(String.format("%06d",productNo.intValue()));
        this.save("TASK_NO",productNo.toString());
        return sb.toString();
    }

}
