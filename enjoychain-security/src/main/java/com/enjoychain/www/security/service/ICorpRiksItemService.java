package com.enjoychain.www.security.service;

import com.enjoychain.www.security.entity.CorpRiksItem;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 * 公司较大以上安全风险信息表 服务类
 * </p>
 *
 * @author ddmonk
 * @since 2023-01-13
 */
public interface ICorpRiksItemService extends IService<CorpRiksItem> {

}
